<?php
	//------------------------------------------------------------------------
	//Title...........: modelos de usuarios
	//File............: usuariosModel.php
	//version.........: 7.0
	//Fecha Cambio....: 16-Junio-2015
	//Autor...........: Angel Florentino Vargas Pool
	//Email...........: angelvargaspool@gmail.com
	//desc............: seccion de empresas
	//------------------------------------------------------------------------
	class usuariosModel extends Model
	{
		public function __contruct()
		{
			parent::__contruct();
		}
		//------------------------------------------------------------------------------
		//Empresas
		//------------------------------------------------------------------------------
		public function agregar_empresa($post = '')
		{
			$clase = 'error';
			$msg = array();
			$this->devolverError = true;
			if (!empty($post) && is_array($post)) {
				foreach ($post as $key => $value) {
					$$key=$value;
				}
				if (!empty($nombre)) {
					$sql = 'SELECT nombre_empresa FROM empresa WHERE nombre_empresa like "%'.$nombre.'%"';
					$this->setQuery($sql);
					$resultado=$this->getResultadoNumerico();
					if($resultado) {
						$msg[] = 'La empresa Se Encuentra Registrada';
					}
					else {
						$cnx = new Conectar();
						$cnx->devolverError = true;
						$sqlj='INSERT INTO empresa VALUES("0", "'.$nombre.'", "", "", "", "", "", "0", "0", "0", "0", "'.$tipo.'")';
						$cnx->setQuery($sqlj);
						$correcto = $cnx->getErrorDeQuery();
						if($correcto==1) {
							$msg[] = 'la empresa ha sido registrada exitosamente';
							$clase = 'correcto';
						}
						else {
							$msg[] = $cnx->getError();
						}
					}
				}
				else {
					$msg[] = 'Campo Vacio, Escriba el nombre de una empresa';
				}
			}
			else {
				if(empty($post))
					$msg[] = 'No existen Datos Para Guardar';
				if(!is_array($post))
					$msg[] = 'Los Datos Se Enviaron De Manera Incorrecta';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		public function lstempresas($tipo = ''){
			return parent::lstempresas($tipo);
		}
		//------------------------------------------------------------------------------
		//Estados, Ciudad, Colonia
		//------------------------------------------------------------------------------
		public function getResidencia($opcion,$id=0)
		{
			$conexion=new Conectar();
			switch ($opcion) {
				case 1:
					$sql='SELECT id_estado as id,nombree as nombre FROM estado ORDER BY nombree';
				break;
				case 2:
					$sql='SELECT id_ciudad as id,nombrec as nombre FROM ciudad WHERE id_estado="'.$id.'" ORDER BY nombrec';
				break;
				case 3:
					$sql='SELECT id_colonia as id,nombrecol as nombre FROM colonia WHERE id_ciudad="'.$id.'" ORDER BY nombrecol';
				break;
			}
			$conexion->setQuery($sql);
			$res=$conexion->getResultadoNumerico();
			$data=array();
			$cont=0;
			if($res){
				while ($fila=$conexion->getResultado('object')){
					$cont++;
					$data[$cont]['id']=$fila->id;
					$data[$cont]['nombre']=$fila->nombre;
				}
			}
			return $data;
		}
		public function agregarEstado($post)
		{
			$clase = 'error';
			$msg = array();
			$this->devolverError = true;
			if ( !empty($post) && is_array($post) ) {
				foreach ($post as $key => $value) {
					$$key = $value;
				}
				if(!empty($nombre)) {
					$sql = 'SELECT nombree FROM estado WHERE nombree="'.$nombre.'"';
					$this->setQuery($sql);
					$resultado = $this->getResultadoNumerico();
					if($resultado) {
						$msg[] = 'El estado se encuentra registrado';
					}
					else {
						$this->devolverError = true;
						$sql2 = 'INSERT INTO estado values("0","'.$nombre.'")';
						$this->setQuery($sql2);
						$correcto = $this->getErrorDeQuery();
						if($correcto == 1) {
							$msg[] = 'El estado se registro exitosamente';
							$clase = 'correcto';
						}
						else {
							$msg[] = $this->getError();;
						}
					}
				}
				else {
					$msg[] = 'Campo Vacio, Escriba el nombre de un Estado';
				}
			}
			else {
				if(empty($post))
					$msg[] = 'No existen Datos Para Guardar';
				if(!is_array($post))
					$msg[] = 'Los Datos Se Enviaron De Manera Incorrecta';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		public function agregarCiudad($post)
		{
			$clase = 'error';
			$msg = array();
			$this->devolverError = true;
			if ( !empty($post) && is_array($post) ) {
				foreach ($post as $key => $value) {
					$$key = $value;
				}
				if(!empty($nombre) && !empty($base)) {
					$sql = 'SELECT nombrec,id_estado FROM ciudad WHERE nombrec="'.$nombre.'" AND id_estado="'.$base.'"';
					$this->setQuery($sql);
					$resultado = $this->getResultadoNumerico();
					if($resultado) {
						$msg[] = 'La ciudad se encuentra registrada';
					}
					else {
						$this->devolverError = true;
						$sql2 = 'INSERT INTO ciudad VALUES("0", "'.$base.'", "'.$nombre.'")';
						$this->setQuery($sql2);
						$correcto = $this->getErrorDeQuery();
						if($correcto == 1) {
							$msg[] = 'La ciudad se registrado exitosamente';
							$clase = 'correcto';
						}
						else {
							$msg[] = $this->getError();;
						}
					}
				}
				else {
					if(empty($nombre))
						$msg[] = 'Campo Vacio, Escriba el nombre de la ciudad';
					if(empty($base))
						$msg[] = 'Especifique un Estado';
				}
			}
			else {
				if(empty($post))
					$msg[] = 'No existen Datos Para Guardar';
				if(!is_array($post))
					$msg[] = 'Los Datos Se Enviaron De Manera Incorrecta';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		public function agregarColonia($post)
		{
			$clase = 'error';
			$msg = array();
			$this->devolverError = true;
			if ( !empty($post) && is_array($post) ) {
				foreach ($post as $key => $value) {
					$$key = $value;
				}
				if(!empty($nombre) && !empty($base)) {
					$sql = 'SELECT nombrecol,id_ciudad FROM colonia WHERE nombrecol="'.$nombre.'" AND id_ciudad="'.$base.'"';
					$this->setQuery($sql);
					$resultado = $this->getResultadoNumerico();
					if($resultado) {
						$msg[] = 'La Colonia se encuentra registrada';
					}
					else {
						$this->devolverError = true;
						$sql2 = 'INSERT INTO colonia VALUES("0","'.$base.'","'.$nombre.'")';
						$this->setQuery($sql2);
						$correcto = $this->getErrorDeQuery();
						if($correcto == 1) {
							$msg[] = 'La colonia se registro exitosamente';
							$clase = 'correcto';
						}
						else {
							$msg[] = $this->getError();;
						}
					}
				}
				else {
					if(empty($nombre))
						$msg[] = 'Campo Vacio, Escriba el nombre de la colonia';
					if(empty($base))
						$msg[] = 'Especifique una Ciudad';
				}
			}
			else {
				if(empty($post))
					$msg[] = 'No existen Datos Para Guardar';
				if(!is_array($post))
					$msg[] = 'Los Datos Se Enviaron De Manera Incorrecta';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		//------------------------------------------------------------------------------
		//Guardar Usuario
		//------------------------------------------------------------------------------
		public function saveusuario($post = '', $flag = 0){
			$msg = array();
			$clase = 'error';
			$this->devolverError = true;
			$fecdef = '0000-00-00';
			$hoy = date('Y-m-d H:i:s');
			$nbrusr = '';
			if (!empty($post)) {
				foreach ($post as $key => $value) {
					if($key == 'nacimiento')
						$$key = empty($value)?$fecdef:$value;
					elseif($key == 'laboral')
						$$key = empty($value)?$fecdef:$value;
					else if($key=='estado' || $key=='colonia' || $key=='ciudad' || $key=='sae')
						$$key = empty($value)?'0':$value;
					else
						$$key = empty($value)?'':$value;
				}
				$validar_nombre = $this->validarnombre($nombre, $ide);
				$validar_usuario = $this->validarusuario($nbrusr);
				if($validar_nombre == 1 && $validar_usuario == 1){
					if ( ( (!empty($perfiles) && is_array($perfiles)) || $ide>0 ) && !empty($nombre) && !empty($direccion)) {
						if($ide>0){
							$sql = 'UPDATE usuario SET nombre="'.$nombre.'", direccion="'.$direccion.'", estado="'.$estado.'", ciudad="'.$ciudad.'", colonia="'.$colonia.'", sae="'.$sae.'", nacimiento="'.$nacimiento.'", registro_laboral="'.$laboral.'" WHERE id_usuario="'.$ide.'"';
							$msgok = 'Usuario Actualizado';
						}
						else{
							$sql = 'INSERT INTO usuario VALUES(0,"'.$nombre.'","'.$direccion.'","'.$estado.'","'.$ciudad.'","'.$colonia.'","'.$hoy.'","1","'.$sae.'","'.$nacimiento.'","'.$laboral.'")';
							$msgok = 'Usuario Agregado';
						}
						parent::setQuery($sql);
						$correcto = parent::getErrorDeQuery();
						if($correcto){
							$clase = 'correcto';
							$msg[] = $msgok;
							if($ide<=0){
								$idusuario = parent::getIdAfectado();
								//-----------------------------------o-----------------------------------
								if ( !empty($perfiles) && is_array($perfiles) ) {
									$count = 0;
									foreach ($perfiles as $key => $value) {
										$año = date('Y',strtotime($hoy));
										$folio = $this->setClaveUser($año);
										$count++;
										if($count==1){
											$user = empty($nbrusr)?strtotime($hoy):$nbrusr;
											$paswrd = (empty($nbrpwss))?strtotime($hoy):$nbrpwss;
											$paswrd = $this->crypt_blowfish($paswrd);
											$fisrt_folio = $folio;
										}
										else{
											$hoy = $fecha = date('Y-m-d H:i:s',strtotime('+'.$count.' seconds'));
											$user = strtotime($hoy);
											$paswrd = $this->crypt_blowfish(strtotime($hoy));
										}
										
										$sqli = 'INSERT INTO usuario_cuenta VALUES("'.$folio.'", "'.$idusuario.'", "'.$user.'", "'.$paswrd.'", "'.$value['perfil'].'", "'.$value['empresa'].'", "'.$value['estatus'].'")';
										$cnx = new Conectar();
										$cnx->devolverError = true;
										$cnx->setQuery($sqli);
										$correcto = $cnx->getErrorDeQuery();
										if($correcto==1)
											$msg[] = 'Perfil Agregado Al Usuario';
										else
											$msg[] = $cnx->getError();
									}
								}
								//-----------------------------------o-----------------------------------
								if ( !empty($contactos) && is_array($contactos) ) {
									foreach ($contactos as $key => $value) {
										$valor = $value['valorcont'];
										$tipo = $value['idcontacto'];
										if(!empty($valor)) {
											$cnxi = new Conectar();
											$clv = 'C'+$tipo;
											$fcontacto = $this->setClaveContacto($clv);
											$cnxi->devolverError = true;
											$sqli = 'INSERT INTO usuario_empresa_contacto VALUES("'.$fcontacto.'", "'.$fisrt_folio.'","'.$tipo.'", "'.$valor.'","1")';
											$cnxi->setQuery($sqli);
											$correcto = $cnxi->getErrorDeQuery();
											if($correcto == 1) {
												$msg[] = 'Contacto de usuario guardado';
												$clase = 'correcto';
											}
											else {
												$msg[] = $cnxi->getError();
											}
										}
									}
								}
								//-----------------------------------o-----------------------------------
								$msg[] = $this->htmlcreator->getTag('a','Modificar Usuario',array('href'=>_PATH_ABS_.'usuarios/modificar_usuario/'.$idusuario.'/','class'=>'enlace'));
							}
							$msg[] = $this->htmlcreator->getTag('a','Ir a lista de Usuarios',array('href'=>_PATH_ABS_.'usuarios/usuarios/','class'=>'enlace'));
							$msg[] = $this->htmlcreator->getTag('a','Ir a lista de Clientes',array('href'=>_PATH_ABS_.'usuarios/clientes/','class'=>'enlace'));
							
							//-----------------------------------o-----------------------------------
							//-----------------------------------o-----------------------------------
						}
						else{
							$msg[] = parent::getError();
						}
					}
					else{
						if(empty($perfiles) )
							$msg[] = 'Debe Tener al menos un perfil';
						if(!is_array($perfiles))
							$msg[] = 'Error De Formato De Perfiles';
						if( empty($nombre) )
							$msg[] = 'Debes Escribir Un nombre';
						if( empty($direccion) )
							$msg[] = 'Debes Escribir la dirección';
					}
				}
				else{
					if($validar_nombre == 0)
						$msg[] = 'Error, Nombre Del Usuario/Cliente/Afiliado(nombre) Existente , elija uno nuevo';
						//$msg[] = 'Este Usuario/Cliente/Afiliado Se encuentra Registrado';
					if($validar_usuario == 0)
						$msg[] = 'Error, Nombre De Usuario(user) Existente , elija uno nuevo';
						//$msg[] = 'El user Se encuentra Registrado';
				}
			}
			else{
				$msg[] = 'Datos enviado vacios';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		
		public function getIdUser($folio)
		{
			$cnxj = new Conectar();
			$sqlj = 'SELECT idusuario FROM usuario_cuenta WHERE folio="'.$folio.'"';
			$cnxj->setQuery($sqlj);
			$res = $cnxj->getResultadoNumerico();
			$idusuario = 0;
			if($res){
				$fila = $cnxj->getResultado('object');
				$idusuario = $fila->idusuario;
			}
			return $idusuario;
		}
		public function unirperfil($post = '')
		{
			$msg = array();
			$clase = 'error';
			$this->devolverError = true;
			if (!empty($post)) {
				foreach ($post as $key => $value) {
					$$key = $value;
				}
				if (!empty($perfiles) && is_array($perfiles)) {
					$valmin = $this->min_by_key($perfiles, 'id');
					foreach ($perfiles as $key => $value) {
						$cnx = new Conectar();
						$cnx->devolverError = true;
						$folio = $value['folio'];
						$sql = 'UPDATE usuario_cuenta SET idusuario="'.$valmin.'" WHERE folio="'.$folio.'"';
						$cnx->setQuery($sql);
						$correcto = $cnx->getErrorDeQuery();
						if($correcto==1){
							$clase = 'correcto';
							$msg[] = 'correcto, perfiles asignados a un solo usuario';
						}
						else{
							$msg[] = $cnx->getError();
						}
					}
					
				}
				else{
					if (!is_array($perfiles))
						$msg[] = 'El Formato de perfiles es incorrecto';
					if (empty($perfiles))
						$msg[] = 'Debes Seleccionar Al menos Un perfil'; 	
				}
				
			}
			else {
				$msg[] = 'Datos enviados vacios';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		private function min_by_key($arr, $key)
		{
		    $min = array(); 
		    foreach ($arr as $val) { 
		        if (!isset($val[$key]) and is_array($val)) { 
		            $min2 = min_by_key($val, $key); 
		            $min[$min2] = 1; 
		        } elseif (!isset($val[$key]) and !is_array($val)) { 
		            return false; 
		        } elseif (isset($val[$key])) { 
		            $min[$val[$key]] = 1; 
		        } 
		    } 
		    return min( array_keys($min) ); 
		}
		//------------------------------------------------------------------------------
		//Contactos
		//------------------------------------------------------------------------------
		public function addcontacto($post  = '')
		{
			$msg = array();
			$clase = 'error';
			if (!empty($post)) {
				foreach ($post as $key => $value) {
					$$key = $value;
				}
				$clv = 'C'.$tipo;
				$fcontacto = $this->setClaveContacto($clv);
				$cnxi = new Conectar();
				$cnxi->devolverError = true;
				if ( !empty($valor) && !empty($tipo) ) {
					$sql = 'INSERT INTO usuario_empresa_contacto VALUES("'.$fcontacto.'","'.$ide.'","'.$tipo.'","'.$valor.'","'.$tipocontacto.'")';
					$cnxi->setQuery($sql);
					$correcto = $cnxi->getErrorDeQuery();
					if ($correcto==1) {
						$clase = 'correcto';
						$msg[] = 'Dato De Contacto Agregado';
					}
					else {
						$msg[] = $cnxi->getError();
					}
				}
				else {
					if (empty($valor)) 
						$msg[] = 'Dato De Contacto Vacio';
					if (empty($tipo)) 
						$msg[] = 'Deber Especificar El tipo de contacto';
				}	
			}
			else{
				$msg[] = 'Datos Vacios';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;			
		}
		public function getTiposContacto()
		{
			$cnx = new Conectar();
			$sql = 'SELECT idtipo_contacto,nombre_tipo,validacion FROM usuario_tipo_contacto';
			$cnx->setQuery($sql);
			$res = $cnx->getResultadoNumerico();
			$data = array();
			$cont = 0;
			if($res){
				while ($fila = $cnx->getResultado('object')){
					$cont++;
					$validacion = $fila->validacion;
					$data[$cont]['id'] = $fila->idtipo_contacto;
					$data[$cont]['nombre'] = $fila->nombre_tipo;
					if(!empty($validacion))
						$data[$cont]['attr'] = array('data-validar'=>$validacion);
				}
			}
			return $data;
		}
		public function getContactos($id)
		{
			$cnxj = new Conectar();
			$sqlj = 'SELECT uco.folio, nombre_tipo, valor FROM usuario_cuenta ucu INNER JOIN usuario_empresa_contacto uco ON ucu.folio=uco.idrel INNER JOIN usuario_tipo_contacto utc ON uco.idtipo=utc.idtipo_contacto WHERE ucu.idusuario="'.$id.'" AND uco.tipo="1" ORDER BY uco.idtipo';
			//echo $sqlj;
			$cnxj->setQuery($sqlj);
			$res = $cnxj->getResultadoNumerico();
			$datos = array();
			if($res){
				$cont = 0;
				while ($fila = $cnxj->getResultado('object')) {
					$cont++;
					$datos[$cont]['folio'] = $fila->folio;
					$datos[$cont]['tipo'] = $fila->nombre_tipo;
					$datos[$cont]['valor'] = $fila->valor;
				}
			}
			return $datos;
		}
		public function delcontacto($post = '')
		{
			$msg = array();
			$clase = 'error';
			if (!empty($post)) {
				foreach ($post as $key => $value) {
					$$key = $value;
				}
				$cnxi = new Conectar();
				$cnxi->devolverError = true;
				if ( !empty($clave) ) {
					$sql = 'DELETE FROM usuario_empresa_contacto WHERE folio="'.$clave.'"';
					$cnxi->setQuery($sql);
					$correcto = $cnxi->getErrorDeQuery();
					if ($correcto==1) {
						$clase = 'correcto';
						$msg[] = 'Dato De Contacto Eliminado';
					}
					else {
						$msg[] = $cnxi->getError();
					}
				}
				else {
					if (empty($clave)) 
						$msg[] = 'No existe Este Contacto';
				}	
			}
			else{
				$msg[] = 'Datos Vacios';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		//------------------------------------------------------------------------------
		//Datos De Usuario
		//------------------------------------------------------------------------------
		public function dataUser($id)
		{
			$conexion = new Conectar();
			$id = $conexion->setEscapar((int)$id);
			$sql='SELECT nombre, sae, direccion, colonia, ciudad, estado, fecha_reg, nacimiento, registro_laboral, nombree, nombrec, nombrecol FROM usuario usr LEFT JOIN estado est ON usr.estado=est.id_estado LEFT JOIN ciudad ciu ON usr.ciudad=ciu.id_ciudad LEFT JOIN colonia col ON usr.colonia = col.id_colonia WHERE id_usuario="'.$id.'" LIMIT 1';
			$conexion->setQuery($sql);
			$resultado=$conexion->getResultadoNumerico();
			if($resultado) {
				$datos=array();
				$usuario_s=$conexion->getResultado('object');
				$dir = explode('_', $usuario_s->direccion);
				$datos['nombre']=$usuario_s->nombre;
				$datos['sae']=$usuario_s->sae;
				$datos['direccion']=$usuario_s->direccion;
				$datos['colonia']=$usuario_s->colonia;
				$datos['ciudad']=$usuario_s->ciudad;
				$datos['estado']=$usuario_s->estado;
				$datos['fechre']=$usuario_s->fecha_reg;
				$datos['nacimiento'] = $usuario_s->nacimiento;
				$datos['laboral'] = $usuario_s->registro_laboral;
				$datos['name_estado']=$usuario_s->nombree;
				$datos['name_ciudad']=$usuario_s->nombrec;
				$datos['name_colonia']=$usuario_s->nombrecol;
			}
			return $datos;
		}
		public function getPerfiles($id)
		{
			$cnxj = new Conectar();
			$sqlj = 'SELECT folio,rolnombre,nombre_empresa,estatus FROM usuario_cuenta ucu LEFT JOIN empresa emp ON ucu.idempresa=emp.id_empresa INNER JOIN rol r ON ucu.idperfil=r.idrol WHERE ucu.idusuario="'.$id.'"';
			//echo $sqlj;
			$cnxj->setQuery($sqlj);
			$res = $cnxj->getResultadoNumerico();
			$datos = array();
			if($res){
				$cont = 0;
				$arrest = array('1'=>'Activo','2'=>'Inactivo');
				while ($fila = $cnxj->getResultado('object')) {
					$cont++;
					$datos[$cont]['folio'] = $fila->folio;
					$datos[$cont]['perfil'] = $fila->rolnombre;
					$datos[$cont]['empresa'] = $fila->nombre_empresa;
					$datos[$cont]['estatus'] = $fila->estatus;
					$datos[$cont]['nombre_estatus'] = $arrest[$fila->estatus];
				}
			}
			return $datos;
		}
		
		public function addperfil($post = '')
		{
			$msg = array();
			$clase = 'error';
			if (!empty($post)) {
				foreach ($post as $key => $value) {
					if($key=='idempresa')
						$$key = empty($value)?'0':$value;
					else
						$$key = empty($value)?'':$value;	
				}

				$hoy = date('Y-m-d H:i:s');
				$año = date('Y',strtotime($hoy));
				$folio = $this->setClaveUser($año);
				
				$cnxi = new Conectar();
				$cnxi->devolverError = true;
				if ( !empty($idperfil) && !empty($estatus) ) {
					$user = strtotime($hoy);
					$paswrd = strtotime($hoy);
					$paswrd = $this->crypt_blowfish($paswrd);

					$sql = 'INSERT INTO usuario_cuenta VALUES("'.$folio.'", "'.$ide.'", "'.$user.'", "'.$paswrd.'", "'.$idperfil.'", "'.$idempresa.'", "'.$estatus.'")';
					$cnxi->setQuery($sql);
					$correcto = $cnxi->getErrorDeQuery();
					if ($correcto==1) {
						$clase = 'correcto';
						$msg[] = 'Dato De Perfil Agregado';
					}
					else {
						$msg[] = $cnxi->getError();
					}

				}
				else {
					if ( empty($idperfil) )
						$msg[] = 'Selecciona Un Perfil';
					if ( empty($estatus) )
						$msg[] = 'Selecciona El estatus Del Perfil';
				}	
			}
			else{
				$msg[] = 'Datos Vacios';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		public function chgest($post = '')
		{
			$msg = array();
			$clase = 'error';
			if (!empty($post)) {
				foreach ($post as $key => $value) {
					$$key = empty($value)?'':$value;	
				}
				
				$cnxi = new Conectar();
				$cnxi->devolverError = true;
				if ( !empty($clave) && !empty($est) ) {

					$sql = 'UPDATE usuario_cuenta SET estatus="'.$est.'" WHERE folio="'.$clave.'"';
					$cnxi->setQuery($sql);
					$correcto = $cnxi->getErrorDeQuery();
					if ($correcto==1) {
						$clase = 'correcto';
						$msg[] = 'Dato De Perfil Modificado';
					}
					else {
						$msg[] = $cnxi->getError();
					}

				}
				else {
					if ( empty($clave) )
						$msg[] = 'El Perfil de usuario No existe';
					if ( empty($est) )
						$msg[] = 'error de estatus';
				}	
			}
			else{
				$msg[] = 'Datos Vacios';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		//------------------------------------------------------------------------------
		//Cambiar password y user de un usuario
		//------------------------------------------------------------------------------
		public function newPass($datos,$id)
		{
			$msg = array();
			$clase = 'error';
			$conexion= new Conectar();
			$conexion->devolverError = true;
			foreach ($datos as $key => $value) {
				$$key=$this->setEscapar($value);
			}
			$sqladd = '';
			$validar_usuario = $this->validarusuario($usuario, $id);
			if($validar_usuario == 1){
				if(!empty($newpass)){
					$pass = $this->crypt_blowfish($newpass);
					$sqladd = ',paswrd="'.$pass.'"';
					$msg[] = 'Contraseña Modificado Correctamente';
				}
				$sql='UPDATE usuario_cuenta SET user="'.$usuario.'"'.$sqladd.' WHERE folio="'.$id.'"';
				$conexion->setQuery($sql);
				$correcto=$conexion->getErrorDeQuery();
				if($correcto==1) {
					$msg[] = 'Usuario Modificado Correctamente';
					$clase = 'correcto';
				}
				else {
					$msg[] = $conexion->getError();;
				}
			}
			else{
				//$msg[] = 'El usuario ya se encuentra registrado, elija uno nuevo';
				$msg[] = 'Error, Nombre De Usuario(user) Existente , elija uno nuevo';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		public function getUser($folio = '')
		{
			$sqlj = 'SELECT user FROM usuario_cuenta WHERE folio="'.$folio.'"';
			$cnxj = new Conectar();
			$cnxj->setQuery($sqlj);
			$res = $cnxj->getResultadoNumerico();
			$datos = array();
			$hoy = date('Y-m-d H:i:s');
			$usuario = strtotime($hoy);
			if($res){
				$fila = $cnxj->getResultado('object');
				$usuario = $fila->user;
			}
			return $usuario;
		}
		public function editPerfil($post)
		{
			$msg = array();
			$clase = 'error';
			$this->devolverError = true;
			$fecdef = '0000-00-00';
			//$hoy = date('Y-m-d H:i:s');
			if (!empty($post)) {
				foreach ($post as $key => $value) {
					if($key == 'nacimiento')
						$$key = empty($value)?$fecdef:$value;
					else if($key=='estado' || $key=='colonia' || $key=='ciudad' || $key=='sae')
						$$key = empty($value)?'0':$value;
					else
						$$key = empty($value)?'':$value;
				}
				$validar_nombre = $this->validarnombre($nombre,$ide);
				if($validar_nombre == 1){
					if (!empty($nombre) && !empty($direccion)) {
						$sql = 'UPDATE usuario SET nombre="'.$nombre.'", direccion="'.$direccion.'", estado="'.$estado.'", ciudad="'.$ciudad.'", colonia="'.$colonia.'", nacimiento="'.$nacimiento.'" WHERE id_usuario="'.$ide.'"';
						parent::setQuery($sql);
						$correcto = parent::getErrorDeQuery();
						if($correcto){
							$clase = 'correcto';
							$msg[] = 'Perfil Editado Correctamente';
						}
						else{
							$msg[] = parent::getError();
						}
					}
					else{
						if( empty($nombre) )
							$msg[] = 'Debes Escribir Un nombre';
						if( empty($direccion) )
							$msg[] = 'Debes Escribir la dirección';
					}
				}
				else{
					if($validar_nombre == 0)
						$msg[] = 'El nombre se encuentra Registrado, No puede Modificarlo';
				}
			}
			else{
				$msg[] = 'Datos enviado vacios';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		//------------------------------------------------------------------------------
		//Empresas
		//------------------------------------------------------------------------------
		public function saveempresa($post = '')
		{
			$msg = array();
			$clase = 'error';
			$this->devolverError = true;
			$hoy = date('Y-m-d H:i:s');
			if (!empty($post)) {
				foreach ($post as $key => $value) {
					if($key=='estado' || $key=='colonia' || $key=='ciudad' || $key=='sae')
						$$key = empty($value)?'0':$value;
					else
						$$key = empty($value)?'':$value;
				}
				$validar_empresa = $this->validarEmpresa($nombreempresa, $razonsocial, $ide);
				if($validar_empresa == 1){
					if ( !empty($nombreempresa) && !empty($direccion)) {
						if($ide>0){

							$sql='UPDATE empresa SET nombre_empresa="'.$nombreempresa.'", razonSocial="'.$razonsocial.'", rfc="'.$rfc.'", descripcionEmpresa="'.$descripcion.'", direccion="'.$direccion.'", estado="'.$estado.'", ciudad="'.$ciudad.'", colonia="'.$colonia.'", sae="'.$sae.'", tipo="'.$tipo.'" WHERE id_empresa="'.$ide.'"';

							$msgok = 'Empresa Modificada Correctamente';
						}
						else{

							$sql='INSERT INTO empresa VALUES("0", "'.$nombreempresa.'", "'.$razonsocial.'", "'.$rfc.'", "'.$descripcion.'", "'.$direccion.'", "'.$logo.'", "'.$sae.'", "'.$estado.'", "'.$ciudad.'", "'.$colonia.'", "'.$tipo.'")';
							$msgok = 'Empresa Registrada Correctamente';
						}
						parent::setQuery($sql);
						$correcto = parent::getErrorDeQuery();
						if($correcto){
							$clase = 'correcto';
							$msg[] = $msgok;
							if($ide<=0){
								$idempresa = parent::getIdAfectado();
								//-----------------------------------o-----------------------------------
								if ( !empty($contactos) && is_array($contactos) ) {
									foreach ($contactos as $key => $value) {
										$valor = $value['valorcont'];
										$tipo = $value['idcontacto'];
										if(!empty($valor)) {
											$cnxi = new Conectar();
											$clv = 'C'+$tipo;
											$fcontacto = $this->setClaveContacto($clv);
											$cnxi->devolverError = true;
											$sqli = 'INSERT INTO usuario_empresa_contacto VALUES("'.$fcontacto.'", "'.$idempresa.'","'.$tipo.'", "'.$valor.'","2")';
											$cnxi->setQuery($sqli);
											$correcto = $cnxi->getErrorDeQuery();
											if($correcto == 1) {
												$msg[] = 'Contacto de empresa guardado';
												$clase = 'correcto';
											}
											else {
												$msg[] = $cnxi->getError();
											}
										}
									}
								}
								//-----------------------------------o-----------------------------------
								$msg[] = $this->htmlcreator->getTag('a','Modificar Empresa',array('href'=>_PATH_ABS_.'usuarios/modificar_empresa/'.$idempresa.'/','class'=>'enlace'));
							}
							$msg[] = $this->htmlcreator->getTag('a','Ir a lista de Empresas',array('href'=>_PATH_ABS_.'usuarios/empresa/','class'=>'enlace'));
						}
						else{
							$msg[] = parent::getError();
						}
					}
					else{
						if( empty($nombreempresa) )
							$msg[] = 'Debes Escribir Un nombre';
						if( empty($direccion) )
							$msg[] = 'Debes Escribir la dirección';
					}
				}
				else{
					if($validar_empresa == 0)
						$msg[] = 'Esta Empresa Se encuentra Registrada';
					
				}
			}
			else{
				$msg[] = 'Datos enviado vacios';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		public function validarEmpresa($nombre, $razon, $empresa=0)
		{
			$cnx = new Conectar();
			$sql='SELECT nombre_empresa FROM empresa WHERE nombre_empresa="'.$nombre.'"';
			if($empresa>0){
				$sql.=' AND id_empresa!='.$empresa.'';
			}
			$cnx->setQuery($sql);
			$resultado = $cnx->getResultadoNumerico();
			if($resultado)
				$html=0;
			else
				$html=1;
			return $html;
		}
		public function datosEmpresa($id)
		{
			$cnx=new Conectar();
			$sql='SELECT * FROM empresa WHERE id_empresa="'.$id.'"';
			$cnx->setQuery($sql);
			$resultado=$cnx->getResultadoNumerico();
			$datos = array();
			if($resultado){
				$fila = $cnx->getResultado('object');
				$datos['nombreEmpresa'] = $fila->nombre_empresa;
				$datos['razonSocial'] = $fila->razonSocial;
				$datos['rfc'] = $fila->rfc;
				$datos['sae'] = $fila->sae;
				$datos['descripcion'] = $fila->descripcionEmpresa;
				$datos['direccion'] = $fila->direccion;
				$datos['estado'] = $fila->estado;
				$datos['ciudad'] = $fila->ciudad;
				$datos['colonia'] = $fila->colonia;
				$datos['tipo'] = $fila->tipo;
			}
			return $datos;
		}
		public function getContactosEmpresa($id)
		{
			$cnxj = new Conectar();
			$sqlj = 'SELECT uco.folio, nombre_tipo, valor FROM usuario_empresa_contacto uco INNER JOIN usuario_tipo_contacto utc ON uco.idtipo=utc.idtipo_contacto WHERE uco.idrel="'.$id.'" AND uco.tipo="2" ORDER BY uco.idtipo';
			//echo $sqlj;
			$cnxj->setQuery($sqlj);
			$res = $cnxj->getResultadoNumerico();
			$datos = array();
			if($res){
				$cont = 0;
				while ($fila = $cnxj->getResultado('object')) {
					$cont++;
					$datos[$cont]['folio'] = $fila->folio;
					$datos[$cont]['tipo'] = $fila->nombre_tipo;
					$datos[$cont]['valor'] = $fila->valor;
				}
			}
			return $datos;
		}
		public function getContactoEmpresaUsuarios($idempresa)
		{
			$cnxj = new Conectar();
			$sqlj = 'SELECT uco.folio, nombre_tipo, valor, usr.nombre FROM usuario_cuenta ucu INNER JOIN usuario_empresa_contacto uco ON ucu.folio=uco.idrel INNER JOIN usuario_tipo_contacto utc ON uco.idtipo=utc.idtipo_contacto INNER JOIN usuario usr ON ucu.idusuario = usr.id_usuario WHERE ucu.idempresa="'.$idempresa.'" AND uco.tipo="1" ORDER BY uco.idtipo';
			//echo $sqlj;
			$cnxj->setQuery($sqlj);
			$res = $cnxj->getResultadoNumerico();
			$datos = array();
			if($res){
				$cont = 0;
				while ($fila = $cnxj->getResultado('object')) {
					$cont++;
					$datos[$cont]['nombre'] = $fila->nombre;
					$datos[$cont]['folio'] = $fila->folio;
					$datos[$cont]['tipo'] = $fila->nombre_tipo;
					$datos[$cont]['valor'] = $fila->valor;
				}
			}
			//var_dump($datos);
			return $datos;
		}
		public function getUsuariosEmpresa($idempresa)
		{
			$cnxj = new Conectar();
			$sqlj = 'SELECT usr.nombre FROM usuario usr INNER JOIN usuario_cuenta uc ON usr.id_usuario = uc.idusuario WHERE uc.idempresa="'.$idempresa.'"';
			//echo $sqlj;
			$cnxj->setQuery($sqlj);
			$res = $cnxj->getResultadoNumerico();
			$datos = array();
			if($res){
				$cont = 0;
				while ($fila = $cnxj->getResultado('object')) {
					$cont++;
					$datos[$cont]['Usuario'] = $fila->nombre;
				}
			}
			return $datos;
		}
		public function sincronizarempresas()
		{
			$msg = array();
			$clase = 'error';
			$datadb['server'] = '192.168.1.10';
			//$datadb['db'] = '192.168.1.10:c:/Program Files (x86)/Common Files/Aspel/Sistemas Aspel/SAE6.00/Empresa01/Datos/SAE60EMPRE01.fdb';
			$datadb['db'] = _PATH_ABS_.'respaldoBD/fdb/SAE60EMPRE01.fdb';
			$datadb['user'] = 'sysdba';
			$datadb['pass'] = 'masterkey';
			$datadb['driver'] = 'firebird';
			$datadb['encode'] = 'ISO8859_1';
			$cnx = new Conectar( $datadb );
			$sql  = 'SELECT CLAVE, STATUS, NOMBRE, RFC, CALLE, NUMINT, NUMEXT, CRUZAMIENTOS, CRUZAMIENTOS2, COLONIA, CODIGO, LOCALIDAD, MUNICIPIO, ESTADO, PAIS, NACIONALIDAD, REFERDIR, TELEFONO, CLASIFIC, FAX, PAG_WEB, CURP, CVE_ZONA, IMPRIR, MAIL, NIVELSEC, ENVIOSILEN, EMAILPRED, DIAREV, DIAPAGO, CON_CREDITO, DIASCRED, LIMCRED, SALDO, LISTA_PREC, CVE_BITA, ULT_PAGOD, ULT_PAGOM, ULT_PAGOF, DESCUENTO, ULT_VENTAD, ULT_COMPM, FCH_ULTCOM, VENTAS, CVE_VEND, CVE_OBS, TIPO_EMPRESA, MATRIZ, PROSPECTO, CALLE_ENVIO, NUMINT_ENVIO, NUMEXT_ENVIO, CRUZAMIENTOS_ENVIO, CRUZAMIENTOS_ENVIO2, COLONIA_ENVIO, LOCALIDAD_ENVIO, MUNICIPIO_ENVIO, ESTADO_ENVIO, PAIS_ENVIO, CODIGO_ENVIO, CVE_ZONA_ENVIO, REFERENCIA_ENVIO, CUENTA_CONTABLE, ADDENDAF, ADDENDAD, NAMESPACE, METODODEPAGO, NUMCTAPAGO FROM CLIE01';
			$cnx->setQuery($sql);
			$cont=0;
			$datos=array();
			while ( $fila = $cnx->getResultado('object') ) {

				$clave = (int)$fila->CLAVE;
				$nombre = utf8_encode($fila->NOMBRE);
				$rfc = utf8_encode($fila->RFC);
				$codigo_postal = utf8_encode($fila->CODIGO);
				$pagina_web = utf8_encode($fila->PAG_WEB);
				$curp = utf8_encode($fila->CURP);
				$e_mail = utf8_encode($fila->MAIL);
				$telefono = utf8_encode($fila->TELEFONO);
				$calle = utf8_encode($fila->CALLE);
				$numero_interior = utf8_encode($fila->NUMINT);
				$numero_exterior = utf8_encode($fila->NUMEXT);
				$cruza1 = utf8_encode($fila->CRUZAMIENTOS);
				$cruza2 = utf8_encode($fila->CRUZAMIENTOS2);

				$direccion = '';
				$direccion.= empty($calle)?'':'Calle '.$calle.' ';
				$direccion.= empty($numero_interior)?'':'N° '.$numero_interior.' ';
				$direccion.= empty($numero_exterior)?'':'NI° '.$numero_exterior.' ';
				$direccion.= empty($cruza1)?'':' x '.$cruza1.' ';
				$direccion.= empty($cruza1)?'':' x '.$cruza1;
				if( $clave > 0 ) {
					$cnx2 = new Conectar();
					$sql2 = 'SELECT sae FROM empresa WHERE sae="'.$clave.'"';
					$cnx2->setQuery( $sql2 );
					$res = $cnx2->getResultadoNumerico();
					//echo $nombre;
					if ( $res ) {
						$sql3 = 'UPDATE empresa SET razonSocial="'.$nombre.'", rfc="'.$rfc.'", direccion="'.$direccion.'" WHERE sae = "'.$clave.'"';
						$msgok = 'Actualizado Correctamente numero sae '.$clave;
					}
					else {
						//$sql3 = 'INSERT INTO empresa VALUES(0, "", "'.$nombre.'", "'.$rfc.'", "empresa sae", "'.$direccion.'", "", "'.$clave.'", "0", "0", "0", "1")';
						$sql3 = 'SELECT * FROM empresa WHERE 1=1';
						$msgok = 'Agregado correctamente numero sae '.$clave;
					}
					$cnx3 = new Conectar();
					$cnx3->devolverError = true;
					$cnx3->setQuery( $sql3 );
					$correcto = $cnx3->getErrorDeQuery();
					if ($correcto) {
						$clase = 'correcto';
						$msg[] = $msgok;
					}
					else {
						$msg[] = $cnx3->getError();
					}
				}
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		//------------------------------------------------------------------------------
		//Conexion a redes sociales
		//------------------------------------------------------------------------------
		
		public function savesocial($post = '')
		{
			$msg = array();
			$clase = 'error';
			$this->devolverError = true;
			$hoy = date('Y-m-d H:i:s');
			$usuario = '';
			if ( !empty($post) ) {
				foreach ($post as $key => $value) {
					if($key == 'photo')
						$$key = empty($value)?'default.png':$value;
					else
						$$key = empty($value)?'':$value;
				}
				if ( $login == 'login' ) {
					$sqli = 'SELECT social_id FROM usuario_social WHERE social_id = "'.$id.'" AND tipo="'.$tipo.'"';
					$cnx = new Conectar();
					$cnx->setQuery($sqli);
					$resultado = $cnx->getResultadoNumerico();
					$datos = array();
					$cont = 0;
					if($resultado) {
						$sqlj = 'UPDATE usuario_social SET social_usuario="'.$name.'", social_email="'.$email.'", social_img="'.$photo.'" WHERE social_id="'.$id.'"';
						$msgok = 'Datos Actualizados';
					}
					else{
						if($tipo==1)//fb //tw //go
							$folio_social = $this->setclave('usuario_social', 'id', 'fb');
						else
							$folio_social = $this->setclave('usuario_social', 'id', 'rs');
						$sqlj = 'INSERT INTO usuario_social VALUES("'.$folio_social.'", "'.$folio_user.'", "'.$id.'", "'.$name.'", "'.$email.'", "'.$photo.'", "'.$tipo.'", "'.$hoy.'")';
						$msgok = 'Agregado Correctamente';
					}
					$cnx3 = new Conectar();
					$cnx3->devolverError = true;
					$cnx3->setQuery( $sqlj );
					$correcto = $cnx3->getErrorDeQuery();
					if ($correcto) {
						$clase = 'correcto';
						$msg[] = $msgok;
					}
					else {
						$msg[] = $cnx3->getError();
					}
				}
				else{
					$msg[] = 'No Hiciste click en conectar';
				}
				
			}
			else {
				$msg[] = 'Datos enviado vacios';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		public function getRedes($idusuario = 0)
		{
			$cnx = new Conectar();
			$sql = 'SELECT id,folio_usuario, social_id, social_usuario, social_email, social_img, tipo FROM usuario_social us INNER JOIN usuario_cuenta uc ON us.folio_usuario = uc.folio WHERE uc.idusuario = "'.$idusuario.'"';
			$cnx->setQuery($sql);
			$resultado = $cnx->getResultadoNumerico();
			$datos = array();
			$cont = 0;
			if($resultado){
				while ( $fila = $cnx->getResultado('object') ) {
					$cont++;
					$datos[$cont]['id'] = $fila->id;
					$datos[$cont]['folio_usuario'] = $fila->folio_usuario;
					$datos[$cont]['social_id'] = $fila->social_id;
					$datos[$cont]['social_usuario'] = $fila->social_usuario;
					$datos[$cont]['social_email'] = $fila->social_email;
					$datos[$cont]['social_img'] = $fila->social_img;
					$datos[$cont]['tipo'] = $fila->tipo;
				}
			}
			return $datos;
		}
		public function desvincular($idsocial = '')
		{
			$msg = array();
			$clase = 'error';
			
			if ( !empty($idsocial) ) {

				$sqlj = 'DELETE FROM usuario_social WHERE id="'.$idsocial.'"';
				$msgok = 'Eliminado Correctamente';
				
				$cnx3 = new Conectar();
				$cnx3->devolverError = true;
				$cnx3->setQuery( $sqlj );
				$correcto = $cnx3->getErrorDeQuery();
				if ($correcto) {
					$clase = 'correcto';
					$msg[] = $msgok;
				}
				else {
					$msg[] = $cnx3->getError();
				}

			}
			else {
				$msg[] = 'Error no existe la cuenta';
			}
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
	}
?>