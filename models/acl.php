<?php 
	class aclmodelo extends Model {
		private $_data;
		//------------------------------------------------------------------------------------
		//constructor
		//------------------------------------------------------------------------------------
		public function construct(){
			parent::__construct();
		}
		
		public function getPermisosRol() {
			$conex = new Conectar();
			$sql='SELECT idpermiso,valor,keypermiso,modulo,nombrepermiso,nombremodulo,keymodulo,imagen,menum,menup,keyapli,nameapli, descripcion FROM permisos pe LEFT JOIN permisos_rol pr ON pr.permiso = pe.idpermiso INNER JOIN modulos mo ON pe.modulo=mo.idmodulos INNER JOIN aplication ap ON mo.menum=ap.idapli WHERE pe.estatus=1 AND mo.estatus=1';
			if( in_array('1', $this->_catuser) ){
				$sql.=' GROUP BY idpermiso ORDER BY mo.idmodulos,pe.idpermiso';
			}
			else{
				$rolesuser = (count($this->_roluser) > 0) ? implode(',', $this->_roluser) : '0';
				$sql.=' AND rol IN ('.$rolesuser.') ORDER BY mo.idmodulos,pe.idpermiso';
			}
			//echo $sql.'<br><br>';
			$conex->setQuery($sql);
			$resultado = $conex->getResultadoNumerico();
			$datos=array();
			$cont=0;
			if($resultado) {
				while ( $fila=$conex->getResultado('object') ) {
					if($fila->keypermiso == '')
						continue;

					if( $fila->valor == 1 ){
						$v = true;
					}
		            elseif( in_array('1', $this->_catuser) ){
		            	$v = true;
		            }
		            else{
		            	$v = false;
		            }
		            $key = $fila->keypermiso.'@'.$fila->keymodulo.'@'.$fila->keyapli;
		            if( array_key_exists($key, $datos) ) {
		            	if($datos[$key]['valor']){
		            		$datos[$key]['valor'] = true;
		            	}
		            	else{
		            		$datos[$key]['valor'] = $v;
		            	}
		            }
		            else {
		            	$datos[$key]['valor'] = $v;
		            }

		            //echo $key.'--'.'<br>';
					$datos[$key]['permiso']=$fila->idpermiso;
					$datos[$key]['nombrepermiso']=$fila->nombrepermiso;
					$datos[$key]['key']=$fila->keypermiso;
					$datos[$key]['modulo']=$fila->modulo;
					$datos[$key]['nombremodulo']=$fila->nombremodulo;
					$datos[$key]['keymodulo']=$fila->keymodulo;
					$datos[$key]['imagen']=$fila->imagen;
					$datos[$key]['menum']=$fila->menum;
					$datos[$key]['menup']=$fila->menup;
					$datos[$key]['apli'] = $fila->keyapli;
					$datos[$key]['nombre_apli'] = $fila->nameapli;
					$datos[$key]['descripcion'] = $fila->descripcion;
					$datos[$key]['heredado']=true;
				}
			}
			return $datos;
		}

		public function getPermisosUsuario() {
			$id_usuario = ( Session::get('l_id') == null ) ? 0 : Session::get('l_id');
			$cnx = new Conectar();
			$sql='SELECT permiso,valor,keypermiso,modulo,nombrepermiso,nombremodulo,keymodulo,imagen,menum,menup,keyapli,nameapli, descripcion FROM permisos_usuario pu INNER JOIN permisos pe ON pu.permiso=pe.idpermiso INNER JOIN modulos mo ON pe.modulo=mo.idmodulos INNER JOIN aplication ap ON mo.menum=ap.idapli WHERE usuario="'.$id_usuario.'" AND pe.estatus=1 AND mo.estatus=1 ORDER BY mo.idmodulos,pe.idpermiso';
			$cnx->setQuery($sql);
			$resultado = $cnx->getResultadoNumerico();
			$datos=array();
			$cont=0;
			if($resultado) {
				while ($fila=$cnx->getResultado('object')) {
					if($fila->keypermiso == '')
						continue;
					
					if($fila->valor== 1)
						$v = true;
		            elseif( in_array('1', $this->_catuser) )
		            	$v = true;
		            else
		            	$v = false;
		            $key=$fila->keypermiso.'@'.$fila->keymodulo.'@'.$fila->keyapli;
		            //$key=$fila->keypermiso;
					$datos[$key]['permiso']=$fila->permiso;
					$datos[$key]['nombrepermiso']=$fila->nombrepermiso;
					$datos[$key]['valor']=$v;
					$datos[$key]['key']=$fila->keypermiso;
					$datos[$key]['modulo']=$fila->modulo;
					$datos[$key]['nombremodulo']=$fila->nombremodulo;
					$datos[$key]['keymodulo']=$fila->keymodulo;
					$datos[$key]['imagen']=$fila->imagen;
					$datos[$key]['menum']=$fila->menum;
					$datos[$key]['menup']=$fila->menup;
					$datos[$key]['apli'] = $fila->keyapli;
					$datos[$key]['nombre_apli'] = $fila->nameapli;
					$datos[$key]['descripcion'] = $fila->descripcion;
					$datos[$key]['heredado']=false;
				}
			}
			return $datos;
		}
	}
?>