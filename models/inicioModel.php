<?php
	class inicioModel extends Model{
		public function __construct(){
			parent::__construct();
		}
		public function login($datos) {
			$user = $datos['lgn_user'];
			$pass = $datos['lgn_password'];
			$conex = new Conectar();
			$sql='SELECT uc.folio, uc.idusuario, uc.user, uc.paswrd, uc.idperfil, uc.idempresa, uc.estatus, usr.nombre, usr.estatus as estuser, ro.categoria FROM usuario_cuenta uc INNER JOIN rol ro ON uc.idperfil=ro.idrol INNER JOIN usuario usr ON usr.id_usuario=uc.idusuario WHERE user="'.$user.'" LIMIT 1';
			$conex->setQuery($sql);
			$resultado = $conex->getResultadoNumerico();
			$datos = array();
			if($resultado) {
				$fila = $conex->getResultado('object');
				$passw = $fila->paswrd;
				if(crypt($pass, $passw) == $passw) {
					if($fila->estatus == 2) {
						$datos['logueado'] = false;
						$datos['l_msg'] = 'Usuario Inactivo';
					}
					elseif($fila->estuser == 2) {
						$datos['logueado'] = false;
						$datos['l_msg'] = 'Usuario Inactivo';
					}
					else {
						$datos['logueado'] = true;
						$datos['l_nombre'] = $fila->nombre;
						$datos['l_tipo'] = $fila->idperfil;
						$datos['l_estatus'] = $fila->estatus;
						$datos['l_id'] = $fila->idusuario;
						$datos['l_empresa'] = $fila->idempresa;
						$datos['l_categoria'] = $fila->categoria;
						$datos['l_folio'] = $fila->folio;
						$datos['l_user'] = $fila->user;
						$datos['l_data_user'] = $this->getRoles_static( $fila->idusuario);
						$datos['l_msg'] = 'login';
					}
				}
				else {
					$datos['logueado'] = false;
					$datos['l_msg'] = 'Contraseña Incorrecta';
				}
								
			}
			else {
				$datos['logueado'] = false;
				$datos['l_msg'] = 'Usuario No registrado';
			}
			return $datos;
		}
		public function cumple() {
			$conexion = new Conectar();
			//-----------------------------------
			$fecha    = date('m-d');
			//-----------------------------------
			$strt = strtotime("+3 day");
			$fecham3 = date('m-d',$strt);
			//-----------------------------------
			//$sql='SELECT nacimiento,nombre FROM usuario WHERE SUBSTR( nacimiento, 6, 5 )="'.$fecha.'"';
			$sql='SELECT nacimiento,nombre,DATE_FORMAT(nacimiento,"%d-%M") AS fnac FROM usuario usr INNER JOIN usuario_cuenta uc ON usr.id_usuario = uc.idusuario WHERE SUBSTR( nacimiento,6,5) BETWEEN "'.$fecha.'" AND "'.$fecham3.'" AND uc.estatus=1 GROUP BY uc.idusuario';
			$conexion->setQuery($sql);
			$resultado=$conexion->getResultadoNumerico();
			$html='';
			if($resultado) {
				$lis='';
				while ($fila=$conexion->getResultado('object')) {
					$nombre=$fila->nombre;
					$fecha=$fila->fnac;
					$lis.=$this->htmlcreator->getTag('li',$nombre.' '.$fecha,array('class'=>'capital'));
					$lis.=$this->htmlcreator->getTagSimple('HR');
				}
				$ul=$this->htmlcreator->getTag('ul',$lis);
				$divcont=$this->htmlcreator->getTag('div',$ul,array('class'=>'prueba negritas'));
				$html=$divcont;
			}
			else{
				$html.='';
			}
			return $html;
		}
		protected function calculaedad($fechanacimiento){
		    list($ano,$mes,$dia) = explode("-",$fechanacimiento);
		    $ano_diferencia  = date("Y") - $ano;
		    $mes_diferencia = date("m") - $mes;
		    $dia_diferencia   = date("d") - $dia;
		    //if ($dia_diferencia < 0 || $mes_diferencia < 0)
		        //$ano_diferencia--;
		    return $ano_diferencia;
		}
		public function laboral() {
			$conexion = new Conectar();
			//-----------------------------------
			$fecha = date('m-d');
			//-----------------------------------
			$strt = strtotime("+3 day");
			$fecham3 = date('m-d',$strt);
			//-----------------------------------
			$sql='SELECT registro_laboral,nombre,DATE_FORMAT(registro_laboral,"%d-%M") AS flab FROM usuario usr INNER JOIN usuario_cuenta uc ON usr.id_usuario = uc.idusuario WHERE SUBSTR( registro_laboral,6,5) BETWEEN "'.$fecha.'" AND "'.$fecham3.'" AND uc.estatus=1 GROUP BY uc.idusuario';
			//$sql='SELECT nacimiento,nombre FROM usuario WHERE SUBSTR( nacimiento,6,5) BETWEEN "'.$fecha.'" AND "'.$fecham3.'"';
			$conexion->setQuery($sql);
			$resultado=$conexion->getResultadoNumerico();
			$html='';
			$count=0;
			if($resultado) {
				$lis='';
				while ($fila = $conexion->getResultado('object')) {
					$edad=$this->calculaedad($fila->registro_laboral);
					if($edad>0) {
						$count++;
						$nombre=$fila->nombre;
						$fecha=$fila->flab;
						if($edad==1)
							$texto='por tu primer año';
						else
							$texto='por tus '.$edad.' años';
						$lis.=$this->htmlcreator->getTag('li',$nombre.' '.$texto.' laborando en Microged '.$fecha,array('class'=>'capital'));
						$lis.=$this->htmlcreator->getTagSimple('HR');
					}
					
				}
				if($count>0) {
					$ul=$this->htmlcreator->getTag('ul',$lis);
					$divcont=$this->htmlcreator->getTag('div','Felicidades :',array('class'=>'prueba negritas'));
					$cont=$this->htmlcreator->getTag('div',$divcont.$ul);
					$html=$cont;
				}
			}
			else {
				$html.='';
			}
			return $html;
		}

		public function getFoliosUser(){
			$cnx = new Conectar();
			$sql = '';
		}
		public function getTiposUser(){
			$cnx = new Conectar();
			$sql = '';
		}
		public function global($param){
			$msg = '';
			$clase = 'error';
			$cnx = new Conectar();
			$cnx->devolverError = true;
			$cnx->setQuery( $param['sql']);
			$correcto = $cnx->getErrorDeQuery();
			if($correcto==1){
				$clase = 'correcto';
				$msg = 'Modificado';
			}
			else{
				$msg = $cnx->getError();
			}
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
		}
		public function actualizar(){
			#############################
			$sql = 'ALTER TABLE nombre_tabla CHANGE nombre_viejo_columna nombre_nuevo_columna';
			$sql = 'ALTER TABLE nombre_tabla RENAME nombre_nuevo_tabla';
			#############################
			$msg = array();
			$clase = 'error';
			#############################
			$param['sql'] = 'ALTER TABLE aplication CHANGE idapli id_aplicacion';
			$r = $this->global($param);
			$resp['msg'] = $r['msg'];
			$clase = $r['clase'];
			#############################
			$param['sql'] = 'ALTER TABLE aplication CHANGE nameapli nombre';
			$r = $this->global($param);
			$resp['msg'] = $r['msg'];
			$clase = $r['clase'];
			#############################
			$param['sql'] = 'ALTER TABLE aplication CHANGE keyapli key';
			$r = $this->global($param);
			$resp['msg'] = $r['msg'];
			$clase = $r['clase'];
			#############################
			$param['sql'] = 'ALTER TABLE aplication RENAME acl_aplicaciones';
			$r = $this->global($param);
			$resp['msg'] = $r['msg'];
			$clase = $r['clase'];
			#############################
			$param['sql'] = 'ALTER TABLE categoryuser CHANGE idcategory id_categoria';
			$r = $this->global($param);
			$resp['msg'] = $r['msg'];
			$clase = $r['clase'];
			#############################
			#############################
			#############################
			#############################
			#############################
			#############################
			#############################
			#############################
			#############################
			$msg = $this->htmlcreator->strconcat($msg,'<br>');
			$resp['msg'] = $msg;
			$resp['clase'] = $clase;
			return $resp;
			#############################
		}
	}
?>