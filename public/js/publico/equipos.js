function aplicaciones(){
  var aplicacion=$('#aplicacion').val();
  var version =$('#version_apli').val();
  //var equipo=genUrl('equipo',2);
  var equipo=$("#equipo").val();
  if(typeof(equipo)=='undefined'){
    var idequipo=0;
  }else{
    var idequipo=equipo;
  }
  $.ajax({
    url:_url_+"orden/aplicaciones/",
    type:'POST',
    data:'idequipo='+idequipo,
    cache:false,
    success: function(opciones){
      $('#lstapli').html(opciones);
      //
      $("#addapli").click(function(){
        $("#frmapli").show("slow");
        return false;
      });
      ///
    }
  });
};
function agregarAplicaciones(){
  var apli=$('#aplicacion').val();
  var ver=$('#version_apli').val();
  $.ajax({
    url:_url_+'orden/agrApli/',
    type:'POST',
    data:'apli='+apli+'&ver='+ver,
    cache:false,
    success:function(opciones){
      $('#msnapli').html(opciones);
    }
  });
}
function tipoEquipos(){
  $.ajax({
    url:_url_+'orden/tipoEquipos/',
    cache:false,
    success:function(opciones){
      $('#tipo').html(opciones);
    }  
  });
};
function agregarTipo(){
  var nombre=$('#ntipo').val();
  $.ajax({
    url:_url_+'orden/agrTipos/',
    type:'POST',
    data:'nombre='+nombre,
    cache:false,
    success:function(opciones){
      $('#msnres').html(opciones);
    }  
  });
};
function agregarmarca(){
  var nombre=$('#nmarca').val();
  $.ajax({
    url:_url_+'orden/agrMarca/',
    type:'POST',
    data:'nombre='+nombre,
    cache:false,
    success:function(opciones){
      $('#msnres').html(opciones);
    }  
  });
};
function marcaEquipo(){
  $.ajax({
    url:_url_+'orden/marcasEquipo/',
    cache:false,
    success:function(opciones){
      $('#marca').html(opciones);
    }  
  });
};
function agregarSO(){
  var nombre=$('#nombre_so').val();
  var version=$('#version_so').val();
  $.ajax({
    url:_url_+'orden/agrSisOpe/',
    type:'POST',
    data:'nombre='+nombre+'&version='+version,
    cache:false,
    success:function(opciones){
      $('#msnres').html(opciones);
      soEquipo();
    }  
  });
};
function soEquipo(){
  $.ajax({
    url:_url_+'orden/sistemaEquipo/',
    cache:false,
    success:function(opciones){
      $('#so').html(opciones);
    }  
  }); 
};
function equipoCuenta(){
  var equipo=$("#equipo").val();
  $.ajax({
    url:_url_+'orden/cuentas/',
    type:'POST',
    data:'equipo='+equipo,
    cache:false,
    success:function(opciones){
      $('#lstcuentas').html(opciones);
    }
  });
};
/**********/
/*******/
function equipoRed(){
  var equipo=$("#equipo").val();
  //var equipo=genUrl('equipo',2)
  $.ajax({
    url:_url_+'orden/red/',
    type:'POST',
    data:'equipo='+equipo,
    cache:false,
    success:function(opciones){
      $('#lstip').html(opciones);
    }
  });
};
/*******/

cuenta=0;
function agregarCampo(){
  cuenta=cuenta+1;
  $("#frmcuenta").append('<div class="div_form account'+cuenta+'"><label class="etiqueta2">Usuario :</label>&nbsp;<input type="text" name="cuentaUsuario[]" class="inp_text mayus" />&nbsp;<label class="etiqueta2">Password :</label>&nbsp;<input type="text" name="cuentaPass[]" class="inp_text mayus" />&nbsp;<input type="button" value="-" onclick="borrar('+cuenta+')" /></div>');
};
red=0;
function agregarRed(){
  red=red+1;
  $("#frmred").append('<div class="div_form red'+red+'"><label class="etiqueta2">ip :</label><input type="text" name="ips[]" class="inp_text mayus" /><label class="etiqueta2">mac :</label><input type="text" name="macs[]" class="inp_text mayus" /><label class="etiqueta2">subred :</label><input type="text" name="subs[]" class="inp_text mayus" /><label class="etiqueta2">gateway :</label><input type="text" name="gates[]" class="inp_text mayus" /><input type="button" value="-" onclick="del('+red+')" /></div>');
};
function borrar(cual) {
  $("div.account"+cual).remove();
}
function del(red){
 $("div.red"+red).remove(); 
};
$(document).ready(function(){
  /***************/
  $('#btngrp').click(function(){
    agregarCampo();
  })
  $('#btnred').click(function(){
    agregarRed();
  })
  /************/
  $("#agr_tipo").hide();
  $("#agr_marca").hide();
  $("#frmapli").hide();
  $("#agr_sistema").hide();
  /*****************/
  $('.cerrar').click(function(){
    $('#agr_tipo').hide('slow');
    $('#agr_marca').hide('slow');
    $('#frmapli').hide('slow');
    $('#agr_sistema').hide('slow');
  });
  /******************/
  aplicaciones();
  tipoEquipos();
  marcaEquipo();
  soEquipo();
  equipoCuenta();
  equipoRed();
  
  $('#agrtipo').click(function(){
     $("#agr_tipo").show("slow");
     return false;
  });
  $('#agrmarca').click(function(){
     $("#agr_marca").show("slow");
     return false;
  });
  $('#agrsisope').click(function(){
     $("#agr_sistema").show("slow");
     return false;
  });
  $('#agr').click(function(){
    agregarTipo();
    tipoEquipos();
    $('#ntipo').val('');
    $("#agr_tipo").hide("slow");
  });

  $('#agrmar').click(function(){
    agregarmarca();
    marcaEquipo();
    $('#nmarca').val('');
    $("#agr_marca").hide("slow");
  });

  $("#agrapli").click(function(){
    agregarAplicaciones();
    aplicaciones();
    $('#aplicacion').val('');
    $('#version_apli').val('');
    $('#frmapli').hide("slow");
  });
  $("#agrso").click(function(){
    agregarSO();
    $('#nombre_so').val('');
    $('#version_so').val('');
    $('#agr_sistema').hide("slow");
  });
  $("#form_equipo").validationEngine();
});