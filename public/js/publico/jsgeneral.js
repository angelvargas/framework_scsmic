function url(){
	var urli='/scsmic/';
	return urli;
}
function ventana(alto, ancho, url){
	var H = (screen.height/2)-(alto/2);
	var L = (screen.width/2)-(ancho/2);
	window.open(url,'ventana','width='+ancho+',height='+alto+',scrollbars=yes,top='+H+',left='+L);
}
function doTheClock()
{
    window.setTimeout( "doTheClock()", 1000 );
    t = new Date();
    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    var hora = ("0"+t.getHours()).substr(-2),
        minuto = ("0"+t.getMinutes()).substr(-2),
        segundo = ("0"+t.getSeconds()).substr(-2);
    $('#hora').html(diasSemana[t.getDay()] + ", " + t.getDate() + " de " + meses[t.getMonth()] + " del " + t.getFullYear() + ' ' + hora+':'+minuto+':'+segundo);
}
doTheClock();