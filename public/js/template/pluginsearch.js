(function($) {
	var metodos = {
		opciones : '',
		init: function(opt) {
			defaults = {
				pagina:1,
				url:_url_,
				tipo:'POST',
				comp : '1'+'/',
				args : {},
				fields : [],
				contenedor : '#cont',
				funciones : []
			};
			metodos.opciones = $.extend({}, defaults, opt);
		},
		execute : function(parametros){
			metodos.init(parametros);
			metodos.showValues();
			metodos.paginador();
		},
		paginador : function(){
			opt = metodos.opciones;
			$.ajax({
				url:opt.url+opt.pagina+'/'+opt.comp,
				type:opt.tipo,
				data:opt.args,
				cache:false,
			}).then(
				function(res){
					$(opt.contenedor).html(res);
					for(var i in  opt.funciones){
						func = opt.funciones[i];
						if (typeof func=="string")
							func = window[func];
						func();
					}
				},
				function(){
					$(opt.contenedor).html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
				}
			).progress(
				$(opt.contenedor).html('<div class="loader"><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
			);
		},
		busqueda : function(parametros){
			metodos.init(parametros);
			$('.inputsearch').keypress(function(e){
				if(e.which == 13){
					metodos.showValues();
					metodos.paginador();
				}
			});
			$(".selsearch").on( "change", function(){
				metodos.showValues();
				metodos.paginador();
			});
			$('.chksearch').on( "click", function(){
				metodos.showValues();
				metodos.paginador();
			});
		},
		showValues : function(){
			opt = metodos.opciones;
			for(var i in opt.fields){
				opt.args[opt.fields[i]] = $('#'+opt.fields[i]).val();
			}
		}
	};
	$.fn.extend({
		ejecutar : function(parametros){
			this.each(function(){
				$(this).click(function(){
					parametros['pagina'] = (parametros['pagina']===undefined)?1:$(this).data('page');
					parametros['url'] = (parametros['url']===undefined)?$(this).data('link'):parametros['url'];
					//parametros['url'] = $(this).data('link');
					metodos.execute(parametros);
					return false;
				});
			});
			metodos.busqueda(parametros);
		}
	});
	$.extend({
		redraw : function(parametros){
			metodos.execute(parametros);
			metodos.busqueda(parametros);
		}
	});
})(jQuery);