var Ventana = ( function(){
	function Ventana(){
		this.formulario = '';
		this.func = '';
		this.idbtn = '';
		this.fields = [];
		this.datas = '';
		this.dataSel = '';
		this.efecto_show = 'blind';
		this.efecto_hide = 'blind';
		this.width = '500';
		this.fclose = [];
	};
	Ventana.prototype.ejecutar = function(){
		var self = this;
		//console.log(this.dataSel);
		//console.log(this.datas);
		if(!this.formulario && !this.func && !this.idbtn && !this.fields) {
			console.log('datos vacios');
		}
		else {
			var	allFields = $([]),
				fn = window[this.func],
				btn = this.idbtn;

			for(var i in this.fields) {
				data =  this.fields[i];
				var datax = $('#'+data);
				allFields = $.merge(allFields, datax);
			}
			
			$( "#"+this.formulario ).dialog({
				autoOpen: false,
				modal: true,
				width: this.width,
				show:{effect: this.efecto_show,duration: 1000},
				hide:{effect: this.efecto_hide,duration: 1000},
				buttons: [{
						text: 'Guardar',
						id:btn,
						click: function(){
							//var fn = window[this.func];
							//var fn = this.func;
							fn();
						}
					},
					{
						text: "Cancelar",
						click: function(){
							$( this ).dialog('close');
						}
					}
				],
				close: function(){
					//console.log(this);
					allFields.val('');
					self.deldatas();
					$('#'+btn).removeAttr('data-ide');
					$('#'+btn).removeData('ide');
					for(var i in  self.fclose){
						fclose = self.fclose[i];
						if (typeof fclose=="string"){
							fclose = window[fclose];
							fclose();
						}
						
					}
				}
			});
		}
	};
	Ventana.prototype.deldatas = function(){
		//console.log(this);
		if(this.datas && this.dataSel) {
			$('#'+this.dataSel).html('');
			$('#'+this.dataSel).removeData();
			$('#'+this.dataSel).removeAttr(this.datas);
			//console.log('datas eliminados');
		}
	};
	return Ventana;
}());