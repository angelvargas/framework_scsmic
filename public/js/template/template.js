$(document).on('ready',init_template);
function init_template(){
	$(".collapsiblemenu > .drpdown > a").on("click", function(e){
		if(!$(this).hasClass("active")) {
	      // hide any open menus and remove all other classes
			$(".collapsiblemenu > .drpdown ul").slideUp(350);
			$(".collapsiblemenu >.drpdown a").removeClass("active");
			// open our new menu and add the open class
			$(this).next("ul").slideDown(350);
			$(this).addClass("active");	
		}
		else if($(this).hasClass("active")) {
			
			$(this).removeClass("active");
			$(this).next("ul").slideUp(350);
		}
	});

	$(".drpdown > ul > .drpdown > div").on("click", function(e){
		if(!$(this).hasClass("active")) {
	      // hide any open menus and remove all other classes
			$(".drpdown > ul > .drpdown ul").slideUp(350);
			$(".drpdown > ul > .drpdown a").removeClass("active");
			// open our new menu and add the open class
			$(this).next("ul").slideDown(350);
			$(this).addClass("active");	
		}
		else if($(this).hasClass("active")) {
			
			$(this).removeClass("active");
			$(this).next("ul").slideUp(350);
		}
	});
}
//################################################
//Objeto generador de buscador en tablas
//################################################
function busqueda(funcion,datos)
{
	$('.inputsearch').keypress(function(e)
	{
		if(e.which == 13)
		{
			create_function(funcion,datos);
		}
	});
	$(".selsearch").on( "change", function(){
		create_function(funcion,datos);
	});
	$('.chksearch').on( "click", function(){
		create_function(funcion,datos);
	});
}
function create_function(funcion,datos)
{
	showValues(datos);
	var fn = window[funcion];
	fn();
}
function showValues(allFields)
{
	jsonArg = {};
	for(var i in allFields)
	{
		jsonArg[allFields[i]] = $('#'+allFields[i]).val();
	}
	return jsonArg;
}
//################################################
//Generador De Ventanas
//Modificado Para subir
//obsoleto
//################################################
function objVentana() {
	self = this;
	this.formulario = '';
	this.func = '';
	this.idbtn = '';
	this.fields = [];
	this.datas = '';
	this.dataSel = '';
	this.width = '500';
	this.ejecutar = function(){
		if(!this.formulario && !this.func && !this.idbtn && !this.fields){
			console.log('datos vacios');
		}
		else{
			var	allFields = $([]),
				fn = window[this.func],
				btn = this.idbtn;

			eliminardata = this.deldatas;

			for(var i in this.fields){
				data =  this.fields[i];
				var datax = $('#'+data);
				allFields = $.merge(allFields, datax);
			}
			
			$( "#"+this.formulario ).dialog({
				autoOpen: false,
				modal: true,
				width: this.width,
				show:{effect: 'blind',duration: 1000},
				hide:{effect: 'blind',duration: 1000},
				buttons: [
					{
						text: 'Guardar',
						id:btn,
						click: function(){
							//var fn = window[this.func];
							//var fn = this.func;
							fn();
						}
					},
					{
						text: "Cancelar",
						click: function(){
							$( this ).dialog('close');
						}
					}
				],
				close: function(){
					allFields.val('');
					eliminardata();
					$('#'+btn).removeAttr('data-ide');
					$('#'+btn).removeData('ide');
				}
			});
		}
	};
}
objVentana.prototype.deldatas = function(){
	//self por que es llmado desde fuera del objeto
	if(self.datas && self.dataSel){
		$('#'+self.dataSel).html('');
		$('#'+self.dataSel).removeData();
		$('#'+self.dataSel).removeAttr(self.datas);
		//console.log('datas eliminados');
	}
};
//################################################
//Generador De Ventanas
//################################################
var Ventana = ( function(){
	function Ventana(){
		this.formulario = '';
		this.func = '';
		this.idbtn = '';
		this.fields = [];
		this.datas = '';
		this.dataSel = '';
		this.efecto_show = 'blind';
		this.efecto_hide = 'blind';
		this.width = '500';
		this.fclose = [];
	};
	Ventana.prototype.ejecutar = function(){
		var self = this;
		//console.log(this.dataSel);
		//console.log(this.datas);
		if(!this.formulario && !this.func && !this.idbtn && !this.fields) {
			console.log('datos vacios');
		}
		else {
			var	allFields = $([]),
				fn = window[this.func],
				btn = this.idbtn;

			for(var i in this.fields) {
				data =  this.fields[i];
				var datax = $('#'+data);
				allFields = $.merge(allFields, datax);
			}
			
			$( "#"+this.formulario ).dialog({
				autoOpen: false,
				modal: true,
				width: this.width,
				show:{effect: this.efecto_show,duration: 1000},
				hide:{effect: this.efecto_hide,duration: 1000},
				buttons: [{
						text: 'Guardar',
						id:btn,
						click: function(){
							//var fn = window[this.func];
							//var fn = this.func;
							fn();
						}
					},
					{
						text: "Cancelar",
						click: function(){
							$( this ).dialog('close');
						}
					}
				],
				close: function(){
					//console.log(this);
					allFields.val('');
					self.deldatas();
					$('#'+btn).removeAttr('data-ide');
					$('#'+btn).removeData('ide');
					for(var i in  self.fclose){
						fclose = self.fclose[i];
						if (typeof fclose=="string"){
							fclose = window[fclose];
							fclose();
						}
						
					}
				}
			});
		}
	};
	Ventana.prototype.deldatas = function(){
		//console.log(this);
		if(this.datas && this.dataSel) {
			$('#'+this.dataSel).html('');
			$('#'+this.dataSel).removeData();
			$('#'+this.dataSel).removeAttr(this.datas);
			//console.log('datas eliminados');
		}
	};
	return Ventana;
}());
//################################################
//Generador De Selects
//################################################
var Wcombo = (function(){
	function Wcombo(){
		this.elemento = '';
		this.new_element = 'combo';
		this.widg = "";
		this.widg_class = "";
		this.placeholder = 'Buscador...';
		this.validation = false;
		this.class_validation = '';
		this.funciones = [];
	}
	Wcombo.prototype.__widget =  function(){
		var self = this;
		this.widg = 'custom.'+this.new_element;
		if( self.validation ){
			self.class_validation = ' validate[required]';
		}
		$.widget( this.widg, {
			_create: function() {
				this.wrapper = $( "<span>" )
				.addClass( "custom-combobox cont-flex flex-no-wrap "+self.widg_class )
				.insertAfter( this.element );
				this.element.hide();
				this._createAutocomplete();
				this._createShowAllButton();
			},
			_createAutocomplete: function() {
				var selected = this.element.children( ":selected" ),
				value = selected.val() ? selected.text() : '';//1*
				this.input = $( "<input>" )
				.appendTo( this.wrapper )
				.val( value )
				.attr( {'title':'', 'id':self.new_element, 'placeholder':self.placeholder} )//2*
				.addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left item-input-11"+self.class_validation )
				.autocomplete({
					delay: 0,
					minLength: 0,
					source: $.proxy( this, "_source" )
				})
				.tooltip({
					classes: {
						"ui-tooltip": "ui-state-highlight"
					}
				});

				this._on( this.input, {
					autocompleteselect: function( event, ui ) {
						ui.item.option.selected = true;
						this._trigger( "select", event, {
							item: ui.item.option
						});
						
						for(var i in  self.funciones){
							func = self.funciones[i];
							if (typeof func=="string")
								func = window[func];
							func();
						}
					},
					autocompletechange: "_removeIfInvalid"
				});
			},
			_createShowAllButton: function() {
				var input = this.input,
				wasOpen = false;
				$( "<a>" )
				.attr( "tabIndex", -1 )
				.attr( "title", "Ver Lista Completa" )
				.tooltip().appendTo( this.wrapper )
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				})
				.removeClass( "ui-corner-all" )
				.addClass( "custom-combobox-toggle ui-corner-right item-1" )
				.on( "mousedown", function() {
					wasOpen = input.autocomplete( "widget" ).is( ":visible" );
				})
				.on( "click", function() {
					input.trigger( "focus" );
					// Close if already visible
					if ( wasOpen ) {
						return;
					}
					// Pass empty string as value to search for, displaying all results
					input.autocomplete( "search", "" );
				});
			},
			_source: function( request, response ) {
				var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
				response( this.element.children( "option" ).map(function() {
					var text = $( this ).text();
					if ( this.value && ( !request.term || matcher.test(text) ) )
						return {
							label: text,
							value: text,
							option: this
						};
					}) );
			},
			_removeIfInvalid: function( event, ui ) {
				// Selected an item, nothing to do
				if ( ui.item ) {
					return;
				}
				// Search for a match (case-insensitive)
				var value = this.input.val(),
				valueLowerCase = value.toLowerCase(),
				valid = false;
				this.element.children( "option" ).each(function() {
					if ( $( this ).text().toLowerCase() === valueLowerCase ) {
						this.selected = valid = true;
						return false;
					}
				});
				// Found a match, nothing to do
				if ( valid ) {
					return;
				}
				// Remove invalid value
				this.input
				.val( "" )
				.attr( "title", /*value + */" No ha seleccionado elemento de la lista" )
				.tooltip( "open" );
				this.element.val( "" );
				this._delay(function() {
					this.input.tooltip( "close" ).attr( "title", "" );
				}, 2500 );
				this.input.autocomplete( "instance" ).term = "";
			},
			refresh: function(){
				console.log('refrescando');
				this.wrapper.remove();
				this.element.show();
				this.element.val('');
				this._create();
			},
			_destroy: function() {
				this.wrapper.remove();
				this.element.show();
			}
		});
	};

	Wcombo.prototype.ejecutar =  function(){
		this.__widget();
	};
	return Wcombo;
}());
//################################################
//Funcion para mostrar el reloj
//################################################
function doTheClock(){
    window.setTimeout( "doTheClock()", 1000 );
    t = new Date();
    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    var hora = ("0"+t.getHours()).substr(-2),
        minuto = ("0"+t.getMinutes()).substr(-2),
        segundo = ("0"+t.getSeconds()).substr(-2);
    $('#hora').html(diasSemana[t.getDay()] + ", " + t.getDate() + " de " + meses[t.getMonth()] + " del " + t.getFullYear() + ' ' + hora+':'+minuto+':'+segundo);
}
doTheClock();
//################################################
function empty(e) {
	switch (e) {
		case "":
		case 0:
		case "0":
		case null:
		case false:
		case typeof this == "undefined":
			return true;
		default:
			return false;
	}
}