$(document).on('ready',inicio);
function inicio()
{
	nombre=$('#nombrep');
	key=$('#key');
	modulo=$('#modulo');
	descripcion=$('#descripcion');
	menu = $('#menu');
	allFields2 = $([]).add(nombre).add(key).add(modulo).add(descripcion).add(menu);
	jsonArg='';

	ventana_permiso('frmpermisos','verificar_permiso','save');
	$('.addper').on('click',function(){
		$('#frmpermisos').dialog('open');
	});
	$('.editper').on('click',getPer);
	$('.editest').on('click',editEst);

	pagina = $('.paginador>.activo').data('page');
	pagina = (pagina===undefined)?1:pagina;
	url = $('.paginador>.activo').data('link');
	contenedor = '#conttbl';
	func = ['inicio'];
	fields = ['nombre','cod','mod'];
	opciones = {url:url,pagina:pagina,contenedor:contenedor,funciones:func,fields:fields};
	$('.page').ejecutar(opciones);
}

function ventana_permiso(ventana,func,idbtn)
{
	//console.log(medida);
	$( "#"+ventana ).dialog({
		autoOpen: false,
		modal: true,
		width: 500,
		show:{effect: 'blind',duration: 800},
		hide:{effect: 'blind',duration: 800},
		buttons: [
			{
				text: 'Guardar',
				id:idbtn,
				click: function(){
					var fn = window[func];
					fn();
				}
			},
			{
				text: "Cancelar",
				click: function(){
					$( this ).dialog('close');
				}
			}
		],
		close: function(){
			allFields2.val('');
			$('#save').removeAttr('data-ide');
			$("#save").removeData('ide');
		}
	});
}
function verificar_permiso()
{
	var validate=$("#frm_permiso").validationEngine('validate');
	if(validate)
	{
		metodo_permiso();
		$('#frmpermisos').dialog('close');
	}
}
function metodo_permiso()
{
	var id = $('#save').data('ide'),jsonArg = {};
	jsonArg['id'] = id;
	jsonArg['nombre'] = nombre.val();
	jsonArg['llave'] = key.val();
	jsonArg['modulo'] = modulo.val();
	jsonArg['descripcion'] = descripcion.val();
	jsonArg['menu'] = menu.val();
	//console.log(jsonArg);
	$.ajax({
		url:_url_+'acl/permisos/metodo_permisos/',
		cache: false,
		data: jsonArg,
		type: 'post',
	}).then(
		function(res){
			//success
			$.redraw(opciones);
		},
		function(){
			//error
			//$('#conttbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		//beforesend
		//$('#conttbl').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
	$('#frmpermisos').dialog('close');
}
function getPer(datos)
{
	var idper=datos.currentTarget.dataset.ide;
	$.ajax({
		url:_url_+'acl/permisos/getpermiso/',
		type:'POST',
		data:{id:idper},
		dataType:'json',
		cache:false
	}).then(
		function(res){
			//success
			$('#save').attr('data-ide',idper);
			$.each(res, function(name, value){
				$('#'+name).val(value);
			});
			$('#frmpermisos').dialog('open');
		},
		function(){
			//error
			//$('#conttbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		//beforesend
		//$('#conttbl').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}
function editEst(datos)
{
	var idper=datos.currentTarget.dataset.ide;
	var newest=datos.currentTarget.dataset.est;
	$.ajax({
		url:_url_+'acl/permisos/editest/',
		data: {per:idper,est:newest},
		type: 'post',
		cache: false,
	}).then(
		function(res){
			//success
			$.redraw(opciones);
		},
		function(){
			//error
			//$('#conttbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		//beforesend
		//$('#conttbl').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}