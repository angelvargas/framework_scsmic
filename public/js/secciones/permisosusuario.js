function permisos_user(){
	var user=$('#iduser').val();
	var rol=$('#idrol').val();
	var modulo=$('#modulos').val();
	$.ajax({
		url:_url_+'acl/users/permisos_user_ajax/',
		type:'POST',
		data:'user='+user+'&rol='+rol+'&modulo='+modulo,
		cache:false,
		success:function(opciones){
			$('#permisos').html(opciones);
			$('.permiso_rol').change(function(){
				var permiso=$(this).attr("name");
				var valor=$(this).val();
				metodos_permisos(user,permiso,valor);
			});
		}
	});
}
function metodos_permisos(user,permiso,valor){
	$.ajax({
		url:_url_+'acl/users/metodo_permisos_user/',
		type:'POST',
		data:'user='+user+'&permiso='+permiso+'&valor='+valor,
		cache:false,
		success:function(opciones){
			permisos_user();
		}
	});
}
$(document).ready(function(){
	permisos_user();
	$('#modulos').change(function(){
		permisos_user();
	});
});