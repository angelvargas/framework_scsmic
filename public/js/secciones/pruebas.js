$(document).on('ready',inicio);
function inicio()
{
	pruebas();
}
function pruebas()
{
	$.ajax({
		url:_url_+'principal/datos/',
		type:'POST',
		cache:false,
	}).then(
		function(res){
			//success
			$('#recibir').html(res);
			//inicio();
		},
		function(){
			//error
			$('#recibir').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		//beforesend
		$('#recibir').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif" /></div>')
	);
}