$(document).on('ready',function(){
	date_range();
	init_tareas();
});
function init_tareas(){
	pagina = $('.paginador>.activo').data('page');
	pagina = (pagina===undefined)?1:pagina;
	url = $('.paginador>.activo').data('link');
	contenedor = '#tareas';
	func = ['init_tareas'];
	fields = [];
	opciones = {url:url, pagina:pagina, contenedor:contenedor, funciones:func, fields:fields};
	$('.page').ejecutar(opciones);
	//---------------------------------------------------------------------
	var wtarea = new Ventana();
	wtarea.formulario = 'dialog-form';
	wtarea.func = 'validar_tarea';
	wtarea.fields = ['titulot', 'descripcion', 'fecha_inicio', 'fecha_fin', 'usuario'];
	wtarea.idbtn = 'savetarea';
	wtarea.width = '500';
	wtarea.ejecutar();
	//
	$('.add-tarea').on('click',function(){
		$( '#dialog-form' ).dialog( 'open' );
		return false;
	});
	$('.btn').on('click',finalizar);
}
function validar_tarea(){
	var validate = $("#frmprecio").validationEngine('validate');
	if(validate) {
		guardar_tarea();
	}
}
function guardar_tarea() {
	var jsonArg = {};
	campos = ['titulot','descripcion','fecha_inicio','fecha_fin','usuario'];
	for(var i in campos) {
		jsonArg[campos[i]] = $('#'+campos[i]).val();
		$('#'+campos[i]).val('');
	}
	$.ajax({
		url:_url_+'principal/addtarea/',
		type:'POST',
		data:jsonArg,
		cache:false,
		dataType:'json',
		success:function(res) {
			$.redraw(opciones);
		}
	});
	$( '#dialog-form' ).dialog( 'close' );
}

function finalizar(datos) {
	var decision = confirm('¿Deseas Finalizar esta tarea?');
	if (decision) {
		id=datos.currentTarget.dataset.tarea;
		$.ajax({
			url:_url_+'principal/finalizar/',
			type:'POST',
			data:{id:id},
			cache:false,
			success:function(res) {
				$.redraw(opciones);
			}
		});
	}
}
/*
function inicio()
{
	//tooltip
	//variables de forn en js
	slider();
	var titulo=$('#titulot'),
		descripcion=$('#descripcion'),
		fecha_inicio=$('#fecha_inicio'),
		fecha_fin=$('#fecha_fin'),
		usuario=$('#usuario'),
		allFields = $( [] ).add(titulo).add(descripcion).add(fecha_inicio).add(fecha_fin).add(usuario);
	
	$('.add-tarea').on('click',function()
	{
		$( '#dialog-form' ).dialog( 'open' );
		return false;
	});
	$( "#dialog-form" ).dialog({
		autoOpen: false,
		modal: true,
		width: 500,
		show: {
			effect: "blind",
			duration: 800
		},
		hide: {
			effect: "blind",
			duration: 800
		},
		buttons: [
			{
				text: "Guardar",
				id:'guardar',
				click: function()
				{
					validar();
				}
			},
			{
				text: "Cancelar",
				click: function() {
					$( this ).dialog( "close" );
					allFields.val('');
					//$('#guardar').removeAttr('data-ide');
					//$('#guardar').removeData('ide');
				}
			}
		],
		close: function() {
			allFields.val('');
			//$('#guardar').removeAttr('data-ide');
			//$('#guardar').removeData('ide');
		}
	});
	
	$('.slider1').bxSlider({
		speed:4000,
		minSlides: 1,
		maxSlides: 1,
		moveSlides: 1,
		auto:true,
		adaptiveHeight:true,
		slideWidth:900,
		autoHover:true,
		pause:20000,
		autoControls: true,
	});
}
function validar()
{
	titulov=$('#titulot').validationEngine('validate');
	descripcionv=$('#descripcion').validationEngine('validate');
	fecha_iniciov=$('#fecha_inicio').validationEngine('validate');
	fecha_finv=$('#fecha_fin').validationEngine('validate');
	if(!titulov && !descripcionv && !fecha_iniciov && !fecha_iniciov)
	{
		guardar_tarea();
		$( '#dialog-form' ).dialog( 'close' );
	}
}

function lista_tareas()
{
	$('#lts-tareas').html('');
	$.ajax({
		url:_url_+'principal/lsttareas/',
		type:'POST',
		//data:{page:page},
		cache:false,
		beforeSend: function(){
			$('#lts-tareas').html('<div class="loader"><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>');
		},
		success:function(res)
		{
			$('#lts-tareas').html(res);
			slider();
			$('.btn').on('click',finalizar);
		},
		error: function(){
			$('#lts-tareas').html(
				$('<div>').attr({
					class:'error'
				}).html('Ha ocurrido un error')
			);
        }
	});
}

//
function slider()
{
	$('ul.lstTareas').bxSlider({
		speed:4000,
		minSlides: 6,
		moveSlides: 6,
		auto:true,
		autoStart:true,
		autoControls: true,
		mode: 'vertical',
		controls:false,
		autoHover:true,
		slideMargin:2,
		adaptiveHeight:true,
		slideWidth: 0,
		pause:20000,
		touchEnabled:true
	});

	$('a.datos').cluetip({activation: 'click', sticky: true, width: 500, heigth:500, attribute:'href',ajaxCache: false,closePosition: 'title',closeText:'cerrar'});
}

*/
function date_range() {
	var fecha_inicio_c = $('#fecha_inicio'),
		fecha_fin_c = $('#fecha_fin');
	fecha_inicio_c.datetimepicker({
		timeFormat: 'HH:mm',
		dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		onClose: function(dateText, inst) {
			if (fecha_fin_c.val() !== '')
			{
				var testStartDate = fecha_inicio_c.datetimepicker('getDate');
				var testEndDate = fecha_fin_c.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					fecha_fin_c.datetimepicker('setDate', testStartDate);
			}
			else
			{
				fecha_fin_c.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			fecha_fin_c.datetimepicker('option', 'minDate', fecha_inicio_c.datetimepicker('getDate') );
		}
	});
	fecha_fin_c.datetimepicker({
		timeFormat: 'HH:mm',
		dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		onClose: function(dateText, inst) {
			if (fecha_inicio_c.val() !== '')
			{
				var testStartDate = fecha_inicio_c.datetimepicker('getDate');
				var testEndDate = fecha_fin_c.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					fecha_inicio_c.datetimepicker('setDate', testEndDate);
			}
			else
			{
				fecha_inicio_c.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			fecha_inicio_c.datetimepicker('option', 'maxDate', fecha_fin_c.datetimepicker('getDate') );
		}
	});
}