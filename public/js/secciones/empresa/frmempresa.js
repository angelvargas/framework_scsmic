$(document).on('ready',inicio);
function inicio()
{
	arrcontactos = [];
	count = 0;
	//--------------------------------------------------------------------------
	fields_estado = ['nombre_estado'];
	ventana_empresa('sec_add_estado', 'validar_estado', 'save_estado', fields_estado, '', '');
	$('#addestado').on('click',function(){
		$('#sec_add_estado').dialog('open');
	});
	$('#save_estado').on('click', validar_estado);
	//--------------------------------------------------------------------------
	fields_ciudad = ['nombre_ciudad'];
	ventana_empresa('sec_add_ciudad', 'validar_ciudad', 'save_ciudad', fields_ciudad, '', '');
	$('#addciudad').on('click',function(){
		$('#sec_add_ciudad').dialog('open');
	});
	$('#save_ciudad').on('click', validar_ciudad);
	//--------------------------------------------------------------------------
	fields_colonia = ['nombre_colonia'];
	ventana_empresa('sec_add_colonia', 'validar_colonia', 'save_colonia', fields_colonia, '', '');
	$('#addcolonia').on('click',function(){
		$('#sec_add_colonia').dialog('open');
	});
	$('#save_colonia').on('click', validar_colonia);
	//--------------------------------------------------------------------------
	$('#estado').on('change', function(e){
		getResidencia($('#estado'), true);
	});
	$('#ciudad').on('change', function(e){
		getResidencia($('#ciudad'), true);
	});
	//--------------------------------------------------------------------------
	$('#addcontacto').on('click', addcontacto);
	//--------------------------------------------------------------------------
	$('#savecontacto').on('click',savecontacto);
	$('.delcontacto').on('click', eliminarContacto);
	//--------------------------------------------------------------------------
	$('#enviar').on('click', validar_empresa);
}
//--------------------------------------------------------------------------
function ventana_empresa(ventana,func,idbtn,fields,datas,datasel)
{
	var windw =  new objVentana();
	windw.formulario = ventana;
	windw.idbtn = idbtn;
	windw.func = func;
	windw.fields = fields;
	windw.datas = datas;
	windw.dataSel = datasel;
	windw.ejecutar();
}
//--------------------------------------------------------------------------
function validar_estado()
{
	var validate = $("#frmestado").validationEngine('validate');
	if (validate) {
		agregarEstado();
	}
}
function agregarEstado()
{
	var nombre = $('#nombre_estado').val();
	$.ajax({
		url:_url_+'usuarios/agr_residencia/',
		type:'POST',
		data:{nombre:nombre, tipo:'estado'},
		cache:false,
	}).then(
		function(res){
			//success
			$('#msgresidencia').html(res);
			getResidencia($('#estado'),false);
		},
		function(){
			//error
			$('#msgresidencia').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#sec_add_estado').dialog('close');
}
//--------------------------------------------------------------------------
function validar_ciudad()
{
	var validate = $("#frmciudad").validationEngine('validate');
	if (validate) {
		agregarCiudad();
	}
}
function agregarCiudad()
{
	var nombre = $('#nombre_ciudad').val(),
		estado = $('#estado option:selected').val();
	$.ajax({
		url:_url_+'usuarios/agr_residencia/',
		type:'POST',
		data:{nombre:nombre, tipo:'ciudad', base:estado},
		cache:false,
	}).then(
		function(res){
			//success
			$('#msgresidencia').html(res);
			getResidencia($('#ciudad'), false, estado);
		},
		function(){
			//error
			$('#msgresidencia').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#sec_add_ciudad').dialog('close');
}
//--------------------------------------------------------------------------
function validar_colonia()
{
	var validate = $("#frmcolonia").validationEngine('validate');
	if (validate) {
		agregarColonia();
	}
}
function agregarColonia()
{
	var nombre = $('#nombre_colonia').val(),
		ciudad = $('#ciudad option:selected').val();
	$.ajax({
		url:_url_+'usuarios/agr_residencia/',
		type:'POST',
		data:{nombre:nombre, tipo:'colonia', base:ciudad},
		cache:false
	}).then(
		function(res){
			//success
			$('#msgresidencia').html(res);
			getResidencia($('#colonia'), false, ciudad);
		},
		function(){
			//error
			$('#msgresidencia').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#sec_add_colonia').dialog('close');
}
//--------------------------------------------------------------------------
function getResidencia(data,flag,valor)
{
	//console.log(data);
	var newvalor = valor? valor : data.val(),
		tipo = data.attr('id'),
		ntipo = tipo;
	if (flag) {
		if(tipo == 'estado')
			ntipo = 'ciudad';
		else if(tipo == 'ciudad')
			ntipo = 'colonia';
		else
			ntipo = tipo;
	}
	selected = $('#frmuser').data(ntipo);
	//console.log(tipo+','+ntipo);
	$.ajax({
		url:_url_+'usuarios/getResidencia/',
		type:'POST',
		data:{tipo:ntipo, selected:selected, id:newvalor},
		cache:false,
	}).then(
		function(res){
			//success
			$('#'+ntipo).html(res);
			if(ntipo == 'ciudad'){
				getResidencia($('#ciudad'),true);
				//getResidencia($('#colonia'),false);
			}
		},
		function(){
			//error
			$('#'+tipo).html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}
//--------------------------------------------------------------------------
function addcontacto()
{
	//------------------------o------------------------
	var nombre = $('#tipocontacto option:selected').text();
	var idtipocont = $('#tipocontacto option:selected').val();
	//------------------------o------------------------
	var valorcontacto = $('#valorcontacto').val();
	//------------------------o------------------------
	count++;
	var id = idtipocont+idtipocont+count;
	if(valorcontacto)
	{
		var flag = 0;
		var cont;
		for ( cont=0; cont < arrcontactos.length; cont++ ) {
			if(id === arrcontactos[cont]) {
				flag=1;
			}
		}
		if (flag === 0) {
			arrcontactos.push(id);
			$('#sec_contactos').append(
				$('<div>').attr(
					{class:'item', name:'contactos[]', id:'contacto'+id}
				).append(
					$('<div>').attr({class:'data'}).html(nombre+':'+valorcontacto),
					$('<a>').attr({'class':'delint', 'data-ref':'contacto'+id, 'data-val':id, 'id':'delcontacto'+id, 'href':'javascript\:void\(0\)'}).html('eliminar')
				)
			);
			//------------------------o------------------------
			$('#contacto'+id).attr('data-ide', idtipocont);
			$('#contacto'+id).attr('data-nombre', nombre);
			//------------------------o------------------------
			$('#contacto'+id).attr('data-valorcont', valorcontacto);
			//------------------------o------------------------
			$('#delcontacto'+id).on('click', delcontacto);
			//------------------------o------------------------
			//$(this).val('');
			//console.log(arrperfil);//eliminar
		}
		$('#valorcontacto').val('');
	}
	return false;
}
function delcontacto(data)
{
	var ref = data.currentTarget.dataset.ref;
	var val = data.currentTarget.dataset.val;
	$('#'+ref).remove();
	var pos = arrcontactos.indexOf(val);
	arrcontactos.splice(pos,1);
	return false;
}
//--------------------------------------------------------------------------
function validar_empresa()
{
	var validate = $("#frmempresa").validationEngine('validate');
	if (validate) {
		saveEmpresa();
	}
}
function saveEmpresa()
{
	var jsonArg = {};
	allFields = ['nombreempresa', 'razonsocial', 'frc', 'sae', 'descripcion', 'direccion', 'estado', 'ciudad', 'colonia'];
	for(var i in allFields) {
		jsonArg[allFields[i]] = $('#'+allFields[i]).val();
	}
	var tipo = $('input:radio[name=radioemp]:checked').val();
	var ide = $('#frmempresa').data('ide');
	jsonArg['ide'] = ide;
	jsonArg['tipo'] = tipo;
    //--------------------------------------------------------------------
    contactos = [];
    $("div[name='contactos[]']").each(function(){
		contactos.push({
			idcontacto:$(this).data('ide'),
			valorcont:$(this).data('valorcont'),
		});
    });
    jsonArg['contactos'] = contactos;
    //--------------------------------------------------------------------
    
    if(ide<=0){
		url=_url_+'usuarios/agregar_empresa/1/';
    }
	else{
		url=_url_+'usuarios/modificar_empresa/'+ide+'/1/';
	}
	$.ajax({
		url:url,
		type:'POST',
		data:jsonArg,
		cache:false
	}).then(
		function(res){
			//success
			$('#form').html(res);
			inicio();
		},
		function(){
			//error
			$('#form').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		$('#form').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}
function savecontacto()
{
	var tipo = $('#tipocontacto option:selected').val(),
		valor = $('#valorcontacto').val(),
		ide = $('#frmempresa').data('ide');
		tipocontacto = $('#frmempresa').data('contacto');
	$.ajax({
		url:_url_+'usuarios/addcontacto/',
		type:'POST',
		data:{valor:valor, tipo:tipo, ide:ide, tipocontacto:tipocontacto},
		cache:false
	}).then(
		function(res){
			//success
			$('#msgcontacto').html(res);
			getContactos();
		},
		function(){
			//error
			$('#msgcontacto').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#valorcontacto').val('');
}
function getContactos()
{
	var ide = $('#frmempresa').data('ide'),
		tipocontacto = $('#frmempresa').data('contacto');
	$.ajax({
		url:_url_+'usuarios/getcontactos/',
		type:'POST',
		data:{ide:ide, tipo:tipocontacto},
		cache:false
	}).then(
		function(res){
			//success
			$('#sec_contactos').html(res);
			$('.delcontacto').on('click', eliminarContacto);
		},
		function(){
			//error
			$('#sec_contactos').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		$('#sec_contactos').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}
function eliminarContacto(data)
{
	var clave = data.currentTarget.dataset.clave;
	ide = $('#frmempresa').data('ide');
	tipocontacto = $('#frmempresa').data('contacto');
	$.ajax({
		url:_url_+'usuarios/delcontacto/',
		type:'POST',
		data:{clave:clave, ide:ide, tipo:tipocontacto},
		cache:false
	}).then(
		function(res){
			//success
			$('#msgcontacto').html(res);
			getContactos();
		},
		function(){
			//error
			$('#msgcontacto').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}