$(document).on('ready',inicio);
function inicio()
{
	$('a.datos').cluetip({
		activation: 'click',
		sticky: true,
		width: 500,
		heigth:500,
		attribute:'href',
		closeText: '<img src="'+_url_+'public/img/plantilla/stop.png"/>',
		closePosition: 'title',
		arrows : true,
		ajaxCache: false
	});
	pagina = $('.paginador>.activo').data('page');
	pagina = (pagina===undefined)?1:pagina;
	url = $('.paginador>.activo').data('link');
	contenedor = '#conttbl';
	func = ['inicio'];
	fields = ['nombre', 'razon'];
	opciones = {url:url,pagina:pagina,contenedor:contenedor,funciones:func,fields:fields};
	$('.page').ejecutar(opciones);
	$('.sincronizar').on('click', sincronizar);
}
function sincronizar()
{
	$.ajax({
		url:_url_+'usuarios/sincronizarempresas/',
		type:'POST',
		cache:false
	}).then(
		function(res){
			//success
			$('#msg_tbl').html(res);
			$.redraw(opciones);
		},
		function(){
			//error
			$('#msg_tbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		$('#conttbl').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}