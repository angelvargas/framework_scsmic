$(document).on('ready',inicio);
function inicio()
{
	nombre=$('#nombrem');
	key=$('#keym');
	menu=$('#menuapp');
	imgfile=$('#imgfile');
	allFields2 = $([]).add(nombre).add(key).add(imgfile).add(menu);
	jsonArg='';
	//$('.page').on('click',listar_modulos);
	allFields = ['nombre','cod','app'];
	//busqueda('listar_modulos',allFields);
	ventana_modulo('frmmod','verificar_modulo','save');
	$('.addmod').on('click',function(){
		$('#frmmod').dialog('open');
	});
	$('.editmod').on('click',getMod);
	$('.editest').on('click',editEst);

	pagina = $('.paginador>.activo').data('page');
	pagina = (pagina===undefined)?1:pagina;
	url = $('.paginador>.activo').data('link');
	contenedor = '#conttbl';
	func = ['inicio'];
	fields = ['nombre','cod','app'];
	opciones = {url:url,pagina:pagina,contenedor:contenedor,funciones:func,fields:fields};
	$('.page').ejecutar(opciones);
}

function ventana_modulo(ventana,func,idbtn)
{
	//console.log(medida);
	$( "#"+ventana ).dialog({
		autoOpen: false,
		modal: true,
		width: 500,
		show:{effect: 'blind',duration: 800},
		hide:{effect: 'blind',duration: 800},
		buttons: [
			{
				text: 'Guardar',
				id:idbtn,
				click: function(){
					var fn = window[func];
					fn();
				}
			},
			{
				text: "Cancelar",
				click: function(){
					$( this ).dialog('close');
				}
			}
		],
		close: function(){
			allFields2.val('');
			$('#save').removeAttr('data-ide');
			$("#save").removeData('ide');
		}
	});
}
function verificar_modulo()
{
	var validate=$("#formmod").validationEngine('validate');
	if(validate)
	{
		metodo_modulo();
		$('#frmrol').dialog('close');
	}
}
function metodo_modulo()
{
	var form_data = new FormData();
	form_data.append('archivo', imgfile.prop('files')[0]);
	form_data.append('nombre', nombre.val());
	form_data.append('llave', key.val());
	form_data.append('menu', menu.val());
	form_data.append('id', $('#save').data('ide'));
	//console.log(form_data);
	$.ajax({
		url:_url_+'acl/modulos/metodo_modulo/',
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'post',
	}).then(
		function(res){
			//success
			$.redraw(opciones);
		},
		function(){
			//error
			//$('#conttbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		//beforesend
		//$('#conttbl').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
	$('#frmmod').dialog('close');
}
function getMod(datos)
{
	var idmod=datos.currentTarget.dataset.ide;
	$.ajax({
		url:_url_+'acl/modulos/getmodulo/',
		type:'POST',
		data:{id:idmod},
		dataType:'json',
		cache:false
	}).then(
		function(res){
			//success
			$('#save').attr('data-ide',idmod);
			$.each(res, function(name, value){
				$('#'+name).val(value);
			});
			$('#frmmod').dialog('open');
		},
		function(){
			//error
			//$('#conttbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		//beforesend
		//$('#conttbl').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}
function editEst(datos)
{
	var idmod=datos.currentTarget.dataset.ide;
	var newest=datos.currentTarget.dataset.est;
	$.ajax({
		url:_url_+'acl/modulos/editest/',
		data: {mod:idmod,est:newest},
		type: 'post',
		cache: false,
	}).then(
		function(res){
			//success
			$.redraw(opciones);
		},
		function(){
			//error
			//$('#conttbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		//beforesend
		//$('#conttbl').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}