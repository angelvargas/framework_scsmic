function permisos_rol(){
	var rol=$('#idrol').val();
	var modulo=$('#modulos').val();
	$.ajax({
		url:_url_+'acl/rol/permisos_rol_ajax/',
		type:'POST',
		data:'rol='+rol+'&modulo='+modulo,
		cache:false,
		success:function(opciones){
			$('#permisos').html(opciones);
			$('.permiso_rol').change(function(){
				var permiso=$(this).attr("name");
				var valor=$(this).val();
				metodos_permisos(rol,permiso,valor);
			});
		}
	});
}
function metodos_permisos(rol,permiso,valor){
	$.ajax({
		url:_url_+'acl/rol/metodo_permisos_rol/',
		type:'POST',
		data:'rol='+rol+'&permiso='+permiso+'&valor='+valor,
		cache:false,
		success:function(opciones){
			permisos_rol();
		}
	});
}
$(document).ready(function(){
	permisos_rol();
	$('#modulos').change(function(){
		permisos_rol();
	});
});