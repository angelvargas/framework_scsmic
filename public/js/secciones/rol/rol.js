$(document).on('ready',inicio);
function inicio()
{
	nombre=$('#nombre');
	categoria=$('#categoria');
	imgfile=$('#imgfile');
	allFields = $([]).add(nombre).add(categoria).add(imgfile);
	$('.editrol').on('click',getRol);
	//$('.page').on('click',lstrol);
	$('.addrol').on('click',function(){
		$('#frmrol').dialog('open');
	});
	ventana_inventario('frmrol','verificarrol','save');
	/***/
	pagina = $('.paginador>.activo').data('page');
	pagina = (pagina===undefined)?1:pagina;
	url = $('.paginador>.activo').data('link');
	contenedor = '#conttbl';
	func = ['inicio'];
	fields = ['selcat','txtrol'];
	opciones = {url:url,pagina:pagina,contenedor:contenedor,funciones:func,fields:fields};
	$('.page').ejecutar(opciones);
}
function ventana_inventario(ventana,func,idbtn)
{
	//console.log(medida);
	$( "#"+ventana ).dialog({
		autoOpen: false,
		modal: true,
		width: 500,
		show:{effect: 'blind',duration: 800},
		hide:{effect: 'blind',duration: 800},
		buttons: [
			{
				text: 'Guardar',
				id:idbtn,
				click: function(){
					var fn = window[func];
					fn();
				}
			},
			{
				text: "Cancelar",
				click: function(){
					$( this ).dialog('close');
				}
			}
		],
		close: function(){
			allFields.val('');
			$('#save').removeAttr('data-ide');
			$("#save").removeData('ide');
		}
	});
}
function verificarrol()
{
	var validate=$("#formrol").validationEngine('validate');
	if(validate)
	{
		metodo_rol();
		$('#frmrol').dialog('close');
	}
}
function metodo_rol()
{
	var form_data = new FormData();
	form_data.append('archivo', imgfile.prop('files')[0]);
	form_data.append('nombre', nombre.val());
	form_data.append('categoria', categoria.val());
	form_data.append('id', $('#save').data('ide'));
	$.ajax({
		url:_url_+'acl/rol/metodorol/',
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'post',
	}).then(
		function(res){
			//success
			$.redraw(opciones);
		},
		function(){
			//error
			//$('#conttbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		//beforesend
		//$('#conttbl').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
	$('#frmrol').dialog('close');
}
function getRol(datos)
{
	var idrol=datos.currentTarget.dataset.ide;
	$.ajax({
		url:_url_+'acl/rol/getRol/',
		type:'POST',
		data:{id:idrol},
		dataType:'json',
		cache:false
	}).then(
		function(res){
			//success
			$('#save').attr('data-ide',idrol);
			$.each(res, function(name, value){
				$('#'+name).val(value);
			});
			$('#frmrol').dialog('open');
		},
		function(){
			//error
			//$('#conttbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		//beforesend
		//$('#conttbl').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}
/*
function lstrol(datos)
{
	if(datos===undefined)
	{
		page=$('a.activo').data('page');
	}
	else
	{
		page=datos.currentTarget.dataset.page;
	}
	$.ajax({
		url:_url_+'acl/rol/roles/'+page+'/1/',
		type:'POST',
		cache:false
	}).then(
		function(res){
			//success
			$('#conttbl').html(res);
			$('.page').on('click',lstrol);
			$('.addrol').on('click',function(){
				$('#frmrol').dialog('open');
			});
			$('.editrol').on('click',getRol);
		},
		function(){
			//error
			$('#conttbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		//beforesend
		$('#conttbl').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
	return false;
}
*/