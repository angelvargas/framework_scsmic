$(document).on('ready',inicio_permisos);
function inicio_permisos() {
	rol = $('#permisos').data('rol');
	modulo = 0;
	$('#modulos').on('change',permisos_rol);
	$('.permiso_rol').on('change',metodo_permiso);
}
function metodo_permiso(datos) {
	//console.log(datos.currentTarget);
	//console.log(this);
	//modulo=$('#modulos').val();
	var permiso = datos.currentTarget.name,
		valor = datos.currentTarget.value;
	$.ajax({
		url:_url_+'acl/rol/metodo_permisos_rol/',
		type:'POST',
		data:'rol='+rol+'&permiso='+permiso+'&valor='+valor,
		cache:false,
	}).then(
		function(res){
			//success
			permisos_rol();
		},
		function(){
			//error
			//$('#permisos').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}
function permisos_rol() {
	rol = $('#permisos').data('rol');
	modulo=$('#modulos').val();
	$.ajax({
		url:_url_+'acl/rol/permisos_rol/'+rol+'/1/',
		type:'POST',
		data:{modulo:modulo},
		cache:false,
	}).then(
		function(opciones){
			//success
			$('#permisos').html(opciones);
			inicio_permisos();
		},
		function(){
			//error
			$('#permisos').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}