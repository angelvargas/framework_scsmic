$(document).on('ready',inicio);
function inicio()
{
	$('#changepass').on('click',function(){
		validar(changepass);
	});
	$('#editpass').on('click',function(){
		validar(editpass);
	});
	//$('#newpass').dPassword();
	//la parte de ver la contraseña por partes no funciona
}
function validar(metodo)
{
	var validate=$("#frmusuario").validationEngine('validate');
	if(validate) {
		metodo();
	}
}
function changepass()
{
	var jsonArg = {};
	allFields = ['newpass','usuario'];
	for(var i in allFields) {
		jsonArg[allFields[i]] = $('#'+allFields[i]).val();
	}
	$.ajax({
		url:_url_+'usuarios/password/1/',
		type:'POST',
		data:jsonArg,
		cache:false,
		success:function(res)
		{
			$('.contenido').html(res);
			inicio();
		},
		beforeSend: function(){
			$('.contenido').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>');
		},
		error: function(){
			$('.contenido').html(
				$('<div>').attr({
					class:'error'
				}).html('Ha ocurrido un error')
			);
        }
	});
}
function editpass()
{
	var jsonArg = {};
	allFields = ['newpass','usuario'];
	for(var i in allFields)
	{
		jsonArg[allFields[i]] = $('#'+allFields[i]).val();
	}
	var ide=$('#frmusuario').data('ide');
	$.ajax({
		url:_url_+'usuarios/editpass/'+ide+'/1/',
		type:'POST',
		data:jsonArg,
		cache:false,
		success:function(res)
		{
			$('.contenido').html(res);
			inicio();
		},
		beforeSend: function(){
			$('.contenido').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>');
		},
		error: function(){
			$('.contenido').html(
				$('<div>').attr({
					class:'error'
				}).html('Ha ocurrido un error')
			);
        }
	});
}