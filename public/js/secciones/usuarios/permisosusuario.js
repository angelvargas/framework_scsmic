$(document).on('ready',inicio);
function inicio() {
	rol = $('#permisos').data('rol');
	user = $('#permisos').data('user');
	$('#modulos').on('change',permisos_user);
	$('.permiso_rol').on('change',metodos_permisos);
}

function permisos_user() {
	var modulo=$('#modulos').val();
	$.ajax({
		url:_url_+'acl/users/permisos_usuario/'+user+'/1/',
		type:'POST',
		data:{modulo:modulo},
		cache:false,
	}).then(
		function(opciones){
			//success
			$('#permisos').html(opciones);
			inicio();
		},
		function(){
			//error
			$('#permisos').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}

function metodos_permisos(datos) {
	var permiso = datos.currentTarget.name,
		valor = datos.currentTarget.value;
	$.ajax({
		url:_url_+'acl/users/metodo_permisos_user/',
		type:'POST',
		data:'user='+user+'&permiso='+permiso+'&valor='+valor,
		cache:false,
	}).then(
		function(res){
			//success
			permisos_user();
		},
		function(){
			//error
			//$('#permisos').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}