$(document).on('ready',init_usuario);
function init_usuario()
{
	arrperfil = [];
	arrcontactos = [];
	count = 0;
	fechas();
	//--------------------------------------------------------------------------
	getPerfil();
	getEmpresas();
	$('input:radio[name=radiocategoria]').on('click', function(){
		getPerfil();
		getEmpresas();
	});
	//--------------------------------------------------------------------------
	$("#selusepswd").on('click', function(){verseccionpass('slow')});
	verseccionpass(0);
	//--------------------------------------------------------------------------
	$("#selinglab").on('click', function(){verseccionlaboral('slow')});
	verseccionlaboral(0);
	//--------------------------------------------------------------------------
	fields_estado = ['nombre_estado'];
	ventana_usuarios('sec_add_estado', 'validar_estado', 'save_estado', fields_estado, '', '');
	$('#addestado').on('click',function(){
		$('#sec_add_estado').dialog('open');
	});
	$('#save_estado').on('click', validar_estado);
	//--------------------------------------------------------------------------
	fields_ciudad = ['nombre_ciudad'];
	ventana_usuarios('sec_add_ciudad', 'validar_ciudad', 'save_ciudad', fields_ciudad, '', '');
	$('#addciudad').on('click',function(){
		$('#sec_add_ciudad').dialog('open');
	});
	$('#save_ciudad').on('click', validar_ciudad);
	//--------------------------------------------------------------------------
	fields_colonia = ['nombre_colonia'];
	ventana_usuarios('sec_add_colonia', 'validar_colonia', 'save_colonia', fields_colonia, '', '');
	$('#addcolonia').on('click',function(){
		$('#sec_add_colonia').dialog('open');
	});
	$('#save_colonia').on('click', validar_colonia);
	//--------------------------------------------------------------------------
	fields_empresa = ['nombre_empresa'];
	ventana_usuarios('sec_add_empresa', 'validar_empresa', 'save_empresa', fields_empresa, '', '');
	$('#addempresa').on('click',function(){
		$('#sec_add_empresa').dialog('open');
	});
	$('#save_empresa').on('click', validar_empresa);
	//--------------------------------------------------------------------------
	$('#addperfil').on('click', addperfil);
	$('#addcontacto').on('click', addcontacto);
	$('#saveperfil').on('click', saveperfil);
	$('.chgest').on('click', chgest);
	//--------------------------------------------------------------------------
	$('#savecontacto').on('click',savecontacto);
	$('.delcontacto').on('click', eliminarContacto);
	//--------------------------------------------------------------------------
	$('#estado').on('change', function(e){
		getResidencia($('#estado'), true);
	});
	$('#ciudad').on('change', function(e){
		getResidencia($('#ciudad'), true);
	});
	//--------------------------------------------------------------------------
	$('#save').on('click', validar_usuario);
}
function fechas()
{
	$( '#nacimiento, #laboral' ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat : 'yy-mm-dd',
		yearRange: "c-30:c+3",
		showButtonPanel: true
	});
}
function getPerfil()
{
	var valor = $('input:radio[name=radiocategoria]:checked').val();
	$.ajax({
		url:_url_+'usuarios/getperfil/',
		type:'POST',
		data:{tipo:valor},
		cache:false,
	}).then(
		function(res){
			//success
			$('#perfil').html(res);
		},
		function(){
			//error
			$('#msnres').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}
function verseccionpass(velocidad)
{
	if($("#selusepswd").is(':checked'))
		$('#secusepswd').show(velocidad);
	else
		$('#secusepswd').hide(velocidad);
}
function verseccionlaboral(velocidad)
{
	if($("#selinglab").is(':checked'))
		$('#seccion_laboral').show(velocidad);
	else
		$('#seccion_laboral').hide(velocidad);
}
//--------------------------------------------------------------------------
function validar_estado()
{
	var validate = $("#frmestado").validationEngine('validate');
	if (validate) {
		agregarEstado();
	}
}
function agregarEstado()
{
	var nombre = $('#nombre_estado').val();
	$.ajax({
		url:_url_+'usuarios/agr_residencia/',
		type:'POST',
		data:{nombre:nombre, tipo:'estado'},
		cache:false,
	}).then(
		function(res){
			//success
			$('#msgresidencia').html(res);
			getResidencia($('#estado'),false);
		},
		function(){
			//error
			$('#msgresidencia').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#sec_add_estado').dialog('close');
}
//--------------------------------------------------------------------------
function validar_ciudad()
{
	var validate = $("#frmciudad").validationEngine('validate');
	if (validate) {
		agregarCiudad();
	}
}
function agregarCiudad()
{
	var nombre = $('#nombre_ciudad').val(),
		estado = $('#estado option:selected').val();
	$.ajax({
		url:_url_+'usuarios/agr_residencia/',
		type:'POST',
		data:{nombre:nombre, tipo:'ciudad', base:estado},
		cache:false,
	}).then(
		function(res){
			//success
			$('#msgresidencia').html(res);
			getResidencia($('#ciudad'), false, estado);
		},
		function(){
			//error
			$('#msgresidencia').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#sec_add_ciudad').dialog('close');
}
//--------------------------------------------------------------------------
function validar_colonia()
{
	var validate = $("#frmcolonia").validationEngine('validate');
	if (validate) {
		agregarColonia();
	}
}
function agregarColonia()
{
	var nombre = $('#nombre_colonia').val(),
		ciudad = $('#ciudad option:selected').val();
	$.ajax({
		url:_url_+'usuarios/agr_residencia/',
		type:'POST',
		data:{nombre:nombre, tipo:'colonia', base:ciudad},
		cache:false
	}).then(
		function(res){
			//success
			$('#msgresidencia').html(res);
			getResidencia($('#colonia'), false, ciudad);
		},
		function(){
			//error
			$('#msgresidencia').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#sec_add_colonia').dialog('close');
}
//--------------------------------------------------------------------------
function ventana_usuarios(ventana,func,idbtn,fields,datas,datasel)
{
	var windw =  new objVentana();
	windw.formulario = ventana;
	windw.idbtn = idbtn;
	windw.func = func;
	windw.fields = fields;
	windw.datas = datas;
	windw.dataSel = datasel;
	windw.ejecutar();
}
//--------------------------------------------------------------------------
function addperfil()
{
	//------------------------o------------------------
	var nombre = $('#perfil option:selected').text();
	var idperfil = $('#perfil option:selected').val();
	//------------------------o------------------------
	var empresa = $('#empresa option:selected').text();
	var idempresa = $('#empresa option:selected').val();
	//------------------------o------------------------
	var nestatus = $('#estatus option:selected').text();
	var estatus = $('#estatus option:selected').val();
	//------------------------o------------------------
	var texto = '';
	if(idempresa){
		texto = nombre+'/'+empresa+'/'+nestatus;
	}
	else{
		texto = nombre+'/'+nestatus;
		idempresa = 0;
	}
	var id = idperfil+idempresa;
	if(idperfil)
	{
		var flag = 0;
		var cont;
		for ( cont=0; cont < arrperfil.length; cont++ ) {
			if(id === arrperfil[cont]) {
				flag=1;
			}
		}
		if (flag === 0) {
			arrperfil.push(id);
			//dat = $('<div>').attr({class:'data'}).html(nombre);
			$('#lsttiposuser').append(
				$('<div>').attr(
					{class:'item', name:'perfiles[]', id:'perfil'+id}
				).append(
					$('<div>').attr({class:'data'}).html(texto),
					$('<a>').attr({'class':'delint', 'data-ref':'perfil'+id, 'data-val':id, 'id':'delperfil'+id, 'href':'javascript\:void\(0\)'}).html('eliminar')
				)
			);
			//------------------------o------------------------
			$('#perfil'+id).attr('data-ide', idperfil);
			$('#perfil'+id).attr('data-nombre', nombre);
			//------------------------o------------------------
			$('#perfil'+id).attr('data-emp', idempresa);
			//------------------------o------------------------
			$('#perfil'+id).attr('data-est', estatus);
			//------------------------o------------------------
			$('#delperfil'+id).on('click', delperfil);
			//------------------------o------------------------
			//$(this).val('');
			//console.log(arrperfil);//eliminar
		}
	}
	return false;
}
function delperfil(data)
{
	var ref = data.currentTarget.dataset.ref;
	var val = data.currentTarget.dataset.val;
	$('#'+ref).remove();
	var pos = arrperfil.indexOf(val);
	arrperfil.splice(pos,1);
	return false;
}
function saveperfil()
{
	var idperfil = $('#perfil option:selected').val(),
		idempresa = $('#empresa option:selected').val(),
		estatus = $('#estatus option:selected').val(),
		ide = $('#frmuser').data('ide');
	$.ajax({
		url:_url_+'usuarios/addperfil/',
		type:'POST',
		data:{idperfil:idperfil, idempresa:idempresa, estatus:estatus, ide:ide},
		cache:false
	}).then(
		function(res){
			//success
			$('#msgemp').html(res);
			getPerfiles();
		},
		function(){
			//error
			$('#msgemp').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#valorcontacto').val('');
}
function getPerfiles()
{
	var ide = $('#frmuser').data('ide');
	$.ajax({
		url:_url_+'usuarios/getperfiles/',
		type:'POST',
		data:{ide:ide},
		cache:false
	}).then(
		function(res){
			//success
			$('#lsttiposuser').html(res);
			$('.chgest').on('click', chgest);
		},
		function(){
			//error
			$('#lsttiposuser').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		$('#lsttiposuser').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}
function chgest(data)
{
	var clave = data.currentTarget.dataset.clave,
		est = data.currentTarget.dataset.newest;
	$.ajax({
		url:_url_+'usuarios/chgest/',
		type:'POST',
		data:{clave:clave, est:est},
		cache:false
	}).then(
		function(res){
			//success
			$('#msgemp').html(res);
			getPerfiles();
		},
		function(){
			//error
			$('#msgemp').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}
//--------------------------------------------------------------------------
function addcontacto()
{
	//------------------------o------------------------
	var nombre = $('#tipocontacto option:selected').text();
	var idtipocont = $('#tipocontacto option:selected').val();
	//------------------------o------------------------
	var valorcontacto = $('#valorcontacto').val();
	//------------------------o------------------------
	count++;
	var id = idtipocont+idtipocont+count;
	if(valorcontacto)
	{
		var flag = 0;
		var cont;
		for ( cont=0; cont < arrcontactos.length; cont++ ) {
			if(id === arrcontactos[cont]) {
				flag=1;
			}
		}
		if (flag === 0) {
			arrcontactos.push(id);
			$('#sec_contactos').append(
				$('<div>').attr(
					{class:'item', name:'contactos[]', id:'contacto'+id}
				).append(
					$('<div>').attr({class:'data'}).html(nombre+':'+valorcontacto),
					$('<a>').attr({'class':'delint', 'data-ref':'contacto'+id, 'data-val':id, 'id':'delcontacto'+id, 'href':'javascript\:void\(0\)'}).html('eliminar')
				)
			);
			//------------------------o------------------------
			$('#contacto'+id).attr('data-ide', idtipocont);
			$('#contacto'+id).attr('data-nombre', nombre);
			//------------------------o------------------------
			$('#contacto'+id).attr('data-valorcont', valorcontacto);
			//------------------------o------------------------
			$('#delcontacto'+id).on('click', delcontacto);
			//------------------------o------------------------
		}
		$('#valorcontacto').val('');
	}
	return false;
}
function delcontacto(data)
{
	var ref = data.currentTarget.dataset.ref;
	var val = data.currentTarget.dataset.val;
	$('#'+ref).remove();
	var pos = arrcontactos.indexOf(val);
	arrcontactos.splice(pos,1);
	return false;
}
function savecontacto()
{
	var tipo = $('#tipocontacto option:selected').val(),
		valor = $('#valorcontacto').val(),
		ide = $('#frmuser').data('folio');
		tipocontacto = $('#frmuser').data('contacto');
	$.ajax({
		url:_url_+'usuarios/addcontacto/',
		type:'POST',
		data:{valor:valor, tipo:tipo, ide:ide, tipocontacto:tipocontacto},
		cache:false
	}).then(
		function(res){
			//success
			$('#msgcontacto').html(res);
			getContactos();
		},
		function(){
			//error
			$('#msgcontacto').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#valorcontacto').val('');
}
function getContactos()
{
	var ide = $('#frmuser').data('ide'),
		tipocontacto = $('#frmuser').data('contacto');
	$.ajax({
		url:_url_+'usuarios/getcontactos/',
		type:'POST',
		data:{ide:ide, tipo:tipocontacto},
		cache:false
	}).then(
		function(res){
			//success
			$('#sec_contactos').html(res);
			$('.delcontacto').on('click', eliminarContacto);
		},
		function(){
			//error
			$('#sec_contactos').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		$('#sec_contactos').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}
function eliminarContacto(data)
{
	var clave = data.currentTarget.dataset.clave,
		ide = $('#frmuser').data('folio'),
		tipocontacto = $('#frmuser').data('contacto');
	$.ajax({
		url:_url_+'usuarios/delcontacto/',
		type:'POST',
		data:{clave:clave, ide:ide, tipo:tipocontacto},
		cache:false
	}).then(
		function(res){
			//success
			$('#msgcontacto').html(res);
			getContactos();
		},
		function(){
			//error
			$('#msgcontacto').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}
//--------------------------------------------------------------------------
function validar_empresa()
{
	var validate = $("#frmempresa").validationEngine('validate');
	if (validate) {
		agregarEmpresa();
	}
}
function agregarEmpresa()
{
	var valor = $('input:radio[name=radiocategoria]:checked').val();
	if(valor==2)
		tipo = 3;
	else if(valor == 4)
		tipo=2;
	else
		tipo = 1;
	var nombre = $('#nombre_empresa').val();
	$.ajax({
		url:_url_+'usuarios/agr_empresa/',
		type:'POST',
		data:{nombre:nombre,tipo:tipo},
		cache:false
	}).then(
		function(res){
			//success
			$('#msgemp').html(res);
			getEmpresas();
		},
		function(){
			//error
			$('#msgemp').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#sec_add_empresa').dialog('close');
}
function getEmpresas()
{
	var valor = $('input:radio[name=radiocategoria]:checked').val();
	if(valor==2)
		tipo = 3;
	else if(valor == 4)
		tipo=2;
	else
		tipo = 1;
	$.ajax({
		url:_url_+'usuarios/empresas/',
		type:'POST',
		data:{tipo:tipo},
		cache:false
	}).then(
		function(res){
			//success
			$('#empresa').html(res);
		},
		function(){
			//error
			$('#empresa').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}
//--------------------------------------------------------------------------
function getResidencia(data,flag,valor)
{
	//console.log(data);
	var newvalor = valor? valor : data.val(),
		tipo = data.attr('id'),
		ntipo = tipo;
	if (flag) {
		if(tipo == 'estado')
			ntipo = 'ciudad';
		else if(tipo == 'ciudad')
			ntipo = 'colonia';
		else
			ntipo = tipo;
	}
	selected = $('#frmuser').data(ntipo);
	//console.log(tipo+','+ntipo);
	$.ajax({
		url:_url_+'usuarios/getResidencia/',
		type:'POST',
		data:{tipo:ntipo, selected:selected, id:newvalor},
		cache:false,
	}).then(
		function(res){
			//success
			$('#'+ntipo).html(res);
			if(ntipo == 'ciudad'){
				getResidencia($('#ciudad'),true);
				//getResidencia($('#colonia'),false);
			}
		},
		function(){
			//error
			$('#'+tipo).html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
}
//--------------------------------------------------------------------------
function validar_usuario()
{
	var validate = $("#frmuser").validationEngine('validate');
	if (validate) {
		saveusuario();
	}
}
function saveusuario()
{
	var jsonArg = {};
	allFields = ['nombre', 'nbrusr', 'nbrpwss', 'sae', 'nacimiento', 'direccion', 'estado', 'ciudad', 'colonia', 'laboral'];
	for(var i in allFields)
	{
		jsonArg[allFields[i]] = $('#'+allFields[i]).val();
	}
	var ide = $('#frmuser').data('ide');
	var folio = $('#frmuser').data('folio');
	var editperfil = $('#frmuser').data('editperfil');
	jsonArg['ide'] = ide;
	//--------------------------------------------------------------------
    perfiles = [];
    $("div[name='perfiles[]']").each(function(){
		perfiles.push({
			perfil:$(this).data('ide'),
			empresa:$(this).data('emp'),
			estatus:$(this).data('est')
		});
    });
    jsonArg['perfiles'] = perfiles;
    //--------------------------------------------------------------------
    contactos = [];
    $("div[name='contactos[]']").each(function(){
		contactos.push({
			idcontacto:$(this).data('ide'),
			valorcont:$(this).data('valorcont'),
		});
    });
    jsonArg['contactos'] = contactos;
    //--------------------------------------------------------------------
    
    if(ide<=0){
		url=_url_+'usuarios/agregar_usuario/1/';
    }
	else{
		url=_url_+'usuarios/modificar_usuario/'+folio+'/1/';
		if(editperfil==1)
			url=_url_+'usuarios/perfil/1/';
	}
	$.ajax({
		url:url,
		type:'POST',
		data:jsonArg,
		cache:false
	}).then(
		function(res){
			//success
			$('#form').html(res);
			init_usuario();
		},
		function(){
			//error
			$('#form').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	).progress(
		$('#form').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
	);
}