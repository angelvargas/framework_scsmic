//este codigo esta en desuso, se debe de eliminar
$(document).on('ready',inicio);
function inicio()
{
	//Boton para perfil
	$('#btnperfil').on('click',function(){
		validar(editPerfil);
	});
	//Boton para agregar usuario
	$('#addcliente').on('click',function(){
		validar(addcliente);
	});
	//Boton para agregar usuario
	$('#addusuario').on('click',function(){
		validar(addusuario);
	});
	//Boton para modificar cliente o usuario
	$('#editusuario').on('click',function(){
		validar(editusuario);
	});
	//oculta o muestra la seccion para agregar una empresa
	$('#agremp').on('click',function(){
		showOrHidden('frm_empresa');
		return false;
	});
	//oculta o muestra la seccion para agregar un estado
	$('#agrest').click(function(){
		showOrHidden('frm_estado');
		return false;
	});
	//oculta o muestra la seccion para agregar una ciudad
	$('#agrciu').click(function(){
		showOrHidden('frm_ciudad');
		return false;
	});
	//oculta o muestra la seccion para gergar una colonia
	$('#agrcol').click(function(){
		showOrHidden('frm_colonia');
		return false;
	});
	//cerrar todos los mini-formularios
	$('.cerrar').on('click',ocultar);
	//calendario
	fechas();
	//cambio de residencia
	$('#estado,#ciudad').on('change',function(){
		getResidencia($(this));
	});
	//cambio de pefil-tipo usuario
	$('input:radio[name=tipo]').on('click',getPerfil);
	$('#registroemp').on('click',agregarEmpresa);
	$('#registroest').on('click',agregarEstado);
	$('#registrociu').on('click',agregarCiudad);
	$('#registrocol').on('click',agregarColonia);
}
function validar(metodo)
{
	var validate=$("#frmusuario").validationEngine('validate');
	if(validate)
	{
		metodo();
	}
}
function ocultar()
{
	$("#frm_empresa").hide('slow');
		$('#frm_empresa input[type="text"]').val('');
	$("#frm_estado").hide('slow');
		$('#frm_estado input[type="text"]').val('');
	$("#frm_ciudad").hide('slow');
		$('#frm_ciudad input[type="text"]').val('');
	$("#frm_colonia").hide('slow');
		$('#frm_colonia input[type="text"]').val('');
}
function showOrHidden(selector)
{
	if ($('#'+selector).is(':hidden'))
	{
		$('#'+selector).show('slow');
	}
	else
	{
		$('#'+selector).hide('slow');
		$('#'+selector+' input[type="text"]').val('');
	}
}
function editPerfil()
{
	var jsonArg = {};
	allFields = ['nombre','nacimiento','estado','ciudad','colonia','telefono','email','calle','cruz1','cruz2','num','usuario'];
	for(var i in allFields)
	{
		jsonArg[allFields[i]] = $('#'+allFields[i]).val();
	}
	var empresa=$('#frmusuario').data('empresa');
	jsonArg['empresa'] = empresa;
	//console.log(jsonArg);
	//alert(jsonArg);
	$.ajax({
		url:_url_+'web/clientes/perfil/1/',
		type:'POST',
		data:jsonArg,
		cache:false,
		success:function(res)
		{
			$('.contenido').html(res);
			inicio();
		},
		beforeSend: function(){
			$('.contenido').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>');
		},
		error: function(){
			$('.contenido').html(
				$('<div>').attr({
					class:'error'
				}).html('Ha ocurrido un error')
			);
        }
	});
}
function editusuario()
{
	var jsonArg = {};
	allFields = ['nombre','nacimiento','estado','ciudad','colonia','telefono','email','calle','cruz1','cruz2','num','usuario','sae','laboral','empresa','perfil','estatus'];
	for(var i in allFields)
	{
		jsonArg[allFields[i]] = $('#'+allFields[i]).val();
	}
	var ide=$('#frmusuario').data('ide');
	var tipo=$('input:radio[name=tipo]:checked').val();
	jsonArg['tipo'] = tipo;
	//jsonArg['id'] = id;
	//console.log(jsonArg);
	//alert(jsonArg);
	$.ajax({
		url:_url_+'web/clientes/modificar_cliente/'+ide+'/1/',
		type:'POST',
		data:jsonArg,
		cache:false,
		success:function(res)
		{
			$('.contenido').html(res);
			inicio();
		},
		beforeSend: function(){
			$('.contenido').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>');
		},
		error: function(){
			$('.contenido').html(
				$('<div>').attr({
					class:'error'
				}).html('Ha ocurrido un error')
			);
        }
	});
}

/*function addusuario()
{
	var jsonArg = {};
	allFields = ['nombre','nacimiento','estado','ciudad','colonia','telefono','email','calle','cruz1','cruz2','num','usuario','perfil','empresa','estatus','laboral'];
	for(var i in allFields)
	{
		jsonArg[allFields[i]] = $('#'+allFields[i]).val();
	}
	var tipo=$('input:radio[name=tipo]:checked').val();
	jsonArg['tipo'] = tipo;
	//console.log(jsonArg);
	//alert(jsonArg);
	$.ajax({
		url:_url_+'usuarios/agregar_usuario/1/',
		type:'POST',
		data:jsonArg,
		cache:false,
		success:function(res)
		{
			$('.contenido').html(res);
			inicio();
		},
		beforeSend: function(){
			$('.contenido').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>');
		},
		error: function(){
			$('.contenido').html(
				$('<div>').attr({
					class:'error'
				}).html('Ha ocurrido un error')
			);
        }
	});
}*/
/*function addcliente()
{
	var jsonArg = {};
	allFields = ['nombre','nacimiento','estado','ciudad','colonia','telefono','email','calle','cruz1','cruz2','num','usuario','sae','perfil','empresa','estatus'];
	for(var i in allFields)
	{
		jsonArg[allFields[i]] = $('#'+allFields[i]).val();
	}
	var tipo=$('input:radio[name=tipo]:checked').val();
	jsonArg['tipo'] = tipo;
	//console.log(jsonArg);
	//alert(jsonArg);
	$.ajax({
		url:_url_+'usuarios/agregar_cliente/1/',
		type:'POST',
		data:jsonArg,
		cache:false,
		success:function(res)
		{
			$('.contenido').html(res);
			inicio();
		},
		beforeSend: function(){
			$('.contenido').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>');
		},
		error: function(){
			$('.contenido').html(
				$('<div>').attr({
					class:'error'
				}).html('Ha ocurrido un error')
			);
        }
	});
}*/
function getResidencia(datos,flag,selflag,chg)
{
	flag = (flag) ? flag : 'true';
	var cambiar=datos.data('cambiar'),
		sel=datos.val(),
		id,
		selector;
	if(cambiar==='ciu')
	{
		selector='ciudad';
		id=$('#'+selector).data('ide');
	}
	else if(cambiar==='col')
	{
		selector='colonia';
		id=$('#'+selector).data('ide');
	}
	if(selflag)
	{
		selector=selflag;
		cambiar=chg;
		id=$('#'+selector).data('ide');
	}
	//var tipo=datos.currentTarget.dataset.tipo;
	$.ajax({
		url:_url_+'usuarios/getResidencia/',
		type:'POST',
		data:{cambiar:cambiar,sel:sel,id:id},
		cache:false,
		success:function(res)
		{
			$('#'+selector).html(res);
			if(flag=='true' && cambiar=='ciu')
				getResidencia($('#ciudad'),'false');
		},
		beforeSend: function(){
			$('#'+selector).html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>');
		},
		error: function(){
			$('#'+selector).html(
				$('<div>').attr({
					class:'error'
				}).html('Ha ocurrido un error')
			);
        }
	});
}
function fechas()
{
	$(function(){
		var now = new Date();
		$('#nacimiento,#laboral').mobiscroll().date({
			minDate: new Date(now.getFullYear()-100, now.getMonth(), now.getDate()),
			//maxDate: new Date(now.getFullYear()+5, now.getMonth(), now.getDate()),
			theme: 'android-ics',
			lang: 'es',
			display: 'modal',
			animate: 'slidevertical',
			mode: 'mixed',
			dateFormat:'yy-mm-dd',
			dateOrder: 'Mddyy'
		});
	});
}
function getPerfil()
{
	var valor=$(this).val(),tiposel;
	valor=$('input:radio[name=tipo]:checked').val();
	$.ajax({
		url:_url_+"usuarios/selects/",
		type:'POST',
		data:'tipo='+valor+'&tiposel='+tiposel,
		cache:false,
		success: function(opciones){
			$('#perfil').html(opciones);
		}
	});
	getEmpresas();
}
function getEmpresas()
{
	valor = $('input:radio[name=tipo]:checked').val();
	$.ajax({
		url:_url_+"usuarios/empresas/",
		type:'POST',
		data:'tipo='+valor,
		cache:false,
		success: function(opciones){
			$('#empresa').html(opciones);
		},
		beforeSend: function(){
			$('#empresa').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>');
		},
		error: function(){
			$('#empresa').html(
				$('<div>').attr({
					class:'error'
				}).html('Ha ocurrido un error')
			);
        }
	});
}
function agregarEmpresa()
{
	var nombre=$('#nombre_empresa').val();
	$.ajax({
		url:_url_+'usuarios/agr_empresa/',
		type:'POST',
		data:{nombre:nombre},
		dataType:'json',
		cache:false,
		success: function(res)
		{
			$('#msnres').html(
				$('<div>').attr({
					class:res.clase
				}).html(res.mensaje)
			);
			getEmpresas();
		}
	});
	$('#nombre_empresa').val('');
	$('#frm_empresa').hide('slow');
}
function agregarEstado()
{
	var nombre=$('#nombre_estado').val(),b='e';
	$.ajax({
		url:_url_+'usuarios/agr_residencia/',
		type:'POST',
		data:{nombre:nombre,tipo:b},
		cache:false,
		dataType:'json',
		success: function(res)
		{
			$('#msnres').html(
				$('<div>').attr({
					class:res.clase
				}).html(res.mensaje)
			);
			getResidencia($('#estado'),'false','estado','est');
			//$('#msnres').html(opciones);
		}
	});
	$('#nombre_estado').val('');
	$('#frm_estado').hide('slow');
}
function agregarCiudad()
{
	var nombre=$('#nombre_ciudad').val();
	var est=$('#estado option:selected').val();
	var tipo='c';
	$.ajax({
		url:_url_+"usuarios/agr_residencia/",
		type:"POST",
		data:{nombre:nombre,tipo:tipo,base:est},
		cache:false,
		dataType:'json',
		success: function(res){
			$('#msnres').html(
				$('<div>').attr({
					class:res.clase
				}).html(res.mensaje)
			);
			getResidencia($('#estado'),'false');
		}
	});
	$('#nombre_ciudad').val('');
	$('#frm_ciudad').hide('slow');
}
function agregarColonia()
{
	var nombre=$('#nombre_colonia').val();
	var ciudad=$('#ciudad option:selected').val();
	var tipo='col';
	$.ajax({
		url:_url_+'usuarios/agr_residencia/',
		type:"POST",
		data:{nombre:nombre,tipo:tipo,base:ciudad},
		cache:false,
		dataType:'json',
		success: function(res){
			$('#msnres').html(
				$('<div>').attr({
					class:res.clase
				}).html(res.mensaje)
			);
			getResidencia($('#ciudad'),'false');
		}
	});
	$('#nombre_colonia').val('');
	$('#frm_colonia').hide('slow');
}