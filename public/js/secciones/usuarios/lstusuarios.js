$(document).on('ready',inicio);
function inicio()
{
	$('a.datos').cluetip({
		activation: 'click',
		sticky: true,
		width: 500,
		heigth:500,
		attribute:'href',
		closeText: '<img src="'+_url_+'public/img/plantilla/stop.png"/>',
		closePosition: 'title',
		arrows : true,
		ajaxCache: false
	});
	pagina = $('.paginador>.activo').data('page');
	pagina = (pagina===undefined)?1:pagina;
	url = $('.paginador>.activo').data('link');
	contenedor = '#conttbl';
	func = ['inicio'];
	fields = ['nombre', 'email', 'empresa'];
	opciones = {url:url,pagina:pagina,contenedor:contenedor,funciones:func,fields:fields};
	$('.page').ejecutar(opciones);
	$('.union').on('click', unir);
}
function unir()
{
	var jsonArg = {};
	perfiles = [];
    $('input[name="perfiles[]"]:checked').each(function(){
		perfiles.push({
			folio:$(this).data('folio'),
			id:$(this).data('id')
		});
    });
    jsonArg['perfiles'] = perfiles;
    //--------------------------------------------------------------------
	$.ajax({
		url:_url_+'usuarios/unirperfil/',
		type:'POST',
		data:jsonArg,
		cache:false,
	}).then(
		function(res){
			//success
			$('#msg_tbl').html(res);
			$.redraw(opciones);
		},
		function(){
			//error
			$('#msg_tbl').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
		}
	);
	$('#seccion_deposito').dialog('close');
}