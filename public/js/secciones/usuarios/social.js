$(document).on('ready',inicio);
function inicio()
{
	lg = '';
	social();
	//$('#conecttw').on('click', twitter);
}
function social()
{
	var facebookUser = {};
	$('.desvincular').on('click', desvincular);
	//------------------------------------------------
	//Iniciada de forma asíncrona por FB.getLoginStatus()
	//------------------------------------------------
	var statusChangeCallback = function (response) {		
		if (response.status === 'connected') {
			//	Login y autorización correctas
			$('#conectfb').html($('<a>').attr({'id':'cnxfb', 'class':'links', 'href':'javascript\:void\(0\)'}).html('conectar'));
			$('#cnxfb').on('click', Login);
			Facebook_correctLogin();

			//$('#conectfb').html($('<a>').attr({'id':'lgoutfb', 'class':'links', 'href':'javascript\:void\(0\)'}).html('salir'))
			//$('#lgoutfb').on('click', Logout);
		}
		else if (response.status === 'not_authorized') {
			//	Login correcto, sin autorización
			$('#conectfb').html($('<a>').attr({'id':'cnxfb', 'class':'links', 'href':'javascript\:void\(0\)'}).html('conectar'));
			$('#cnxfb').on('click', Login);
			Facebook_notAuthorized();
		}
		else {
			$('#conectfb').html($('<a>').attr({'id':'cnxfb', 'class':'links', 'href':'javascript\:void\(0\)'}).html('conectar'));
			$('#cnxfb').on('click', Login);
			//Usuario no conectado a Facebook
			Facebook_notConnected();
		}
	};
	//------------------------------------------------
	//funcion para loguear con la aplicacion facebook, 
	//servira igualmente para la vinculacion con facebook
	//------------------------------------------------
	var Login = function() {
		FB.login(function(response) {
			if (response.authResponse) {
				facebookUser['login'] = 'login';
		    	Facebook_correctLogin();
		    }
		    else{
		    	console.log('User cancelled login or did not fully authorize.');
   			}
   		}, {scope: 'email,user_photos,user_videos'} );
	}
	var Logout = function () {
		FB.logout( function(){
			document.location.reload();
		});
	}

	var getPhoto = function () {
		facebookUser['photo'] = '';
		FB.api('/me/picture?type=normal', function(response) {
			facebookUser['photo'] = response.data.url;
		});
	}
	//------------------------------------------------
	//Función a llamar cuando el login se realiza correctamente
	//------------------------------------------------
	var Facebook_correctLogin	= function () {
		//	Obtener los datos del usuario
		getPhoto();
		FB.api('/me', {fields: 'id,name,email'}, function(response) {
			// Guardar los datos en una variable global
			//console.log(facebookUser);
			facebookUser['id'] = response.id;
			facebookUser['name'] = response.name;
			facebookUser['email'] = response.email;

			
			lg = '#lg'+response.id;
			//console.log(lg);
			saveperfil();
			//$('#conectfb').html($('<a>').attr({'id':'lgoutfb', 'class':'links', 'href':'javascript\:void\(0\)'}).html('salir'))
			//$('#lgoutfb').on('click', Logout);
		})
	};
	//------------------------------------------------
	//funcion encargada de verificar la informacion
	// de la conexion en la aplicacion web
	//------------------------------------------------
	var saveperfil = function() {
		//console.log(facebookUser);
		facebookUser['folio_user'] = $('#frmuser').data('folio');
		facebookUser['tipo'] = 1;//facebook
		$.ajax({
			url:_url_+'usuarios/savesocial/',
			type:'POST',
			data:facebookUser,
			cache:false
		}).then(
			function(res){
				//success
				//$('#msgemp').html(res);
				getRedes();
			},
			function(){
				//error
				$('#msnres').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
			}
		);
	}
	function desvincular(data) {
		//console.log(facebookUser);
		var idsocial = data.currentTarget.dataset.id;
		var tipo = data.currentTarget.dataset.tipo;
		$.ajax({
			url:_url_+'usuarios/desvincular/',
			type:'POST',
			data:{idsocial:idsocial},
			cache:false
		}).then(
			function(res){
				//success
				//$('#msgemp').html(res);
				getRedes();
			},
			function(){
				//error
				$('#msnres').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
			}
		);
	}

	var getRedes = function() {
		var iduser = $('#frmuser').data('ide');
		$.ajax({
			url:_url_+'usuarios/getRedes/',
			type:'POST',
			data:{id:iduser},
			cache:false
		}).then(
			function(res){
				//success
				$('#red_social').html(res);
				if(lg!=''){
					$(lg).html($('<a>').attr({'id':'lgoutfb', 'class':'links', 'href':'javascript\:void\(0\)'}).html('salir'));
					$('#lgoutfb').on('click', Logout);
				}
				$('.desvincular').on('click', desvincular);
			},
			function(){
				//error
				$('#msnres').html($('<div>').attr({class:'error'}).html('Ha ocurrido un error'));
			}
		).progress(
			$('#red_social').html('<div><img src="'+_url_+'public/img/plantilla/loader.gif"/></div>')
		);
	}
	//------------------------------------------------
	// Función a llamar cuando la persona esta conectada a Facebook, 
	// pero no a tu aplicación
	//------------------------------------------------
	var Facebook_notAuthorized	= function () {
		console.log('Es necesario conectarse a la aplicación');
		/*
		if (document.getElementById('fbStatus')){
			document.getElementById('fbStatus').innerHTML = 'Es necesario conectarse a la aplicación.'
		}
		*/
	};
	//	Función a llamar si la persona no esta conectada a Facebook
	var Facebook_notConnected	= function () {
		console.log('Es necesario estar conectado a Facebook');
		/*
		if (document.getElementById('fbStatus')){
			document.getElementById('fbStatus').innerHTML = 'Es necesario estar conectado a Facebook.'
		}
		*/
	};
	//------------------------------------------------
	//Iniciar la SDK de Facebook de forma asíncrona
	//------------------------------------------------
	window.fbAsyncInit	= function() {
		//	Ajuste de opciones
		FB.init({
			appId : $('#fbinfo').data('fbid'),
			cookie : true,
			xfbml : true,
			version : $('#fbinfo').data('fbver')
		});
		//	Llamar statusChangeCallback() al iniciar sesión
		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		})
	};
	//------------------------------------------------
	//Cargar la SDK de Facebook de foma asíncrona
	//------------------------------------------------
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs)
	}(document, 'script', 'facebook-jssdk'))
}