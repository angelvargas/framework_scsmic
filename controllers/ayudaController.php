<?php
	class ayudaController extends Controller{
		public function __construct(){
			parent::__construct();
			Session::acceso($this->__app, $this->__mod, $this->__sec, $this->__arg);

			//$this->vista->nav=$this->acl->menuPrincipal($this->_controlador);
			//$this->vista->nav2=$this->acl->menuSecundario($this->_controlador,$this->_metodo);
		}
		public function index(){
			$this->redireccionar('principal/index/');
		}
		public function version(){
			$this->vista->title='Version';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('nav','nav2')
			));
			$this->vista->renderizar();
		}
		public function documentacion(){
			$this->vista->title='Documentacion';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'secciones'=>array('ayuda/ayuda')
			));
			$this->vista->setJS(array(
				'secciones'=>array('ayuda/ayuda')
			));
			$this->vista->contenido = 'ayuda:ayuda.html';
			$this->vista->renderizar();
		}
	}
?>