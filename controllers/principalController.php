<?php
	class principalController extends Controller {
		public function __construct() {	
			parent::__construct();
			Session::acceso($this->__app, $this->__mod, $this->__sec, $this->__arg);
			//var_dump($_SESSION);
			$this->modelo = $this->loadModel('principal');
			$this->vista->nombre_session = Session::get('l_nombre');
		}

		public function index() {
			$this->vista->setCss(array(
				'publico'=>array('formulario', 'validationEngine.jquery', 'jqueryuicss/jqueryui', 'jquery.cluetip', 'jquery-ui-timepicker-addon', 'bxslider/jquery.bxslider', 'tablas'),
				'secciones'=>array('pizarron', 'agenda/calendario')
			));
			$this->vista->setJs(array(
				'template'=>array(
					'pluginsearch'
				),
				'publico'=>array(
					'jqueryui',
					'jquery-ui-timepicker-addon',
					'jquery.validationEngine-es',
					'jquery.validationEngine',
					'jquery.bxslider.min',
					'jquery.cluetip'),
				'secciones'=>array(
					'inicio',
					'inicio/solicitud')
			));
			//$this->vista->title='Bienvenido '.Session::get('l_nombre');
			$this->vista->title='Bienvenido '.Session::get('l_nombre');
			$this->vista->contenido = 'inicio:pizarron.html';
			

			$this->vista->renderizar();
		}

		
		public function cerrar(){
			/*include(_LIBRERIAS_.'httpRequestValidate.class.php');
			$curl = new HttpRequestAcuc;
			//$res=$curl->validar($_COOKIE['iauscsmic']);
			*/
			$this->modelo->registrar_log(Session::get('l_nombre'), 'Fin de Sesion');
			Session::destroy();
			//$res=$curl->terminar($_COOKIE['iau']);
			$this->redireccionar();
		}
	}
?>