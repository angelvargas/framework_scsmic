<?php
	//------------------------------------------------------------------------
	//Title.............: controladores de usuarios
	//File..............: usuariosController.php
	//version Sistema...: 7.0.0
	//version...........: 3.0.0
	//Fecha Cambio......: 16-Junio-2015
	//Autor.............: Angel Florentino Vargas Pool
	//Email.............: angelvargaspool@gmail.com
	//desc..............: seccion de empresas
	//------------------------------------------------------------------------
	require _ROOT_."libs/TwitterOAuth/vendor/autoload.php";
	use Abraham\TwitterOAuth\TwitterOAuth;
	class usuariosController extends Controller{
		public function __construct() {
			parent::__construct();
			Session::acceso($this->__app, $this->__mod, $this->__sec, $this->__arg);
			$this->modelo=$this->loadModel('usuarios');
			$this->vista->nombre_session=$_SESSION['l_nombre'];
			$this->vista->fb_app = _FB_APP;
			$this->vista->fb_version = _FB_VERSION_;
		}
		public function index()
		{
			//redirige al principio
			$this->redireccionar('principal/index/');
		}
		//--------------------------------------------------
		//Secciones Corregidas
		//--------------------------------------------------
		public function clientes($pagina = 1, $flag = 0)
		{
			$post = '';
			$this->acl->acceso($this->_metodo);
			$parametros['categorias'] = array(3,4);
			$parametros['campos'] = array('editar', 'password');
			$this->vista->title = 'Lista De Clientes';
			$this->vista->titulo_tabla = 'Lista De Clientes';
			$this->lstUsuarios($pagina, 'usuarios/clientes/', $parametros);
			if ( $this->acl->permiso('agregar_cliente') ){
				$this->vista->btnextra = $this->htmlcreator->getTag( 'a', 'Agregar Cliente', array('class'=>'btnext', 'id'=>'addusuario', 'href'=>_PATH_ABS_.'usuarios/agregar_usuario/' ) );
			}
			
			if($flag==0){
				$this->vista->contenido = 'publico:table.html';
				$this->vista->renderizar();
			}
			else{
				$this->vista->contenido = 'publico:tableajax.html';
				$this->vista->renderizar('blanco');
			}
		}
		public function usuarios($pagina = 1, $flag = 0)
		{
			$post = '';
			$this->acl->acceso($this->_metodo);
			$parametros['categorias'] = array(2);
			$parametros['campos'] = array('editar', 'password');
			$this->vista->title = 'Lista De Usuarios';
			$this->vista->titulo_tabla = 'Lista De Usuarios';
			$this->lstUsuarios($pagina, 'usuarios/usuarios/', $parametros);
			if ( $this->acl->permiso('agregar_usuario') ){
				$this->vista->btnextra = $this->htmlcreator->getTag( 'a', 'Agregar Usuario', array('class'=>'btnext', 'id'=>'addusuario', 'href'=>_PATH_ABS_.'usuarios/agregar_usuario/' ) );
			}
			
			if($flag==0){
				$this->vista->contenido = 'publico:table.html';
				$this->vista->renderizar();
			}
			else{
				$this->vista->contenido = 'publico:tableajax.html';
				$this->vista->renderizar('blanco');
			}
		}
		public function agregar_usuario($flag=0) {
			if ( !$this->acl->permiso('agregar_cliente') && !$this->acl->permiso('agregar_usuario') ){
				echo file_get_contents(_PATH_ABS_.'error/access/401/');
    			exit();
			}
			$base = array('estado'=>0, 'ciudad'=>0, 'colonia'=>0, 'categoria'=>2, 'usuario'=>'', 'laboral'=>'');
			foreach ($base as $key => $value) {
				$$key = $value;
			}
			if(!empty($_POST)){
				$data=array(
					'ide'=>FILTER_SANITIZE_NUMBER_INT,
					'nombre'=>FILTER_SANITIZE_STRING,
					'nbrusr'=>FILTER_SANITIZE_STRING,
					'nbrpwss'=>FILTER_SANITIZE_STRING,
					'sae'=>FILTER_SANITIZE_STRING,
					'nacimiento'=>FILTER_SANITIZE_STRING,
					'direccion'=>FILTER_SANITIZE_STRING,
					'estado'=>FILTER_SANITIZE_NUMBER_INT,
					'ciudad'=>FILTER_SANITIZE_NUMBER_INT,
					'colonia'=>FILTER_SANITIZE_NUMBER_INT,
					'laboral'=>FILTER_SANITIZE_STRING,
					'perfiles'=>array(
						'flags'  => FILTER_REQUIRE_ARRAY,
						'options'=> array(
							'perfil'=>FILTER_SANITIZE_NUMBER_INT,
							'empresa'=>FILTER_SANITIZE_NUMBER_INT,
							'estatus'=>FILTER_SANITIZE_NUMBER_INT
						)
					),
					'contactos'=>array(
						'flags'  => FILTER_REQUIRE_ARRAY,
						'options'=> array(
							'idcontacto'=>FILTER_SANITIZE_NUMBER_INT,
							'valorcont'=>FILTER_SANITIZE_STRING
						)
					),
				);
				$post=filter_input_array(INPUT_POST,$data);
				$resp=$this->modelo->saveusuario($post);//modificar este modelo
				$clase = $resp['clase'];
				$msg = $resp['msg'];
				$msgcont = '';
				if($clase == 'error'){
					foreach ($post as $key => $value){
						$$key = $value;
						$this->vista->$key = $value;
					}
				}
				$this->vista->mensaje = $this->htmlcreator->getTag('div',$msg,array('class'=>$clase));
			}
			if ( $this->acl->permiso('agregar_cliente') ){
				$categoria = 3;
				$this->vista->seccion_usuario_cliente = 'true';
			}
			if ( $this->acl->permiso('agregar_usuario') ){
				$categoria = 2;
				$this->vista->seccion_usuario_scsmic = 'true';
			}
			if($categoria==3)
				$this->vista->chckd_cliente = 'checked';
			elseif($categoria==2)
				$this->vista->chckd_usuario = 'checked';

			$this->vista->title = 'Registro de Usuarios';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('jqueryuicss/jqueryui', 'cssfrm', 'nav', 'nav2', 'validationEngine.jquery', 'mobiscroll')
			));
			$this->vista->setJs(array(
				'publico'=>array('jqueryui', 'datepicker-es', 'jquery.validationEngine-es', 'jquery.validationEngine', 'mobiscroll'),
				'secciones'=>array('usuarios/frmusuario')
			));

			$this->vista->titulo_frm = 'Registro de Usuarios/Clientes/Afiliados';
			$this->vista->estado = $estado;
			$this->vista->ciudad = $ciudad;
			$this->vista->colonia = $colonia;
			$this->vista->activar_agregar_usuario = 'activo';
			//-------------------------------------------------------------------------------------
			//Listado De Empresas
			//-------------------------------------------------------------------------------------
			$dataEmpresa = $this->modelo->lstempresas();
			$this->vista->lstempresas = $this->htmlcreator->getOptions($dataEmpresa,0,'Selecciona Empresa');
			//-------------------------------------------------------------------------------------
			//ESTADOS
			//-------------------------------------------------------------------------------------
			$dataestados = $this->modelo->getResidencia(1);
			$this->vista->lstestados = $this->htmlcreator->getOptions($dataestados,$estado,'Selecciona Estado');
			//-------------------------------------------------------------------------------------
			//CIUDADES / MUNICIPIOS
			//-------------------------------------------------------------------------------------
			$dataciudades = $this->modelo->getResidencia(2,$estado);
			$this->vista->lstciudades = $this->htmlcreator->getOptions($dataciudades,$ciudad,'Selecciona Ciudad');
			//-------------------------------------------------------------------------------------
			//COLONIAS
			//-------------------------------------------------------------------------------------
			$datacolonias = $this->modelo->getResidencia(3,$ciudad);
			$this->vista->lstcolonias = $this->htmlcreator->getOptions($datacolonias,$colonia,'Selecciona Colonia');
			//-------------------------------------------------------------------------------------
			//TIPOS-PERFILES
			//-------------------------------------------------------------------------------------
			$tipos = $this->modelo->perfiles($categoria);
			$this->vista->tipoperfil = $this->htmlcreator->getOptions($tipos,0,'Selecciona Perfil');
			//-------------------------------------------------------------------------------------
			//TIPOS DE ESTATUS
			//-------------------------------------------------------------------------------------
			$estarray[] = array('id'=>1,'nombre'=>'Activo');
			$estarray[] = array('id'=>2,'nombre'=>'Inactivo');
			$this->vista->estatusopt=$this->htmlcreator->getOptions($estarray,1);
			$tipos_contacto = $this->modelo->getTiposContacto();
			$this->vista->lsttipocontacto = $this->htmlcreator->getOptions($tipos_contacto);
			//------------------------------------------o------------------------------------------
			$this->vista->idusuario = 0;
			//$this->vista->seccion_user_pass ='true';
			if(!empty($usuario))
				$this->vista->chkuser = 'checked';
			if(!empty($laboral))
				$this->vista->chklaboral = 'checked';
			$this->vista->seccion_perfil = 'true';
			$this->vista->editperfil = 0;
			$this->vista->btncontacto = 'addcontacto';
			$this->vista->btnperfil = 'addperfil';
			//------------------------------------------o------------------------------------------
			if($flag==0) {
				$this->vista->contenido='publico:frmHtml.html';
				$this->vista->form='users:frmusuarios.html';
				$this->vista->renderizar();
			}
			else {
				$this->vista->contenido='users:frmusuarios.html';
				$this->vista->renderizar('blanco');	
			}
		}
		public function getperfil()
		{
			$datos=array(
				'tipo'=>FILTER_SANITIZE_NUMBER_INT,
				'tiposel'=>FILTER_SANITIZE_NUMBER_INT
			);
			$post = filter_input_array(INPUT_POST,$datos);
			$resultado = $this->modelo->perfiles($post['tipo']);
			$this->vista->contenido = $this->htmlcreator->getOptions($resultado,$post['tiposel'],'Selecciona Perfil');
			$this->vista->renderizar('blanco');
		}
		//-------------------------------------------------------------------------------------------
		//SECCION DE EMPRESAS PARA LA SECCION DE REGISTRO Y MODIFICACION
		//-------------------------------------------------------------------------------------------
		public function agr_empresa()
		{
			$msgaviso = '';
			if (!empty($_POST)) {
				$data=array(
					'nombre'=>FILTER_SANITIZE_STRING,
					'tipo'=>FILTER_SANITIZE_NUMBER_INT
				);
				$post = filter_input_array(INPUT_POST, $data);
				$resp = $this->modelo->agregar_empresa($post);
				$msgaviso = $this->htmlcreator->getTag('div', $resp['msg'], array('class'=>$resp['clase']));
			}
			$this->vista->contenido = $msgaviso;
			$this->vista->renderizar('blanco');
		}
		public function empresas() {
			$lstempresas = array();
			if (!empty($_POST)) {
				$data=array(
					'tipo'=>FILTER_SANITIZE_NUMBER_INT
				);
				$post = filter_input_array(INPUT_POST, $data);
				$lstempresas = $this->modelo->lstempresas($post['tipo']);
			}
			$this->vista->contenido = $this->htmlcreator->getOptions($lstempresas,0,'selecciona Empresa');
			$this->vista->renderizar('blanco');
		}
		//-------------------------------------------------------------------------------------------
		//SECCION DE RESIDENCIAS(ESTADO,CIUDAD,COLONIA) PARA LA SECCION DE REGISTRO Y MODIFICACION
		//-------------------------------------------------------------------------------------------
		public function getResidencia()
		{
			$data=array(
				'tipo'=>FILTER_SANITIZE_STRING,
				'selected'=>FILTER_SANITIZE_NUMBER_INT,
				'id'=>FILTER_SANITIZE_NUMBER_INT
			);
			$post = filter_input_array(INPUT_POST,$data);
			$tipo = $post['tipo'];//tipo que se actualizara
			$idsel = $post['id'];//valor de opt seleccionado
			$id = $post['selected'];
			if($tipo == 'estado') {
				//lista de estados ajax
				$dataestados=$this->modelo->getResidencia(1);
				$this->vista->contenido=$this->htmlcreator->getOptions($dataestados, $id, 'Selecciona Estado');
			}
			elseif($tipo=='ciudad') {
				//lista de ciudades ajax
				$dataestados=$this->modelo->getResidencia(2,$idsel);
				//selc
				$this->vista->contenido=$this->htmlcreator->getOptions($dataestados, $id, 'Selecciona Ciudad');
			}
			elseif ($tipo=='colonia') {
				//lista de colonias ajax
				$dataestados=$this->modelo->getResidencia(3,$idsel);
				//selcol
				$this->vista->contenido=$this->htmlcreator->getOptions($dataestados, $id, 'Selecciona Colonia');
			}
			else {
				//opt default
				$this->vista->contenido=$this->htmlcreator->getTag('option', 'No existen datos Disponibles',array('value'=>'0'));
			}
			$this->vista->renderizar('blanco');
		}
		public function agr_residencia()
		{
			$msgaviso = '';
			if (!empty($_POST)) {
				
				$data=array(
					'nombre'=>FILTER_SANITIZE_STRING,
					'tipo'=>FILTER_SANITIZE_STRING,
					'base'=>FILTER_SANITIZE_NUMBER_INT
				);
				//base es el id de estado, o ciudad segun corresponda, que es la base
				//Es decir la base de una ciudad es un estado, y la base de una colonia es una ciudad 
				$post = filter_input_array(INPUT_POST,$data);
				switch ($post['tipo']) {
					case 'estado':
						$resp = $this->modelo->agregarEstado($post);
						break;
					case 'ciudad':
						$resp = $this->modelo->agregarCiudad($post);
						break;
					case 'colonia':
						$resp = $this->modelo->agregarColonia($post);	
						break;
					default:
						$resp = $this->modelo->agregarEstado($post);
						break;
				}
				$msgaviso = $this->htmlcreator->getTag('div', $resp['msg'], array('class'=>$resp['clase']));
			}
			$this->vista->contenido = $msgaviso;
			$this->vista->renderizar('blanco');
		}
		//-------------------------------------------------------------------------------------------
		//MODIFICAR USUARIOS
		//-------------------------------------------------------------------------------------------
		public function modificar_usuario($folio,$flag=0)
		{
			$id =$this->modelo->getIdUser($folio);
			$this->getParam($id,'usuarios/usuarios/');
			$this->acl->acceso($this->_metodo);
			$base = array('estado'=>0, 'ciudad'=>0, 'colonia'=>0, 'categoria'=>2);
			foreach ($base as $key => $value) {
				$$key = $value;
			}
			if(!empty($_POST)){
				$data=array(
					'ide'=>FILTER_SANITIZE_NUMBER_INT,
					'nombre'=>FILTER_SANITIZE_STRING,
					'sae'=>FILTER_SANITIZE_STRING,
					'nacimiento'=>FILTER_SANITIZE_STRING,
					'direccion'=>FILTER_SANITIZE_STRING,
					'estado'=>FILTER_SANITIZE_NUMBER_INT,
					'ciudad'=>FILTER_SANITIZE_NUMBER_INT,
					'colonia'=>FILTER_SANITIZE_NUMBER_INT,
					'laboral'=>FILTER_SANITIZE_STRING,
				);
				$post=filter_input_array(INPUT_POST,$data);
				$resp=$this->modelo->saveusuario($post);//modificar este modelo
				$clase = $resp['clase'];
				$msg = $resp['msg'];
				$this->vista->mensaje = $this->htmlcreator->getTag('div',$msg,array('class'=>$clase));
			}
			$datuser=$this->modelo->dataUser($id);
			foreach ($datuser as $key => $value) {
				$$key = $value;
				$this->vista->$key = $value;
			}
			//-------------------------------------------------------------------------------------
			$ali=$this->htmlcreator->getTag('a','Modificar Usuario');
			$this->vista->navextra=$this->htmlcreator->getTag('li',$ali,array('class'=>'activo'));
			$this->vista->title = 'Modificar de Usuario';
			//-------------------------------------------------------------------------------------
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('jqueryuicss/jqueryui', 'cssfrm', 'nav', 'nav2', 'validationEngine.jquery', 'mobiscroll')
			));
			$this->vista->setJs(array(
				'publico'=>array('jqueryui', 'datepicker-es', 'jquery.validationEngine-es', 'jquery.validationEngine', 'mobiscroll'),
				'secciones'=>array('usuarios/frmusuario')
			));
			//-------------------------------------------------------------------------------------
			if ( $this->acl->permiso('agregar_cliente') ){
				$categoria = 3;
				$this->vista->seccion_usuario_cliente = 'true';
			}
			if ( $this->acl->permiso('agregar_usuario') ){
				$categoria = 2;
				$this->vista->seccion_usuario_scsmic = 'true';
			}
			if($categoria==3)
				$this->vista->chckd_cliente = 'checked';
			elseif($categoria==2)
				$this->vista->chckd_usuario = 'checked';

			$this->vista->titulo_frm = 'Modificación de Usuarios/Clientes/Afiliados';
			$this->vista->estado = $estado;
			$this->vista->ciudad = $ciudad;
			$this->vista->colonia = $colonia;
			if($laboral!='0000-00-00')
				$this->vista->chklaboral = 'checked';
			
			$this->vista->seccion_perfil = 'true';
			$this->vista->editperfil = 0;
			$this->vista->btncontacto = 'savecontacto';
			$this->vista->btnperfil = 'saveperfil';
			$this->vista->folio = $folio;
			$this->vista->idusuario = $id;
			//-------------------------------------------------------------------------------------
			//Listado De Empresas
			//-------------------------------------------------------------------------------------
			$dataEmpresa = $this->modelo->lstempresas();
			$this->vista->lstempresas = $this->htmlcreator->getOptions($dataEmpresa,0,'Selecciona Empresa');
			//-------------------------------------------------------------------------------------
			//ESTADOS
			//-------------------------------------------------------------------------------------
			$dataestados = $this->modelo->getResidencia(1);
			$this->vista->lstestados = $this->htmlcreator->getOptions($dataestados,$estado,'Selecciona Estado');
			//-------------------------------------------------------------------------------------
			//CIUDADES / MUNICIPIOS
			//-------------------------------------------------------------------------------------
			$dataciudades = $this->modelo->getResidencia(2,$estado);
			$this->vista->lstciudades = $this->htmlcreator->getOptions($dataciudades,$ciudad,'Selecciona Ciudad');
			//-------------------------------------------------------------------------------------
			//COLONIAS
			//-------------------------------------------------------------------------------------
			$datacolonias = $this->modelo->getResidencia(3,$ciudad);
			$this->vista->lstcolonias = $this->htmlcreator->getOptions($datacolonias,$colonia,'Selecciona Colonia');
			//-------------------------------------------------------------------------------------
			//TIPOS-PERFILES
			//-------------------------------------------------------------------------------------
			$tipos = $this->modelo->perfiles($categoria);
			$this->vista->tipoperfil = $this->htmlcreator->getOptions($tipos,0,'Selecciona Perfil');
			//-------------------------------------------------------------------------------------
			//TIPOS DE ESTATUS
			//-------------------------------------------------------------------------------------
			$estarray[] = array('id'=>1,'nombre'=>'Activo');
			$estarray[] = array('id'=>2,'nombre'=>'Inactivo');
			$this->vista->estatusopt=$this->htmlcreator->getOptions($estarray,1);
			$tipos_contacto = $this->modelo->getTiposContacto();
			$this->vista->lsttipocontacto = $this->htmlcreator->getOptions($tipos_contacto);
			//------------------------------------------o------------------------------------------
			$this->vista->lstcontactos = $this->htmlcontacto($id);
			//------------------------------------------o------------------------------------------
			$this->vista->lsttiposuser = $this->htmlperfil($id);
			//------------------------------------------o------------------------------------------
			if($flag==0) {
				$this->vista->contenido='publico:frmHtml.html';
				$this->vista->form='users:frmusuarios.html';
				$this->vista->renderizar();
			}
			else {
				$this->vista->contenido='users:frmusuarios.html';
				$this->vista->renderizar('blanco');	
			}
		}
		//-------------------------------------------------------------------------------------------
		//Datos Personales
		//-------------------------------------------------------------------------------------------
		public function datospersonales($id=false)
		{
			if($id){
				$datosUser=$this->modelo->dataUser($id);
				$this->htmlcreator->unset = array('colonia','ciudad','estado');
				$this->vista->contenido = $this->htmlcreator->getTablaVertical($datosUser,'',array('class'=>'tablas'));
				$datos_contacto = $this->modelo->getContactos($id);
				$this->vista->contenido.= $this->htmlcreator->getTabla($datos_contacto,'',array('class'=>'tablas'));
			}
			$this->vista->renderizar('blanco');
		}
		//-------------------------------------------------------------------------------------------
		//Seccion para editar password
		//-------------------------------------------------------------------------------------------
		public function editpass($id, $flag = 0)
		{
			$this->getParam($id,'usuarios/usuarios/','string');
			$this->acl->acceso('pass_user');
			if(!empty($_POST)) {
				$data=array(
					'newpass'=>FILTER_SANITIZE_STRING,
					'usuario'=>FILTER_SANITIZE_STRING
				);
				$post=filter_input_array(INPUT_POST,$data);
				$resp = $this->modelo->newPass($post,$id);
				$msgaviso=$this->htmlcreator->getTag('div',$resp['msg'],array('class'=>$resp['clase']));
				$this->vista->mensaje = $msgaviso;
			}
			$ali=$this->htmlcreator->getTag('a','Modificar Password');
			$this->vista->navextra=$this->htmlcreator->getTag('li',$ali,array('class'=>'activo'));
			$this->vista->title='Cambio de Contraseña';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('cssfrm','nav','nav2','validationEngine.jquery')
			));
			$this->vista->setJs(array(
				'publico'=>array('jquery.validationEngine-es','jquery.validationEngine','jQuery.dPassword'),
				'secciones'=>array('usuarios/chgpass')
			));
			$this->vista->titulo_frm='Cambio de Usuario Y Contraseña';
			$this->vista->attrform=$this->htmlcreator->attributes(array('id'=>'frmusuario','name'=>'frmusuario', 'data-ide'=>$id));
			$this->vista->nombre = $this->modelo->getNombreUsuario($id);
			$this->vista->usuario = $this->modelo->getUser($id);
			$this->vista->changepass = 'editpass';
			$this->vista->contenido = 'users:frmchgpassuser.html';
			if($flag==0){
				$this->vista->renderizar();
			}
			else{
				$this->vista->renderizar('blanco');	
			}
		}
		public function password($flag=0)
		{
			$id_usuario=Session::get('l_folio');
			if(!empty($_POST)) {
				$data=array(
					'newpass'=>FILTER_SANITIZE_STRING,
					'usuario'=>FILTER_SANITIZE_STRING
				);
				$post=filter_input_array(INPUT_POST,$data);
				$resp=$this->modelo->newPass($post,$id_usuario);
				$msgaviso=$this->htmlcreator->getTag('div',$resp['msg'],array('class'=>$resp['clase']));
				$this->vista->mensaje=$msgaviso;
			}
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('cssfrm','nav','nav2','validationEngine.jquery')
			));
			$this->vista->setJs(array(
				'publico'=>array('jquery.validationEngine-es','jquery.validationEngine','jQuery.dPassword'),
				'secciones'=>array('usuarios/chgpass')
			));
			$ali=$this->htmlcreator->getTag('a','Cambiar Contraseña');
			$this->vista->navextra=$this->htmlcreator->getTag('li',$ali,array('class'=>'activo'));
			$this->vista->title='Cambio de Contraseña';
			$this->vista->attrform=$this->htmlcreator->attributes(array('id'=>'frmusuario','name'=>'frmusuario'));
			$this->vista->titulo_frm='Cambio de Contraseña';

			$this->vista->nombre = $this->modelo->getNombreUsuario($id_usuario);
			$this->vista->usuario = $this->modelo->getUser($id_usuario);
			$this->vista->contenido = 'users:frmchgpassuser.html';
			$this->vista->changepass='changepass';
			
			if($flag==0)
				$this->vista->renderizar();
			else
				$this->vista->renderizar('blanco');
		}
		public function perfil($flag=0)
		{
			$base = array('estado'=>0, 'ciudad'=>0, 'colonia'=>0, 'categoria'=>2);
			foreach ($base as $key => $value) {
				$$key = $value;
			}
			$id = Session::get('l_id');
			if(!empty($_POST)){
				$data=array(
					'ide'=>FILTER_SANITIZE_NUMBER_INT,
					'nombre'=>FILTER_SANITIZE_STRING,
					'nacimiento'=>FILTER_SANITIZE_STRING,
					'direccion'=>FILTER_SANITIZE_STRING,
					'estado'=>FILTER_SANITIZE_NUMBER_INT,
					'ciudad'=>FILTER_SANITIZE_NUMBER_INT,
					'colonia'=>FILTER_SANITIZE_NUMBER_INT
				);
				$post=filter_input_array(INPUT_POST,$data);
				//$resp=$this->modelo->saveusuario($post);//modificar este modelo
				$resp = $this->modelo->editPerfil($post);
				$clase = $resp['clase'];
				$msg = $resp['msg'];
				$this->vista->mensaje = $this->htmlcreator->getTag('div',$msg,array('class'=>$clase));
			}
			$datuser=$this->modelo->dataUser($id);
			foreach ($datuser as $key => $value) {
				$$key = $value;
				$this->vista->$key = $value;
			}
			//-------------------------------------------------------------------------------------
			$ali=$this->htmlcreator->getTag('a','Modificar Perfil');
			$this->vista->navextra=$this->htmlcreator->getTag('li',$ali,array('class'=>'activo'));
			$this->vista->title = 'Modificar Perfil';
			//-------------------------------------------------------------------------------------
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('jqueryuicss/jqueryui', 'cssfrm', 'nav', 'nav2', 'validationEngine.jquery', 'mobiscroll'),
				'secciones'=>array('usuarios/social')
			));
			$this->vista->setJs(array(
				'publico'=>array('jqueryui', 'datepicker-es', 'jquery.validationEngine-es', 'jquery.validationEngine', 'mobiscroll'),
				'secciones'=>array('usuarios/frmusuario', 'usuarios/social')
			));
			//-------------------------------------------------------------------------------------
			$this->vista->titulo_frm = 'Modificación de Perfil';
			$this->vista->estado = $estado;
			$this->vista->ciudad = $ciudad;
			$this->vista->colonia = $colonia;
			if($laboral!='0000-00-00')
				$this->vista->chklaboral = 'checked';
			//-------------------------------------------------------------------------------------
			//Listado De Empresas
			//-------------------------------------------------------------------------------------
			$dataEmpresa = $this->modelo->lstempresas();
			$this->vista->lstempresas = $this->htmlcreator->getOptions($dataEmpresa,0,'Selecciona Empresa');
			//-------------------------------------------------------------------------------------
			//ESTADOS
			//-------------------------------------------------------------------------------------
			$dataestados = $this->modelo->getResidencia(1);
			$this->vista->lstestados = $this->htmlcreator->getOptions($dataestados,$estado,'Selecciona Estado');
			//-------------------------------------------------------------------------------------
			//CIUDADES / MUNICIPIOS
			//-------------------------------------------------------------------------------------
			$dataciudades = $this->modelo->getResidencia(2,$estado);
			$this->vista->lstciudades = $this->htmlcreator->getOptions($dataciudades,$ciudad,'Selecciona Ciudad');
			//-------------------------------------------------------------------------------------
			//COLONIAS
			//-------------------------------------------------------------------------------------
			$datacolonias = $this->modelo->getResidencia(3,$ciudad);
			$this->vista->lstcolonias = $this->htmlcreator->getOptions($datacolonias,$colonia,'Selecciona Colonia');
			//-------------------------------------------------------------------------------------
			//TIPOS-PERFILES
			//-------------------------------------------------------------------------------------
			$tipos = $this->modelo->perfiles($categoria);
			$this->vista->tipoperfil = $this->htmlcreator->getOptions($tipos,0,'Selecciona Perfil');
			//-------------------------------------------------------------------------------------
			//TIPOS DE ESTATUS
			//-------------------------------------------------------------------------------------
			$estarray[] = array('id'=>1,'nombre'=>'Activo');
			$estarray[] = array('id'=>2,'nombre'=>'Inactivo');
			$this->vista->estatusopt=$this->htmlcreator->getOptions($estarray,1);
			$tipos_contacto = $this->modelo->getTiposContacto();
			$this->vista->lsttipocontacto = $this->htmlcreator->getOptions($tipos_contacto);
			//------------------------------------------o------------------------------------------
			$this->vista->idusuario = $id;
			//------------------------------------------o------------------------------------------
			//------------------------------------------o------------------------------------------
			$this->vista->lstcontactos = $this->htmlcontacto($id);
			//------------------------------------------o------------------------------------------
			$this->vista->lsttiposuser = $this->htmlperfil($id,2);
			//------------------------------------------o------------------------------------------
			$this->vista->editperfil = 1;
			$this->vista->btncontacto = 'savecontacto';
			$this->vista->folio = Session::get('l_folio');
			//------------------------------------------o------------------------------------------

			$this->vista->seccion_usuario_social = 'true';
			$this->vista->frmsocial = 'users:frmsocial.html';
			//------------------------------------------o------------------------------------------
			$this->vista->red_social = $this->getRedesData($id);
			//------------------------------------------o------------------------------------------
			if( isset( $_SESSION['access_token'] ) ) {
				$access_token = $_SESSION['access_token'];
				$connection = new TwitterOAuth( _TW_CKEY_, _TW_CSEC_, $access_token['oauth_token'], $access_token['oauth_token_secret'] );
				$user = $connection->get("account/verify_credentials");
			}
			if($flag==0) {
				$this->vista->contenido='publico:frmHtml.html';
				$this->vista->form='users:frmusuarios.html';
				$this->vista->renderizar();
			}
			else {
				$this->vista->contenido='users:frmusuarios.html';
				$this->vista->renderizar('blanco');	
			}
		}
		private function getRedesData($id)
		{
			$redes = $this->modelo->getRedes($id);
			$htmlredes = '';
			foreach ( $redes as $key => $value ) {
				$getcont = $this->vista->getContent('users:frmredsocial.html');
				$getcont = str_replace('{{idsocial}}', $value['social_id'], $getcont);
				$getcont = str_replace('{{imagen_social}}', $value['social_img'], $getcont);
				$getcont = str_replace('{{social_usuario}}', $value['social_usuario'], $getcont);
				$getcont = str_replace('{{id}}', $value['id'], $getcont);
				$getcont = str_replace('{{social_email}}', $value['social_email'], $getcont);
				$getcont = str_replace('{{logout}}', $value['social_id'], $getcont);
				$getcont = str_replace('{{tipo}}', $value['tipo'], $getcont);
				$htmlredes.=$getcont;
			}
			return $htmlredes;
		}
		public function getredes()
		{
			if( isset($_POST) ) {
				$datos = array(
					'id'=>FILTER_SANITIZE_NUMBER_INT
				);
				$posts = filter_input_array(INPUT_POST,$datos);
				$this->vista->contenido =  $this->getRedesData( $posts['id'] );
			}
			$this->vista->renderizar('blanco');
		}
		//-------------------------------------------------------------------------------------------
		//Seccion de empresas
		//-------------------------------------------------------------------------------------------
		public function empresa($pagina = 1, $flag = 0)
		{
			$this->acl->acceso($this->_metodo);
			$parametros['campos'] = array('modificar');
			$this->vista->title = 'Lista De Empresas';
			$this->vista->titulo_tabla = 'Lista De Empresas';
			$url = 'usuarios/empresa/';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('tablas','cssfrm','nav','nav2','validationEngine.jquery','jqueryuicss/jqueryui','jquery-ui-timepicker-addon','jquery.cluetip')
			));
	    	$this->vista->setJs(array(
	    		'template'=>array('pluginsearch'),
				'publico'=>array('jquery.cluetip','jqueryui'),
				'secciones'=>array('empresa/empresa')
			));
			parent::listempresas($pagina, $flag, $url, $parametros);
		}
		public function agregar_empresa($flag = 0)
		{
			$base = array('estado'=>0, 'ciudad'=>0, 'colonia'=>0, 'ide'=>'0');
			foreach ($base as $key => $value) {
				$$key = $value;
			}
			if(!empty($_POST)){
				$data=array(
					'ide'=>FILTER_SANITIZE_NUMBER_INT,
					'nombreempresa'=>FILTER_SANITIZE_STRING,
					'razonsocial'=>FILTER_SANITIZE_STRING,
					'rfc'=>FILTER_SANITIZE_STRING,
					'descripcion'=>FILTER_SANITIZE_STRING,
					'direccion'=>FILTER_SANITIZE_STRING,
					'estado'=>FILTER_SANITIZE_NUMBER_INT,
					'ciudad'=>FILTER_SANITIZE_NUMBER_INT,
					'colonia'=>FILTER_SANITIZE_NUMBER_INT,
					'tipo'=>FILTER_SANITIZE_NUMBER_INT,
					'logo'=>FILTER_SANITIZE_STRING,
					'sae'=>FILTER_SANITIZE_STRING,
					'contactos'=>array(
						'flags'  => FILTER_REQUIRE_ARRAY,
						'options'=> array(
							'idcontacto'=>FILTER_SANITIZE_NUMBER_INT,
							'valorcont'=>FILTER_SANITIZE_STRING
						)
					),
				);
				$post=filter_input_array(INPUT_POST,$data);
				$resp=$this->modelo->saveempresa($post);//modificar este modelo
				$clase = $resp['clase'];
				$msg = $resp['msg'];
				$msgcont = '';
				if($clase == 'error'){
					foreach ($post as $key => $value){
						$$key = $value;
						$this->vista->$key = $value;
					}
				}
				$this->vista->mensaje = $this->htmlcreator->getTag('div',$msg,array('class'=>$clase));
			}

			$this->vista->estado = $estado;
			$this->vista->ciudad = $ciudad;
			$this->vista->colonia = $colonia;
			$this->vista->chkemp1 = 'checked';
			$this->vista->idempresa = 0;

			$this->acl->acceso($this->_metodo);
			$this->vista->title = 'Agregar Empresa';
			$this->vista->titulo_form='Agregar Nueva Empresa';

			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('jqueryuicss/jqueryui', 'cssfrm', 'nav', 'nav2', 'validationEngine.jquery')
			));
			$this->vista->setJs(array(
				'publico'=>array('jqueryui', 'datepicker-es', 'jquery.validationEngine-es', 'jquery.validationEngine'),
				'secciones'=>array('empresa/frmempresa')
			));

			//-------------------------------------------------------------------------------------
			//ESTADOS
			//-------------------------------------------------------------------------------------
			$dataestados = $this->modelo->getResidencia(1);
			$this->vista->lstestados = $this->htmlcreator->getOptions($dataestados,$estado,'Selecciona Estado');
			//-------------------------------------------------------------------------------------
			//CIUDADES / MUNICIPIOS
			//-------------------------------------------------------------------------------------
			$dataciudades = $this->modelo->getResidencia(2,$estado);
			$this->vista->lstciudades = $this->htmlcreator->getOptions($dataciudades,$ciudad,'Selecciona Ciudad');
			//-------------------------------------------------------------------------------------
			//COLONIAS
			//-------------------------------------------------------------------------------------
			$datacolonias = $this->modelo->getResidencia(3,$ciudad);
			$this->vista->lstcolonias = $this->htmlcreator->getOptions($datacolonias,$colonia,'Selecciona Colonia');
			//------------------------------------------o------------------------------------------
			$tipos_contacto = $this->modelo->getTiposContacto();
			$this->vista->lsttipocontacto = $this->htmlcreator->getOptions($tipos_contacto);
			//------------------------------------------o------------------------------------------
			$this->vista->btnval = 'agregar';
			$this->vista->btncontacto = 'addcontacto';
			//------------------------------------------o------------------------------------------
			if($flag==0) {
				$this->vista->contenido='publico:frmHtml.html';
				$this->vista->form='empresa:frmempresa.html';
				$this->vista->renderizar();
			}
			else {
				$this->vista->contenido='empresa:frmempresa.html';
				$this->vista->renderizar('blanco');	
			}
		}
		public function modificar_empresa($empresa, $flag = 0)
		{
			$this->getParam($empresa,'usuarios/usuarios/','string');
			$this->acl->acceso($this->_metodo);
			if(!empty($_POST)){
				$data=array(
					'ide'=>FILTER_SANITIZE_NUMBER_INT,
					'nombreempresa'=>FILTER_SANITIZE_STRING,
					'razonsocial'=>FILTER_SANITIZE_STRING,
					'rfc'=>FILTER_SANITIZE_STRING,
					'descripcion'=>FILTER_SANITIZE_STRING,
					'direccion'=>FILTER_SANITIZE_STRING,
					'estado'=>FILTER_SANITIZE_NUMBER_INT,
					'ciudad'=>FILTER_SANITIZE_NUMBER_INT,
					'colonia'=>FILTER_SANITIZE_NUMBER_INT,
					'tipo'=>FILTER_SANITIZE_NUMBER_INT,
					'logo'=>FILTER_SANITIZE_STRING,
					'sae'=>FILTER_SANITIZE_STRING,
					'contactos'=>array(
						'flags'  => FILTER_REQUIRE_ARRAY,
						'options'=> array(
							'idcontacto'=>FILTER_SANITIZE_NUMBER_INT,
							'valorcont'=>FILTER_SANITIZE_STRING
						)
					),
				);
				$post=filter_input_array(INPUT_POST,$data);
				$resp=$this->modelo->saveempresa($post);//modificar este modelo
				$clase = $resp['clase'];
				$msg = $resp['msg'];
				$this->vista->mensaje = $this->htmlcreator->getTag('div',$msg,array('class'=>$clase));
			}
			$datos_empresa = $this->modelo->datosEmpresa($empresa);
			foreach ($datos_empresa as $key => $value) {
				$this->vista->$key = $value;
				$$key = $value;
			}
			$this->vista->estado = $estado;
			$this->vista->ciudad = $ciudad;
			$this->vista->colonia = $colonia;
			$this->vista->idempresa = $empresa;

			$tipochk = 'chkemp'.$tipo;
			$this->vista->$tipochk = 'checked';
			$this->vista->seccion_contactos_personal = 'true';
			$this->acl->acceso($this->_metodo);
			//-------------------------------------------------------------------------------------
			$ali=$this->htmlcreator->getTag('a','Modificar Empresa');
			$this->vista->navextra=$this->htmlcreator->getTag('li',$ali,array('class'=>'activo'));
			$this->vista->title = 'Modificar Empresa';
			$this->vista->titulo_form = 'Modificar Empresa';
			//-------------------------------------------------------------------------------------
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('jqueryuicss/jqueryui', 'cssfrm', 'nav', 'nav2', 'validationEngine.jquery')
			));
			$this->vista->setJs(array(
				'publico'=>array('jqueryui', 'datepicker-es', 'jquery.validationEngine-es', 'jquery.validationEngine'),
				'secciones'=>array('empresa/frmempresa')
			));
			//-------------------------------------------------------------------------------------
			//ESTADOS
			//-------------------------------------------------------------------------------------
			$dataestados = $this->modelo->getResidencia(1);
			$this->vista->lstestados = $this->htmlcreator->getOptions($dataestados,$estado,'Selecciona Estado');
			//-------------------------------------------------------------------------------------
			//CIUDADES / MUNICIPIOS
			//-------------------------------------------------------------------------------------
			$dataciudades = $this->modelo->getResidencia(2,$estado);
			$this->vista->lstciudades = $this->htmlcreator->getOptions($dataciudades,$ciudad,'Selecciona Ciudad');
			//-------------------------------------------------------------------------------------
			//COLONIAS
			//-------------------------------------------------------------------------------------
			$datacolonias = $this->modelo->getResidencia(3,$ciudad);
			$this->vista->lstcolonias = $this->htmlcreator->getOptions($datacolonias,$colonia,'Selecciona Colonia');
			//------------------------------------------o------------------------------------------
			$tipos_contacto = $this->modelo->getTiposContacto();
			$this->vista->lsttipocontacto = $this->htmlcreator->getOptions($tipos_contacto);
			//------------------------------------------o------------------------------------------
			$this->vista->btnval = 'Modificar';
			$this->vista->btncontacto = 'savecontacto';
			//------------------------------------------o------------------------------------------
			$this->vista->lstcontactos = $this->htmlcontacto($empresa, 2);
			$this->vista->cpersonal = $this->htmlcontacto($empresa, 3);
			//------------------------------------------o------------------------------------------
			if($flag==0) {
				$this->vista->contenido='publico:frmHtml.html';
				$this->vista->form='empresa:frmempresa.html';
				$this->vista->renderizar();
			}
			else {
				$this->vista->contenido='empresa:frmempresa.html';
				$this->vista->renderizar('blanco');	
			}
		}
		public function datosempresa($id=false)
		{
			if($id){
				$datosEmpresa=$this->modelo->datosEmpresa($id);
				$this->htmlcreator->unset = array('colonia','ciudad','estado');
				$this->vista->contenido = $this->htmlcreator->getTablaVertical($datosEmpresa,'',array('class'=>'tablas'));
				$datos_contacto = $this->modelo->getContactosEmpresa($id);
				$this->vista->contenido.= $this->htmlcreator->getTabla($datos_contacto, '', array('class'=>'tablas'));
				$lstclientes = $this->modelo->getUsuariosEmpresa($id);
				$this->vista->contenido.=$this->htmlcreator->getTabla($lstclientes, '', array('class'=>'tablas'));
			}
			$this->vista->renderizar('blanco');
		}
		public function sincronizarempresas()
		{
			$resp = $this->modelo->sincronizarempresas();
			$contenido = $this->htmlcreator->getTag('div',$resp['msg'],array('class'=>$resp['clase']));
			$this->vista->contenido = $contenido;
			$this->vista->renderizar('blanco');
		}
		//-------------------------------------------------------------------------------------------
		//Seccion para agregar nuevos perfiles a un usuario
		//-------------------------------------------------------------------------------------------
		private function htmlperfil($id, $flag=1)
		{
			$lstperfiles = $this->modelo->getPerfiles($id);
			$html = '';
			foreach ($lstperfiles as $key => $value) {
				$getcont = $this->vista->getContent('users:htmlperfil.html');
				$getcont = str_replace('{{perfil}}', $value['perfil'], $getcont);
				$getcont = str_replace('{{empresa}}', $value['empresa'], $getcont);
				$getcont = str_replace('{{estatus}}', $value['nombre_estatus'], $getcont);
				$getcont = str_replace('{{clave}}', $value['folio'], $getcont);
				if( $value['estatus'] == 1){
					$getcont = str_replace('{{name_newest}}', 'Desactivar', $getcont);
					$getcont = str_replace('{{newest}}', '2', $getcont);
				}
				else{
					$getcont = str_replace('{{name_newest}}', 'Activar', $getcont);
					$getcont = str_replace('{{newest}}', '2', $getcont);
				}
				if($flag==1){
					if( $value['estatus'] == 1){
						$name_newest = 'Desactivar';
						$newest = '2';
					}
					else{
						$name_newest = 'Activar';
						$newest = '1';
					}
					$enlace = $this->htmlcreator->getTag('a',$name_newest,array('class'=>'chgest', 'data-clave'=>$value['folio'], 'data-newest'=>$newest, 'href'=>'javascript:void(0)'));
					$link = $this->htmlcreator->getTag('li',$enlace);
					$getcont = str_replace('{{menu_activar}}', $link, $getcont);
				}
				$html.=$getcont;
			}
			$html = $this->htmlcreator->getTag('div',$html,array('class'=>'seccion_datos'));
			return $html;
		}
		public function addperfil()
		{
			$msgaviso = '';
			if(!empty($_POST)){
				$datos=array(
					'idperfil'=>FILTER_SANITIZE_NUMBER_INT,
					'idempresa'=>FILTER_SANITIZE_NUMBER_INT,
					'estatus'=>FILTER_SANITIZE_NUMBER_INT,
					'ide'=>FILTER_SANITIZE_NUMBER_INT
				);
				$posts = filter_input_array(INPUT_POST,$datos);
				$resp = $this->modelo->addperfil($posts);
				$msgaviso = $this->htmlcreator->getTag('div',$resp['msg'],array('class'=>$resp['clase']));
			}
			$this->vista->contenido = $msgaviso;
			$this->vista->renderizar('blanco');
		}
		public function getperfiles()
		{
			$perfiles = '';
			if(!empty($_POST)){
				$datos=array(
					'ide'=>FILTER_SANITIZE_NUMBER_INT
				);
				$posts = filter_input_array(INPUT_POST,$datos);
				$perfiles = $this->htmlperfil($posts['ide']);
			}
			$this->vista->contenido = $perfiles;
			$this->vista->renderizar('blanco');
		}
		public function chgest()
		{
			$msgaviso = '';
			if(!empty($_POST)){
				$datos=array(
					'clave'=>FILTER_SANITIZE_STRING,
					'est'=>FILTER_SANITIZE_NUMBER_INT
				);
				$posts = filter_input_array(INPUT_POST,$datos);
				$resp = $this->modelo->chgest($posts);
				$msgaviso = $this->htmlcreator->getTag('div',$resp['msg'],array('class'=>$resp['clase']));
			}
			$this->vista->contenido = $msgaviso;
			$this->vista->renderizar('blanco');
		}
		//-------------------------------------------------------------------------------------------
		//Seccion para agregar nuevos datos de contacto de usuario
		//-------------------------------------------------------------------------------------------
		private function htmlcontacto($id, $tipo=1)
		{
			if($tipo==1)
				$lstcontactos = $this->modelo->getContactos($id);
			elseif($tipo==3)
				$lstcontactos = $this->modelo->getContactoEmpresaUsuarios($id);
			else
				$lstcontactos = $this->modelo->getContactosEmpresa($id);
			$html = '';
			foreach ($lstcontactos as $key => $value) {
				if ($tipo==3) {
					$getcont = $this->vista->getContent('users:htmlcontactousuarios.html');
					$getcont = str_replace('{{nombre}}', $value['nombre'], $getcont);
				}
				else {
					$getcont = $this->vista->getContent('users:htmlcontacto.html');
				}
				$getcont = str_replace('{{tipo}}', $value['tipo'], $getcont);
				$getcont = str_replace('{{valor}}', $value['valor'], $getcont);
				$getcont = str_replace('{{clave}}', $value['folio'], $getcont);
				$html.=$getcont;
			}
			$html = $this->htmlcreator->getTag('div',$html,array('class'=>'seccion_datos'));
			return $html;
		}
		public function addcontacto()
		{
			$msgaviso = '';
			if(!empty($_POST)){
				$datos=array(
					'valor'=>FILTER_SANITIZE_STRING,
					'tipo'=>FILTER_SANITIZE_NUMBER_INT,
					'ide'=>FILTER_SANITIZE_STRING,
					'tipocontacto'=>FILTER_SANITIZE_NUMBER_INT
				);
				$posts = filter_input_array(INPUT_POST,$datos);
				$resp = $this->modelo->addcontacto($posts);
				$msgaviso = $this->htmlcreator->getTag('div',$resp['msg'],array('class'=>$resp['clase']));
			}
			$this->vista->contenido = $msgaviso;
			$this->vista->renderizar('blanco');
		}
		public function getcontactos()
		{
			$contactos = '';
			if(!empty($_POST)){
				$datos=array(
					'ide'=>FILTER_SANITIZE_STRING,
					'tipo'=>FILTER_SANITIZE_NUMBER_INT
				);
				$posts = filter_input_array(INPUT_POST,$datos);
				$contactos = $this->htmlcontacto($posts['ide'], $posts['tipo']);
			}
			$this->vista->contenido = $contactos;
			$this->vista->renderizar('blanco');
		}
		public function delcontacto()
		{
			$msgaviso = '';
			if(!empty($_POST)){
				$datos=array(
					'clave'=>FILTER_SANITIZE_STRING,
					'tipo'=>FILTER_SANITIZE_NUMBER_INT,
					'ide'=>FILTER_SANITIZE_STRING,
				);
				$posts = filter_input_array(INPUT_POST,$datos);
				$tipo = $posts['tipo'];
				$ide = $posts['ide'];
				$ejecutar = false;
				if ( $this->acl->permiso('delcontacto') && $tipo==1 ){
					$ejecutar = true;
				}
				if( $this->acl->permiso('delcontactoempresa') && $tipo==2){
					$ejecutar = true;
				}
				if($ide==Session::get('l_folio') ){
					$ejecutar = true;
				}
				if($ejecutar==false){
					$resp['msg'] = 'No tienes El Permiso Para Realizar Esta Accion';
					$resp['clase'] = 'error';
				}
				else{
					$resp = $this->modelo->delcontacto($posts);
				}
				$msgaviso = $this->htmlcreator->getTag('div',$resp['msg'],array('class'=>$resp['clase']));
			}
			$this->vista->contenido = $msgaviso;
			$this->vista->renderizar('blanco');
		}
		//-------------------------------------------------------------------------------------------
		//Seccion social
		//-------------------------------------------------------------------------------------------
		public function savesocial()
		{
			if( isset($_POST) ) {
				$datos = array(
					'photo'=>FILTER_SANITIZE_STRING,
					'id'=>FILTER_SANITIZE_STRING,
					'name'=>FILTER_SANITIZE_STRING,
					'folio_user'=>FILTER_SANITIZE_STRING,
					'email'=>FILTER_SANITIZE_STRING,
					'tipo'=>FILTER_SANITIZE_STRING,
					'login'=>FILTER_SANITIZE_STRING
				);
				$posts = filter_input_array(INPUT_POST,$datos);
				$datos = $this->modelo->savesocial($posts);
				//var_dump($datos);
				//$this->vista->contenido =  $this->getRedesData( $posts['id'] );
			}
			$this->vista->renderizar('blanco');
		}
		public function login()
		{		
			$connection = new TwitterOAuth(_TW_CKEY_, _TW_CSEC_);
			$connection->setTimeouts(10, 15);
			$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));

			$_SESSION['oauth_token'] = $request_token['oauth_token'];
			$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

			$url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
			header('Location: ' . $url);
		}
		public function getdata()
		{
			$request_token = [];
			$request_token['oauth_token'] = $_SESSION['oauth_token'];
			$request_token['oauth_token_secret'] = $_SESSION['oauth_token_secret'];
			if (isset($_REQUEST['oauth_token']) && $request_token['oauth_token'] !== $_REQUEST['oauth_token']) {
				echo 'error';
			// Abort! Something is wrong.
			}
			$connection = new TwitterOAuth(_TW_CKEY_, _TW_CSEC_, $request_token['oauth_token'], $request_token['oauth_token_secret']);
			$access_token = $connection->oauth("oauth/access_token", array("oauth_verifier" => $_REQUEST['oauth_verifier']));
			$_SESSION['access_token'] = $access_token;
			//echo '<pre>';
			//var_dump($access_token);
			//echo '</pre>';
			header('Location: ' . _PATH_ABS_.'usuarios/perfil/');
		}
		public function desvincular()
		{
			if( isset($_POST) ) {
				$datos = array(
					'idsocial'=>FILTER_SANITIZE_STRING,
				);
				$posts = filter_input_array(INPUT_POST,$datos);
				$datos = $this->modelo->desvincular($posts['idsocial']);
				//var_dump($datos);
				//$this->vista->contenido =  $this->getRedesData( $posts['id'] );
			}
			$this->vista->renderizar('blanco');
		}
	}
?>