<?php
	class inicioController extends Controller {
		public function __construct() {
        	parent::__construct();
        	$this->modelo=$this->loadModel('inicio');
        	unset($this->vista->socket_io);
        	unset($this->vista->notificacion_ordenes);
        	unset($this->vista->bar_contenido);
        	unset($this->vista->menu_slide);
        	unset($this->vista->js_menu);
        	$this->vista->bar_index = true;
    	}
		public function index() {
			///
			$this->redireccionar('inicio/login/');
		}
		public function login() {
			if(Session::get('logueado')) {
	           $this->redireccionar('principal/index/');
	           exit();
	        }
			if(isset($_POST['enviar'])) {
				$datos=array(
				  'lgn_user'=>FILTER_SANITIZE_STRING,
				  'lgn_password'=>FILTER_SANITIZE_STRING,
				  'urlredirect'=>FILTER_SANITIZE_STRING
				);
				$entradas=filter_input_array(INPUT_POST,$datos);
				$resultado=$this->modelo->login($entradas);
				if($resultado['logueado']==true) {
					foreach ($resultado as $key => $value) {
						$_SESSION[$key]=$value;
					}
					//$this->_registrar($_POST['usuario'], 'Inicio de Sesion');
					$this->modelo->registrar_log(Session::get('l_nombre'), 'Inicio de Sesion');
					if( empty($entradas['urlredirect']) ){
						$this->redireccionar('principal/index/');
					}
					else{
						$this->redireccionar( $entradas['urlredirect'] );
					}
				}
				else{
					$this->modelo->registrar_log($_POST['usuario'], 'Conexion Fallida');
					$msg = $resultado['l_msg'];
					$this->vista->mensaje = $this->htmlcreator->getTag('div',$msg,array('class'=>'error'));
					//$this->_registrar($_POST['usuario'], 'false');

					if( empty($entradas['urlredirect']) ){
						$this->redireccionar('principal/index/');
					}
					else{
						$this->redireccionar( $entradas['urlredirect'] );
					}
				}
			}
			$this->vista->cumple=$this->modelo->cumple();
			$this->vista->aniv_lab=$this->modelo->laboral();
			$this->vista->title='SCSMIC::Login';
			$this->vista->contenido='inicio:frm_login.html';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('validationEngine.jquery','formulario','calendario/jscal2','calendario/border-radius', 'calendario/steel/steel'),
				'secciones'=>array('inicio')
			));
			$this->vista->setJs(array(
				'publico'=>array('jquery.validationEngine-es','jquery.validationEngine','calendario/jscal2','calendario/lang/es'),
				'secciones'=>array('iniciojs')
			));

			$this->vista->renderizar();
			Session::destroy();
		}
	}
	
?>