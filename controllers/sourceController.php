<?php
	class sourceController extends Controller {
		public function __construct() {
			//cosntructor
        	parent::__construct();
    	}
		public function index() {
			///
			$this->redireccionar('inicio/login/');
		}
		
		public function css($template, $datacss){
			header('Content-type: text/css; charset: UTF-8');
			$defaulcss = array();
			$csscont = '';
			switch ($template) {
				case 'default':
					$defaulcss = array(
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'publico'._DS_.'reset.css',
							'url'=> _PATH_ABS_.'public/css/publico/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'publico'._DS_.'forms.css',
							'url'=> _PATH_ABS_.'public/css/publico/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'Design'._DS_.'alux'._DS_.'alux.css',
							'url'=>_PATH_ABS_.'public/Design/alux/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'Design'._DS_.'mdl'._DS_.'material.min.css',
							'url'=>_PATH_ABS_.'public/Design/mdl/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'template'._DS_.'material-icons'._DS_.'material_icons.css',
							'url'=>_PATH_ABS_.'public/css/template/material-icons/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'publico'._DS_.'menu'._DS_.'menu_sideslide.css',
							'url'=>_PATH_ABS_.'public/css/publico/menu/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'publico'._DS_.'alertify'._DS_.'alertify.min.css',
							'url'=>_PATH_ABS_.'public/css/publico/alertify/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'publico'._DS_.'jquery.dropdown.css',
							'url'=>_PATH_ABS_.'public/css/publico/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'template'._DS_.'flexmenu.css',
							'url'=>_PATH_ABS_.'public/css/template/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'template'._DS_.'estructura.css',
							'url'=>_PATH_ABS_.'public/css/template/'
						)
					);
					break;
				case 'print':
					$default = array();
					break;
				case 'template2':
					$default = array();
					break;
				case 'template_blanco':
					$default = array();
					break;
				case 'pdf':
					$default = array();
					break;
				case 'cortes':
					$default = array();
					break;
				case 'imprimir_template':
					$default = array();
					break;
				default:
					$defaulcss = array(
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'publico'._DS_.'reset.css',
							'url'=> _PATH_ABS_.'public/css/publico/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'publico'._DS_.'forms.css',
							'url'=> _PATH_ABS_.'public/css/publico/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'Design'._DS_.'alux'._DS_.'alux.css',
							'url'=>_PATH_ABS_.'public/Design/alux/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'Design'._DS_.'mdl'._DS_.'material.min.css',
							'url'=>_PATH_ABS_.'public/Design/mdl/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'template'._DS_.'material-icons'._DS_.'material_icons.css',
							'url'=>_PATH_ABS_.'public/css/template/material-icons/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'publico'._DS_.'menu'._DS_.'menu_sideslide.css',
							'url'=>_PATH_ABS_.'public/css/publico/menu/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'publico'._DS_.'alertify'._DS_.'alertify.min.css',
							'url'=>_PATH_ABS_.'public/css/publico/alertify/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'publico'._DS_.'jquery.dropdown.css',
							'url'=>_PATH_ABS_.'public/css/publico/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'template'._DS_.'flexmenu.css',
							'url'=>_PATH_ABS_.'public/css/template/'
						),
						array(
							'css'=>_ROOT_.'public'._DS_.'css'._DS_.'template'._DS_.'estructura.css',
							'url'=>_PATH_ABS_.'public/css/template/'
						)
					);
					break;
			}

	    	foreach ($defaulcss as $key => $cssfile) {
	    		if ( is_readable($cssfile['css']) ) {
					$file = $cssfile['css'];
					$fd = fopen($file,'r');
					$file = fread($fd,filesize($file));
					fclose($fd);
					$file = str_replace('url("', 'url("'.$cssfile['url'], $file);
					$file = str_replace('url(.', 'url('.$cssfile['url'].'.', $file);
					$file = str_replace('url(/', 'url('.$cssfile['url'].'/', $file);
					$file = str_replace('url(\'', 'url(\''.$cssfile['url'], $file);
					$file = str_replace('url("'.$cssfile['url'].'data:image/gif;base64', 'url("data:image/gif;base64', $file);
					//url("data:image
					$csscont.=$file;
				}				
	    	}
			
	    	if($datacss!='default.css'){
	    		$datacss = explode('.', $datacss);
	    		$data = $datacss[0];
	    		$data = $this->vista->desencriptar_AES($data);
	    		$css = json_decode($data, true);
	    		foreach ($css as $k => $v) {
	    			$file_css = $v['css'];
	    			if ( is_readable($file_css) ) {
						$file = $file_css;
						$fd = fopen($file,'r');
						$file = fread($fd,filesize($file));
						fclose($fd);

						$file = str_replace('url("', 'url("'.$v['url'], $file);
						$file = str_replace('url(.', 'url('.$v['url'].'.', $file);
						$file = str_replace('url(/', 'url('.$v['url'].'/', $file);
						$file = str_replace('url(\'', 'url(\''.$v['url'], $file);
						$file = str_replace('url("'.$v['url'].'data:image/gif;base64', 'url("data:image/gif;base64', $file);
						//$file = str_replace('url(\'..', 'url(\''.$v['url'].'/..', $file);
						$csscont.=$file;
					}
	    		}
	    	}
	    	
	    	$csscont = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $csscont);//remover comentarios
			$csscont = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $csscont);
	    	echo $csscont;
	    	//$this->vista->contenido = $csscont;
	    	//$this->vista->renderizar('blanco');
		}
		/*
		*Modulo que importa css
		*/
		public function cssimport($template, $datacss){
			header('Content-type: text/css; charset: UTF-8');
			$defaulcss = array();
			$csscont = '';
			switch ($template) {
				case 'default':
					$defaulcss = array(
						_PATH_ABS_.'public'.'/'.'css'.'/'.'publico'.'/'.'reset.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'publico'.'/'.'forms.css',
						_PATH_ABS_.'public'.'/'.'Design'.'/'.'alux'.'/'.'alux.css',
						_PATH_ABS_.'public'.'/'.'Design'.'/'.'mdl'.'/'.'material.min.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'template'.'/'.'material-icons'.'/'.'material_icons.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'publico'.'/'.'menu'.'/'.'menu_sideslide.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'publico'.'/'.'alertify'.'/'.'alertify.min.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'publico'.'/'.'jquery.dropdown.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'template'.'/'.'flexmenu.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'template'.'/'.'estructura.css'
					);
					break;
				default:
					$defaulcss = array(
						_PATH_ABS_.'public'.'/'.'css'.'/'.'publico'.'/'.'reset.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'publico'.'/'.'forms.css',
						_PATH_ABS_.'public'.'/'.'Design'.'/'.'alux'.'/'.'alux.css',
						_PATH_ABS_.'public'.'/'.'Design'.'/'.'mdl'.'/'.'material.min.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'template'.'/'.'material-icons'.'/'.'material_icons.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'publico'.'/'.'menu'.'/'.'menu_sideslide.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'publico'.'/'.'alertify'.'/'.'alertify.min.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'publico'.'/'.'jquery.dropdown.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'template'.'/'.'flexmenu.css',
						_PATH_ABS_.'public'.'/'.'css'.'/'.'template'.'/'.'estructura.css'
					);
					break;
			}

	    	foreach ($defaulcss as $key => $cssfile) {
				$csscont.='@import url("'.$cssfile.'");'."\n";
				
	    	}
			
	    	if($datacss!='default.css'){
	    		$datacss = explode('.', $datacss);
	    		$data = $datacss[0];
	    		$data = $this->vista->desencriptar_AES($data);
	    		$css = json_decode($data, true);
	    		
	    		foreach ($css as $k => $v) {
					$file_css = $v['css'];
					$csscont.='@import url("'.$file_css.'");'."\n";
	    		}
	    	}
	    	
	    	$csscont = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $csscont);//remover comentarios
			$csscont = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $csscont);
	    	$this->vista->contenido = $csscont;
	    	$this->vista->renderizar('blanco');
		}
		public function js($template, $datajs=''){
			header('Content-Type: application/javascript; charset: UTF-8');
			$defauljs = array();
			$jscont = '';
			if( !empty($template) ){
				switch ($template) {
					case 'default':
						$defauljs = array(
							array(
								'js'=>_ROOT_.'public'._DS_.'Design'._DS_.'MDB'._DS_.'js'._DS_.'jquery.min.js',
								'url'=>_PATH_ABS_.'public/Design/MDB/js/'
							),
							array(
								'js'=>_ROOT_.'public'._DS_.'Design'._DS_.'mdl'._DS_.'material.min.js',
								'url'=>_PATH_ABS_.'public/Design/mdl/'
							),
							array(
								'js'=>_ROOT_.'public'._DS_.'js'._DS_.'publico'._DS_.'jquerymigrate.js',
								'url'=>_PATH_ABS_.'public/js/publico/'
							),
							array(
								'js'=>_ROOT_.'public'._DS_.'js'._DS_.'publico'._DS_.'modernizr.custom.js',
								'url'=>_PATH_ABS_.'public/js/publico/'
							),
							array(
								'js'=>_ROOT_.'public'._DS_.'js'._DS_.'publico'._DS_.'alertify'._DS_.'alertify.min.js',
								'url'=>_PATH_ABS_.'public/js/publico/alertify/'
							),
							array(
								'js'=>_ROOT_.'public'._DS_.'js'._DS_.'publico'._DS_.'flexmenu.min.js',
								'url'=>_PATH_ABS_.'public/js/publico/'
							),
							array(
								'js'=>_ROOT_.'public'._DS_.'js'._DS_.'template'._DS_.'template.js',
								'url'=>_PATH_ABS_.'public/js/template/'
							)
						);
						break;
					case 'defaulturl':
						$defauljs = array(
							array(
								'js'=>_PATH_ABS_.'public'.'/'.'Design'.'/'.'MDB'.'/'.'js'.'/'.'jquery.min.js',
								'url'=>_PATH_ABS_.'public/Design/MDB/js/'
							),
							array(
								'js'=>_PATH_ABS_.'public'.'/'.'Design'.'/'.'mdl'.'/'.'material.min.js',
								'url'=>_PATH_ABS_.'public/Design/mdl/'
							),
							array(
								'js'=>_PATH_ABS_.'public'.'/'.'js'.'/'.'publico'.'/'.'jquerymigrate.js',
								'url'=>_PATH_ABS_.'public/js/publico/'
							),
							array(
								'js'=>_PATH_ABS_.'public'.'/'.'js'.'/'.'publico'.'/'.'modernizr.custom.js',
								'url'=>_PATH_ABS_.'public/js/publico/'
							),
							array(
								'js'=>_PATH_ABS_.'public'.'/'.'js'.'/'.'publico'.'/'.'alertify'.'/'.'alertify.min.js',
								'url'=>_PATH_ABS_.'public/js/publico/alertify/'
							),
							array(
								'js'=>_PATH_ABS_.'public'.'/'.'js'.'/'.'publico'.'/'.'flexmenu.min.js',
								'url'=>_PATH_ABS_.'public/js/publico/'
							),
							array(
								'js'=>_PATH_ABS_.'public'.'/'.'js'.'/'.'template'.'/'.'template.js',
								'url'=>_PATH_ABS_.'public/js/template/'
							)
						);
						break;
					case 'sockets':
						$defauljs = array( 
							array(
								'js'=>_ROOT_.'public'._DS_.'js'._DS_.'secciones'._DS_.'socket.js',
								'url'=>_PATH_ABS_.'public/js/secciones/'
							)
						);
						break;
					default:
						# code...
						break;
				}
			}

			foreach ($defauljs as $key => $jsfile) {
	    		if ( is_readable($jsfile['js']) ) {
					$file = $jsfile['js'];
					$fd = fopen($file,'r');
					$file = fread($fd,filesize($file));
					fclose($fd);
					$jscont.=$file;
				}				
	    	}

	    	if($datajs!='default.js'){
	    		$datajs = explode('.', $datajs);
	    		$data = $datajs[0];
	    		$data = $this->vista->desencriptar_AES($data);
	    		$js = json_decode($data, true);
	    		if(!empty($js)){
		    		foreach ($js as $k => $v) {
		    			$file_js = $v['js'];
		    			//echo '-->'.$file_js."\n";
		    			if ( is_readable($file_js) ) {
		    				//echo $file_js."\n";
							$file = $file_js;
							$fd = fopen($file,'r');
							$file = fread($fd,filesize($file));
							fclose($fd);
							$jscont.=$file;
							//$jscont = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $jscont );
							$jscont.="\n";
						}
		    		}
	    		}
	    	}
	    	echo $jscont;
		}

	}
?>