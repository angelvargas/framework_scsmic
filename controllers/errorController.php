<?php
	class errorController extends Controller {
		public function __construct() {
			parent::__construct();
			unset($this->vista->socket_io);
			unset($this->vista->notificacion_ordenes);
			unset($this->vista->bar_contenido);
			unset($this->vista->menu_slide);
			$this->vista->bar_index = true;
		}
		
		public function index(){
			$this->vista->contenido='error:error.html';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'secciones'=>array('inicio')
			));
			$this->vista->title='Error';
			$this->vista->mensaje=$this->_getError();
			$this->vista->renderizar();
		}

		public function access($codigo, $url='') {
			//echo _PATH_ABS_;
			$url = str_replace('@', '/', $url);
			//echo $url;
			$this->vista->urlredirect = $url;
			//$this->vista->urlredirect
			$this->vista->contenido = $this->_getError($codigo);
			$this->vista->setCss(array(
				'template'=>array('estructura')
			));
			$this->vista->title = 'Error';
			$this->vista->renderizar();
		}
		/*    
		public function access($codigo) {
			$this->vista->contenido=$this->_getError($codigo);
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'secciones'=>array('inicio')
			));
			$this->vista->title='Error';
			$this->vista->renderizar();
		}
		*/
		
		private function _getError($codigo = false) {
			$error['default']      = 'error:error404.html';
			$error['404']          = 'error:error404.html';
			$error['401']          = 'error:error401.html';
			$error['errorsession'] = 'error:errorsession.html';
			if(array_key_exists($codigo, $error)){
				return $error[$codigo];
			}
			else{
				return $error['default'];
			}
		}
	}
?>