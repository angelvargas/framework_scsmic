<?php
	date_default_timezone_set('America/Mexico_City');
	//setlocale(LC_TIME, 'es_MX.UTF-8');
	setlocale(LC_ALL, 'es_MX.UTF-8'); 
	//echo setlocale(LC_ALL, 0);
	//setlocale(LC_TIME, 'es_MX'); // o el de tu país/idioma
	error_reporting(E_ALL);
	ini_set("display_errors", 1);

	define('_DS_', DIRECTORY_SEPARATOR);//separador de carpetas dependiendo del SO
	define('_ROOT_', realpath(dirname(__FILE__))._DS_);//devuelve la ruta absoluta valida de la carpeta contenedora
	define('_PATH_', _ROOT_ . 'application' ._DS_);//devuelve la ruta absoluta valida de la carpeta de aplicacion
	try{
		require_once _PATH_.'registry.php';//registro singleton
		require_once _PATH_.'Config.php';//archivo de configuracion del programa
		require_once _PATH_.'Acl.php';
		require_once _PATH_.'Request.php';
		require_once _PATH_.'Bootstrap.php';//arachivo de arranque del sistema
		require_once _PATH_.'Controller.php';
		require_once _PATH_.'View.php';
		require_once _PATH_.'Conexion.php';
		require_once _PATH_.'Model.php';
		require_once _PATH_.'Session.php';
		require_once _PATH_.'htmlcreator.php';//9-dic-2013 clase generadora de etiquetas html
		require_once _PATH_.'paginator.php';//9-dic-2013 clase paginadora
		///////////////////////////////////////////////////////////////
		$registro = Registry::getInstancia();
	    $registro->acl = new ACL();
	    $registro->htmlcreator = new htmlcreator();
	    $registro->paginator = new paginator();
		///////////////////////////////////////////////////////////////
		Session::init();//inicio de sesion
		Bootstrap::Run(new Request);
		//llamado al metodo estatico encargado de correr el programa
		//intancia de la clase requerst como parametro
	}catch(Exception $e){//manejador de errores
		echo $e->getMessage();
	}
?>