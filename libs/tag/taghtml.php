<?php
	//Metodos Encadenados
	class htmlcreator
	{
		private $basetag;//base para las etiquetas html
		private $simpletag;//base para las etiquetas html sin cierre
		//private $iniTag;//base de apertura de una etiqueta html
		//private $finTag;//base de cierre de una etiqueta html
		//private $arrayTag;//coleccion de tags por cerrar;
		//private $arrayHtml;//coleccion de etiquetas iniciales html

		private $tipo;
		//private $iniTag;
		//private $endTag;
		private $valTag;
		private $properties;
		private $Tag;

		public function __construct()
		{
			$this->basetag='<%s %s>%s</%s>';
			$this->simpletag='<%s %s/>';
			$this->properties = array();
			//$this->Tag='<%s %s>';
			//$this->finTag='</%s>';
			//$this->arrayTag=array();
		}
		public function Tag( $tag, $tipo =1 )
		{
			$this->tipo == 2;
			$this->Tag = $tag;
		}

		public function setProperty( $property, $value )
		{
			$this->properties[$property] = $value;
		}

		public function setValue( $value )
		{
			if($this->tipo == 1)
				$this->valTag = $value;
		}
		public function result($flag = 0)
		{
			$this->properties = '';
			if($this->tipo == 1)
				$html = sprintf($this->basetag, $this->Tag, $this->properties, $this->valTag, $this->Tag);
			else
				$html = sprintf($this->simpletag, $this->Tag, $this->properties);

			if( $flag == 0)
				return $html;
			else
				echo $html;
		}
	}
?>