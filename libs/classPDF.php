<?php
require('fpdf17/fpdf.php');

class PDF extends FPDF
{
    // Cabecera de página
    function Header()
    {
        // Logo
        $this->Image(_IMG_.'plantilla/logo-caribe.png',10,8,33);
        //$this->Image(_IMG_.'plantilla/logo-caribe.png',170,8,33);
        // Arial bold 15
        $this->SetFont('Arial','B',10);
        // Movernos a la derecha
        $this->Cell(80);
        $this->Cell(30,5,'Caribe Mexicano Com S.A. de C.V.',0,0,'C');
        $this->Ln();
        $this->Cell(80);
        $this->Cell(30,5,'Calle 37 N'.utf8_decode('º').' 170-L x 26 Col. Santa Ana',0,0,'C');
        $this->Ln();
        $this->Cell(80);
        $this->Cell(30,5,'Valladolid, Yucat'.utf8_decode('á').'n C.P. 97780',0,0,'C');
        $this->Ln();
        $this->Cell(80);
        $this->Cell(30,5,'Tel. (985)85-60450',0,0,'C');
        $this->Ln();
        $this->Cell(80);
        $this->Cell(30,5,'e-mail: yuded@caribemexicano.com',0,0,'C');
        $this->Ln();
        $this->Cell(80);
        $this->Cell(30,5,'R.F.C.: CMC040322BV3',0,0,'C');
        // Salto de línea
        $this->Ln(20);

        $this->Cell(80);
        $this->Cell(30,5,'Reporte De Actividades IT Valladolid',0,0,'C');
        $this->Ln(20);

        $this->Cell(45,5,'Fecha',0,0,'C');
        $this->Ln();
        $alt=$this->GetY()-5;
        $anc=55;
        $this->SetXY($anc,$alt);//(x,y)
        $this->Cell(60,5,'Problema',0,0,'C');
        $this->Ln();
        $alt=$this->GetY()-5;
        $anc=115;
        $this->SetXY($anc,$alt);//(x,y)
        $this->Cell(60,5,'Actividad',0,0,'C');
        $this->Ln();
        $alt=$this->GetY()-5;
        $anc=175;
        $this->SetXY($anc,$alt);//(x,y)
        $this->Cell(20,5,'Orden',0,0,'C');
        $this->Ln();
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
?>