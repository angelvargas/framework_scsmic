<?php
/*
@@ modo de uso
if(!isset($_COOKIE['iau'])){
	die('falta cookie');
}else{
	$curl = new httpRequestValidate;
	$res=$curl->validar($_COOKIE['iau']);
	$res=json_decode($res);//recuperamos el array con los datos
}
*/
class HttpRequestAcuc{
	//private $nameApp;
	private $ruta='http://127.0.0.1/usuarios/login/';
	private $nuevaRuta='';

	public function __construct(){//$nameApp
		if(!function_exists('curl_init')){
			die('Necesita habilitar las funciones de "curl"');
		}
		//$this->nameApp=$nameApp;
	}
	//ejecuta el curl para la solicitud
	private function ejecutar(){
			$ch = curl_init($this->nuevaRuta);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$res=curl_exec($ch);
			curl_close($ch);
			return $res;
	}
	//Valida la cookie pasada como parámetro
	public function validar($cookie){
		if(isset($cookie) && !empty($cookie)){
			$this->nuevaRuta=$this->ruta.'verificar/'.$cookie;
			return $this->ejecutar();
		}else{
			die('Se requiere el valor de la cookie como par&aacute;metro');
		}
	}
	//termina la session y elimina la cookie
	public function terminar($cookie){
		if(isset($cookie) && !empty($cookie)){
			$this->nuevaRuta=$this->ruta.'terminar/'.$cookie;
			return $this->ejecutar();
		}else{
			die('Se requiere el valor de la cookie como par&aacute;metro');
		}
	}
	/*
	public function cookie($nombre, $valor){
		if(!setcookie("iau".$nombre,$valor,time()+(3600*24*7),"/","",false,true)){
			throw new Exception("No tiene habilitado sus cookies");
		}
	}*/
	//agrega un nuevo usuario
	public function addUser($aDatos){
		//el array recibido debe de contener en orden estricto
		//usuario, password, nombre, apellido, tipo de usuario,nombre o id aplicacion
		if(isset($aDatos) && is_array($aDatos)){
			$datos='';
			foreach ($aDatos as $key => $value) {
				$datos.= filter_var($value, FILTER_VALIDATE_URL) . '/';
			}
			$this->nuevaRuta=$this->ruta.'AgregarUsuario/'.$datos;
			return $this->ejecutar();
		}else{
			die('Se requiere un array como par&aacute;metro');
		}
	}
	//elimina un nuevo usuario
	public function delUser($id){
		if(isset($id) && is_int($id)){
			$this->nuevaRuta=$this->ruta.'EliminarUsuario/'.$id;
			return $this->ejecutar();
		}else{
			die('Se requiere un valor como par&aacute;metro');
		}
	}
}
?>