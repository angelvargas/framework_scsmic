<?php
/*********************************
Paginador
v 1.0.1
ISC Angel Florentno Vargas Pool
*********************************/
    class paginador{
        private $totalpaginas;
        private $pagina;
        private $limite;
        
        public function paginar($datos,$pagina=false){
            /*
            if($limite && is_numeric($limite)){
                $limite = $limite;
            } else {
                $limite = 10;
            }
            */
            
            $this->limite=$datos['mostrar'];
            if($this->limite==0)
            {
                $limite=1;
            }
            else
            {
                $limite=$datos['mostrar'];
            }
            //echo $limite;
            $totalregistros = $datos['total'];
            
       
            if($pagina && is_numeric($pagina)){
                $inicio = ($pagina - 1) * $limite;
            } else {
                $pagina = 1;
                $inicio = 0;
            }

            $this->pagina=$pagina;
            
            $this->totalpaginas = ceil($totalregistros / $limite);
            //echo '-'.$this->totalpaginas;
            //$newdatos            = array_slice($datos, $inicio, $limite);

            //return $newdatos;
        }
        public function display_paginas($ruta,$ver=0){
            $paginador='';
            $basea='<a href="%s" %s>%s</a>';
            $ruta=_PATH_ABS_.$ruta;
            $clase='';
            if($ver==0){
                $principal = sprintf($basea,$ruta.$this->pagina.'/','class="activo"','Pagina '.$this->pagina).' ';
            }else{
                $principal='';
                if($this->totalpaginas==1){
                    $principal = sprintf($basea,$ruta,'class="activo"',1).' ';
                }else{
                    if($ver>$this->totalpaginas){
                        $desde=1;
                        $hasta=$this->totalpaginas;
                    }else{
                        $desde=$this->pagina-ceil(($ver/2)-1);
                        $hasta=$this->pagina+ceil(($ver/2)-1);
                        if($desde<1){
                            $hasta -=($desde-1);
                            $desde=1;
                        }
                        if($hasta>$this->totalpaginas){
                            $desde -=($hasta-$this->totalpaginas);
                            $hasta=$this->totalpaginas;
                        }
                    }
                    for ($i=$desde; $i<=$hasta ; $i++){
                        if($i==$this->pagina){
                            $clase='class="activo"';
                        }else{
                            $clase='';
                        }
                        $principal .= sprintf($basea,$ruta.$i.'/',$clase,$i).' ';
                    }
                }
            }
            $pagsig=$this->pagina+1;
            $pagant=$this->pagina-1;
            $primera   = sprintf($basea,$ruta,'','<<');
            $siguiente = sprintf($basea,$ruta.$pagsig.'/','','>');
            $anterior  = sprintf($basea,$ruta.$pagant.'/','','<');
            $ultimo    = sprintf($basea,$ruta.$this->totalpaginas.'/','','>>');
            if($this->totalpaginas==1){
                $paginador=$principal;
            }else{
                if($this->pagina==1){
                    $paginador=$principal.$siguiente.' '.$ultimo;
                }elseif($this->pagina==$this->totalpaginas){ 
                    $paginador=$primera.' '.$anterior.' '.$principal;
                }else{
                    $paginador=$primera.' '.$anterior.' '.$principal.$siguiente.' '.$ultimo;
                }
            }
            if($this->limite>0)
            {
                return $paginador;
            }
            
        }
    }
?>