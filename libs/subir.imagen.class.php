<?php
/**
 * SubirImg
 *
 * @package
 * @author
 #------------------------------------------#
 #realizado por:							#
 #				Edgar Israel Tamayo Silva	#
 #email:									#
 #				eits2@hotmail.com			#
 #------------------------------------------#
 * @copyright Copyright (c) 2009
 * @version 0.10
 * @access public
 */
class SubirImg{
	public $tiposmime=array("image/jpeg", "image/pjpeg", "image/gif", "image/png", "image/bmp");//los mas comunes
	public $msg_noTipoImg='El tipo de imagen no esta permitido';
	public $msg_correcto='La imagen se subi&oacute; con &eacute;xito.';//mensaje de respuesta cuando una imagen suba correctamente
	public $msg_incorrecto='La imagen no pudo guardarse.';//mensaje de respuesta cuando una imagen no se pueda subir
	public $msg_pesoIncorrecto='El Peso de la imagen es superior al permitido';
	public $msg_noImagen='No hay Imagen';
	public $msg_existe='El Archivo ya existe';
	public $remplazar=false;//no remplazar archivos
	public $peso = 102400;//100 kb
	public $mostrarMsg=false;//cero o false para no devolver y utilizar el die
	public $imagen, $ancho, $alto, $nombreNuevo='';
	public $guardarEn='./';
	private $img_type, $tmp_name, $size, $name, $prefijo, $error=FALSE, $msg, $ext, $ruta;

	/**
	* Constructor de la clase
	*/
	public function __construct($img){
		if(empty($img)){
			$this->error=true;
			$this->msg=$this->msg_noImagen;
		}else{
				$this->imagen=$img;
		}
	}
	
	/**
	* Asignar Imagen
	*/
	public function setImagen($img){
		$this->error=false;
		$this->msg='';
		$this->imagen=$img;
	}

	/**
	* Devuelve mensajes de la clase
	*/
	public function getMsg(){
		return $this->msg;
	}

	/**
	* colocar prefijo al nombre de la imagen
	*/
	public function setPrefijo($val)
	{
		$this->prefijo = $val;
	}
	public function getPrefijo(){
		if(empty($this->prefijo))
			$this->prefijo = substr(md5(uniqid(rand())),0,6).'-';
	}

	/**
	* Remplazar caracteres especiales en los nombres de las imagenes
	*/
	public function setName($nombre)
	{
		$data = explode('.', $this->imagen['name']);
		$this->name = $nombre.'.'.$data[1];
	}
	private function setRemplazar(){
		if(empty($this->name))
			$this->name = $this->imagen['name'];
		/*$this->name = ereg_replace('[áàâãª]','a',$this->name);
		$this->name = ereg_replace('[ÁÀÂÃ]','A',$this->name);
		$this->name = ereg_replace('[ÍÌÎ]','I',$this->name);
		$this->name = ereg_replace('[íìî]','i',$this->name);
		$this->name = ereg_replace('[éèê]','e',$this->name);
		$this->name = ereg_replace('[ÉÈÊ]','E',$this->name);
		$this->name = ereg_replace('[óòôõº]','o',$this->name);
		$this->name = ereg_replace('[ÓÒÔÕ]','O',$this->name);
		$this->name = ereg_replace('[úùû]','u',$this->name);
		$this->name = ereg_replace('[ÚÙÛ]','U',$this->name);
		*/
		$this->name = str_replace(' ','-',$this->name);
		$this->name = str_replace('ç','c',$this->name);
		$this->name = str_replace('Ç','C',$this->name);
		$this->name = str_replace('ñ','n',$this->name);
		$this->name = str_replace('Ñ','N',$this->name);
		
		$this->name = str_replace('á','a',$this->name);
		$this->name = str_replace('à','a',$this->name);
		$this->name = str_replace('Á','A',$this->name);
		$this->name = str_replace('À','A',$this->name);
		
		$this->name = str_replace('é','e',$this->name);
		$this->name = str_replace('è','e',$this->name);
		$this->name = str_replace('É','E',$this->name);
		$this->name = str_replace('È','E',$this->name);
		
		$this->name = str_replace('í','i',$this->name);
		$this->name = str_replace('ì','i',$this->name);
		$this->name = str_replace('Í','I',$this->name);
		$this->name = str_replace('Ì','I',$this->name);
		
		$this->name = str_replace('ó','o',$this->name);
		$this->name = str_replace('ò','o',$this->name);
		$this->name = str_replace('Ó','O',$this->name);
		$this->name = str_replace('Ò','O',$this->name);
		
		$this->name = str_replace('ú','u',$this->name);
		$this->name = str_replace('ù','u',$this->name);
		$this->name = str_replace('Ú','U',$this->name);
		$this->name = str_replace('Ù','U',$this->name);
	}

	/**
	* Checa el tipo, el peso, y si se puede subir el archivo
	*/
	public function verifica(){
		if(!empty($this->imagen)){
			if($this->getEsPermitido()){
				if($this->getPeso()){
					if($this->getSePuedeSubir()){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			$this->error=true;
			$this->msg=$this->msg_noImagen;
			return false;
		}
	}
	
	/**
	* Obtiene los datos necesarios de la imagen subida
	*/
	private function inicializa(){
		$this->getPrefijo();
		$this->getGenerarNombre();
		$this->getRuta();
		$this->tmp_name=$this->imagen['tmp_name'];
		$this->img_type=$this->imagen['type'];
		$this->size=$this->imagen['size'];
	}
	
	/**
	* Devuelve el nuevo nombre de la imagen
	*/
	public function getGenerarNombre(){
		$this->setRemplazar();
		$this->nombreNuevo = $this->prefijo.$this->name;
	}
	/**
	* Devuelve el nuevo nombre de la imagen
	*/
	public function getNombre(){
		return $this->nombreNuevo;
	}
	public function getNameOut(){
        $data = explode('.', $this->nombreNuevo);
        return $data[0];
    }
    public function setNameOut($nameout){
        $data = explode('.', $this->nombreNuevo);
        return $nameout.'.'.$data[1];
    }
	/**
	* Devuelve true si ocurrió algun error, de lo contrario false
	*/
	public function getError(){
		return $this->error;
	}

	/**
	* Checar el peso de la imagen
	*/
	public function getPeso(){
		if($this->size <= $this->peso){
			return true;
		}else{
			$this->error=true;
			$this->msg=$this->msg_pesoIncorrecto;
			return false;
		}
	}
	
	/**
	* Checar si el tipo de imagen es poermitido
	*/
	public function getEsPermitido(){
		if(in_array($this->img_type,$this->tiposmime)){
			return true;
		}else{
			$this->error=true;
			$this->msg=$this->msg_noTipoImg;
			return false;
		}
	}
	
	/**
	* Extraer la extension de la imagen
	*/
	public function getExtension(){
		return $this->ext=substr($this->name,-3);
	}
	
	/**
	* Ruta donde se guardara el archivo
	*/
	public function getRuta(){
		$this->ruta=$this->guardarEn.$this->prefijo.$this->name;
	}
	
	/**
	* Verifica si es posible subir la imagen
	*/
	public function getSePuedeSubir(){
		if(is_uploaded_file($this->tmp_name)){
			return true;
		}else{
			$this->error=true;
			$this->msg=$this->msg_incorrecto;
			return false;
		}
	}
	
	/**
	* Verifica si el Archivo Existe
	*/
	public function getExiste(){
		if(file_exists($this->ruta)){
			return true;
		}else{
			$this->error=true;
			$this->msg=$this->msg_incorrecto;
			return false;
		}
	}
	/**
	* Subir la imagen
	*/
	public function setSubir(){
		if (move_uploaded_file($this->tmp_name,$this->ruta)){
			$this->error=false;
			$this->msg=$this->msg_correcto;
			$this->setBorrarTmp();
		}else{
			$this->error=true;
			$this->msg=$this->msg_incorrecto;
		}
	}
	
	/**
	* pasa la imagen del tmp a la ruta especificada en $guardarEn
	*/
	public function setSubirImg(){
		$this->inicializa();
		if($this->verifica()){
			if($this->remplazar){
				$this->setSubir();
			}else{
				if(!$this->getExiste()){
					$this->setSubir();
				}
			}
		}
		if($this->mostrarMsg){
			die($this->msg);
		}
	}

	/**
	* Elimina la imagen creada en la carpeta temp
	*/
	function setBorrarTmp(){
		if(file_exists($this->tmp_name)){
			unlink($this->tmp_name);
		}
	}
}
?>