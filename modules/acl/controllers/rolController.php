<?php
	class rolController extends aclController {
		public function __construct()
		{
			parent::__construct();
			Session::acceso($this->__app, $this->__mod, $this->__sec, $this->__arg);
			$this->modelo=$this->loadModel('rol',$this->_modulo);
			//--------------------------------------------------------------------
			//$this->menu_principal($this->_controlador, $this->_modulo);
			//$this->menu_secundario($this->_controlador,$this->_metodo, $this->_modulo);
			//--------------------------------------------------------------------
			//$this->vista->dat='publico:navuseracl.html';
			$this->vista->dat='publico:navuser.html';
			$this->vista->nombre_session=$_SESSION['l_nombre'];
		}
		
		public function index()
		{
			$this->redireccionar('acl/');
		}

		public function roles($pagina=false,$flag=0) {
			$this->acl->acceso($this->_metodo);

			if( Session::get('_roles') ){
				$post = Session::get('_roles');
			}
			else{
				$post = array('txtrol'=>'', 'selcat'=>'');
			}
			if(!empty($_POST)){
				$data=array(
					'txtrol'=>FILTER_SANITIZE_STRING,
					'selcat'=>FILTER_SANITIZE_NUMBER_INT
				);
				$post = filter_input_array(INPUT_POST,$data);
				Session::set('_roles', $post);	
			}
			//------------------------------------------
			foreach ($post as $key => $value){
				$$key = $value;
			}
			//------------------------------------------
			$this->vista->title='Roles';
			$this->vista->titulo_tabla=$this->htmlcreator->getTag('h2','Lista De Roles');
			
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('tablas','cssfrm','nav','nav2','jqueryuicss/jqueryui','validationEngine.jquery')
			));
			$this->vista->setJs(array(
				'template'=>array('pluginsearch'),
				'publico'=>array('jquery-ui-1.10.4.custom.min','jquery.validationEngine-es','jquery.validationEngine'),
				'secciones'=>array('rol/rol')
			));
			//------------------------------------------------------
			$this->paginator->mostrar = 10;

			$this->paginator->pagina = $pagina;

			$datos_roles=$this->modelo->roles( $post );

			$categorias = $this->modelo->categorias();
			//------------------------------------------------------
			if($this->acl->permiso('agregar_rol')) {
				$btnadd=$this->htmlcreator->getTag('a','Agregar Rol',array('href'=>'javascript:void(0);','class'=>'btnext addrol'));
				$this->vista->frmtablaoculto='rol:frm_rol.html';
				
				$this->vista->lstcategoria = $this->htmlcreator->getOptions($categorias, 0, 'Selecciona Categoria');
			}
			else {
				$btnadd='';
			}
			//----------------------------------------
			$this->htmlcreator->form=array(
				'Rol'=>array('type'=>'text','name'=>'txtrol','id'=>'txtrol','class'=>'inputsearch tbinput','value'=>$txtrol),
				'Categoria'=>array('type'=>'select','name'=>'selcat','id'=>'selcat','data'=>$categorias, 'class'=>'selsearch', 'msgdata'=>'Categoria', 'seldata'=>$selcat)
			);
			$this->vista->tabla = $this->htmlcreator->getTabla($datos_roles,'',array('class'=>'tablas'));
			$this->vista->btnextra = $btnadd;

			$this->vista->paginador = $this->paginator->htmlpaginador('acl/rol/roles/', 7);
			
			$this->vista->infopag = $this->paginator->info();

			if( $flag==0 ) {
				$this->vista->contenido = 'publico:table.html';
				$this->vista->renderizar();
			}
			else{
				$this->vista->contenido = 'publico:tableajax.html';
				$this->vista->renderizar('blanco');
			}
		}

		public function metodorol()
		{
			if(!empty($_POST))
			{
				$data=array(
					'nombre'=>FILTER_SANITIZE_STRING,
					'categoria'=>FILTER_SANITIZE_NUMBER_INT,
					'id'=>FILTER_SANITIZE_NUMBER_INT
				);
				$posts=filter_input_array(INPUT_POST,$data);
			}
			$res=$this->modelo->metodorol($posts);
			$this->vista->contenido=$res;
			$this->vista->renderizar('blanco');
		}

		public function getRol()
		{
			$data=array(
				'id'=>FILTER_SANITIZE_NUMBER_INT
			);
			$post=filter_input_array(INPUT_POST,$data);
			$res=$this->modelo->datosRol($post['id']);
			$this->vista->contenido=$res;
			$this->vista->renderizar('blanco');
		}

		public function permisos_rol($rol, $flag = 0) {
			$this->acl->acceso($this->_metodo);
			$this->getParam($rol, 'acl/rol/roles/');
			//------------------------------------------
			if( Session::get('_permisos_rol') ){
				$post = Session::get('_permisos_rol');
			}
			else{
				$post = array('modulo'=>'0');
			}
			if(!empty($_POST)){
				$data=array(
					'modulo'=>FILTER_SANITIZE_NUMBER_INT
				);
				$post = filter_input_array(INPUT_POST,$data);
				Session::set('_permisos_rol', $post);	
			}
			//------------------------------------------
			foreach ($post as $key => $value){
				$$key = $value;
			}
			//------------------------------------------
			$this->vista->title = 'Permisos de Rol {{name_rol}}';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('nav','nav2','tablas')
			));
			$this->vista->setJS(array(
				'secciones'=>array('rol/permisosrol')
			));
			$this->vista->idrol = $rol;
			
			$this->vista->name_rol = $this->modelo->getNameRol($rol);
			//-------------------------------------------------------------------------------------
			$metodos=$this->modelo->optMetodos();
			//-------------------------------------------------------------------------------------
			$ali=$this->htmlcreator->getTag('a', 'Permisos de Rol');
			$this->vista->navextra=$this->htmlcreator->getTag('li', $ali, array('class'=>'activo'));
			//-------------------------------------------------------------------------------------
			$this->htmlcreator->form=array(
				'Modulo'=>array('type'=>'select', 'id'=>'modulos', 'name'=>'modulos', 'data'=>$metodos, 'msgdata'=>'seleccione', 'seldata'=>$modulo)
			);
			//-------------------------------------------------------------------------------------
			$entradas=array('modulo'=>$modulo, 'rol'=>$rol);
			$permisos = array_merge( $this->modelo->all_permisos($entradas['modulo']), $this->modelo->permisos_rol($entradas) );
			$dat=array();
			$cont=0;
			foreach ($permisos as $key => $value) {
				$cont++;
				$dat[$cont]=$value;
			}
			$lstpermisos = $dat;
			//-------------------------------------------------------------------------------------
			$this->vista->name_rol = $this->modelo->getNameRol($rol);
			$contenido_tabla = $this->htmlcreator->getTabla($lstpermisos, '', array('class'=>'tablas'));
			$this->vista->lstpermisos_rol = $contenido_tabla;
			//-------------------------------------------------------------------------------------

			if( $flag==0 ) {
				$this->vista->contenido='rol:permisos_rol.html';
				$this->vista->renderizar();
			}
			else{
				$this->vista->contenido = $contenido_tabla;
				$this->vista->renderizar('blanco');
			}
		}
		
		public function metodo_permisos_rol()
		{
			$datos=array(
					'rol'=>FILTER_SANITIZE_NUMBER_INT,
					'permiso'=>FILTER_SANITIZE_NUMBER_INT,
					'valor'=>FILTER_SANITIZE_STRING	
				);
			$entradas=filter_input_array(INPUT_POST,$datos);
			$this->modelo->metodo_permisos_rol($entradas);
		}
	}
?>