<?php
	class usersController extends aclController
	{
		public function __construct()
		{
			parent::__construct();
			Session::acceso($this->__app, $this->__mod, $this->__sec, $this->__arg);
			$this->modelo=$this->loadModel('users',$this->_modulo);
			//--------------------------------------------------------------------
			//$this->menu_principal($this->_controlador, $this->_modulo);
			//$this->menu_secundario($this->_controlador,$this->_metodo, $this->_modulo);
			//--------------------------------------------------------------------
			//$this->vista->dat='publico:navuseracl.html';
			$this->vista->dat='publico:navuser.html';
			$this->vista->nombre_session=$_SESSION['l_nombre'];
		}
		public function index()
		{
			//redirige al principio
			$this->redireccionar('acl/users/users/');
		}
		public function users($pagina = 1, $flag = 0)
		{
			$post = '';
			$this->acl->acceso($this->_metodo);
			//$parametros['estatus'] = array(1,2);
			$parametros['campos'] = array('permisos');
			$this->vista->title = 'Lista De Clientes';
			$this->vista->titulo_tabla = 'Lista De Usuarios';
			$this->lstUsuarios($pagina, 'acl/users/users/', $parametros);
			if($flag==0){
				$this->vista->contenido = 'publico:table.html';
				$this->vista->renderizar();
			}
			else{
				$this->vista->contenido = 'publico:tableajax.html';
				$this->vista->renderizar('blanco');
			}
		}
		//---------------------------------------------------------
		private function permisos_user_merge($entradas) {
			//$id = $this->filtrarInt($rol);
			$permisos_rol = $this->modelo->permisos_rol($entradas);
			$permisos_usuario = $this->modelo->permisos_user($entradas);
			$comun = array_intersect_key($permisos_usuario,$permisos_rol);
			$permisos = array_merge($permisos_rol,$comun);
			$dat = array();
			$cont = 0;
			foreach ($permisos as $key => $value){
				$cont++;
				$dat[$cont]=$value;
			}
			return $dat;	
		}

		public function permisos_usuario($usuario,$flag=0) {
			//multi rol falta
			$this->acl->acceso($this->_metodo);
			//------------------------------------------
			if( Session::get('permisos_usuario') ){
				$post = Session::get('permisos_usuario');
			}
			else{
				$post = array('modulo'=>'0');
			}
			if(!empty($_POST)){
				$data=array(
					'modulo'=>FILTER_SANITIZE_NUMBER_INT
				);
				$post = filter_input_array(INPUT_POST,$data);
				Session::set('permisos_usuario', $post);	
			}
			//------------------------------------------
			foreach ($post as $key => $value){
				$$key = $value;
			}
			//------------------------------------------
			$this->getParam($usuario,'acl/users/users/');
			$this->vista->title='Permisos de Usuario';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('nav','nav2','tablas')
			));
			$this->vista->setJS(array(
				'secciones'=>array('usuarios/permisosusuario')
			));
			
			//-------------------------------------------------------------
			$metodos = $this->modelo->optMetodos();
			//-------------------------------------------------------------
			$ali=$this->htmlcreator->getTag('a','Permisos de Usuario');
			$this->vista->navextra=$this->htmlcreator->getTag('li',$ali,array('class'=>'activo'));
			//-------------------------------------------------------------
			$this->htmlcreator->form=array(
				'Modulo'=>array('type'=>'select', 'id'=>'modulos', 'name'=>'modulos', 'data'=>$metodos, 'msgdata'=>'seleccione', 'seldata'=>$modulo)
			);
			//-------------------------------------------------------------
			$rol=$this->modelo->rol_user($usuario);
			$entradas = array('modulo'=>$modulo, 'user'=>$usuario, 'rol'=>$rol, 'flag'=>1);
			$lstpermisos = $this->permisos_user_merge($entradas);
			//$this->vista->name_rol = $this->modelo->getNameRol($entradas['rol']);
			//$this->vista->lstpermisos_user=$this->htmlcreator->getTag('h2','Permisos de Usuario');
			$contenido_tabla = $this->htmlcreator->getTabla($lstpermisos, '', array('class'=>'tablas') );
			$this->vista->lstpermisos_user = $contenido_tabla;
			//-------------------------------------------------------------
			$this->vista->idrol=$rol;
			$this->vista->iduser=$usuario;
			if( $flag==0 ) {
				$this->vista->contenido='permisos:permisos_user.html';
				$this->vista->renderizar();
			}
			else{
				$this->vista->contenido = $contenido_tabla;
				$this->vista->renderizar('blanco');
			}
		}

		public function metodo_permisos_user()
		{
			$datos=array(
					'user'=>FILTER_SANITIZE_NUMBER_INT,
					'permiso'=>FILTER_SANITIZE_NUMBER_INT,
					'valor'=>FILTER_SANITIZE_STRING	
				);
			$entradas=filter_input_array(INPUT_POST,$datos);
			$this->modelo->metodo_permisos_user($entradas);
		}
		//---------------------------------------------------------
	}
?>