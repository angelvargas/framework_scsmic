<?php
	class inicioController extends aclController
	{
		public function __construct()
		{
			parent::__construct();
			Session::acceso($this->__app, $this->__mod, $this->__sec, $this->__arg);
			//modulo, controlador, metodo
			//--------------------------------------------------------------------
			//$this->menu_principal($this->_controlador, $this->_modulo);
			//$this->menu_secundario($this->_controlador,$this->_metodo, $this->_modulo);
			//--------------------------------------------------------------------
			//$this->vista->dat='publico:navuseracl.html';
			$this->vista->dat='publico:navuser.html';
			$this->vista->nombre_session=$_SESSION['l_nombre'];
		}
		public function index()
		{
			//$this->acl->acceso($this->_metodo);
			$this->vista->title='ACL';
			$this->vista->contenido='acl:inicio.html';
			$this->vista->setCss(array(
									'template'=>array('estructura'),
									'publico'=>array('nav','nav2')
								));
			$this->vista->renderizar();
		}
	}
?>