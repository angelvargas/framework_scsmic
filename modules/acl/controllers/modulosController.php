<?php
	/*
	**@Title...........: controlador de modulos
	**@File............: modulosController.php
	**@version.........: 2.0.1
	**@Fecha Cambio....: 17-noviembre-2016
	**@Autor...........: Angel Florentino Vargas Pool
	**@Email...........: angelvargaspool@gmail.com
	**@desc............: correcciones del sistema
	*/
	class modulosController extends aclController
	{
		public function __construct()
		{
			parent::__construct();
			Session::acceso($this->__app, $this->__mod, $this->__sec, $this->__arg);
			$this->modelo=$this->loadModel('modulos',$this->_modulo);
			//--------------------------------------------------------------------
			//$this->menu_principal($this->_controlador, $this->_modulo);
			//$this->menu_secundario($this->_controlador,$this->_metodo, $this->_modulo);
			//--------------------------------------------------------------------
			//$this->vista->dat='publico:navuseracl.html';
			$this->vista->dat='publico:navuser.html';
			$this->vista->nombre_session=$_SESSION['l_nombre'];
		}
		public function index()
		{
			$this->redireccionar('acl');
		}
		public function modulos($pagina=false,$flag=0) {
			$this->acl->acceso($this->_metodo);
			//------------------------------------------
			if( Session::get('_modulos') ){
				$post = Session::get('_modulos');
			}
			else{
				$post = array('nombre'=>'','cod'=>'','app'=>'');
			}
			if(!empty($_POST)){
				$data=array(
					'nombre'=>FILTER_SANITIZE_STRING,
					'cod'=>FILTER_SANITIZE_STRING,
					'app'=>FILTER_SANITIZE_NUMBER_INT
				);
				$post = filter_input_array(INPUT_POST,$data);
				Session::set('_modulos', $post);	
			}
			//------------------------------------------
			foreach ($post as $key => $value){
				$$key = $value;
			}
			//------------------------------------------			
			$this->vista->title='Lista de Modulos Del Sistema';
			//-----------------------------------------------------------------------------------------
			//Css y Js de este modulo
			//-----------------------------------------------------------------------------------------
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('tablas','cssfrm','nav','nav2','jqueryuicss/jqueryui','validationEngine.jquery')
			));
			$this->vista->setJs(array(
				'template'=>array('pluginsearch'),
				'publico'=>array('jquery-ui-1.10.4.custom.min','jquery.validationEngine-es','jquery.validationEngine'),
				'secciones'=>array('modulos/lstmodulos')
			));
			//------------------------------------------------------
			$this->paginator->mostrar=10;
            $this->paginator->pagina=$pagina;
			$datos_modulos=$this->modelo->lstModulos($post);
			$datOpt=$this->modelo->getApp();//par la lista de aplicaciones
			$this->vista->apps=$this->htmlcreator->getOptions($datOpt,0,'Selecciona Aplicacion');
			//------------------------------------------------------
			if($this->acl->permiso('agregar_modulo') || $this->acl->permiso('modificar_modulo')) {
				if($this->acl->permiso('agregar_modulo')){
					$boton=$this->htmlcreator->getTag('a','Agregar Modulo',array('href'=>'javascript:void(0);','class'=>'btnext addmod'));
				}
				$this->vista->frmtablaoculto='modulos:frm_modulo.html';
			}
			else{
				$boton='';
			}
			$this->vista->btnextra = $boton;
			$this->htmlcreator->form=array(
				'Nombre'=>array('type'=>'text', 'id'=>'nombre', 'name'=>'nombre', 'class'=>'selsearch', 'value'=>$nombre),
				'Key'=>array('type'=>'text', 'id'=>'cod', 'name'=>'cod', 'class'=>'selsearch', 'value'=>$cod),
				'Aplicacion'=>array('type'=>'select', 'id'=>'app', 'name'=>'app', 'data'=>$datOpt, 'msgdata'=>'Aplicacion', 'seldata'=>$app, 'class'=>'selsearch')
			);
			//-----------------------------------------------------------------------------------------
			$this->vista->tabla=$this->htmlcreator->getTabla($datos_modulos,'',array('class'=>'tablas'));
			$this->vista->paginador = $this->paginator->htmlpaginador('acl/modulos/modulos/', 7);
			$this->vista->infopag = $this->paginator->info();
			//-----------------------------------------------------------------------------------------
			if( $flag==0 ) {
				$this->vista->contenido = 'publico:table.html';
				$this->vista->renderizar();
			}
			else{
				$this->vista->contenido = 'publico:tableajax.html';
				$this->vista->renderizar('blanco');
			}
		}
		public function metodo_modulo()
		{
			if(!empty($_POST))
			{
				$data=array(
					'nombre'=>FILTER_SANITIZE_STRING,
					'llave'=>FILTER_SANITIZE_STRING,
					'id'=>FILTER_SANITIZE_NUMBER_INT,
					'menu'=>FILTER_SANITIZE_NUMBER_INT
				);
				$posts=filter_input_array(INPUT_POST,$data);
			}
			//var_dump($posts);
			$res=$this->modelo->metodo_modulo($posts);
			$this->vista->contenido=$res;
			$this->vista->renderizar('blanco');
		}
		public function getmodulo()
		{
			$data=array(
				'id'=>FILTER_SANITIZE_NUMBER_INT
			);
			$post=filter_input_array(INPUT_POST,$data);
			$res=$this->modelo->datos_modulo($post['id']);
			$this->vista->contenido=$res;
			$this->vista->renderizar('blanco');
		}
		public function editest()
		{
			$data=array(
				'mod'=>FILTER_SANITIZE_NUMBER_INT,
				'est'=>FILTER_SANITIZE_NUMBER_INT
			);
			$post=filter_input_array(INPUT_POST,$data);
			$res=$this->modelo->editest($post);
			$this->vista->contenido=$res;
			$this->vista->renderizar('blanco');
		}
	}
?>