<?php
	class permisosController extends aclController
	{
		public function __construct()
		{
			parent::__construct();
			Session::acceso($this->__app, $this->__mod, $this->__sec, $this->__arg);
			$this->modelo=$this->loadModel('permisos',$this->_modulo);
			//--------------------------------------------------------------------
			//$this->menu_principal($this->_controlador, $this->_modulo);
			//$this->menu_secundario($this->_controlador,$this->_metodo, $this->_modulo);
			//--------------------------------------------------------------------
			//$this->vista->dat='publico:navuseracl.html';
			$this->vista->dat='publico:navuser.html';
			$this->vista->nombre_session=$_SESSION['l_nombre'];
		}
		public function index() {
			//----------------------
			//redireccion al acl
			//----------------------
			$this->redireccionar('acl/');
		}
		public function permisos($pagina=false,$flag=0) {
			$this->acl->acceso($this->_metodo);
			//------------------------------------------
			if( Session::get('_permisos') ){
				$post = Session::get('_permisos');
			}
			else{
				$post = array('nombre'=>'','cod'=>'','mod'=>'');
			}
			if(!empty($_POST)){
				$data=array(
					'nombre'=>FILTER_SANITIZE_STRING,
					'cod'=>FILTER_SANITIZE_STRING,
					'mod'=>FILTER_SANITIZE_NUMBER_INT
				);
				$post = filter_input_array(INPUT_POST,$data);
				Session::set('_permisos', $post);	
			}
			//------------------------------------------
			foreach ($post as $key => $value){
				$$key = $value;
			}
			//------------------------------------------
			$this->vista->title='Lista de Permisos Del Sistema';
			$this->vista->titulo_tabla=$this->htmlcreator->getTag('h2','Lista de Permisos');
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('tablas','cssfrm','nav','nav2','jqueryuicss/jqueryui','validationEngine.jquery')
			));
			$this->vista->setJs(array(
				'template'=>array('pluginsearch'),
				'publico'=>array('jquery-ui-1.10.4.custom.min','jquery.validationEngine-es','jquery.validationEngine'),
				'secciones'=>array('permisos/lstpermisos')
			));
			//------------------------------------------------------
			$this->paginator->mostrar=20;
            $this->paginator->pagina=$pagina;
			$datos_permisos=$this->modelo->lstPermisos($post);
			$modulos=$this->modelo->optMetodos();
			$this->vista->lstmodulos=$this->htmlcreator->getOptions($modulos,0,'Selecciona');
			//------------------------------------------------------
			if($this->acl->permiso('agregar_permiso') || $this->acl->permiso('modificar_permiso')) {
				if($this->acl->permiso('agregar_permiso'))
					$boton=$this->htmlcreator->getTag('a','Agregar Permiso',array('href'=>'javascript:void(0);','class'=>'btnext addper'));

				$this->vista->frmtablaoculto='permisos:frm_permiso.html';
			}
			else {
				$boton='';
			}
			$this->vista->btnextra = $boton;
			if(empty($datos_permisos)) {
				$this->vista->tabla='No existe Roles';
			}
			else {
				$datMod = $this->modelo->getMod();
				//------------------------------------------------------
				$this->htmlcreator->form=array(
					'Nombre'=>array('type'=>'text', 'id'=>'nombre', 'name'=>'nombre', 'class'=>'inputsearch', 'value'=>$nombre),
					'Key'=>array('type'=>'text', 'id'=>'cod', 'name'=>'cod', 'class'=>'inputsearch', 'value'=>$cod),
					'Modulo'=>array('type'=>'select', 'id'=>'mod', 'name'=>'mod', 'data'=>$datMod, 'msgdata'=>'Seleccione', 'seldata'=>$mod, 'class'=>'selsearch')
				);
				//------------------------------------------------------
				$this->vista->tabla=$this->htmlcreator->getTabla($datos_permisos,'',array('class'=>'tablas'));
				$this->vista->paginador = $this->paginator->htmlpaginador('acl/permisos/permisos/', 7);
				$this->vista->infopag = $this->paginator->info();
			}

			if( $flag==0 ) {
				$this->vista->contenido = 'publico:table.html';
				$this->vista->renderizar();
			}
			else{
				$this->vista->contenido = 'publico:tableajax.html';
				$this->vista->renderizar('blanco');
			}
		}
		public function metodo_permisos()
		{
			if(!empty($_POST))
			{
				$data=array(
					'nombre'=>FILTER_SANITIZE_STRING,
					'llave'=>FILTER_SANITIZE_STRING,
					'id'=>FILTER_SANITIZE_NUMBER_INT,
					'modulo'=>FILTER_SANITIZE_STRING,
					'descripcion'=>FILTER_SANITIZE_STRING,
					'menu'=>FILTER_SANITIZE_NUMBER_INT
				);
				$posts=filter_input_array(INPUT_POST,$data);
			}
			//var_dump($posts);
			$res=$this->modelo->metodo_permisos($posts);
			$this->vista->contenido=$res;
			$this->vista->renderizar('blanco');
		}
		public function getpermiso()
		{
			$data=array(
				'id'=>FILTER_SANITIZE_NUMBER_INT
			);
			$post=filter_input_array(INPUT_POST,$data);
			$res=$this->modelo->getpermiso($post['id']);
			$this->vista->contenido=$res;
			$this->vista->renderizar('blanco');
		}
		public function editest()
		{
			$data=array(
				'per'=>FILTER_SANITIZE_NUMBER_INT,
				'est'=>FILTER_SANITIZE_NUMBER_INT
			);
			$post=filter_input_array(INPUT_POST,$data);
			$res=$this->modelo->editest($post);
			$this->vista->contenido=$res;
			$this->vista->renderizar('blanco');
		}
	}
?>