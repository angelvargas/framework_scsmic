<?php
	class usersModel extends Model
	{
		public function __construct()
		{
			parent::__construct();
		}
		public function rol_user($id)
		{
			$conex=new Conectar();
			$conex->setEscapar($id);
			$sql='SELECT idperfil as tipo FROM usuario_cuenta uc INNER JOIN usuario usr ON uc.idusuario=usr.id_usuario WHERE id_usuario="'.$id.'" LIMIT 1';
			$conex->setQuery($sql);
			$res=$conex->getResultadoNumerico();
			if($res){
				$fila=$conex->getResultado('object');
				$rol=$fila->tipo;
			}else{
				$rol=0;
			}
			//echo $rol;
			return $rol;
		}
		public function permisos_user($entradas)
		{
			$conex = new Conectar();
			$user = $conex->setEscapar($entradas['user']);
			$modulo = $conex->setEscapar($entradas['modulo']);
			$sql = 'SELECT m.nombremodulo,m.menum,p.nombrepermiso,p.keypermiso,pu.valor,p.idpermiso,p.modulo,p.descripcion FROM modulos m INNER JOIN permisos p ON m.idmodulos = p.modulo LEFT JOIN permisos_usuario pu ON p.idpermiso = pu.permiso WHERE usuario="'.$user.'" AND p.estatus=1 AND m.estatus=1';
			if($modulo>0) {
				$sql.=' AND m.idmodulos="'.$modulo.'" ';
			}
			$sql.=' ORDER BY m.idmodulos,p.idpermiso';
			$conex->setQuery($sql);
			$res = $conex->getResultadoNumerico();
			$datos = array();
			//echo $sql;
			if($res)
			{
				while ($fila=$conex->getResultado('object'))
				{
					//$key=$fila->keypermiso;
					$key=$fila->keypermiso.'_'.$fila->modulo.'_'.$fila->menum;
					$valor=$fila->valor;
					$idpermiso=$fila->idpermiso;
					$datos[$key]['Modulo'] = $fila->nombremodulo;
					$datos[$key]['Permiso'] = $fila->nombrepermiso;
					$datos[$key]['descripcion'] = $fila->descripcion;
					$datos[$key]['Estado']=$fila->valor;
					//-------------------------------------------------------------------
					$checked1='';
					$checked2='';
					$estado='Denegado';
					if($valor==1)
					{
						$checked1='checked';
						$estado='Habilitado';
					}
					elseif($valor==0)
					{
						$checked2='checked';
						$estado='Denegado';
					}
					$datos[$key]['Estado'] = $estado;
					$datos[$key]['Permitir'] = $this->htmlcreator->getInput('radio',array('class'=>'permiso_rol','name'=>$idpermiso,'value'=>1,$checked1));
					$datos[$key]['Denegar'] = $this->htmlcreator->getInput('radio',array('class'=>'permiso_rol','name'=>$idpermiso,'value'=>0,$checked2));
					$datos[$key]['Ignorar'] = $this->htmlcreator->getInput('radio',array('class'=>'permiso_rol','name'=>$idpermiso,'value'=>"x"));
				}
			}
			return $datos;
		}
		public function metodo_permisos_user($datos)
		{
			$conex=new Conectar();
			$user=$conex->setEscapar($datos['user']);
			$permiso=$conex->setEscapar($datos['permiso']);
			$valor=$conex->setEscapar($datos['valor']);
			if($valor=='x')
			{
				$sql='DELETE FROM permisos_usuario WHERE permiso = "'.$permiso.'" AND usuario = "'.$user.'"';
			}
			elseif($valor==1 || $valor==0)
			{
				$sql='REPLACE INTO permisos_usuario SET usuario = "'.$user.'", permiso = "'.$permiso.'", valor = "'.$valor.'" ';
			}
			else
			{
				$sql='';
			}
			$conex->setQuery($sql);
			$correcto=$conex->getErrorDeQuery();
			if($correcto==1)
			{
				$mensaje="Permiso guardado";
			}
			else
			{
				$mensaje='Erro al guardar el permiso';
			}
			return $mensaje;
		}
		
	}
?>