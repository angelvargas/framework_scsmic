<?php
	/*
	**@Title.............: modelo de modulos
	**@File..............: modulosModel.php
	**@version...........: 2.0
	**@Fecha Cambio......: 22-agosto-2014
	**@Autor.............: Angel Florentino Vargas Pool
	**@Email.............: angelvargaspool@gmail.com
	**@desc..............: correcciones del sistema
	*/
	class modulosModel extends Model
	{
		public function __construct()
		{
			parent::__construct();
		}
		public function lstModulos($post='')
		{
			$sql='SELECT imagen,idmodulos,nombremodulo,keymodulo,menum,nameapli,estatus FROM modulos m LEFT JOIN aplication ap ON m.menum=ap.idapli WHERE idmodulos>0';
			if(!empty($post))
			{
				foreach ($post as $key => $value)
				{
					$$key=$this->setEscapar($value);
				}
				if(!empty($nombre))
				{
					//$this->paginator->pagina=1;
					$sql.=' AND nombremodulo LIKE "%'.$nombre.'%"';
				}
				if(!empty($cod))
				{
					//$this->paginator->pagina=1;
					$sql.=' AND keymodulo LIKE "%'.$cod.'%"';
				}	
				if(!empty($app) && $app>0)
				{
					//$this->paginator->pagina=1;
					$sql.=' AND menum="'.$app.'"';
				}	
			}
			$sql.=' ORDER BY estatus ASC,menum ASC,idmodulos';
			$this->sqlpag($sql);//modificar esta funcion que permita ver si el resultado obtenido es menor al total y manrlo a pagina1
			$res=$this->getResultadoNumerico();
			$cont=0;
			$datos=array();
			if($res)
			{
				while ($fila = $this->getResultado('object'))
				{
					$cont++;
					$estatus=$fila->estatus;
					$datos[$cont]['id']=$fila->idmodulos;
					$datos[$cont]['Nombre']=$fila->nombremodulo;
					$datos[$cont]['Key']=$fila->keymodulo;
					$datos[$cont]['Aplicacion']=$fila->nameapli;
					if(empty($fila->imagen)){
						$imagen='default.png';
					}else{
						$imagen=$fila->imagen;
					}
					//-----------------------------------------------------------------------------------------
					$ruta=_ROOT_.'public'._DS_.'img'._DS_.'iconosmenu'._DS_;
		    		$urlimg=_IMG_.'iconosmenu/';
					$r_img = file_exists($ruta.$imagen)?$urlimg.$imagen:$urlimg.'default.png';
					$datos[$cont]['acciones']['imagen']=$this->htmlcreator->getTagSimple('img',array('src'=>$r_img,'alt'=>$fila->nombremodulo));
					//-----------------------------------------------------------------------------------------
					if($this->acl->permiso('modificar_modulo'))
					{
						$imgmod=$this->htmlcreator->getTagSimple('img',array('src'=>_IMG_.'plantilla/modificar.png','alt'=>'[Modificar]')); 
						$datos[$cont]['acciones']['Modificar']=$this->htmlcreator->getTag('a',$imgmod,array('class'=>'editmod','data-ide'=>$fila->idmodulos,'href'=>'javascript:void(0);'));
					}
					if($this->acl->permiso('estatus_modulo'))
					{
						if($estatus==1)
						{
							$imgest=_IMG_.'plantilla/activo.png';
							$estnew=2;
						}
						else
						{
							$imgest=_IMG_.'plantilla/inactivo.png';
							$estnew=1;
						}
						$imgmod=$this->htmlcreator->getTagSimple('img',array('src'=>$imgest,'alt'=>'[Modificar]'));
						$datos[$cont]['acciones']['estatus']=$this->htmlcreator->getTag('a',$imgmod,array('class'=>'editest','data-ide'=>$fila->idmodulos,'data-est'=>$estnew,'href'=>'javascript:void(0);'));
					}			
				}
			}
			else
			{
				$datos[1]['id']='';
				$datos[1]['Nombre']='';
				$datos[1]['Key']='';
				$datos[1]['Aplicacion']='';
			}
			return $datos;
		}
		public function getApp()
		{
			$sql='SELECT idapli,nameapli FROM aplication';
			$this->setQuery($sql);
			$res=$this->getResultadoNumerico();
			$datos=array();
			$cont=0;
			if($res)
			{
				while($fila=$this->getResultado('object'))
				{
					$cont++;
					$datos[$cont]['id']=$fila->idapli;
					$datos[$cont]['nombre']=$fila->nameapli;
				}
			}
			return $datos;
		}
		public function metodo_modulo($post)
		{
			$clave='error';
			foreach ($post as $key => $value)
			{
				$$key=$value;
			}
			$rutafile=_ROOT_.'public'._DS_.'img'._DS_.'iconosmenu'._DS_;
			if(isset($_FILES['archivo']))
			{
				include(_LIBRERIAS_.'subir.imagen.class.php');
				include(_LIBRERIAS_.'thumb.class.php');
				$foto=new SubirImg($_FILES['archivo']);
				$foto->tiposmime=array("image/jpeg", "image/pjpeg", "image/gif", "image/png");
				$foto->guardarEn=$rutafile;
				$foto->peso=4194304;//peso maximo por foto es de 4 mb
				$foto->setSubirImg();
				if(!$foto->getError())
				{
					$nombre_foto=$foto->getNombre();
					list($width_s, $height_s, $type, $attr) = getimagesize($rutafile.$nombre_foto);//saca las propiedades de la imagen para redimensionarla
					if ($height_s > 40){
	                    $oResize = new ImageResize($rutafile.$nombre_foto);
	                    $oResize->resizeHeight(40);//reducir imagen
	                    $oResize->save($rutafile.$nombre_foto,$type);
	                }#if
	                list($width_s, $height_s, $type, $attr) = getimagesize($rutafile.$nombre_foto);
	                if ($width_s > 40){
	                    $oResize = new ImageResize($rutafile.$nombre_foto);
	                    $oResize->resizeWidth(40);//reducir imagen
	                    $oResize->save($rutafile.$nombre_foto,$type);
	                }
	                $archivo=$foto->getNombre();
				}
				else
				{
					$archivo='';
				}
			}
			else
			{
				$archivo='';
			}
			if($id>0)
			{
				$sqli='SELECT imagen FROM modulos WHERE idmodulos="'.$id.'"';
				$this->setQuery($sqli);
				$res=$this->getResultadoNumerico();
				if($res)
				{
					$fila=$this->getResultado('object');
					$imgold=$fila->imagen;
				}
				else
				{
					$imgold='';
				}
				$sql='UPDATE modulos SET nombremodulo="'.$nombre.'",keymodulo="'.$llave.'",menum="'.$menu.'"';
				if(!empty($archivo))
				{
					$sql.=',imagen="'.$archivo.'"';
				}
				$sql.=' WHERE idmodulos="'.$id.'"';
				$msgc='Modulo Modificado';
			}
			else
			{
				$sql='INSERT INTO modulos VALUES("0","'.$nombre.'","'.$llave.'","'.$menu.'","'.$archivo.'",1)';
				$msgc='Modulo guardado';
			}
			//echo $sql;
			$this->setQuery($sql);
			$correcto=$this->getErrorDeQuery($sql);
			if($correcto==1)
			{
				$msg='';
				if($id>0)
				{
					if(!empty($archivo))
					{
						if(!empty($imgold))
							unlink($rutafile.$imgold);
					}
				}
				$clave='correcto';
			}
			else
			{
				$msg='Error al guardar el Modulo';
			}
			$resp['mensaje']=$msg;
			$resp['clase']=$clave;
			return $resp;
		}
		public function datos_modulo($id)
		{
			$id=$this->setEscapar($id);
			$sql='SELECT nombremodulo,menum,keymodulo,idmodulos FROM modulos WHERE idmodulos="'.$id.'"'; 
			$this->setQuery($sql);
			$res=$this->getResultadoNumerico();
			$datos=array();
			if($res)
			{
				$fila=$this->getResultado('object');
				$datos['nombrem']=$fila->nombremodulo;
				$datos['menuapp']=$fila->menum;
				$datos['keym']=$fila->keymodulo;
			}
			$datos=json_encode($datos);
			return $datos;
		}
		public function editest($post)
		{
			foreach ($post as $key => $value)
			{
				$$key=$value;
			}
			$sql='UPDATE modulos SET estatus="'.$est.'" WHERE idmodulos="'.$mod.'"';
			$this->setQuery($sql);
			$correcto=$this->getErrorDeQuery($sql);
			if($correcto==1)
			{
				$msg='Estatus Cambiado Correctamente';
				$clave='correcto';
			}
			else
			{
				$msg='Error al guardar el Modulo';
			}
			$resp['mensaje']=$msg;
			$resp['clase']=$clave;
			return $resp;
		}
	}
?>