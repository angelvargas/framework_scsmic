<?php
	class permisosModel extends Model
	{
		public function __construct()
		{
			parent::__construct();
		}
		public function lstPermisos($post='')
		{
			$sql='SELECT b.idpermiso,a.nombremodulo,b.nombrepermiso,b.keypermiso,b.menup,modulo,b.estatus,b.descripcion FROM modulos a INNER JOIN permisos b ON a.idmodulos = b.modulo WHERE idpermiso>0';
			if(!empty($post))
			{
				foreach ($post as $key => $value)
				{
					$$key=$this->setEscapar($value);
				}
				if(!empty($nombre))
				{
					//$this->paginator->pagina=1;
					$sql.=' AND nombrepermiso LIKE "%'.$nombre.'%"';
				}
				if(!empty($cod))
				{
					//$this->paginator->pagina=1;
					$sql.=' AND keypermiso LIKE "%'.$cod.'%"';
				}	
				if(!empty($mod) && $mod>0)
				{
					//$this->paginator->pagina=1;
					$sql.=' AND modulo="'.$mod.'"';
				}	
			}
			$sql.=' ORDER BY b.estatus ASC,a.idmodulos,b.idpermiso';
			$this->sqlpag($sql);
			$res=$this->getResultadoNumerico();
			$cont=0;
			$datos=array();
			if($res)
			{
				while ($fila = $this->getResultado('object'))
				{
					$cont++;
					$estatus=$fila->estatus;
					$datos[$cont]['N°']=$fila->idpermiso;
					$datos[$cont]['Nombre']=$fila->nombrepermiso;
					$datos[$cont]['Key']=$fila->keypermiso;
					$datos[$cont]['Modulo']=$fila->nombremodulo;
					$datos[$cont]['Descripcion']=$fila->descripcion;
					$datos[$cont]['val']=$fila->menup==0?'x':'Visible';
					if($this->acl->permiso('modificar_permiso'))
					{
						$imgmod=$this->htmlcreator->getTagSimple('img',array('src'=>_IMG_.'plantilla/modificar.png','alt'=>'[Modificar]'));
						$datos[$cont]['acciones']['Modificar']=$this->htmlcreator->getTag('a',$imgmod,array('class'=>'editper','data-ide'=>$fila->idpermiso,'href'=>'javascript:void(0);'));	
					}
					if($this->acl->permiso('estatus_permiso'))
					{
						if($estatus==1)
						{
							$imgest=_IMG_.'plantilla/activo.png';
							$estnew=2;
						}
						else
						{
							$imgest=_IMG_.'plantilla/inactivo.png';
							$estnew=1;
						}
						$imgmod=$this->htmlcreator->getTagSimple('img',array('src'=>$imgest,'alt'=>'[Modificar]'));
						$datos[$cont]['acciones']['estatus']=$this->htmlcreator->getTag('a',$imgmod,array('class'=>'editest','data-ide'=>$fila->idpermiso,'data-est'=>$estnew,'href'=>'javascript:void(0);'));
					}	
						
				}
			}
			else
			{
				$datos[1]['N°']='';
				$datos[1]['Nombre']='';
				$datos[1]['Key']='';
				$datos[1]['Modulo']='';
				$datos[1]['val']='';
			}
			/*
			if($this->acl->permiso('modificar_permiso'))
			{
				$imgmod=$this->htmlcreator->getTagSimple('img',array('src'=>_IMG_.'plantilla/modificar.png','alt'=>'[Modificar]'));
				$datos[$cont]['acciones']['Modificar']=$this->htmlcreator->getTag('a',$imgmod,array('class'=>'editper','data-ide'=>$fila->idpermiso,'href'=>'javascript:void(0);'));	
			}
			*/
			return $datos;
		}
		public function getMod()
		{
			$sql='SELECT idmodulos,nombremodulo FROM modulos WHERE estatus=1';
			$this->setQuery($sql);
			$res=$this->getResultadoNumerico();
			$datos=array();
			$cont=0;
			if($res)
			{
				while($fila=$this->getResultado('object'))
				{
					$cont++;
					$datos[$cont]['id']=$fila->idmodulos;
					$datos[$cont]['nombre']=$fila->nombremodulo;
				}
			}
			return $datos;
		}
		public function metodo_permisos($post)
		{
			$clave='error';
			foreach ($post as $key => $value)
			{
				$$key=$value;
			}
			if($id>0)
			{
				$sql='UPDATE permisos SET modulo="'.$modulo.'",nombrepermiso="'.$nombre.'",keypermiso="'.$llave.'",menup="'.$menu.'",descripcion="'.$descripcion.'" WHERE idpermiso="'.$id.'"';
				$msgc='Permiso Modificado';
			}
			else
			{
				$sql='INSERT INTO permisos VALUES("0","'.$modulo.'","'.$nombre.'","'.$llave.'","'.$menu.'","'.$descripcion.'",1)';
				$msgc='Permiso guardado';
			}
			//echo $sql;
			$this->setQuery($sql);
			$correcto=$this->getErrorDeQuery();
			if($correcto==1)
			{
				$msg=$msgc;
				$clave='correcto';
			}
			else
			{
				$msg='Error al guardar el permiso';
			}
			$resp['mensaje']=$msg;
			$resp['clase']=$clave;
			return $resp;
		}
		public function getpermiso($id)
		{
			$id=$this->setEscapar($id);
			$sql='SELECT nombrepermiso,keypermiso,menup,modulo,descripcion FROM permisos WHERE idpermiso="'.$id.'"';
			$this->setQuery($sql);
			$res=$this->getResultadoNumerico();
			$datos=array();
			if($res)
			{
				$fila=$this->getResultado('object');
				$datos['nombrep']=$fila->nombrepermiso;
				$datos['key']=$fila->keypermiso;
				$datos['modulo']=$fila->modulo;
				$datos['descripcion']=$fila->descripcion;
				$datos['menu']=$fila->menup;
			}
			$datos=json_encode($datos);
			return $datos;
		}
		public function editest($post)
		{
			$clave="error";
			foreach ($post as $key => $value)
			{
				$$key=$value;
			}
			$sql='UPDATE permisos SET estatus="'.$est.'" WHERE idpermiso="'.$per.'"';
			$this->setQuery($sql);
			$correcto=$this->getErrorDeQuery();
			if($correcto==1)
			{
				$msg='Estatus Cambiado Correctamente';
				$clave='correcto';
			}
			else
			{
				$msg='Error al modificar el estatus';
			}
			$resp['mensaje']=$msg;
			$resp['clase']=$clave;
			return $resp;
		}
	}
?>