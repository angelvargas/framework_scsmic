<?php
	class rolModel extends Model
	{
		public function __construct()
		{
			parent::__construct();
		}
		//--------------------------------------------------------------------------------------------
		public function roles($post) {
			$sql='SELECT idrol,imagenrol,rolnombre,namecategory FROM rol a LEFT JOIN categoryuser b ON a.categoria=b.idcategory WHERE categoria>1';
			if( !empty($post) ){
				foreach ($post as $key => $value) {
					$$key = $value;
				}
				if (isset($selcat) && !empty($selcat)) {
					$sql.=' AND a.categoria="'.$selcat.'"';
				}

				if (isset($txtrol) && !empty($txtrol)) {
					$sql.=' AND ('.$this->textsearch(array('rolnombre'), $txtrol, 'OR').')';
				}
			}
			$this->sqlpag($sql);
			$res=$this->getResultadoNumerico();
			$datos=array();
			$cont=0;
			if($res) {
				while ( $fila=$this->getResultado('object') ) {
					$cont++;
					$datos[$cont]['id']=$fila->idrol;
					$datos[$cont]['Rol']=$fila->rolnombre;
					$datos[$cont]['Categoria']=$fila->namecategory;
					if(empty($fila->imagenrol)){
						$imagen='inactivo.png';
					}else{
						$imagen=$fila->imagenrol;
					}
					$datos[$cont]['imagen']=$this->htmlcreator->getTagSimple('img',array('src'=>_IMG_.'iconosrol/'.$imagen,'alt'=>$fila->rolnombre));
					if($this->acl->permiso('modificar_rol')) {
						$imgmod=$this->htmlcreator->getTagSimple('img',array('src'=>_IMG_.'plantilla/modificar.png','alt'=>'[Modificar]')); 
						$datos[$cont]['acciones']['Modificar']=$this->htmlcreator->getTag('a',$imgmod,array('class'=>'editrol','data-ide'=>$fila->idrol,'href'=>'javascript:void(0);'));
					}
					$imgpermisos=$this->htmlcreator->getTagSimple('img',array('src'=>_IMG_.'plantilla/permisos.png','alt'=>'[Permisos]'));
					$datos[$cont]['acciones']['permisos']=$this->htmlcreator->getTag('a',$imgpermisos,array('href'=>_PATH_ABS_.'acl/rol/permisos_rol/'.$fila->idrol.'/'));
				}
			}
			else{
				$datos[0]['N°']='';
				$datos[0]['Rol']='';
				$datos[0]['Categoria']='';
			}
			return $datos;
		}
		public function metodorol($posts) {
			foreach ($posts as $key => $value) {
				$$key=$value;
			}
			$rutafile=_ROOT_.'public'._DS_.'img'._DS_.'iconosrol'._DS_;
			if(isset($_FILES['archivo'])) {
				include(_LIBRERIAS_.'subir.imagen.class.php');
				include(_LIBRERIAS_.'thumb.class.php');
				$foto=new SubirImg($_FILES['archivo']);
				$foto->tiposmime=array("image/jpeg", "image/pjpeg", "image/gif", "image/png");
				$foto->guardarEn=$rutafile;
				$foto->peso=4194304;//peso maximo por foto es de 4 mb
				$foto->setSubirImg();
				if(!$foto->getError())
				{
					$nombre_foto=$foto->getNombre();
					list($width_s, $height_s, $type, $attr) = getimagesize($rutafile.$nombre_foto);//saca las propiedades de la imagen para redimensionarla
					if ($height_s > 40){
	                    $oResize = new ImageResize($rutafile.$nombre_foto);
	                    $oResize->resizeHeight(40);//reducir imagen
	                    $oResize->save($rutafile.$nombre_foto,$type);
	                }#if
	                list($width_s, $height_s, $type, $attr) = getimagesize($rutafile.$nombre_foto);
	                if ($width_s > 40){
	                    $oResize = new ImageResize($rutafile.$nombre_foto);
	                    $oResize->resizeWidth(40);//reducir imagen
	                    $oResize->save($rutafile.$nombre_foto,$type);
	                }
	                $archivo=$foto->getNombre();
				}
				else
				{
					$archivo='';
				}
			}
			else
			{
				$archivo='';
			}
			if($id>0)
			{
				$sqli='SELECT imagenrol FROM rol WHERE idrol="'.$id.'"';
				$this->setQuery($sqli);
				$res=$this->getResultadoNumerico();
				if($res)
				{
					$fila=$this->getResultado('object');
					$imgold=$fila->imagenrol;
				}
				else
				{
					$imgold='';
				}
				$sql='UPDATE rol SET rolnombre="'.$nombre.'",categoria="'.$categoria.'"';
				if(!empty($archivo))
				{
					$sql.=',imagenrol="'.$archivo.'"';
				}
				$sql.=' WHERE idrol="'.$id.'"';
				$msgc='Rol Modificado';
			}
			else
			{
				$sql='INSERT INTO rol VALUES("0","'.$nombre.'","'.$archivo.'","'.$categoria.'")';
				$msgc='Rol Guardado';
			}
			$this->setQuery($sql);
			$correcto=$this->getErrorDeQuery($sql);
			if($correcto==1)
			{
				$msg='';
				if($id>0)
				{
					if(!empty($archivo))
					{
						if(!empty($imgold))
							unlink($rutafile.$imgold);
					}
				}
			}
			else
			{
				$msg='Error al guardar el rol';
			}
			$resp['mensaje']=$msg;
			$resp['clase']=$clave;
			return $resp;
		}
		public function categorias()
		{
			$sql='SELECT namecategory,idcategory FROM categoryuser WHERE idcategory>1';
			$this->setQuery($sql);
			$res=$this->getResultadoNumerico();
			$datos=array();
			$cont=0;
			if($res)
			{
				while ($fila=$this->getResultado('object'))
				{
					$cont++;
					$datos[$cont]['id']=$fila->idcategory;
					$datos[$cont]['nombre']=$fila->namecategory;
				}
			}
			return $datos;
		}
		public function datosRol($id)
		{
			$id=$this->setEscapar($id);
			$sql='SELECT idrol,rolnombre,categoria FROM rol WHERE idrol="'.$id.'"';
			$this->setQuery($sql);
			$res=$this->getResultadoNumerico();
			$datos=array();
			if($res)
			{
				$fila=$this->getResultado('object');
				$datos['nombre']=$fila->rolnombre;
				$datos['categoria']=$fila->categoria;
				
			}
			$datos=json_encode($datos);
			return $datos;		
		}
		public function getNameRol($idrol)
		{
			$conex=new Conectar();
			$sql='SELECT rolnombre FROM rol WHERE idrol="'.$idrol.'"';
			$conex->setQuery($sql);
			$res=$conex->getResultadoNumerico();
			if($res){
				$fila=$conex->getResultado('object');
				$nombre=$fila->rolnombre;
			}else{
				$nombre='';
			}
			return $nombre;
		}
		public function all_permisos($modulo)
		{
			$conex=new Conectar();
			$sql='SELECT m.nombremodulo,m.menum,p.nombrepermiso,p.keypermiso,p.idpermiso,p.modulo,p.descripcion FROM modulos m INNER JOIN permisos p ON m.idmodulos = p.modulo WHERE p.estatus=1 AND m.estatus=1';
			if($modulo>0)
			{
				$sql.=' AND m.idmodulos="'.$modulo.'"';
			}
			$sql.=' ORDER BY m.idmodulos,p.idpermiso';
			//echo $sql;
			$conex->setQuery($sql);
			$res=$conex->getResultadoNumerico();
			$datos=array();
			if($res)
			{
				while ($fila=$conex->getResultado('object'))
				{
					$key=$fila->keypermiso.'_'.$fila->modulo.'_'.$fila->menum;
					//$key=$fila->keypermiso;
					$idpermiso=$fila->idpermiso;
					$datos[$key]['Modulo'] = $fila->nombremodulo;
					$datos[$key]['Permiso'] = $fila->nombrepermiso;
					$datos[$key]['descripcion'] = $fila->descripcion;
					$datos[$key]['Estado'] = 'x';
					$datos[$key]['Permitir'] = $this->htmlcreator->getInput('radio',array('class'=>'permiso_rol','name'=>$idpermiso,'value'=>1));
					$datos[$key]['Denegar'] = $this->htmlcreator->getInput('radio',array('class'=>'permiso_rol','name'=>$idpermiso,'value'=>0));
					$datos[$key]['Ignorar'] = $this->htmlcreator->getInput('radio',array('class'=>'permiso_rol','name'=>$idpermiso,'value'=>"x",'checked'));
				}
			}
			else
			{
				$datos[0]['Modulo'] = '';
			}
			return $datos;
		}
		//--------------------------------------------------------------------------------------------
		public function metodo_permisos_rol($datos){
			$conex=new Conectar();
			$rol=$conex->setEscapar($datos['rol']);
			$permiso=$conex->setEscapar($datos['permiso']);
			$valor=$conex->setEscapar($datos['valor']);
			if($valor=='x'){
				$sql='DELETE FROM permisos_rol WHERE permiso = "'.$permiso.'" AND rol = "'.$rol.'"';
			}elseif($valor==1 || $valor==0){
				$sql='REPLACE INTO permisos_rol SET rol = "'.$rol.'", permiso = "'.$permiso.'", valor = "'.$valor.'" ';
			}else{
				$sql='';
			}
			$conex->setQuery($sql);
			$correcto=$conex->getErrorDeQuery();
			if($correcto==1){
				$mensaje="Permiso guardado";
			}else{
				$mensaje='Erro al guardar el permiso';
			}
			return $mensaje;
		}
	}
?>