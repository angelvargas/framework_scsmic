<?php
	//include_once _PATH_.'grid/dataGrid.php';
	//include_once _PATH_.'grid/dataSource.php';
	ini_set('display_errors',1);
	error_reporting(E_ALL);
	class respaldodbController extends mantenimientoController {
		public function __construct() {
			parent::__construct();
			Session::acceso($this->__app, $this->__mod, $this->__sec, $this->__arg);
			$this->vista->img=_IMG_;
			$this->vista->nombre_session=$_SESSION['l_nombre'];
		}
		public function index() {
			$this->redireccionar('mantenimiento/respaldodb/lstRespaldoDat');
		}
		/**
		----------------------------------------------------------
		FUNCION PARA RESPALDAR Y BORRAR DATOS Y ESTRUCTURA
		----------------------------------------------------------			
		**/
		public function lstrespaldodat($pagina=1,$flag=0) {
			$this->acl->acceso($this->_metodo);
			//----------------------------------------------------------
			$this->vista->title = 'Respaldos de Base de Datos';
			$this->vista->titulo_tabla = 'Lista de Respaldos de Datos y Estructura de la BD ';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('tablas','formulario','nav','nav2','jqueryuicss/jqueryui','validationEngine.jquery','grid'),
			));
			//----------------------------
			//---paginador----------------------------------------------------
			$this->paginator->mostrar = 15;
			$this->paginator->pagina = $pagina;
			$mostrar = 15; 
			if($pagina>1){
				$inicio = ($mostrar)*($pagina-1)+1;
				$mostrar = $mostrar * $pagina;
			}
			else{
				$inicio=1;
			}
			//------------------------------------------------
			$this->vista->btnextra = $this->htmlcreator->getTag('a', 'Respaldar Datos', array('href'=>_PATH_ABS_.'mantenimiento/respaldodb/respaldardat/', 'class'=>'btnext'));
			//------------------------------------------------
			$bak_dir = _ROOT_.'respaldoBD'._DS_.'datos_estructura';
			$datos = array();
			$dir = opendir($bak_dir);
			// Se leen todos los ficheros de la carpeta
			while ($salida_db_sqlE = readdir($dir)) {
				// SE almacean los elementos en la carpeta de respaldo
				if( $salida_db_sqlE != "." && $salida_db_sqlE != ".." && $salida_db_sqlE != ".gitignore") {
					// Se muestran los ficheros
					$datos[]['file'] = $salida_db_sqlE;
				}
			}
			array_multisort($datos, SORT_DESC);

			if(count($datos)>=1) {
				$img_descargar =  $this->htmlcreator->getTag('i', 'archive', array('class'=>'material-icons indigo500'));
				$img_eliminar =  $this->htmlcreator->getTag('i', 'delete', array('class'=>'material-icons red500'));
				foreach($datos as $data => $file) {
					$patron = '/\w+-(\d{4})(\d{2})(\d{2})-(\d{2})(\d{2})(\d{2})[\w|.]+/i';
					$val = preg_replace($patron, '$1/$2/$3 $4:$5:$6', $file['file']);
					$datos[$data]['fecha'] = $val;
					$datos[$data]['size'] = round(((filesize($bak_dir.'/'.$file['file'])/1024)/1024)*100)/100  . ' MB';
					$datos[$data]['tareas']['descargar'] = $this->htmlcreator->getTag('a', $img_descargar, array('href'=>_PATH_ABS_.'mantenimiento/respaldodb/descargarDat/'.$file['file']) );
					$datos[$data]['tareas']['eliminar'] = $this->htmlcreator->getTag('a', $img_eliminar, array('href'=>_PATH_ABS_.'mantenimiento/respaldodb/eliminarDat/'.$file['file']) );
				}
			}
			$data = array();
			for ($i=$inicio-1; $i <= $mostrar-1; $i++) {
				if (array_key_exists($i, $datos))
					array_push($data, $datos[$i]);
			}
			$this->paginator->totaldatos = count($datos);
			//-----------------------------------------------------
			$this->vista->paginador = $this->paginator->htmlpaginador('mantenimiento/respaldodb/lstrespaldodat/',7);
			$this->vista->infopag = $this->paginator->info();
			$this->vista->tabla = $this->htmlcreator->getTabla($data, 'xxxx', array('class'=>'tablas'));
			//-----------------------------------------------------
			if($flag==0) {
				$this->vista->contenido = 'publico:table.html';
				//$this->vista->tabla=$dg;
				$this->vista->renderizar();
			}
			else {
				$this->vista->contenido = 'publico:tableajax.html';
				$this->vista->renderizar('blanco');
			}
		}
		public function respaldardat() {
			##############Fecha y carpeta de salida
			$fecha_hoy = date("Ymd-His"); //fecha en que se hace el respaldo
			$bak_dir = _ROOT_.'respaldoBD'._DS_.'datos_estructura';    //directorio donde se guarda el respaldo
			##############Base de datos y tablas
			$db_host = _LOCALHOST_;
			$db_user = _USUARIO_;
			$db_pass = _PASS_;
			$db_name = _DB_;
			##############Archivos de Salida (extension que genera al guardar)
			$name_fichero = $db_name.'Dat-'.$fecha_hoy;
			$salida_db_sql = $bak_dir._DS_.$name_fichero.'.sql'; // Datos
			//echo $salida_db_sql;
			//$salida_db_tar = $bak_dir._DS_.$name_fichero.'.zip';
			# Dumps
			##############
			/*
				if (DIRECTORY_SEPARATOR == '/') {
				// linux
				}

				if (DIRECTORY_SEPARATOR == '\\') {
				// windows
				}
			*/
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				$dump = "C:"._DS_."xampp"._DS_."mysql"._DS_."bin"._DS_."mysqldump --result-file=$salida_db_sql --default-character-set=utf8 --add-locks=FALSE --disable-keys=FALSE --extended-insert --user=$db_user --password=$db_pass $db_name";
			} else {
				$dump = "mysqldump --result-file=$salida_db_sql --default-character-set=utf8 --add-locks=FALSE --disable-keys=FALSE --extended-insert --user=$db_user --password=$db_pass $db_name";
			}
			##############
			$a = system($dump);
			$this->redireccionar('mantenimiento/respaldodb/lstrespaldodat/');
			$mensaje='Respaldo Exitoso';
		}
		public function descargarDat($archivo) {
			$file = _ROOT_.'respaldoBD'._DS_.'datos_estructura'._DS_.$archivo;
			header("Content-disposition: attachment; filename=$archivo");
			header("Content-type: application/octet-stream");
			readfile($file);
			//$this->redireccionar('mantenimiento/respaldodb/lstRespaldoDat');
		}
		public function eliminarDat($archivo) {
			$file = _ROOT_.'respaldoBD'._DS_.'datos_estructura'._DS_.$archivo;
			unlink($file);
			$this->redireccionar('mantenimiento/respaldodb/lstRespaldoDat');
		}
		/**
		-------------------------------------------------------
		FUNCION PARA RESPALDAR Y BORRAR SOLO LA ESTRUCTURA
		-------------------------------------------------------
		**/
		public function lstrespaldoest($pagina=1,$flag=0) {
			$this->acl->acceso($this->_metodo);
			//----------------------------------------------------------
			$this->vista->title = 'Respaldos de Base de Datos';
			$this->vista->titulo_tabla = 'Lista de Respaldos de Estructura de la BD ';
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('tablas','formulario','nav','nav2','jqueryuicss/jqueryui','validationEngine.jquery','grid'),
			));
			//----------------------------
			//---paginador----------------------------------------------------
			$this->paginator->mostrar = 15;
			$this->paginator->pagina = $pagina;
			$mostrar = 15; 
			if($pagina>1){
				$inicio = ($mostrar)*($pagina-1)+1;
				$mostrar = $mostrar * $pagina;
			}
			else{
				$inicio=1;
			}
			//------------------------------------------------
			$this->vista->btnextra = $this->htmlcreator->getTag('a', 'Respaldar Datos', array('href'=>_PATH_ABS_.'mantenimiento/respaldodb/respaldarEst/', 'class'=>'btnext'));
			//------------------------------------------------
			$bak_dir = _ROOT_.'respaldoBD'._DS_.'estructura';
			$datos = array();
			$dir = opendir($bak_dir);
			// Se leen todos los ficheros de la carpeta
			while ($salida_db_sqlE = readdir($dir)) {
				// SE almacean los elementos en la carpeta de respaldo
				if( $salida_db_sqlE != "." && $salida_db_sqlE != ".." && $salida_db_sqlE != ".gitignore") {
					// Se muestran los ficheros
					$datos[]['file'] = $salida_db_sqlE;
				}
			}
			array_multisort($datos, SORT_DESC);

			if(count($datos)>=1) {
				$img_descargar =  $this->htmlcreator->getTag('i', 'archive', array('class'=>'material-icons indigo500'));
				$img_eliminar =  $this->htmlcreator->getTag('i', 'delete', array('class'=>'material-icons red500'));
				foreach($datos as $data => $file) {
					$patron = '/\w+-(\d{4})(\d{2})(\d{2})-(\d{2})(\d{2})(\d{2})[\w|.]+/i';
					$val = preg_replace($patron, '$1/$2/$3 $4:$5:$6', $file['file']);
					$datos[$data]['fecha'] = $val;
					$datos[$data]['size'] = round(((filesize($bak_dir.'/'.$file['file'])/1024)/1024)*100)/100  . ' MB';
					$datos[$data]['tareas']['descargar'] = $this->htmlcreator->getTag('a', $img_descargar, array('href'=>_PATH_ABS_.'mantenimiento/respaldodb/descargarEst/'.$file['file']) );
					$datos[$data]['tareas']['eliminar'] = $this->htmlcreator->getTag('a', $img_eliminar, array('href'=>_PATH_ABS_.'mantenimiento/respaldodb/eliminarEst/'.$file['file']) );
				}
			}
			$data = array();
			for ($i=$inicio-1; $i <= $mostrar-1; $i++) {
				if (array_key_exists($i, $datos))
					array_push($data, $datos[$i]);
			}
			$this->paginator->totaldatos = count($datos);
			//-----------------------------------------------------
			$this->vista->paginador = $this->paginator->htmlpaginador('mantenimiento/respaldodb/lstRespaldoEst/',7);
			$this->vista->infopag = $this->paginator->info();
			$this->vista->tabla = $this->htmlcreator->getTabla($data, 'xxxx', array('class'=>'tablas'));
			//-----------------------------------------------------
			if($flag==0) {
				$this->vista->contenido = 'publico:table.html';
				//$this->vista->tabla=$dg;
				$this->vista->renderizar();
			}
			else {
				$this->vista->contenido = 'publico:tableajax.html';
				$this->vista->renderizar('blanco');
			}
		}
		public function respaldarest() {
			############## Fecha y carpeta de salida
			$fecha_hoy = date("Ymd-His"); //fecha en que se hace el respaldo
			$bak_dir = _ROOT_.'respaldoBD'._DS_.'estructura';    //directorio donde se guarda el respaldo
			############## Base de datos y tablas
			$db_host = _LOCALHOST_;
			$db_user = _USUARIO_;
			$db_pass = _PASS_;
			$db_name = _DB_;
			############## Archivos de Salida (extension que genera al guardar)
			$salida_db_sqlE = $bak_dir._DS_.$db_name.'Est-'.$fecha_hoy.'.sql'; //Estructura
			# Dumps
			########## Estructura
			
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				$dump = "C:"._DS_."xampp"._DS_."mysql"._DS_."bin"._DS_."mysqldump --result-file=$salida_db_sqlE --default-character-set=utf8 --add-locks=FALSE --disable-keys=FALSE --no-data --user=$db_user --password=$db_pass $db_name";
			} else {
				$dump = "mysqldump --result-file=$salida_db_sqlE --default-character-set=utf8 --add-locks=FALSE --disable-keys=FALSE --no-data --user=$db_user --password=$db_pass $db_name";
			}
			$a = system($dump);
			$this->redireccionar('mantenimiento/respaldodb/lstrespaldoest/');
		}
		public function descargarEst($archivo){
			$file = _ROOT_.'respaldoBD'._DS_.'estructura'._DS_.$archivo;
			header("Content-disposition: attachment; filename=$archivo");
			header("Content-type: application/octet-stream");
			readfile($file);
		}
		public function eliminarEst($archivo){
			$file = _ROOT_.'respaldoBD'._DS_.'estructura'._DS_.$archivo;
			unlink($file);
			$this->redireccionar('mantenimiento/respaldodb/lstRespaldoEst');
		}
	}
?>