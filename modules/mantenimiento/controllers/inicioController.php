<?php
	class inicioController extends mantenimientoController
	{
		public function __construct()
		{
			parent::__construct();
			Session::acceso($this->__app, $this->__mod, $this->__sec, $this->__arg);
			//$this->vista->nav=$this->acl->menuPrincipal($this->_controlador,5,$this->_modulo);
			//$this->vista->nav2=$this->acl->menuSecundario($this->_controlador,$this->_metodo,$this->_modulo);
			//$this->vista->dat='publico:navuseracl.html';
			//$this->vista->dat1='publico:navapps.html';
			//$this->vista->dat='publico:navuser.html';
			$this->vista->img=_IMG_;
			$this->vista->nombre_session=$_SESSION['l_nombre'];
		}
		public function index()
		{
			$this->vista->title='Mantenimiento del Sistema';
			$this->vista->contenido='mantenimiento:inicio.html';
			$this->vista->setCss(array(
									'template'=>array('estructura'),
									'publico'=>array('nav','nav2')
								));
			$this->vista->renderizar();
		}
	}
?>
