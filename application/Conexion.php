<?php
//conectarDB()
//prueba commit
class Conectar
{
	//private $resultado;
	//public $devolverError;
	//private static $_instace;
    private $_pdo, $_error, $resultado, $_driver, $_server, $_user, $_pass, $_base, $_charset;
    //$_query, 
    //$_results, 
    //$_count, 
    //$_error = false;
	public $_options, $devolverError; 
	public function __construct( $datadb = array() )
	{
		$this->devolverError = false;
		
		$this->_server = !isset($datadb['server'])?_LOCALHOST_:$datadb['server'];
		$this->_user = !isset($datadb['user'])?_USUARIO_:$datadb['user'];
		$this->_base = !isset($datadb['db'])?_DB_:$datadb['db'];
		$this->_pass = !isset($datadb['pass'])?_PASS_:$datadb['pass'];
		$this->_driver = !isset($datadb['driver'])?_DRIVER_:$datadb['driver'];
		$this->_charset = !isset($datadb['encode'])?_CHARSET_:$datadb['encode'];
		
		$dsn = $this->_driver.':dbname='.$this->_base.';host='.$this->_server.';charset='.$this->_charset.';';
		$usuario = $this->_user;
		$contraseña = $this->_pass;
		try {
			$this->_pdo = new PDO($dsn, $usuario, $contraseña);
			$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			if($this->_driver == 'mysql') {
				$this->_pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES '.$this->_charset);
				$this->_pdo->exec("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
				$this->_pdo->exec("SET SESSION sql_mode='ALLOW_INVALID_DATES'");
				$this->_pdo->exec("SET lc_time_names ='"._TIME_NAMES_."'");
				$this->_pdo->exec('SET NAMES '.$this->_charset);
			}
		}
		catch(PDOException $e) {
			$this->_error = $e->getCode().'-'.$e->getMessage();
			$this->getError();
		}
	}

	public function getError(){
		$err='Error -';
		if($this->devolverError){
			return 'Error -'.$this->_error;
		}else{
			die('Error -'.$this->_error);
		}
	}
	private function cleanInput($input) {
		$search = array(
		'@<script[^>]*?>.*?</script>@si',   // Strip out javascript
		'@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
		'@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
		'@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
		);
		$output = preg_replace($search, '', $input);
		return $output;
	}
	/**
	private function sanitize($input) {
	    if (is_array($input)) {
	        foreach($input as $var=>$val) {
	            $output[$var] = sanitize($val);
	        }
	    }
	    else {
	        if (get_magic_quotes_gpc()) {
	            $input = stripslashes($input);
	        }
	        $input  = cleanInput($input);
	        $output = mysql_real_escape_string($input);
	    }
	    return $output;
	}
	**/
	public function setEscapar($cadena)
	{
		$cadena  = self::cleanInput($cadena);
		$cadena = addcslashes( $cadena, '/&%$@-<>');
		//$cadena = addcslashes( $this->_pdo->quote($cadena), '/&%$@-<>');//not working
		return $cadena;
	}
	public function setQuery($query) {
		//echo $query.'<br><br>';
		$this->resultado = $this->_pdo->query($query);
		if(!$this->resultado) {
			$this->getError();
		}
	}
	public function first()
    {
    	$datos = $this->resultado->fetch(PDO::FETCH_OBJ); 
        $temp = $datos;
        return $temp[0];
    }

    /**
    Experimental
    **/
    public function insert($table, $values = array())
    {
    	/*
        // check if $values set
        if(count($values)) {
           
            $fields = array_keys($values);
            
            $value = '';
          
            $x = 1;
            foreach($values as $field) {
                // add new value
                $value .="?";
                
                if($x < count($values)) {
                    // add comma between values
                    $value .= ", ";
                }
                $x++;
            }
             // generate sql statement
            $sql = "INSERT INTO {$table} (`" . implode('`,`', $fields) ."`)";
            $sql .= " VALUES({$value})";
            // check if query is not have an error

            if(!$this->query($sql, $values)->error()) {
                    return true;
            }
        }
        
        return false;
        */
    }
    public function update($table, $values = array(), $where = array())
    {
    	/*
        $set = ''; // initialize $set
        $x = 1;
        // initialize feilds and values
        foreach($values as $i => $row) {
            $set .= "{$i} = ?";
            // add comma between values
            if($x < count($values)) {
                $set .= " ,";
            }
            $x++;
        }
        // generate sql statement
        $sql = "UPDATE {$table} SET {$set} WHERE " . implode(" ", $where);
        // check if query is not have an error
        if(!$this->query($sql, $values)->error()) {
            return true;
        }
        return false;
        */
    }
    public function delete($table, $where = array())
    {
    	/*
        // check if $where is set
        if(count($where)) {
            // call action method
            if($this->action($table, "DELETE", $where)) {
                return true;
            }
        }
        return false;
        */
    }
    private function action($table, $action, $where = array(), $moreWhere = array())
    {
    	/*
        // check if where = 3 fields (field, operator, value))
        if(count($where === 3)) {
            $field = $where[0]; // name of feild
            $operator = $where[1]; // operator 
            $value = $where[2]; // value of feild

            $operators = array('=', '<', '>', '<=', '>=', 'BETWEEN', 'LIKE');
            // check if operator user set is allowed
            if(in_array($operator, $operators)) {
                // do sql statement
                $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ? ";
                // check if query is not have errors
                if(!$this->query($sql, array($value))->error()) {
                    return $this;
                }
            }
        }
        return false;
        */
    }
    /**
    Experimental
    **/

    
	public function getResultadoNumerico()
	{
		return $resNum = $this->resultado->rowCount();
	}
	private function getTipos($modo)
	{
		$tipos = array('array','object','row','assoc');
		if(in_array($modo,$tipos))
			return true;
		else
			throw new Exception('Error - El tipo de retorno '.$modo.'" no existe');
	}
	public function getResultado($modo='assoc')
	{
		$modo=strtolower($modo);
		if($this->getTipos($modo)) {
			if(is_object($this->resultado)) {
				switch ($modo) {
					case 'array':
						$datos = $this->resultado->fetch(PDO::FETCH_BOTH);
						break;
					case 'object':
						$datos = $this->resultado->fetch(PDO::FETCH_OBJ);
						break;
					case 'row':
						$datos = $this->resultado->fetch(PDO::FETCH_NUM);
						break;
					case 'assoc':
					default:
						$datos = $this->resultado->fetch(PDO::FETCH_ASSOC);
						break;
				}
				//print_r($datos);
				return $datos;

				//return $this->resultado->fetch();
			}
			else {
				throw new Exception('Error - El Resultado No Es Un Objeto');
			}
		}
	}
	/*
	*Not Yet
	*/
	public function getArrayDatos($modo='assoc')
	{
		if($this->getTipos($modo)) {		
			$resultado = array();
			while($res = self::getResultado($modo)) {
				$resultado[] = $res;
			}
			return $resultado;
		}
	}

	public function getIdAfectado()
	{
		return $idAfectado = $this->_pdo->lastInsertId();
	}
	public function setLiberarMemoria()
	{
		$this->resultado->closeCursor();
	}
	public function getErrorDeQuery()
	{
		return ($this->resultado != true)?false:true;
	}
	public function setCerrarConexion()
	{
		$this->_pdo = null;
	}
	public function setTerminar()
	{
		if (!empty($this->resultado)) {
			self::setLiberarMemoria();
		}
		self::setCerrarConexion();
	}
	/**

	public $trans_started = false; 
    public $trans_ok = true; 
	 public function setTransOk($ok=true) { 
        $this->trans_ok = $ok; 
    } 
    
    public function trans_start() { 
        if(!$this->trans_started && $this->beginTransaction()) 
            $this->trans_started = true; 
        return $this->trans_started; 
    } 
    
    public function trans_stop() { 
        if($this->inTransaction()) { 
            if($this->trans_ok) 
                $this->commit(); 
            else 
                $this->rollBack(); 
        } 
        $this->trans_started = false; 
    }
	**/
}
?>