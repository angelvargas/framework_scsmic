<?php
	abstract class Controller {
		protected $vista;
		protected $modelo;
		protected $peticion;
		//
		protected $_modulo;
		protected $_controlador;
		protected $_metodo;
		//
		protected $acl;
		protected $htmlcreator;
		protected $paginator;
		//
		protected $_catuser = array();//categorias del usuario
		protected $_roluser = array();//roles del usuario
		protected $_foluser = array();//folios del usurio
		protected $_empuser = array();//empresas del usuario
		//
		protected $accesos = array();
		public $privatekey;
		//
		public function __construct() {
			$registro = Registry::getInstancia();
			$this->acl = $registro->acl;
			$this->acl->getid();
			$this->htmlcreator = $registro->htmlcreator;
			$this->paginator = $registro->paginator;
			//$this->acl=new ACL();
			$this->peticion = new Request();
			$this->vista = new View(/*$this->peticion*/);

			/*delete
			$this->__app_default = 'scsmic';
			$this->__app = $this->peticion->getModulo();
			$this->__app_name = empty($this->__app)?'scsmic':$this->__app;
			$this->__mod = $this->peticion->getControlador();
			$this->__sec = $this->peticion->getMetodo();
			*/
			//-----------------------------------------------
			//nuevas variables 19 de mayo de 2017
			$this->__app_default = 'frame';
			$this->__app = $this->peticion->getModulo();
			$this->__app_name = empty($this->__app)?$this->__app_default:$this->__app;
			$this->__mod = $this->peticion->getControlador();
			$this->__sec = $this->peticion->getMetodo();
			$this->__arg = $this->peticion->getArgumentos();
			//-----------------------------------------------
			//echo $this->__app_name.'@'.$this->__mod.'@'.$this->__sec;
			$this->_modulo = $this->peticion->getModulo();
			//echo $this->_modulo;
			$this->_controlador = $this->peticion->getControlador();
			$this->_metodo = $this->peticion->getMetodo();
			//-----------------------------------------------------------------
			$this->acl->_modulo = $this->_controlador;
			$this->acl->_app = empty($this->_modulo)?'frame':$this->_modulo;
			//-----------------------------------------------------------------
			/***/
			$this->_catuser = Session::get(_PREFIX_.'l_categorias');
			$this->_roluser = Session::get(_PREFIX_.'l_roles');
			$this->_foluser = Session::get(_PREFIX_.'l_folios');
			$this->_empuser = Session::get(_PREFIX_.'l_empresas');
			/***/
			$this->privatekey = "ABC48950938549038KSIROPGHDKLNCBVGAHQWKSIE27658942864";
			$this->dublin = 'dublin';
			///Verificar que mostrara de acuerdo a su categoria, o un banner o los anuncios
			

			if( Session::get('l_folio') )
				define('_IDUSER_', Session::get('l_folio'));//definimos constante de usuario
			//////////////////////////////////////////////////////////////////
			if(Session::get('logueado')) {
				$this->vista->nombre_user = Session::get('l_nombre');
			}

			///////////////////////////////////////////////////////////////
			$this->vista->activar_agregar_usuario = 'inactivo';
			$this->vista->socket_io = 'true';

			
			$this->vista->menu_slide = true;
			$this->vista->bar_contenido = true;
			$this->vista->js_menu = true;
			$this->vista->newmenu = $this->renderMenu();
			$this->vista->accesos = $this->getaccesosdirectos();
		}
		########################################################
		//Funciones De Framework
		########################################################
		abstract public function index();
		protected function loadModel($modelo, $modulo=false) {
			$modelo = $modelo . 'Model';
			$rutaModelo = _MODELO_.$modelo.'.php';

			if($modulo){
				if($modulo!='default'){
					$rutaModelo = _ROOT_.'modules'._DS_.$modulo._DS_.'models'._DS_.$modelo.'.php';
					//echo $rutaModelo.'<br>';
				}
			}
			//echo $rutaModelo; exit();
			if(is_readable($rutaModelo)){
				require_once $rutaModelo;
				$modelo = new $modelo;
				return $modelo;
			}
			else {
				throw new Exception('Error de modelo');
			}
		}
		protected function getLibrary($libreria)
		{
			$rutaLibreria = _LIBRERIAS_ . $libreria . '.php'; 
			if(is_readable($rutaLibreria))
			{
				require_once $rutaLibreria;
			}
			else
			{
				throw new Exception('Error de libreria '.$rutaLibreria);
			}
		}
		protected function redireccionar($ruta = false) {
			if ( $ruta )
				header('location:'._PATH_ABS_ . $ruta);
			else
				header('location:'._PATH_ABS_);
			exit;
		}
		protected function getParam($id,$dir='',$type='int') {
			if(!isset($id) || empty($id))
				$this->redireccionar($dir);
			switch ( $type ) {
				case 'int':
					if(!is_numeric($id))
						$this->redireccionar($dir);
					break;
				case 'array':
					if(!is_array($id))
						$this->redireccionar($dir);
					break;
				default:
					if(!is_string($id))
						$this->redireccionar($dir);
					break;
			}
		}
		public function get_var_dump($val)
		{
			echo '<pre>';
			print_r($val);
			echo '</pre>';
		}
		public function change_key( $array, $old_key, $new_key ) {
			if( ! array_key_exists( $old_key, $array ) )
			return $array;

			$keys = array_keys( $array );
			$keys[ array_search( $old_key, $keys ) ] = $new_key;

			return array_combine( $keys, $array );
		}
		public function excelToArray($filePath, $header=true) {
			$this-> getLibrary('PHPExcel/PHPExcel/IOFactory');
			//Create excel reader after determining the file type
			$inputFileName = $filePath;
			/**  Identify the type of $inputFileName  **/
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			/**  Create a new Reader of the type that has been identified  **/
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			/** Set read type to read cell data onl **/
			$objReader->setReadDataOnly(true);
			/**  Load $inputFileName to a PHPExcel Object  **/
			$objPHPExcel = $objReader->load($inputFileName);
			//Get worksheet and built array with first row as header
			$objWorksheet = $objPHPExcel->getActiveSheet();

			//excel with first row header, use header as key
			if($header){
				$highestRow = $objWorksheet->getHighestRow();
				$highestColumn = $objWorksheet->getHighestColumn();
				$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
				$headingsArray = $headingsArray[1];

				$r = -1;
				$namedDataArray = array();
				for ($row = 2; $row <= $highestRow; ++$row) {
					$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
					if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
						++$r;
						foreach($headingsArray as $columnKey => $columnHeading) {
							$namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
						}
					}
				}
			}
			else{
				//excel sheet with no header
				$namedDataArray = $objWorksheet->toArray(null,true,true,true);
			}
			return $namedDataArray;
		}
		/*
		**Funcion encargada de encriptar un valor
		*/
		protected function encrypt($string, $key) {
			$result = '';
			for($i=0; $i<strlen($string); $i++) {
				$char = substr($string, $i, 1);
				$keychar = substr($key, ($i % strlen($key))-1, 1);
				$char = chr(ord($char)+ord($keychar));
				$result.=$char;
			}
			return base64_encode($result);
		}
		/*
		**Funcion encargada de desencriptar un valor
		*/
		protected function decrypt($string, $key) {
			$result = '';
			$string = base64_decode($string);
			for($i=0; $i<strlen($string); $i++) {
				$char = substr($string, $i, 1);
				$keychar = substr($key, ($i % strlen($key))-1, 1);
				$char = chr(ord($char)-ord($keychar));
				$result.=$char;
			}
			return $result;
		}
		public function sort_by_orden ($a, $b) {
			$a = $a['puntos'];
			$b = $b['puntos'];
			if ($a == $b) {
				return 0;
			}
			return ($a > $b) ? -1 : 1;
		}
		########################################################
		//Menus
		########################################################
		protected function renderMenu(){
			include _PATH_.'menuarray.php';
			$datos = $this->acl->_permisos_publicos;
			$menu = $menu_array;
			$pool = array();
			$pool2 = array();
			$html = '';
			$url = _IMG_.'iconosmenu/';
			$ruta = _ROOT_.'public'._DS_.'img'._DS_.'iconosmenu'._DS_;
			
			foreach ($menu as $k => $v) {
				$clase_app = '';
				$display_mod = '';
				$clase_mod = '';
				$style_mod = '';
				$key = $v['key'];
				$default = isset( $v['default'] ) ? $v['default'] : 0;

				if( array_key_exists($key, $datos) || $default == 1){

					if($default == 1){
						$descsec = $v['descripcion'];
						$name_sec = $v['nombre'];
						$name_mod = $v['nombre_modulo'];
						$name_app = $v['nombre_app'];
						$img_menu = isset($v['img']) ? $v['img'] : '';
						$img_menu = file_exists($ruta.$img_menu) ? $img_menu : '';
						$valu_app = true;
						$valu_des = empty($descsec)?$this->__app_default:$descsec;
					}
					else{
						$descsec = $datos[$key]['descripcion'];
						$name_sec = $datos[$key]['nombrepermiso'];
						$name_mod = $datos[$key]['nombremodulo'];
						$name_app = $datos[$key]['nombre_apli'];
						$valu_app = $datos[$key]['valor'];
						$valu_des = empty($descsec)?$this->__app_default:$descsec;
						$img_menu = empty($datos[$key]['imagen']) ? '' : $datos[$key]['imagen'];
						$img_menu = file_exists($ruta.$img_menu) ? $img_menu : '';
					}
					if($valu_app == true){
						$kdata = explode('@', $key);
						$seccion = $kdata[0];
						$modulo = $kdata[1];
						$app = $kdata[2];
						$x = 'menu_'.$app;
						$y = $app.'_'.$modulo.'_modulos';
						$z = $app.'_'.$modulo.'_secciones';

						if($app == $this->__app_default)
							$link = _PATH_ABS_.$modulo.'/'.$seccion.'/';
						else
							$link = _PATH_ABS_.$app.'/'.$modulo.'/'.$seccion.'/';

						if( $modulo == $this->__mod){
							$style_mod = 'display: block;';
							$clase_mod = 'active';
							$this->accesos[$seccion]['link'] = $link;
							$this->accesos[$seccion]['nombre'] = $name_sec;
							$this->accesos[$seccion]['desc'] = $valu_des;
						}

						if($app == $this->__app_name){
							$clase_app = 'active';
							$display_mod = 'display: block;';
						}
						$icon_sec = $this->htmlcreator->getTag('i','chevron_right',array('class'=>'material-icons icon_menu_mod item-auto'));
						$name_sec = $this->htmlcreator->getTag('span', $name_sec, array('class'=>'item-auto'));
						$enlace_seccion = $this->htmlcreator->getTag('a', $icon_sec.$name_sec, array('href'=>$link, 'title'=>$valu_des, 'class'=>'cont-flex items-end flex-no-wrap'));
						$li_seccion = $this->htmlcreator->getTag('li', $enlace_seccion, array());
						if( !empty($$z) ){
							$$z.= $li_seccion;
						}
						else{
							$$z = $li_seccion;
						}

						if(empty($img_menu)){
							$imagen_modulo  = $this->htmlcreator->getTag('i','image',array('class'=>'material-icons icon_menu_mod item-auto'));
						}
						else{
							$imagen_modulo = $this->htmlcreator->getTagSimple('img', array('src'=>$url.$img_menu, 'alt'=>$name_mod, 'width'=>'25', 'height'=>'25'));
						}
						
						//$imagen_modulo = $this->htmlcreator->getTag('i', '&#xE5C3;', array('class'=>'material-icons item-auto'));
						$name_modulo = $this->htmlcreator->getTag('a', $name_mod, array('href'=>'javascript:void(0)', 'class'=>'item-auto'));
						$div_name = $this->htmlcreator->getTag('div', $imagen_modulo.$name_modulo, array('class'=>'cont-flex items-end flex-no-wrap '.$clase_mod));
						$ul_Secciones = $this->htmlcreator->getTag('ul', $$z, array('style'=>$style_mod));
						
						$li_modulo = $this->htmlcreator->getTag('li', $div_name.$ul_Secciones, array('class'=>'drpdown'));
						//$$y = $li_modulo;
						if( !empty($$y) ){
							$$y = $li_modulo;
						}
						else{
							$$y = $li_modulo;
						}

						$pool2[$app][$y] = $$y;
						
						$contapp = '';
						foreach ($pool2[$app] as $ke => $va) {
							$contapp.= $va;
						}

						$ul_modulos = $this->htmlcreator->getTag('ul', $contapp, array('style'=>$display_mod));
						$icon_app = $this->htmlcreator->getTag('i','menu',array('class'=>'material-icons icon_menu_mod item-auto'));
						$name_app = $this->htmlcreator->getTag('span', $name_app, array('class'=>'item-auto'));
						$name_app = $this->htmlcreator->getTag('a', $icon_app.$name_app, array('href'=>'javascript:void(0)', 'class'=>'cont-flex items-end flex-no-wrap '.$clase_app));
						$$x = $this->htmlcreator->getTag('li', $name_app.$ul_modulos, array('class'=>'drpdown'));
						$pool[$x] = $$x;
					}
					
				}
			}
			$pool2 = '';
			foreach ($pool as $key => $value) {
				$html.=$value;
			}
			return $html;
		}
		public function getaccesosdirectos(){
			$accesos = '';
			if( !empty($this->accesos) ){
				foreach ($this->accesos as $key => $value) {
					$link = $value['link'];
					$nombre = $value['nombre'];
					$desc = $value['desc'];
					$accesos.= $this->htmlcreator->getTag('a', $nombre, array('class'=>'btnacceso', 'href'=>$link, 'title'=>$desc)).' ';
				}
			}
			return $accesos;
		}
		protected function menu_principal($activo, $modulo = '') {}
		########################################################
		//Funciones De Aplicacion
		########################################################
		public function getusuario()
		{
			$datos=array(
				'term'=>FILTER_SANITIZE_STRING
			);
			$posts=filter_input_array(INPUT_POST,$datos);
			$res=$this->modelo->getusuario($posts);
			$this->vista->contenido=$res;
			$this->vista->renderizar('blanco');
		}
		protected function lstUsuarios($pagina = 1, $url, $parametros = '') {
			if( Session::get('_lstUsuarios') ){
				$post = Session::get('_lstUsuarios');
			}
			else{
				$post = array('nombre'=>'', 'empresa'=>'', 'email'=>'');
			}
			if(!empty($_POST)){
				$data=array(
					'nombre'=>FILTER_SANITIZE_STRING,
					'empresa'=>FILTER_SANITIZE_STRING,
					'email'=>FILTER_SANITIZE_STRING
				);
				$post = filter_input_array(INPUT_POST,$data);
				Session::set('_lstUsuarios', $post);		
			}
			//------------------o------------------------
			if(!empty($post))
				foreach ($post as $key => $value)
					$$key = $value;
			//------------------o------------------------
			$this->vista->setCss(array(
				'template'=>array('estructura'),
				'publico'=>array('tablas','cssfrm','nav','nav2','validationEngine.jquery','jqueryuicss/jqueryui','jquery-ui-timepicker-addon','jquery.cluetip')
			));
			$this->vista->setJs(array(
				'template'=>array('pluginsearch'),
				'publico'=>array('jquery.cluetip','jqueryui'),
				'secciones'=>array('usuarios/lstusuarios')
			));
			$this->paginator->mostrar = 20;
			$this->paginator->pagina = $pagina;
			$ltsOrdenes = $this->modelo->buscar_user($parametros,$post);
			$this->htmlcreator->form=array(
				'nombre'=>array('type'=>'text','name'=>'nombre','id'=>'nombre','class'=>'inputsearch','value'=>$nombre),
				'email'=>array('type'=>'text','name'=>'email','id'=>'email','class'=>'inputsearch wnormal','value'=>$email),
				'empresa'=>array('type'=>'text','name'=>'empresa','id'=>'empresa','class'=>'inputsearch wnormal','value'=>$empresa)
			);
			$this->vista->tabla = $this->htmlcreator->getTabla($ltsOrdenes,'',array('class'=>'tablas'));
			$this->vista->paginador = $this->paginator->htmlpaginador($url,7);
			$this->vista->infopag = $this->paginator->info();
		}
		public function busqueda_empresas()
		{
			$datos=array(
				'term'=>FILTER_SANITIZE_STRING
			);
			$posts = filter_input_array(INPUT_POST,$datos);
			$res = $this->modelo->busqueda_empresas($posts);
			$this->vista->contenido = $res;
			$this->vista->renderizar('blanco');
		}
		public function listempresas($pagina =1, $flag = 1, $url, $parametros = '') {
			if( Session::get('_listempresas') ){
				$post = Session::get('_listempresas');
			}
			else{
				$post = array('nombre'=>'', 'razon'=>'');
			}
			if(!empty($_POST)){
				$data=array(
					'nombre'=>FILTER_SANITIZE_STRING,
					'razon'=>FILTER_SANITIZE_STRING
				);
				$post = filter_input_array(INPUT_POST,$data);
				Session::set('_listempresas', $post);	
			}
			//------------------o------------------------
			if(!empty($post))
				foreach ($post as $key => $value)
					$$key = $value;
			//------------------o------------------------
			$this->paginator->mostrar = 20;
			$this->paginator->pagina = $pagina;
			$datos = $this->modelo->listEmpresa($post, $parametros);
			$this->htmlcreator->form=array(
				'nombre de la empresa'=>array('type'=>'text','name'=>'nombre','id'=>'nombre','class'=>'inputsearch','value'=>$nombre),
				'razon social'=>array('type'=>'text','name'=>'razon','id'=>'razon','class'=>'inputsearch','value'=>$razon)
			);
			$btnadd = $this->htmlcreator->getTag('a', 'Sincronizar Empresas', array('class'=>'btnext sincronizar'));
			$this->vista->btnextra = $btnadd;
			$this->vista->tabla = $this->htmlcreator->getTabla($datos,'',array('class'=>'tablas'));
			$this->vista->btnextra = $this->htmlcreator->getTag( 'a', 'Agregar Empresa', array('class'=>'btnext', 'id'=>'addusuario', 'href'=>_PATH_ABS_.'usuarios/agregar_empresa/' ) );
			$this->vista->paginador = $this->paginator->htmlpaginador($url, 7);
			$this->vista->infopag = $this->paginator->info();
			if($flag==0){
				$this->vista->contenido = 'publico:table.html';
				$this->vista->renderizar();
			}
			else{
				$this->vista->contenido = 'publico:tableajax.html';
				$this->vista->renderizar('blanco');
			}
		}
	}
?>