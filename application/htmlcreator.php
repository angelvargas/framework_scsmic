<?php
	//------------------------------------------------------------------------
	//Title..........: Clase para generar codigo html
	//File...........: htmlcreator.php
	//version........: 2.1.1
	//v1.0...........: generador de tags de html
	//v2.0...........: incluye generador de tablas, generador de opciones para select 11-dic-2013
	//Last changed...: 12-Octubre-2017
	//Autor..........: Angel Florentino Vargas Pool
	//Email..........: angelvargaspool@gmail.com
	//desc.::........: correccion en formato de codigo
	//------------------------------------------------------------------------

	//------------------------------------------------------------------------
	//Funcionamiento
	//$html = new htmlcreator();
	//$html->getTag('div','contenido',array('class'=>'clase'));
	//Resultado....:<div class="clase">contenido</div>
	//$html->getTag('div','contenido2');
	//Resultado....:<div>contenido2</div>
	//------------------------------------------------------------------------
	class htmlcreator {
		private $basetag;//base para las etiquetas html
		private $simpletag;//base para las etiquetas html sin cierre
		private $iniTag;//base de apertura de una etiqueta html
		private $finTag;//base de cierre de una etiqueta html
		private $arrayTag;//coleccion de tags por cerrar;
		private $arrayHtml;//coleccion de etiquetas iniciales html
		//private $basesel;
		private $keyheader;
		private $subHeader;
		public $columnas;
		public $form;
		public $foot;
		public $attrhead;
		public $attrfoot;
		public $unset = array();
		private $vista;
		//------------------------------------------------------------------------------
		//Funcion constructura
		//------------------------------------------------------------------------------
		public function __construct(){
			$this->basetag='<%s %s>%s</%s>';
			$this->simpletag='<%s %s/>';
			$this->iniTag='<%s %s>';
			$this->finTag='</%s>';
			$this->arrayTag=array();
			$this->vista = new View(/*$this->peticion*/);
			//$this->arrayHtml=array();
			//$this->baseinput='<input type="%s" %s />';
		}
		//------------------------------------------------------------------------------
		//Funcion creadora de etiquetas html de tipo <></>
		//------------------------------------------------------------------------------
		public function getTag($tag,$contenido='',$attributos=''){
			$html=sprintf($this->basetag,$tag,$this->attributes($attributos),$contenido,$tag);
			return $html;
		}
		//------------------------------------------------------------------------------
		//Funcion creadora de etiquetas html simples de tipo </>
		//------------------------------------------------------------------------------
		public function getTagSimple($tag,$attributos=''){
			$html=sprintf($this->simpletag,$tag,$this->attributes($attributos));
			return $html;
		}
		//------------------------------------------------------------------------------
		//Experimental
		//------------------------------------------------------------------------------
		public function openTag($tag,$attributos=''){
			$this->arrayHtml.=sprintf($this->iniTag,$tag,$this->attributes($attributos));
			$this->arrayTag[]=$tag;
		}
		//------------------------------------------------------------------------------
		//Experimental
		//------------------------------------------------------------------------------
		public function closeTag($tag=''){
			//if($this->getTag() || $this->getTagSimple())
			//	echo 'true';
			$html='';
			foreach ($this->arrayTag as $key => $value){
				$this->arrayHtml.=sprintf($this->finTag,$value);
			}
			unset($this->arrayTag);
			//aqui va el codigo
		}
		//------------------------------------------------------------------------------
		//Experimental
		//------------------------------------------------------------------------------
		public function renderTags(){
			return $this->arrayHtml;
			$this->arrayHtml='';
			//
		}
		//------------------------------------------------------------------------------
		//Funcion creadora de inputs de un formulario
		//------------------------------------------------------------------------------
		public function getInput($tipo,$attributos=''){
			$attributos['type']=$tipo;
			$html=$this->getTagSimple('input',$attributos);
			return $html;
		}
		//------------------------------------------------------------------------------
		//Funcion creadora de selects de un formulario
		//------------------------------------------------------------------------------
		public function getSelect($datos,$attributos='',$sel=false,$optinicio=''){
			//*nota:Es posible que en un futuro se requieran los atributos de los options
			if(is_array($datos)){
				$datos=$this->getOptions($datos,$sel,$optinicio);
			}
			$html=$this->getTag('select',$datos,$attributos);
			return $html;
		}
		public function getOptions($datos, $sel=false, $optinicio='') {
			//*nota:optinicio es el primer dato que debe aparecer en la lista
			//*nota:es posible que se pueda aceptar dos tipos de arrays
			$html='';
			if(is_array($datos)) {
				if(!empty($optinicio)) {
		            $array[]=array('id'=>'','nombre'=>$optinicio);
		            $datos=array_merge($array,$datos);
		        }
		        foreach ($datos as $llave => $opciones) {
	            	$attrsopt = '';
	                foreach ($opciones as $key => $value) {
	                    if($key=='nombre') {
	                        $nombre=ucfirst($value);
	                    }
	                    elseif($key=='attr') {
	                    	if(is_array($value))
	                    		$attrsopt = $value;
	                    }
	                    elseif($key=='id'){
	                        $id=$value;
	                        $selected=($sel==$id)?array('selected'=>'selected'):'';
	                    }
	                }
	                //$selected='value'=>$id;
	                //$sel=empty($selected)?'':'';
	                $attropt=array('value'=>$id);
	                if(!empty($selected)) {
	                    $attropt = array_merge($attropt,$selected);
	                }
	                if(!empty($attrsopt)){
	                	$attropt = array_merge($attropt,$attrsopt);
	                }
	                //var_dump($attropt);
	                $html.=$this->getTag('option',$nombre,$attropt);
	            }
			}
			return $html;
		}
		public function getList($datos){
			$lista='';
			if( is_array($datos) ) {
				$li='';
				foreach ( $datos as $key => $value ) {
					if( is_array($value) ) {
						$li.=$lista=$this->getList($value);
					}
					else {
						$li.=$this->getTag('li',$value);
					}
				}
			}
			$lista = $this->getTag('ul',$li);
			return $lista;
		}
		//-------------------------------------------------------------------------------------------
		public function getTablaVertical($datos,$mensaje='',$atributos=''){
			$tabla='';
			if(is_array($datos)){
				$cont='';
				if(isset($this->unset)){
					foreach ($this->unset as $k => $v){
						unset($datos[$v]);
					}
					unset($this->unset);					
				}
				$count = 0;
				foreach ($datos as $key => $value){
					if ($count==0) {
						$llave=$this->getTag('th','Campo');
						$valor=$this->getTag('th','Valor');
						$cont.=$this->getTag('tr',$llave.$valor);
					}
					$llave = $key;
					if (is_array($this->columnas) && !empty($this->columnas)) {
						if (array_key_exists($key, $this->columnas)) {
							if(array_key_exists('nombre', $this->columnas[$key])) {
								$llave = $this->columnas[$key]['nombre'];
								unset($this->columnas[$key]['nombre']);
							}
						}
					}
					$llave=$this->getTag('td',$llave);
					$valor=$this->getTag('td',$value);
					$cont.=$this->getTag('tr',$llave.$valor);
					$count++;
				}
				$tabla=$this->getTag('table',$cont,$atributos);
			}
			return $tabla;
		}
		public function getTabla($datos,$mensaje='',$atributos='') {
			if( is_array($datos) ) {
				$datos=$this->getDatosTabla($datos,$mensaje);
			}
			$html=$this->getTag('table',$datos,$atributos);
			return $html;
		}
		public function getDatosTabla($datos,$mensaje='') {
			$html='';
			$conttr=0;
			$contth=0;
			if( is_array( $datos ) ) {
				$tr='';//var tag tr[fila]
				foreach ( $datos as $llave => $arreglo ) {
					$conttr++;
					$conttd=0;
					$td='';
					$atributos = array();
					$base_arreglo = $arreglo;
					if( isset( $arreglo['attr'] ) ) {
						$atributos=$arreglo['attr'];
						unset($arreglo['attr']);	
					}
					if( isset($this->unset) ) {
						foreach ($this->unset as $k => $v) {
							unset($arreglo[$v]);
						}					
					}
					foreach ( $arreglo as $key => $value ) {
						$conttd++;
						//generador de cabezal de tabla
						
						if( !is_array( $value ) ) {
							//
							$patron = '@[\w-_\s\.]{1,}:[\w-_\s\.]{1,}\.(html|phtml|php)@';
							$val_file = $value;
							if ( preg_match($patron, $val_file) ) {
								//echo '*'.$val_file.'<br>';
								$val_file = $this->vista->getContent( $val_file );
								if( !empty( $val_file ) ) {
									foreach ($base_arreglo as $k => $v) {
										if( !is_array( $v ) ){
											$key_string = '{{'.$k.'}}';
											$val_file = str_replace( $key_string, $v, $val_file );
										}
									}
									$value = $val_file;
								}
							}
							//
							$td.=$this->getTag('td',$value);
							if($conttr==1)
								$this->keyheader[$key] = $key;
						}
						else {
							//$this->keyheader[$key]['nombre']=$key;
							$total = count($value);
							//echo $total;
							//$td='';
							foreach ( $value as $k => $v ) {
								$this->subHeader[$key][$k] = $k;
								$td.=$this->getTag('td', $v);
							}
							if( $conttr==1 ) {
								//$this->keyheader[$key]['key']=$key;
								$this->keyheader[$key]['nombre'] = $key;
								$this->keyheader[$key]['colspan'] = $total;
							}
							//$td.=$this->getTag('td',$tsub);
						}
							
					}
					$tr.=$this->getTag('tr',$td,$atributos);
				}
				$contenido=$this->getTag('tbody',$tr);
				//si existe el pie/base de la tabla
				$pie=$this->tfoot();
				//almacena el cabezal de la tabla, con los titulos correspondientes
				$cabezal=$this->thead( $mensaje );
				//$cabezal=$this->getTag('thead',$head);
				//union del theader,el tbody y el tfoot
				$html.=$cabezal.$contenido.$pie;
			}
			unset($this->keyheader);
			unset($this->unset);
			$this->columnas = array();
			$this->form = '';
			$this->foot = '';
			//echo '<pre>';
			//var_dump($this->columnas);
			//echo '</pre>';
			return $html;
		}
		private function tfoot() {
			$foot='';
			$attr='';
			if(is_array($this->foot) && !empty($this->foot)) {
				foreach ($this->foot as $llave => $arreglos){
					if($llave=='attr'){
						$attr=$arreglos;
					}
					else{
						$html = '';
						$data_td = array();
						$cont1 = 0;
						$cont2 = 0;
						$flag = 0;//bandera para saber si tiene datos
						$count3 = 0;
						//echo '<pre>';
						//var_dump($this->keyheader);
						//echo '</pre>';
						foreach ($this->keyheader as $key => $value) {
							//si la llave exite en el arreglo	
							if( array_key_exists($key, $arreglos) ) {
								$cont1++;
								if( is_array($value) ){
									$data_td[$cont1]['texto'] = $arreglos[$key];
									$data_td[$cont1]['colspan'] = $value['colspan'];
									$cont2--;
									$cont2 = $cont2 + $value['colspan'];
								}
								else{
									
									$data_td[$cont1]['texto'] = $arreglos[$key];
									$data_td[$cont1]['colspan'] = 1;
									//$html.=$this->getTag('td',$arreglos[$key],array('colspan'=>$cont2));
									$cont2 = 1;//contador de colspan se reinicia
									$flag = 1;//la bandera cambia a 1
									//echo 'true<br>';
								}
								
							}
							else{
								//echo 'false<br>';
								if($flag==0){
									$count3++;
									$data_td[$count3.'n']['texto'] = '';
									$data_td[$count3.'n']['colspan'] = 1;
									//$html.=$this->getTag('td','',array('colspan'=>1));
								}
								$cont2++;
								if( is_array($value) ){
									$cont2--;
									$cont2 = $cont2 + $value['colspan'];
								}
								if($cont1>0)
									$data_td[$cont1]['colspan'] = $cont2;
							}
							
						}
						//echo '<pre>';
						//var_dump($data_td);
						//echo '</pre>';
						foreach ($data_td as $key => $value){
							$html.=$this->getTag('td',$value['texto'],array('colspan'=>$value['colspan']));
						}
						$foot.=$this->getTag('tr',$html);
					}
				}
				$foot=$this->getTag('tfoot',$foot,$attr);
			}
			return $foot;
		}
		private function thead( $mensaje = ''){
			$th='';
			$head='';
			if(!empty($this->keyheader)) {
				$_contth = 0;
				foreach ($this->keyheader as $key => $value) {
					if(is_array($value)) {
						$nombre=$value['nombre'];
						unset($value['nombre']);
						$attrhead=$value;
					}
					else {
						$nombre=$value;
						$attrhead='';
					}
					
					if(is_array($this->columnas) && !empty($this->columnas)) {
						if(array_key_exists($key, $this->columnas)) {

							if(array_key_exists('nombre', $this->columnas[$key])) {
								$nombre=$this->columnas[$key]['nombre'];
								unset($this->columnas[$key]['nombre']);
							}

							if(array_key_exists('campo', $this->columnas[$key])) {
								$campo = $this->columnas[$key]['campo'];
								$htmlname = '';
								if(!empty($campo)){
									$htmlname.= $this->getTag('div', $nombre, array('class'=>'item-11'));
									$enlace = $this->getTag('a', '', array('data-campo'=>$campo, 'data-orden'=>'desc'));
									$htmlname.= $this->getTag('div', $enlace, array('class'=>'item-1'));
									$htmlname = $this->getTag('div', $htmlname, array('class'=>'cont-flex flex-no-wrap flex-jcenter'));
									$nombre = $htmlname;
								}
								unset($this->columnas[$key]['campo']);
							}

							if(array_key_exists('orden', $this->columnas[$key])) {
							}

							$attrhead=$this->columnas[$key];
						}
					}
					$th.=$this->getTag('th',$nombre,$attrhead);
					$_contth++;
				}
				//array('rowspan'=>2)
				$h=$this->getTag('tr',$th);
				if(!empty($this->subHeader))
				{
					//codigo abierto
				}

				

				$head.=$this->getTag('thead', $h.$this->tform() );

				##if( !empty( $mensaje ) ) {
				##	$msg=$this->getTag('td', $mensaje,array('colspan'=>$_contth));
				##	$head.=$this->getTag('tr', $msg, array('class'=>'msnrestab'));
				##}

				
				//$this->subHeader
				//unset($this->unset);
			}
			return $head;
		}
		private function tform() {
			$form='';
			$attrform = '';
			if(isset($this->form['attr'])) {
				$attrform = $this->form['attr'];
				unset($this->form['attr']);
			}			
			foreach ($this->keyheader as $key => $value) {
				$attrhead='';
				if(is_array($value)) {
					$nombre=$value['nombre'];
					unset($value['nombre']);
					$attrhead=$value;
				}
				if(is_array($this->form) && !empty($this->form)) {
					if(array_key_exists($key, $this->form)) {
						$type='text';
						$data='';
						$seldata=false;
						$msgdata='';
						$contenido = '';
						foreach ($this->form[$key] as $keyfrm => $valfrm) {
							if($keyfrm=='type' || $keyfrm=='data' || $keyfrm=='seldata'|| $keyfrm=='msgdata' || $keyfrm=='contenido' || $keyfrm=='item_size') {
								$$keyfrm = $valfrm;
								unset($this->form[$key][$keyfrm]);
							}
						}
						
						$size_item = 'item-input';
						if(isset($item_size)){
							$size_item = $item_size;
						}
						if( array_key_exists('class', $this->form[$key]) ){
							$class_value = $this->form[$key]['class'].' '.$size_item;
						}
						else{
							$class_value = $size_item;
						}
						$this->form[$key]['class'] = $class_value;
						

						switch ($type) {
							case 'select':
								$frm = $this->getSelect($data, $this->form[$key], $seldata, $msgdata);
								break;
							case 'textarea':
								$frm = $this->getTag('textarea', $contenido, $this->form[$key]);
								break;
							case 'grupo':
								$frm = $this->getTag('span', $contenido, $this->form[$key]);
								break;
							default:
								$frm = $this->getInput($type, $this->form[$key]);
								break;
						}
						
						$frmcont = $this->getTag('div', $frm, array('class'=>'cont-flex'));
						$form.=$this->getTag('td', $frmcont, $attrhead);
						
						/*
						$form.=$this->getTag('td', $frm, $attrhead);
						*/
					}
					else {
						$form.=$this->getTag('td', '', $attrhead);
					}
				}
			}
			if(!empty($form)) {
				$form=$this->getTag('tr',$form,$attrform);
			}
			return $form;
		}
		//private function tfoottd(){}
		public function attributes($attributes) {
			$html = array();
			//var_dump((array) $attributes);
			if(!empty($attributes)) {
				foreach ((array) $attributes as $key => $value) {
					// Para las llaves(claves) numéricas, supondremos que la clave y el valor son el
					// El mismo, ya que esto se convertirá atributos HTML como "required" que
					// Puede ser especificado como se required="required", etc
					if (is_numeric($key)) $key = $value;
					//
					if (!is_null($value) && !empty($key) && !empty($value) || ($value=='') ){
						//!empty($value)
						$html[] = $key.'="'.$value.'"';
					}
				}
			}
			return (count($html) > 0) ? ' '.implode(' ', $html) : '';
		}
		public function concatenar($data=array(), $separador=" "){
			//
			return (count($data) > 0) ? ' '.implode($separador, $data) : '';
		}
		public function strconcat($data=array(), $separador=","){
			//
			return (count($data) > 0) ? implode($separador, $data) : '';
		}
	}
?>