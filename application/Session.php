<?php
	class Session{
		//funcion para iniciar la session
		public static function init(){
			//ini_set("session.cookie_lifetime","7200");
			//ini_set("session.gc_maxlifetime","7200");
			session_start();
		}
		//funcion para destruir la asession
		public static function destroy($clave=false){
			//si la clave existe elimina la clave seleccionada
			//si no existe, elimina toda la session
			if($clave){
				if(is_array($clave)){//si es una coleccion de variables de session
					$total=count($clave);
					for ($i = 0; $i < $total ; $i++){
						if(isset($_SESSION[$clave[$i]])){
							unset($_SESSION[$clave[$i]]);
						}
					}
				}else{
					//si existe la variabel de session indicada lo eliminamos
					//if(isset($_SESSION[$clave])){
						unset($_SESSION[$clave]);
					//}
				}
			}else{
				session_destroy();//elimina la session
			}
		}

		//funcion estatica para asignar valores a una session
		public static function set($clave, $valor){
			if(!empty($clave)){
				$_SESSION[$clave]=$valor;
			}
		}

		//funcion para recuperar valores de una session
		public static function get($clave){
			if(isset($_SESSION[$clave])){
				return $_SESSION[$clave];
			}
		}
		public static function acceso($app, $mod, $sec, $args=array()){
			//var_dump($args);
			$args = (count($args) > 0) ? implode('@', $args) : '';
			if(empty($app)){
				$url = $mod.'@'.$sec;
			}
			else{
				$url = $app.'@'.$mod.'@'.$sec;
			}
			//if(!empty($args))
			//	$url = $url.'@'.$args;
	        if( !Session::get('logueado') ) {
	        	//throw new Exception("Session Finalizada");
    			echo file_get_contents(_PATH_ABS_.'error/access/errorsession/'.$url.'/');
    			exit();
	        }
	    }
	}
?>