<?php
	//------------------------------------------------------------------------
	//Title...........: creador de vistas html
	//File............: ordenController.php
	//version.........: 6.0
	//Fecha Creacion..: 03-Marzo-2013
	//Fecha Cambio....: 08-Noviembre-2016
	//Autor...........: Angel Florentino Vargas Pool
	//Email...........: angelvargaspool@gmail.com
	//desc............: correcciones en el codigo encargado de obtener un fichero
	//------------------------------------------------------------------------
	class View {
		private $template='';/***/
		//variable que contendra la plantilla
		private $vars=array();/***/
		//variable array que contendra los valores a reemplazar en la plantilla
		private $rutas;/***/
		private $rutacss;/***/
		private $rutajs;/***/
		private $css;/***/
		private $js;/***/
		private $_request;/***/

		public $returnhtml = false;//para almacenar el html
		public $noreemplazar = false;//no reemplaza datos en el template
		public $no_encrip = true;
		public function __construct(/*Request $peticion*/){
			//constructor, se encargas de inicializar los datos de la clase
			//$this->template=$template;//la variable template toma el valor que le demos con el constructor
			$this->vars['title']='No titulo :( *_*';
			//en caso de que noo exista un titulo, se inicializara con un titulo
			$this->vars['path_abs'] = _PATH_ABS_;
			$this->vars['servidor'] = _SERVER_;
			$this->vars['img'] = _IMG_;
			$this->vars['_iva_'] = _IVA_;
			$this->vars['key_css'] = '';
			//$this->vars['no_encrip'] = false;
			//$this->_request = $peticion;
			//$modulo=$this->_request->getModulo();
			//$controlador = $this->_request->getControlador();
			
			$this->rutas['views'] = _ROOT_.'public'._DS_.'views'._DS_;
			$this->rutas['js'] = _PATH_ABS_.'public/js/';
			$this->rutas['css'] = _PATH_ABS_.'public/css/';
			$this->rutas['js_root'] = _ROOT_.'public'._DS_.'js'._DS_;
			$this->rutas['css_root'] = _ROOT_.'public'._DS_.'css'._DS_;
			
			$this->rutacss=array(
				'publico'=>$this->rutas['css'].'publico/',
				'secciones'=>$this->rutas['css'].'secciones/',
				'template'=>$this->rutas['css'].'template/',
				'web'=>'',
				'framework'=>_PATH_ABS_.'public/Design/'
			);

			$this->rutacssroot=array(
				'publico'=>$this->rutas['css_root'].'publico'._DS_,
				'secciones'=>$this->rutas['css_root'].'secciones'._DS_,
				'template'=>$this->rutas['css_root'].'template'._DS_,
				'web'=>'',
				'framework'=>_ROOT_.'public'._DS_.'Design'._DS_
			);

			$this->rutajs=array(
				'publico'=>$this->rutas['js'].'publico/',
				'secciones'=>$this->rutas['js'].'secciones/',
				'template'=>$this->rutas['js'].'template/',
				'web'=>'',
				'weblink'=>'',
				'framework'=>_PATH_ABS_.'public/Design/'
			);

			$this->rutajsroot=array(
				'publico'=>$this->rutas['js_root'].'publico'._DS_,
				'secciones'=>$this->rutas['js_root'].'secciones'._DS_,
				'template'=>$this->rutas['js_root'].'template'._DS_,
				'web'=>'',
				'weblink'=>'',
				'framework'=>_ROOT_.'public'._DS_.'Design'._DS_
			);

			$this->clave_cripto = "abcdef1234";
		}
		public function __set($name, $val=''){
			//funcion set a signador
			$this->vars[$name]=$val;
		}
		public function __get($name) {
			return $this->vars[$name];
		}
		public function __unset($name) {
			unset($this->vars[$name]);
		}

		//funcion que se encarga de encripntar un valor
		public function encriptar_AES($string) {
			$key = $this->clave_cripto;
			$td = @mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
			$iv = @mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_URANDOM );
			@mcrypt_generic_init($td, $key, $iv);
			$encrypted_data_bin = @mcrypt_generic($td, $string);
			@mcrypt_generic_deinit($td);
			@mcrypt_module_close($td);
			$encrypted_data_hex = bin2hex($iv).bin2hex($encrypted_data_bin);
			return $encrypted_data_hex;
		}
		//funcion encargada de desencripotar un valor
		public function desencriptar_AES($encrypted_data_hex){
			$key = $this->clave_cripto;
			$td = @mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
			$iv_size_hex = @mcrypt_enc_get_iv_size($td)*2;
			$iv = pack("H*", substr($encrypted_data_hex, 0, $iv_size_hex));
			$encrypted_data_bin = pack("H*", substr($encrypted_data_hex, $iv_size_hex));
			@mcrypt_generic_init($td, $key, $iv);
			$decrypted = @mdecrypt_generic($td, $encrypted_data_bin);
			$decrypted = preg_replace( "/\p{Cc}*$/u", "", $decrypted );
			@mcrypt_generic_deinit($td);
			@mcrypt_module_close($td);
			return $decrypted;
		}

		public function setJs($dat) {
			$fecha = date('Y-m-d H:i:s');
			$version = strtotime($fecha);
			if(is_array($dat)){
				$datajs = array();
				$cont = 0;
				$this->js = '';
				//$basejs='<script src="%s" type="text/javascript"></script>'."\n";
				$basejs='<script src="%s?v='.$version.'" type="text/javascript"></script>'."\n";
				//var_dump($this->no_encrip);
				foreach ($dat as $key => $value){
					$url = array_key_exists($key, $this->rutajs) ? $this->rutajs[$key] : $this->rutajs['publico'];
					$ruta_file = array_key_exists($key, $this->rutajsroot) ? $this->rutajsroot[$key] : $this->rutajsroot['publico'];
					if($key == 'web' || $key=='weblink' || $this->no_encrip==true){
						if(is_array($value)) {
							foreach ($value as $llave => $val) {
								if($key == 'weblink')
									$this->js.=sprintf($basejs, $url.$val);
								else
									$this->js.=sprintf($basejs, $url.$val.'.js');
							}
						}
						else {
							$this->js.=sprintf($basejs,$url.$value.'.js');
						}
					}
					else{
						if(is_array($value)){
							foreach ($value as $k => $val){
								$x = explode('/', $val);
								$count = count($x);
								if($count>1){
									$n = $count -1;
									
									unset($x[$n]);

									$new_url = $url;
									foreach ($x as $k => $v) {
										$new_url = $new_url.$v.'/';
									}
								}
								else{
									$new_url = $url;
								}
								$filejs = $ruta_file.$val.'.js';
								$filejs = str_replace('/', _DS_ , $filejs);
								$datajs[$cont]['js'] = $filejs;
								$datajs[$cont]['url'] = $new_url;
								$cont++;
							}
						}
						else{
							$x = explode('/', $value);
							var_dump($x);
							$count = count($x);
							if($count>0){
								$n = $count -1;
								unset($x[$n]);
								foreach ($x as $k => $v) {
									$url = $url.$v.'/';
								}
							}
							$filejs = $ruta_file.$value.'.css';
							$filejs = str_replace('/', _DS_ , $filejs);
							$datajs[$cont]['js'] = $filejs;
							$datajs[$cont]['url'] = $url;
						}
					}		
				}
				//var_dump($datajs);
				$jsonjs = json_encode($datajs, JSON_FORCE_OBJECT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_NUMERIC_CHECK);
				$jsonjs = utf8_encode($jsonjs);
				//echo $jsonjs;
				$jsonjs_encrypted = $this->encriptar_AES($jsonjs);
				$this->vars['jsonjs'] = $jsonjs_encrypted.'.js';
			}
			else{
				$this->vars['jsonjs'] = 'default.js';
			}
		}
		public function setCss($dat) {
			if(is_array($dat)){
				$datacss = array();
				$cont = 0;
				foreach ($dat as $key => $value){

					$url = array_key_exists($key, $this->rutacss) ? $this->rutacss[$key] : $this->rutacss['publico'];
					$ruta_file = array_key_exists($key, $this->rutacssroot) ? $this->rutacssroot[$key] : $this->rutacssroot['publico'];
					
					if($key == 'web' || $key=='weblink'){
						//links con css
					}
					else{
						if(is_array($value)){
							foreach ($value as $key => $val){
								$x = explode('/', $val);
								//var_dump($x);
								$count = count($x);
								//echo $count.'<br>';
								if($count>1){
									$n = $count -1;
									
									unset($x[$n]);

									$new_url = $url;
									foreach ($x as $k => $v) {
										$new_url = $new_url.$v.'/';
									}
								}
								else{
									$new_url = $url;
								}
								$filecss = $ruta_file.$val.'.css';
								$filecss = str_replace('/', _DS_ , $filecss);
								//$filecss = str_replace('-' , '*' ,$filecss);
								//$filecss = str_replace(':' , '*' ,$filecss);
								$datacss[$cont]['css'] = $filecss;
								$datacss[$cont]['url'] = $new_url;
								$cont++;
							}
						}
						else{
							$x = explode('/', $value);
							var_dump($x);
							$count = count($x);
							if($count>0){
								$n = $count -1;
								unset($x[$n]);
								foreach ($x as $k => $v) {
									$url = $url.$v.'/';
								}
							}
							$filecss = $ruta_file.$value.'.css';
							$filecss = str_replace('/', _DS_ , $filecss);
							$datacss[$cont]['css'] = $filecss;
							$datacss[$cont]['url'] = $url;
						}
					}
					
							
				}
				//var_dump($datacss);
				$jsoncss = json_encode($datacss, JSON_FORCE_OBJECT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_NUMERIC_CHECK);
				$jsoncss = utf8_encode($jsoncss);
				//echo $jsoncss;
				$jsoncss_encrypted = $this->encriptar_AES($jsoncss);
				$this->vars['jsoncss'] = $jsoncss_encrypted.'.css';
			}
			else{
				$this->vars['jsoncss'] = 'default.css';
			}
		}
		private function keyGen()
		{
			$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
			$key = "";
			for($i=0;$i<12;$i++) {
				$key .= substr($str,rand(0,62),1);
			}
			return $key;
		}
		
		public function getContenido($ruta)
		{
    		if (is_readable($ruta)) {
				$file=$ruta;
				$fd=fopen($file,'r');
				$file=fread($fd,filesize($file));
				fclose($fd);
				return $file;
			}
		}
		public function getContent($value)
		{
			$file = '';
			$patron = '@[\w-_\s\.]{1,}:[\w-_\s\.]{1,}\.(html|phtml|php)@';
			$value=( is_array( $value ) ) ? '' : $value;
			if ( preg_match( $patron, $value) ) {
				$url = explode(':', $value);
				$ruta = $this->rutas['views'].'secciones'._DS_.$url[0]._DS_.$url[1];
				//echo 'Esta es la ruta '.$ruta.'<br>';
				if(is_readable($ruta)){
					$file=$ruta;
					$fd=fopen($file,'r');
					$file=fread($fd,filesize($file));
					fclose($fd);
				}
			}
			return $file;
		}
		private function load($temp='')
		{
			$this->vars['csslinks']=$this->css;
			$this->vars['jslinks']=$this->js;
		}
		private function limpiar($val)
		{
			$cor=array('{','}','{{','}}');
			$val=str_replace($cor,'',$val);
			return trim($val);
		}
		private function replaceBlock()
		{
			//El primer patron se encarga de conseguir los names
			//El segundo padron tendra de reemplazo los names y ocultara el codigo de acuerdo si existe o no las variables correspondientes
			//echo round(memory_get_usage()/1024,2)." kilobytes"."\n";
			$patron1='#\[tag\s+name="(.*)"\]#sUi';
			preg_match_all($patron1,$this->template,$bloque,PREG_SET_ORDER);
			//echo '<pre>';
			//var_dump($bloque);
			//echo '</pre>';
			$contador = 0;
			if(count($bloque)>0){
				foreach ($bloque as $key => $value) {
					$name = $value[1];
					$patron2 = '#\[(tag\s+name="'.$name.'")\](.*)\[\/\\1\]#sUi';
					if(preg_match($patron2,$this->template,$secccion)){
						//echo htmlentities($secccion[0]);
						//echo '<br>';
						//echo '<br>';
						//echo htmlentities($secccion[2]);
						//echo '<br>';
						//echo '<br>';
						if(array_key_exists($name, $this->vars)){
							$this->template = str_replace($secccion[0], $secccion[2], $this->template);
						}	
						else{
							$this->template = str_replace($secccion[0],'',$this->template);
						}
						unset($secccion);
					}
					
				}
			}
			
			//$this->replaceBlock();
			//echo round(memory_get_usage()/1024,2)." kilobytes"."\n";
		}
		private function remplazar(){	
			preg_match_all('@\{\{[\w-_\s\.]+\}\}@', $this->template, $aTpl);
			//var_dump($aTpl[0]);
			if(count($aTpl[0])>0){
				foreach ($aTpl[0] as $key=>$val){
					//recursivo
					$val=$this->limpiar($val);
					if( array_key_exists( $val, $this->vars ) ) {
						$arr_dat = array('{{'.$val.'}}');
						
						//$patron = '@[\w-_\s\.]{1,}:[\w-_\s\.\\\/]{1,}\.(html|phtml|php)@';
						$patron = '@^[\w-_\s\.]{1,}:[\w-_\s\.\\\/]{1,}\.(html|phtml|php)$@';
						$value = ( is_array( $this->vars[$val] ) ) ? '' : $this->vars[$val];///
						if ( preg_match($patron, $value) ) {
							//echo $count.htmlentities($value).'<br>';
							$url = explode(':', $value);
							$ruta = $this->rutas['views'].'secciones'._DS_.$url[0]._DS_.$url[1];
							//echo 'Esta es la ruta '.$ruta.'<br><br>';
							if ( is_readable( $ruta ) ) {
								$file = $ruta;
								$fd = fopen($file,'r');
								$file = fread($fd,filesize($file));
								$this->template = str_replace($arr_dat, $file, $this->template);
								fclose($fd);
							}
						}
						else {
							$this->template = str_replace($arr_dat, $this->vars[$val], $this->template);
							//remplazo normal
						}
						//$this->template = str_replace('{'.$val.'}', $this->vars[$val], $this->template);
					}
					else{
						$arr_dat = array('{{'.$val.'}}');
						$this->template = str_replace($arr_dat, '', $this->template);
						//$this->template = str_replace('{{'.$val.'}}', '', $this->template);	
						//$this->template = str_replace('{'.$val.'}', '', $this->template);
					}
				}
				$this->remplazar();
			}
		}
		public function renderizar($dirTemp=_DEFAULT_LAYOUT_,$template='template'){
			$this->load($dirTemp);//carga css y js
			//$this->dir_layout=$this->rutas['views'];
			$this->template=$this->rutas['views'].'template'._DS_.$dirTemp._DS_.$template.'.html';
			//echo $this->template; exit();
			if(is_readable($this->template)){
				$fd=fopen($this->template,'r');
				$this->template=fread($fd,filesize($this->template));
				fclose($fd);
				//if(!empty($this->cont))
				//	$this->template = str_replace('{contenido}', $this->cont, $this->template);
				$this->remplazar();
				$this->replaceBlock();
				if($this->returnhtml == true)//almacena la informacion
					return $this->template;	
				else
					echo $this->template;//imprime	
			}
			else{
				throw new Exception('No existe el template');
			}
		}
	}
?>