<?php
    //------------------------------------------------------------------------
    //Title...........: clase paginadora
    //File............: paginator.php
    //version.........: 2.0.0
    //Fecha Cambio....: 26-marzo-2014
    //Autor...........: Angel Florentino Vargas Pool
    //Email...........: angelvargaspool@gmail.com
    //desc............: Funciona Con registro.php,registry.php
    //desc............: modificado info y otras opciones del paginador
    //------------------------------------------------------------------------
    class paginator
    {
    	public $sql;
    	public $totaldatos = 0;
    	public $mostrar = 0;
    	public $inicio = 0;
    	public $pagina = 1;
        private $limite;
        private $totalpaginas;
        private $iniciopag;
        private $finpag;
        ///primero limitamos el sql
        public function contar(){
        	//expresion regular encargada de buscar un sql//
        	$patron = "/.+(from)\s(.*)(\swhere\s.+)?/i";
            //$patron = "/.+(from)\s(\w+)(\swhere\s.+)?/i";
			//el reemplazo
			$coincidencia = 'SELECT COUNT(*) as totaldatos FROM \\2 \\3';
			//generacion de nueva consulta
			$consulta = preg_replace($patron, $coincidencia, $this->sql);
            //echo $consulta.'<br>';
			return $consulta;
        }
        public function getTotalPaginas(){
            return $this->totalpaginas;
        }
        public function sqllimit()
        {
			if ($this->totaldatos==0) {
				$this->mostrar=0;
				$txtsql='';
			}
			else{
				$this->datosPaginar();
				$txtsql=' LIMIT '.$this->inicio.','.$this->mostrar;
			}
			//echo '"'.$this->sql.$txtsql.'"';
			return $this->sql.$txtsql;
        }
        private function datosPaginar()
		{
			if ($this->pagina>1) {
				$mostrar=($this->mostrar)*($this->pagina-1);
				$this->inicio=$mostrar;
				//echo $this->inicio;
			}
			else{
				$this->inicio=0;
			}
		}
		////////////////html-paginador///////////////////
		public function htmlpaginador($ruta,$ver=0)
		{
			$total=$this->totaldatos;//total de datos
			$mostrar=$this->mostrar;//cuanto vamos a mostrar
            //$mostrar=$mostrar;//
            if($mostrar<=0){
                //si mostrar es menor o igual a 0, el limite es igual a 1
                $limite=1;
            }
            else{
                //si mostrar es diferente de 0, limite es igual a mostrar;
                $limite=$mostrar;
            }
            $totalregistros=$total;
            if($this->pagina && is_numeric($this->pagina)){
                $this->iniciopag = ($this->pagina - 1) * $limite;
                $this->iniciopag = $this->iniciopag+1;//dato inicial para info
                if($total<$this->mostrar){
                    $this->finpag=$total;
                }
                else{
                    $this->finpag= $this->mostrar * $this->pagina;//dato fin para info
                }
            }
            else
            {
                $this->pagina = 1;
                $this->iniciopag = 1;//dato inicial para info
                if($total < $this->mostrar){
                    $this->finpag=$total;
                }
                else{
                    $this->finpag= $this->mostrar * $this->pagina;//dato fin para info
                }
            }
            //$pagina=$this->pagina;
            $this->totalpaginas = ceil($totalregistros / $limite);
            ///html///
            $paginador='';
            $basea='<a href="%s" class="page %s" data-page="%d" data-link="%s">%s</a>';
            $ruta=_PATH_ABS_.$ruta;
            $clase='';
            if ($ver==0) {
                $principal = sprintf($basea,$ruta.$this->pagina.'/','activo',$this->pagina,$ruta,'Pagina '.$this->pagina).' ';
            }
            else{
                $principal='';
                if ($this->totalpaginas==1) {
                    $principal = sprintf($basea,$ruta,'activo',$this->pagina,$ruta,1).' ';
                }
                else{
                    if($ver>$this->totalpaginas){
                        $desde=1;
                        $hasta=$this->totalpaginas;
                    }
                    else{
                        $desde=$this->pagina-ceil(($ver/2)-1);
                        if($ver%2==0){
                            $hasta=$this->pagina+ceil(($ver/2));
                        }
                        else{
                            $hasta=$this->pagina+ceil(($ver/2)-1);
                        }
                        if($desde<1){
                            $hasta -=($desde-1);
                            $desde=1;
                        }
                        if($hasta>$this->totalpaginas){
                            $desde -=($hasta-$this->totalpaginas);
                            $hasta=$this->totalpaginas;
                        }
                    }
                    for ($i=$desde; $i<=$hasta ; $i++) {
                        if($i==$this->pagina) {
                            $clase='activo';
                        }
                        else{
                            $clase='';
                        }
                        $principal .= sprintf($basea,$ruta.$i.'/',$clase,$i,$ruta,$i).' ';
                    }
                }
            }
            $pagsig=$this->pagina+1;
            $pagant=$this->pagina-1;
            $primera   = sprintf($basea,$ruta,'',1,$ruta,'&lt;&lt;');
            $siguiente = sprintf($basea,$ruta.$pagsig.'/','',$pagsig,$ruta,'&gt;');
            $anterior  = sprintf($basea,$ruta.$pagant.'/','',$pagant,$ruta,'&lt;');
            $ultimo    = sprintf($basea,$ruta.$this->totalpaginas.'/','',$this->totalpaginas,$ruta,'&gt;&gt;');
            if ($this->totalpaginas==1) {
                $paginador=$principal;
            }
            else{
                if ($this->pagina==1) {
                    $paginador=$principal.$siguiente.' '.$ultimo;
                }
                elseif ($this->pagina==$this->totalpaginas) { 
                    $paginador=$primera.' '.$anterior.' '.$principal;
                }
                else{
                    $paginador=$primera.' '.$anterior.' '.$principal.$siguiente.' '.$ultimo;
                }
            }
            //
            if ($mostrar > 0 && $this->totaldatos > 0) {
                $paginador = $paginador;
            }
            else{
                //$basea='<a href="%s" class="page %s" data-page="%d" data-link="%s">%s</a>';
                $paginador = sprintf($basea,$ruta,'activo',1,$ruta,1);
            }
            return $paginador;
		}
        public function info()
        {
            $base='<div class="pag-info">mostrando %d - %d de %d</div>';
            $inicio = $this->iniciopag;
            $fin = $this->finpag;
            $total = $this->totaldatos;
            $mostrar = $this->mostrar;
            if ($mostrar > 0 && $total > 0)
                $inicio = $inicio;
            else
                $inicio = 0;
            if($total <= $fin)
                $fin = $total;
            $info = sprintf($base, $inicio, $fin, $total);
            return $info;
        }
    }
?>