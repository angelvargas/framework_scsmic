<?php
	//clase de arranque
	class Bootstrap{
		//para instanciar a la clase request
		public static function Run(Request $peticion){
			$modulo=$peticion->getModulo();
			$controlador=$peticion->getControlador().'Controller';
			//obtenemoos el nombre del controlador
			//$rutaControlador=_ROOT_.'controllers'._DS_.$controlador.'.php';
			//echo $rutaControlador;
			//obtenemos la ruta del controlador
			$metodo=$peticion->getMetodo();
			//obtenemos el nombre del metodo
			$args=$peticion->getArgumentos();
			//obtenemos los argumentos
			if($modulo){
				$rutaModulo=_ROOT_.'controllers'._DS_.$modulo.'Controller.php';
				if(is_readable($rutaModulo)){
					require_once $rutaModulo;
					$rutaControlador=_ROOT_.'modules'._DS_.$modulo._DS_.'controllers'._DS_.$controlador.'.php';
				}else{
					throw new Exception("Error de base de Modulo");
					
				}
			}else{
				$rutaControlador=_ROOT_.'controllers'._DS_.$controlador.'.php';
				//obtenemos la ruta del controlador
			}
			//echo $rutaControlador; exit;
			if(is_readable($rutaControlador)){
			//si el controlador es un archivo legible
				require_once($rutaControlador);//incluimos el controlador
				$control=new $controlador;//instanciamos  la clase
				if(is_callable(array($control,$metodo))){
					//$control->$metodo();
					$metodo=$peticion->getMetodo();
					//cargamos el metodo
				}else{
					//echo $metodo;
					//throw new Exception('Error no encontramos este sitio');
					header("HTTP/1.0 404 Not Found");  
	    			echo file_get_contents(_PATH_ABS_.'error/access/404/');
	    			exit();
					//$control->index();
					//$metodo='index';
					//si no existe un metodo, asignamos a un metodo index
				}
				if(isset($args)){//si existen argumentos
	                call_user_func_array(array($control, $metodo), $args);//ejecuta el metodo con sus respectivos argumentos
	            }
	            else{//si no existen argumentos
	                call_user_func(array($control, $metodo));//ejecuta el metodo
	            }

			}else{//si no
				header("HTTP/1.0 404 Not Found");  
    			echo file_get_contents(_PATH_ABS_.'error/access/404/');
    			exit();
				//throw new Exception('Error, no existe el destino');//mensaje de error
			}
		}
	}
?>