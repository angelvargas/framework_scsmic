<?php
/**
**Lista de Control de Acceso
**/
	class ACL {
		private $_id;
		private $_rol;
		private $_permisos;
		private $_modelo;
		public $_modulo;
		public $_app;
		private $_datauser;
		public $permisos_publicos;
		public function __construct() {
			include(_MODELO_.'acl.php');
			$this->_modelo = new aclmodelo();
			//$this->_datauser = $this->_modelo->getRoles();
		}
		public function getid($iduser=0) {
			$this->_modelo->getRoles($iduser);
			$this->compilar();
			return null;
		}
		
		public function getPermisosRole()
		{
			return $this->_modelo->getPermisosRol();
		}
		public function getPermisosUsuario()
		{
			return $this->_modelo->getPermisosUsuario();
		}
		public function compilar()
		{
			$permisos_rol = $this->getPermisosRole();
			$permisos_usuario = $this->getPermisosUsuario();
			$comun = array_intersect_key($permisos_usuario,$permisos_rol);
			$this->_permisos = array_merge($permisos_rol,$comun);
			$this->_permisos_publicos = $this->_permisos;
			return $this->_permisos;
		}
		public function getmetodos()
		{
			$getmetodo=$this->_permisos;
			$metodos=array();
			$cont=0;
			$flag1='';
			foreach ($getmetodo as $key => $value) {
				$valor = $value['valor'];
				$nombre = $value['nombremodulo'];
				$keym = $value['keymodulo'];
				$flag2 = $value['keymodulo'];
				$keyapp = $value['key'];
				$menum = $value['menum'];
				$menup = $value['menup'];
				$img = $value['imagen'];
				$apli = $value['apli'];

				$llave = $apli.'_'.$keym;
				//--------------------------------------------------------------------
				//flag 2 almacena la llave del modulo
				//si son direntes flag 1 o flag 2 se procede a crear un nuevo elemento de array
				//si son iguales se omite, esto es para que tome el primer permiso o seccion como
				//base para crear su url
				//se agrego una nueva condicion en donde la varible menup correspondiente a si una
				//funcion es visible o invisible en la aplicacion debe tener el valor 1, o sea debe
				//ser visible, asi descartar todas aquellas que son invisibles
				//--------------------------------------------------------------------				
				if($flag2!=$flag1 && $menup==1) {
					$flag1=$flag2;
					if(($valor==true || $valor==1)) {
						$metodos[$llave]['nombre'] = $nombre;
						$metodos[$llave]['keym'] = $keym;
						$metodos[$llave]['img'] = $img;
						$metodos[$llave]['key'] = $keyapp;
						$metodos[$llave]['menum'] = $menum;
						$metodos[$llave]['menup'] = $menup;
						$metodos[$llave]['apli'] = $apli;
					}
				}
			}
			return $metodos;
		}
		public function permiso($key,$modulo='',$app='') {
			$modulo = !empty($modulo)?$modulo:$this->_modulo;
			$app = !empty($app)?$app:$this->_app;
			$key = $key.'@'.$modulo.'@'.$app;

			if(array_key_exists($key, $this->_permisos)) {
	            if($this->_permisos[$key]['valor'] == true || $this->_permisos[$key]['valor'] == 1) {
	                return true;
	            }
	        }

	        $categorias = Session::get(_PREFIX_.'l_categorias');
	        if( in_array('1', $categorias, true) )
	        	return true;
	        	
	        return false;
		}
		public function acceso($key,$modulo='',$app='') {  
	        if($this->permiso($key,$modulo,$app)){
	            return;
	        }
	        echo file_get_contents(_PATH_ABS_.'error/access/401/');
    		exit();
	    }

	    public function dataMenu(){
	    	$data = array();
	    	foreach ($this->_permisos as $key => $value) {
	    		$visible = $value['menup'];
	    		$aplicacion = $value['apli'];
	    		$modulo = $value['keymodulo'];
	    		$llave = $value['key'];
	    		$key2 = $llave.'@'.$modulo.'@'.$aplicacion;
	    		$data[$key2] = $key2;
	    		/*
	    		if($visible == 1){
	    			$data[$aplicacion]['nombre'] = $value['nombre_apli'];
	    			$data[$aplicacion][$modulo]['nombre'] = $value['nombremodulo'];
	    			$data[$aplicacion][$modulo]['imagen'] = $value['imagen'];
	    			$data[$aplicacion][$modulo][$llave]['nombre'] = $value['nombrepermiso'];
	    			$data[$aplicacion][$modulo][$llave]['key'] = $key2;
	    		}
	    		*/
	    	}
	    	return $data;
	    }
	}
?>