<?php
	/**
	*imayorista
	*/
	class Registry
	{
		private static $_instancia;
		private $_data;
		//private al constructor para evitar instanciar la clase
		private function __construct(){}
		public static function getInstancia()
		{
			if(!self::$_instancia instanceof self)
			{
				self::$_instancia = new Registry();
			}
			return self::$_instancia;
		}
		public function __set($name, $value)
		{
			$this->_data[$name] = $value;
		}
		public function __get($name)
		{
			if(isset($this->_data[$name]))
			{
				return $this->_data[$name];
			}
			return false;
		}
		public function __clone()
		{
			trigger_error("Operación Invalida: No puedes clonar una instancia de ". get_class($this) ." class.", E_USER_ERROR );
		}
		public function __wakeup()
		{
			trigger_error("No puedes deserializar una instancia de ". get_class($this) ." class.");
		}
	}

	//$registro = $Registro::getInstancia();
	//$registro->acl=new ACL();
	/*
	$registro = $Registro::getInstancia();
	$registro->_request = new Request();
	Bootstrap::ini($registro->_request);

	*/
?>