<?php
	/*
	**@Title...............: Clase base para modelos
	**@File................: Model.php
	**@version.............: 6.0
	**@Last changed........: 11-Abril-2015
	**@Autor...............: Angel Florentino Vargas Pool
	**@Email...............: angelvargaspool@gmail.com
	**@desc................: eventos de ordenes de trabajo
	*/
	class Model extends Conectar {
		protected $sql;
		protected $inicio=0;
		public    $mostrar=0;
		protected $totaldatos=0;
		//
		protected $acl;
		protected $htmlcreator;
		protected $paginator;
		protected $_catuser = array();
		protected $_roluser = array();
		protected $_foluser = array();
		protected $_empuser = array();
		//
		private $vista;
		//
		public $datadbguia = array();

		public function __construct( $datadb = array() ) {
			parent::__construct( $datadb );
			$registro = Registry::getInstancia();
			$this->acl = $registro->acl;
			$this->htmlcreator = $registro->htmlcreator;
			$this->paginator = $registro->paginator;
			/***/
			$this->_catuser = Session::get(_PREFIX_.'l_categorias');
			$this->_roluser = Session::get(_PREFIX_.'l_roles');
			$this->_foluser = Session::get(_PREFIX_.'l_folios');
			$this->_empuser = Session::get(_PREFIX_.'l_empresas');
			/***/
			$this->vista = new View();
			/***/
			//echo '<br>';
			//var_dump(Session::get(_PREFIX_.'l_categorias'));
		}
		########################################################
		//Modelos De Framework
		########################################################
		public function crypt_blowfish($password, $digito = 7)
	    {
			$set_salt = './1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
			//$salt = sprintf('$2y$%02d$', $digito);
			$salt = sprintf('$2x$%02d$', $digito);
			for($i = 0; $i < 22; $i++)
			{
				$salt .= $set_salt[mt_rand(0, 63)];
			}
			return crypt($password, $salt);
		}
		protected function sqlpag($sql, $conex = '') {
			$this->paginator->sql=$sql;
			$count = $this->paginator->contar();
			$this->setQuery($count);
			$total = $this->getResultado();
			$TotalPaginas = $this->paginator->mostrar * $this->paginator->pagina;
			//echo 'Total=>'.$total['totaldatos'].'<br>';
			//echo 'Mostrar=>'.$this->paginator->mostrar.'<br>';
			//echo 'TotalPagina=>'.$TotalPaginas.'<br>';
			$this->paginator->totaldatos = $total['totaldatos'];
			if($total['totaldatos']<=$this->paginator->mostrar){
				$this->paginator->pagina=1;
			}
			if($total['totaldatos']<$TotalPaginas){
				$pag = ceil($total['totaldatos'] / $this->paginator->mostrar);
				$this->paginator->pagina = $pag;
			}
			//echo $this->paginator->pagina;
			$newsql=$this->paginator->sqllimit();
			$cnx = empty($conex)?$this:$conex;
			$cnx->setQuery($newsql);
		}

		public function textsearch($campo, $texto, $logica='AND') {
			$resultado = '';
			$data = array();
			if ( !empty($texto) ) {
				$texto = explode(' ', $texto);
				foreach ($texto as $key => $value) {
					if( !empty($value) ){
						if( is_array($campo) ){
							foreach ($campo as $kccampo => $vcampo) {
								$data[] = $vcampo.' LIKE "%'.$value.'%"';
							}
						}
						else{
							$data[] = $campo.' LIKE "%'.$value.'%"';
						}
					}
				}
				$resultado = $this->htmlcreator->strconcat($data,' '.$logica.' ');
			}
			return $resultado;
		}
		/**
		**Decapred by textsearch
		**/
		public function txtbuscador($campo, $texto) {
			$resultado = $campo.' LIKE "%'.$texto.'%"';
			$data = array();
			$texto = explode(' ', $texto);
			if (!empty($texto)) {
				foreach ($texto as $key => $value) {
					if( is_array($campo) ){
						foreach ($campo as $kccampo => $vcampo) {
							$data[] = $vcampo.' LIKE "%'.$value.'%"';
						}
					}
					else{
						$data[] = $campo.' LIKE "%'.$value.'%"';
					}
				}
				$resultado = $this->htmlcreator->strconcat($data,' AND ');
				//campo and campo and campo
			}
			return $resultado;
		}
		/**
		**Decapred by textsearch
		**/
		public function txtbuscador_or($campo, $texto) {
			$resultado = $campo.' LIKE "%'.$texto.'%"';
			$data = array();
			$texto = explode(' ', $texto);
			if ( !empty($texto) ) {
				foreach ($texto as $key => $value) {
					if( is_array($campo) ){
						foreach ($campo as $kccampo => $vcampo) {
							$data[] = $vcampo.' LIKE "%'.$value.'%"';
						}
					}
					else{
						$data[] = $campo.' LIKE "%'.$value.'%"';
					}
				}
				$resultado = $this->htmlcreator->strconcat($data,' OR ');
				//campo or campo or campo
			}
			return $resultado;
		}
		/**
		**
		**/
		public function convertir_numeros_texto($numero){
			$uni_text=array('','uno','dos','tres','cuatro','cinco','seis','siete','ocho','nueve');
			$dece_text=array('diez','veinte','treinta','cuarenta','cincuenta','sesenta','setenta','ochenta','noventa','cien');
			$num_esp=array('once','doce','trece','catorce','quince');
			$dece_text2=array('dieci','veinti','treinta y ','cuarenta y ','cincuenta y ','sesenta y ','setenta y ','ochenta y ','noventa y ');
			$cent_text=array('ciento ','cientos ','quinientos ','setecientos ','novecientos ');
			$miles_text=array(' mil ');
			if($numero>=0 && $numero<=9){
				$texto_numero=$uni_text[$numero];
			}
			else{
				if($numero>=10 && $numero<=100){
					if($numero>=11 && $numero<=15){
						$texto_numero=$num_esp[$numero-11];
					}
					else{
						$res=$numero/10;
						$mod=$numero%10;
						if($mod==0){
							$texto_numero=$dece_text[$res-1].''.$this->convertir_numeros_texto($mod);
						}
						else{
							$texto_numero=$dece_text2[$res-1].''.$this->convertir_numeros_texto($mod);				
						}			
					}
				}
				elseif($numero>=100 && $numero<1000){
					$res=(int)($numero/100);
					$mod=$numero%100;
					if($res==1){
						$texto_numero=$cent_text[0].$this->convertir_numeros_texto($mod);
					}
					elseif(($res>=2 && $res<=4) || $res==6 || $res==8){//500,700,900
						$texto_numero=$uni_text[$res].$cent_text[1].$this->convertir_numeros_texto($mod);
					}
					elseif($res==5 || $res==7 || $res==9){
						switch($res){
							case 5:
								$val=2;
							break;
							case 7:
								$val=3;
							break;
							case 9:
								$val=4;
							break;
						}
						$texto_numero=$cent_text[$val].$this->convertir_numeros_texto($mod);
					}
				}
				elseif($numero>=1000 && $numero<1000000){
					$res=(int)($numero/1000);
					$mod=$numero%1000;
					if($res==1){
						$texto_numero=$miles_text[0].$this->convertir_numeros_texto($mod);
					}
					else{//500,700,900
						$texto_numero=$this->convertir_numeros_texto($res).$miles_text[0].$this->convertir_numeros_texto($mod);
					}

				}	
			}
			return $texto_numero;
		}
		public function keyGen(){
			$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
			$key = "";
			for ( $i=0; $i<12; $i++ ) {
				$key .= substr($str,rand(0,62),1);
			}
			return $key;
		}
		public function setclave($tabla, $campo, $clave = '') {
			$cnx = new Conectar();
			$real_clave = empty($clave)?date('Y'):$clave;
			$sql = 'SELECT '.$campo.' FROM '.$tabla.' WHERE '.$campo.' REGEXP "^'.$real_clave.'-[0-9]" ORDER BY '.$campo.' DESC LIMIT 1';
			$pre = $clave;
			$cnx->setQuery($sql);
			$res = $cnx->getResultadoNumerico();
			if($res) {
				$fila = $cnx->getResultado('object');
				$folio_actual = explode('-',$fila->$campo);
				$numero_nuevo=$folio_actual['1']+1;
				$folio = $real_clave.'-'.str_pad($numero_nuevo, 4, "0", STR_PAD_LEFT);
			}
			else {
				$folio = $real_clave.'-'.str_pad('1', 4, "0", STR_PAD_LEFT);
			}
			return $folio;
		}
		//Funcion Encargada de verificar si una fecha se encuentra dentro de un rango/****/
		public function check_in_range($start_date, $end_date, $evaluame) {
		    $start_ts = strtotime($start_date);
		    $end_ts = strtotime($end_date);
		    $user_ts = strtotime($evaluame);
		    return ( ($user_ts >= $start_ts) && ($user_ts <= $end_ts) );
		}
		//Para dar formato a fechas, obsoleta, en reportes model queda el ultimo en utilizarlo
		public function cambia_fecha($fecha, $hora=0){
			///////fecha
			$fecha_x = substr($fecha,0,10);
			$mes_c   = array('Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic');
			$date    = explode('-',$fecha_x);
			if($date[1]==00){
				$mes='NNN';
			}else{
				$mes=$mes_c[intval(substr($fecha,5,2))-1];
			}
			$dia     = $date[2].'/'.$mes.'/'.$date[0];
			///////////hora
			if($hora==0){
				$hora_x  = substr($fecha,11,8);
				$hora    = explode(':',$hora_x);
				if($hora[0]>=01 && $hora[0]<12){
					$hora_n=$hora[0];
					$sigla='AM';
				}else if($hora[0]==12){
					$hora_n=$hora[0];
					$sigla='PM';
				}else if($hora[0]>12 && $hora[0]<=23){
					$sigla='PM';
					$hora_n=($hora[0]-12);
					if($hora_n<10){
						$hora_n='0'.$hora_n;
					}else{
						$hora_n=$hora_n;
					}
				}else if($hora[0]==00){
					$sigla='AM';
					$hora_n='12';
				}
				$hour=$hora_n.':'.$hora[1].':'.$hora['2'].' '.$sigla;
			}else{
				$hour='';		
			}
			return $dia.' '.$hour;
		}
		//para limpiar un texto, obsoleto, au se utiliza en inventarioModel y TecnicoModel
		public function texto_formato($texto){
			$texto=strip_tags($texto);
			$texto=mb_strtolower($texto,'utf-8');
			$texto=str_replace('"',"''", $texto);
			return $texto; 
		}
		//Funcion obsoleta generador de usuriarios de utiliza en web usuario model
		public function generador_usuario($nombre){
			$user=explode(' ',$nombre);
			if(isset($user[1])){
				$nombre=$user[1];
			}else{
				$nombre=$user[0];
			}
			$str = "1234567890";
			$cad = "";
			for($i=0;$i<5;$i++) {
				$cad .= substr($str,mt_rand(0,10),1);
			}
			$usuario=substr($nombre,0,1);
			$usuario=$user[0].$usuario.$cad;
			return $usuario;
		}
		//Funcion obsoleta generador de passwords se utiliza en web usuario model
		public function generador_contrasena(){
			$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
			$cad = "";
			for($i=0;$i<8;$i++) {
				$cad .= substr($str,rand(0,62),1);
			}
			return $cad;
		}
		/*
		**Funcion que sirve para permitir usuarios de 4 hasta 28 caracteres de longitud, 
		**alfanuméricos y permitir guiones bajos.
		*/
		public function size_user($estring){
			$resultado = false;
			if (preg_match('/^[a-z\d_]{4,28}$/i', $string)) {
				$resultado = true;
			}
			return $resultado;
		}
		/*
		*Funcion encargada de obtener el primer y ultimo dia de un mes y año especificado
		*/
		public function first_last_day_monday( $m = 1, $y = 2000){
			$month = date($m);
			$year = date($y);

			$day = date("d", mktime(0,0,0, $month+1, 0, $year));

			$last_day =  date('Y-m-d', mktime(0,0,0, $month, $day, $year));

			$first_day =  date('Y-m-d', mktime(0,0,0, $month, 1, $year));

			$data['firstday'] = $first_day.' 00:00:00';
			$data['lastday'] = $last_day.' 23:59:59';

			return $data;
		}
		########################################################
		//Modelo De Sistema
		########################################################
		public function getNombreUsuario($n=0, $sae=false, $emp=false) {
			$conexion= new Conectar();
			$n = $conexion->setEscapar($n);
			if ($n==0) {
				$nombre='No asignado';
			}
			else{
				$sql='SELECT usr.nombre, emp.sae, usr.sae as saec, emp.nombre_empresa FROM usuario usr INNER JOIN usuario_cuenta uc ON usr.id_usuario = uc.idusuario LEFT JOIN empresa emp ON uc.idempresa=emp.id_empresa WHERE uc.folio="'.$n.'" LIMIT 1';
				$conexion->setQuery($sql);
				$resultado=$conexion->getResultadoNumerico();
				if($resultado) {
					$fila=$conexion->getResultado('object');
					$nombre=$fila->nombre;
					if($emp){
						$name_emp = $fila->nombre_empresa;
						$nombre.= !empty($name_emp) ? '('.$name_emp.')' : '';
					}
					if($sae){
						$sae = empty($fila->sae)?$fila->saec:$fila->sae;
						$nombre.= !empty($sae) ? '('.$sae.')' : '';
					}
				}
				else {
					$nombre='no existe';
				}
				$conexion->setTerminar();
			}	
			return $nombre;
		}
		public function lstusers($params='') {
			$datos = array();
			$sql='SELECT usr.id_usuario, usr.nombre, emp.nombre_empresa, uc.folio, emp.sae, usr.sae as saec FROM usuario usr INNER JOIN usuario_cuenta uc ON uc.idusuario=usr.id_usuario INNER JOIN rol r ON uc.idperfil=r.idrol LEFT JOIN empresa emp ON uc.idempresa=emp.id_empresa WHERE uc.estatus=1';
			//WHERE r.categoria>2 AND uc.estatus=1'
			if(!empty($params)){
				foreach ($params as $key => $value) {

				}
			}
			$this->setQuery($sql);
			$res = $this->getResultadoNumerico();
			$cont = 0;
			if($res){
				while ( $fila=$this->getResultado('object') ) {
					$cont++;
					$sae = empty($fila->sae)?$fila->saec:$fila->sae;
					$datos[$cont]['id'] = $fila->folio;
					$datos[$cont]['nombre'] = $fila->nombre.' ('.$fila->nombre_empresa.')'.' ('.$sae.')';;
				}
				
			}
			return $datos;
		}
		public function getusuario($post, $flag=0) {
			$resp = '';
			if( !empty($post) || $flag==1 ) {

				if( !empty($post) ){
					foreach ($post as $key => $value) {
						$$key = $this->setEscapar($value);
					}
				}
				
				$sql='SELECT usr.id_usuario, usr.nombre, emp.nombre_empresa, uc.folio, emp.sae, usr.sae as saec FROM usuario usr INNER JOIN usuario_cuenta uc ON uc.idusuario=usr.id_usuario INNER JOIN rol r ON uc.idperfil=r.idrol LEFT JOIN empresa emp ON uc.idempresa=emp.id_empresa WHERE r.categoria>2 AND uc.estatus=1';
				if($flag == 0){
					$sql.= ' AND ('.$this->textsearch('usr.nombre', $term).' OR '.$this->textsearch('emp.nombre_empresa',$term).')';
				}
				
				$this->setQuery($sql);
				$res = $this->getResultadoNumerico();
				$row_set = array();
				if($res){
					while ($fila=$this->getResultado('object')) {
						$sae = empty($fila->sae)?$fila->saec:$fila->sae;
						if($flag==0){
							$row['value'] = $fila->nombre.' ('.$fila->nombre_empresa.')'.' ('.$sae.')';
							$row['nombre'] = $fila->nombre;
						}
						else{
							$row['nombre'] = $fila->nombre.' ('.$fila->nombre_empresa.')'.' ('.$sae.')';
							$row['value'] = $fila->nombre;
						}
						
						$row['id'] = $fila->folio;
						$row['empresa'] = $fila->nombre_empresa;
						
						$row_set[] = $row;//build an array
					}
					if($flag==0){
						$resp = json_encode($row_set);
					}
					else{
						$resp = $row_set;
					}
				}
			}
			return $resp;
		}
		//------------------------------------------------------------------------
		//Modelo funcion que se encarag de optener una lista de los metodos
		//------------------------------------------------------------------------	
		public function optMetodos( $flag = false )
		{
			$conex = new Conectar();
			$sql = 'SELECT idmodulos,nombremodulo FROM modulos';
			$conex->setQuery($sql);
			$res = $conex->getResultadoNumerico();
			$datos = array();
			$cont = 1;
			if( $res ) {
				while ( $fila=$conex->getResultado( 'object' ) ) {
					$cont++;
					$datos[$cont]['id'] = $fila->idmodulos;
					$datos[$cont]['nombre'] = $fila->nombremodulo;
				}
			}
			return $datos;
		}
		/*
		**Funcion encargada de obtener los permisos de rol 
		**de un usuario o
		**de un rol/perfil especifico
		*/
		public function permisos_rol($entradas) {
			//var_dump($entradas);
			$conex = new Conectar();
			$rol = $conex->setEscapar($entradas['rol']);
			$modulo = $conex->setEscapar($entradas['modulo']);
			$sql = 'SELECT m.nombremodulo,m.menum,p.nombrepermiso,p.keypermiso,pr.valor,p.idpermiso,p.modulo,p.descripcion FROM modulos m INNER JOIN permisos p ON m.idmodulos = p.modulo LEFT JOIN permisos_rol pr ON p.idpermiso = pr.permiso WHERE p.estatus=1 AND m.estatus=1';
			if($modulo>0) {
				$sql.=' AND m.idmodulos="'.$modulo.'" ';
			}
			if( isset($entradas['user']) ) {
				$id_usuario = $entradas['user'];
				$roles =  $this->getrolesuser($id_usuario);
				$rolesuser = (count($roles) > 0) ? implode(',', $roles) : '0';
				$sql.=' AND rol IN ('.$rolesuser.')';
			}
			else {
				$sql.=' AND rol="'.$rol.'"';
			}
			$sql.=' ORDER BY m.idmodulos,p.idpermiso';

			$conex->setQuery($sql);
			$res=$conex->getResultadoNumerico();
			$datos=array();
			if($res) {
				while ($fila=$conex->getResultado('object')) {
					//$key=$fila->keypermiso;
					$key=$fila->keypermiso.'_'.$fila->modulo.'_'.$fila->menum;
					
					if( array_key_exists($key, $datos) ) {
		            	if($datos[$key]['Estado'] == 1){
		            		//$datos[$key]['Estado'] = 1;
		            		$valor = 1;
		            	}
		            	else{
		            		//$datos[$key]['Estado'] = $fila->valor;
		            		$valor = $fila->valor;
		            	}	
		            }
		            else {
		            	//$datos[$key]['Estado'] = $fila->valor;
		            	$valor = $fila->valor;
		            }


					//$key=$fila->keypermiso;
					//$valor = $fila->valor;
					$idpermiso=$fila->idpermiso;
					$datos[$key]['Modulo']=$fila->nombremodulo;
					$datos[$key]['Permiso']=$fila->nombrepermiso;
					$datos[$key]['descripcion'] = $fila->descripcion;
					//$datos[$key]['Estado']=$fila->valor;
					//-------------------------------------------------------------------
					$checked1='';
					$checked2='';
					$estado='Denegado';

					if($valor==1) {
						$checked1='checked';
						$estado='Habilitado';
					}
					elseif($valor==0) {
						$checked2='checked';
						$estado='Denegado';
					}
					$datos[$key]['Estado'] = $estado;
					$datos[$key]['Permitir'] = $this->htmlcreator->getInput('radio',array('class'=>'permiso_rol','name'=>$idpermiso,'value'=>1,$checked1));
					$datos[$key]['Denegar'] = $this->htmlcreator->getInput('radio',array('class'=>'permiso_rol','name'=>$idpermiso,'value'=>0,$checked2));
					$datos[$key]['Ignorar'] = $this->htmlcreator->getInput('radio',array('class'=>'permiso_rol','name'=>$idpermiso,'value'=>"x"));
				}
			}
			else {
				if(isset($entradas['flag']) && $entradas['flag']==1) {
					$datos[1]['Modulo'] = '';
					$datos[1]['Permiso'] = '';
					$datos[1]['descripcion'] = '';
					$datos[1]['Estado'] = '';
				}
				
			}
			return $datos;
		}
		//----------------------------------------------------------------
		//Modelo encargado de obtener el rol de un usuario
		//----------------------------------------------------------------
		public function getrolesuser($id_usuario)
		{
			$cnx = new Conectar();
			$sql = 'SELECT idperfil FROM usuario_cuenta uc INNER JOIN rol ro ON uc.idperfil=ro.idrol WHERE idusuario="'.$id_usuario.'"';
			$cnx->setQuery($sql);
			$resultado = $cnx->getResultadoNumerico();
			//$tipo=0;
			$count = 0;
			$data = array();
			if($resultado) {
				while ( $fila = $cnx->getResultado('object') ) {
					//$count++;
					$data[] = $fila->idperfil;
				}
			}
			return $data;
		}
		//------------------------------------------------------------------------
		// funcion para guardar los LOGS de registros
		//------------------------------------------------------------------------
		public function registrar_log($nombre,$login)
        {
        	//$archivo = "guardaip.txt";                                                    
        	//Esto creará el Archivo donde guarda las ips
        	//$manejador = fopen($archivo,"a") or die("Imposible abrir el archivo\n");      //Esto abre el archivo
			$manejador='';
			$ip = $_SERVER['REMOTE_ADDR'];                                                        //Esto muestra la ip
			$fecha= Date("Y-m-d H:i:s");                                                   //Esto muestra la fecha
			$servidor= $_SERVER['HTTP_USER_AGENT'];         //Esto muestra el navegador
			$nombre;
			$login;
			//fwrite($manejador,'IP = '.$ip."\r\n");  //Esto muestra la ip y la fecha en el archivo
			//fwrite($manejador,'Fecha = '.$fecha."\r\n");  //Esto muestra la ip y la fecha en el archivo
			//fwrite($manejador,'Navegador = '.$servidor."\r\n");  //Esto muestra la ip y la fecha en el archivo
			//fwrite($manejador,'Usuario = '.$nombre."\r\n");  //Esto muestra la ip y la fecha en el archivo
			//fwrite($manejador,'login = '.$login."\r\n");  //Esto muestra la ip y la fecha en el archivo
			//fwrite($manejador,'***************************************************'."\r\n");
			//fclose($manejador);
			$sql='INSERT INTO log_procesos (idlog_procesos,descripcion,usuario,navegador,direccion_ip,fecha) VALUES (0, "'.$login.'","'.$nombre.'","'.$servidor.'","'.$ip.'","'.$fecha.'") ';
			$this->setQuery($sql);
        }
		//para eliminar depues el rela seria tecnicos
		public function usuarios_sistema(){return $this->tecnicos();}
		//----------------------------------------------------------------
		//Obtiene los datos de una orden
		//----------------------------------------------------------------
		public function buscar_user($parametros, $post)
		{
			//parametros antiguos
			//$usuario=' ',$campo=1,$default=1
			$sqljoin = '';
			$sqlsearch = ' WHERE usr.id_usuario>=0';
			$sqlgroup = '';
			$sql = 'SELECT usr.*, emp.nombre_empresa, r.rolnombre, r.imagenrol, uc.idperfil, uc.folio, uc.estatus as est FROM usuario usr INNER JOIN usuario_cuenta uc ON usr.id_usuario = uc.idusuario LEFT JOIN empresa emp ON uc.idempresa=emp.id_empresa LEFT JOIN rol r ON uc.idperfil=r.idrol';//query busqueda de usuario

			if (!empty($parametros)) {
				foreach ($parametros as $key => $value) {
					$key = 'p'.$key;
					$$key = $value;
				}
				if (isset($pcategorias) && !empty($pcategorias)) {
					if (is_array($pcategorias)) {
						$data = array();
						foreach ($pcategorias as $value) {
							$data[]='"'.$value.'"';
						}
						$data = $this->htmlcreator->strconcat($data,',');
						$sqlsearch.=' AND r.categoria IN('.$data.')';
					}
					else{
						$pcategorias = (int)$pcategorias;
						$sqlsearch.=' AND r.categoria ='.$pcategorias;
					}
				}
				if (isset($pcampos) && !empty($pcampos)) {
					if (is_array($pcampos)) {
						foreach ($pcampos as $value) {
							$campo = 'campo_'.$value;
							$$campo = true;
						}
					}
					else{
						$campo = 'campo_'.$value;
						$$campo = true;
					}
				}
			}
			if (!empty($post)) {
				foreach ($post as $key => $value) {
					$$key = $value;
				}
				if (isset($nombre) && !empty($nombre)) {
					$sqlsearch.=' AND '.$this->txtbuscador('usr.nombre', $nombre);
				}
				if (isset($email) && !empty($email)) {
					$sqlsearch.=' AND '.$this->txtbuscador('usr.email', $email);
				}
				if (isset($empresa) && !empty($empresa)) {
					$sqlsearch.=' AND '.$this->txtbuscador('emp.nombre_empresa', $empresa);
				}
			}
			$sqlgroup = ' ORDER BY uc.estatus ASC, uc.idusuario DESC';
			$sql = $sql.$sqljoin.$sqlsearch.$sqlgroup;
			//echo $sql;
			$this->sqlpag($sql);
			$resultado = $this->getResultadoNumerico();
			$cont=0;
			$datos=array();
			if ($resultado) {
				while ($fila=$this->getResultado('object')) {
					$cont++;
					
					$nombre = $fila->nombre;
					$empresa = $fila->nombre_empresa;
					$direccion = $fila->direccion;
					$fecha = $fila->fecha_reg;
					$tipo = $fila->idperfil;
					$estatus = $fila->est;
					$id = $fila->id_usuario;
					$imgrol = $fila->imagenrol;
					$nombrerol = $fila->rolnombre;
					$folio = $fila->folio;
					if(isset($campo_unir)){
						$datos[$cont][' '] = $this->htmlcreator->getInput('checkbox', array('data-folio'=>$folio, 'data-id'=>$id, 'value'=>$folio, 'name'=>'perfiles[]'));
					}
					$info = $this->htmlcreator->getTag('a','',array('href'=>_PATH_ABS_.'usuarios/datospersonales/'.$id.'/','class'=>'datos','title'=>'Detalle'));
					$datos[$cont]['inf'] = $this->htmlcreator->getTag('div',$info,array('class'=>'print icono sistema-info links'));
					//$datos[$cont]['Folio'] = $this->htmlcreator->getTag('a',$idord,array('href'=>_PATH_ABS_.'orden/detalle_orden/'.$idord.'/'));
					$datos[$cont]['nombre'] = $nombre; //link de info antes
					$datos[$cont]['empresa'] = $empresa;
					//$datos[$cont]['telefono'] = $telefono;
					$imgrol = empty($imgrol)?'inactivo.png':$imgrol;
					$rutaimg = _ROOT_.'public'._DS_.'img'._DS_.'iconosrol'._DS_.$imgrol;
					$imgrol = file_exists($rutaimg)?$imgrol:'inactivo.png';
					$datos[$cont]['tipo'] = $this->htmlcreator->getTagSimple('img', array('src'=>_IMG_.'iconosrol/'.$imgrol, 'alt'=>$nombrerol, 'title'=>$nombrerol, 'width'=>'24', 'height'=>'24'));;
					//switch para mostrar una imagen dependiendo de su estado
					switch ($estatus) {
						case 1:
							$imgest=_IMG_.'plantilla/activo.png';
							$namest='Activo';
							break;
						case 2:
							$imgest=_IMG_.'plantilla/inactivo.png';
							$namest='Inactivo';
							break;
						default:
							$imgest=_IMG_.'plantilla/inactivo.png';
							$namest='Inactivo';
							break;
					}
					$datos[$cont]['estado'] = $this->htmlcreator->getTagSimple('img', array('src'=>$imgest, 'alt'=>$namest, 'title'=>$namest, 'width'=>'24', 'height'=>'24'));
					//--------------------------------------------
					if (isset($campo_editar)) {
						if ( $this->acl->permiso('modificar_usuario') ) {
							$ledit = $this->htmlcreator->getTag('a', '', array('href'=>_PATH_ABS_.'usuarios/modificar_usuario/'.$folio.'/'));
							$datos[$cont]['']['edit'] = $this->htmlcreator->getTag('div', $ledit, array('class'=>'print icono sistema-editar links', 'title'=>'Editar'));
						}
					}
					//--------------------------------------------
					if (isset($campo_ordenes)) {
						$ordclie = $this->htmlcreator->getTag('a', '', array('href'=>_PATH_ABS_.'orden/ordenes_cliente/'.$folio.'/'));
						$datos[$cont]['']['ordenes'] = $this->htmlcreator->getTag('div', $ordclie, array('class'=>'print icono sistema-buscar links', 'title'=>'Ordenes'));
					}
					//--------------------------------------------
					if (isset($campo_inventario)) {
						$inventario = $this->htmlcreator->getTag('a', '', array('href'=>_PATH_ABS_.'inventario/inventario/'.$folio.'/'));
						$datos[$cont]['']['inventario'] = $this->htmlcreator->getTag('div', $inventario, array('class'=>'print icono sistema-computer links', 'title'=>'Inventario'));
					}
					//--------------------------------------------
					if (isset($campo_permisos)) {
						if($estatus == 1) {
							$permisos = $this->htmlcreator->getTag('a', '', array('href'=>_PATH_ABS_.'acl/users/permisos_usuario/'.$id.'/'));
							$datos[$cont]['']['permisos'] = $this->htmlcreator->getTag('div', $permisos, array('class'=>'print icono sistema-permisos links', 'title'=>'Permisos'));
						}
						else{
							$datos[$cont]['']['permisos'] = '';
						}
					}
					//--------------------------------------------
					if ( isset( $campo_cuentas_usuario ) ) {
						$cuentas = $this->htmlcreator->getTag('a', '', array('href'=>_PATH_ABS_.'inventario/cuentas_usuario/'.$folio.'/'));
						$datos[$cont]['']['cuentas'] = $this->htmlcreator->getTag('div', $cuentas, array('class'=>'print icono sistema-users links', 'title'=>'Inventario'));
					}
					//--------------------------------------------
					if (isset($campo_password)) {
						if ( $this->acl->permiso('pass_user') ) {
							$pswrd = $this->htmlcreator->getTag('a', '', array('href'=>_PATH_ABS_.'usuarios/editpass/'.$folio.'/'));
							$datos[$cont]['']['pswrd'] = $this->htmlcreator->getTag('div', $pswrd, array('class'=>'print icono sistema-password links', 'title'=>'Modificar Password'));
						}
					}
				}
			}
			else {
				$datos[$cont]['nombre']='';
				$datos[$cont]['email'] = '';
				$datos[$cont]['empresa'] = '';
			}
			return $datos;
		}
		/*
		**Lista de empresas por tipo
		*/
		public function lstempresas($tipo = ''){
			$conexion=new Conectar();
			$sql='SELECT id_empresa,nombre_empresa FROM empresa WHERE id_empresa>0';
			if(!empty($tipo)){
				$sql.=' AND tipo="'.$tipo.'"';
			}
			$sql.=' ORDER BY nombre_empresa';
			//echo $sql;
			$conexion->setQuery($sql);
			$res=$conexion->getResultadoNumerico();
			$datos=array();
			$cont=0;
			if($res) {
				while($fila=$conexion->getResultado('object')) {
					$cont++;
					$datos[$cont]['id']=$fila->id_empresa;
					$datos[$cont]['nombre']=$fila->nombre_empresa;
				}
			}
			return $datos;
		}
		/*
		**susutituir por getNombreUsuario
		*/
		public function nombre_usuario($n){return $this->getNombreUsuario($n);}
		/*
		**Funcion Encargada de buscar empresas por medio de autocomplete
		*/
		public function busqueda_empresas($post) {
			$res = '';
			if(!empty($post)){
				foreach ($post as $key => $value){
					$$key = $this->setEscapar($value);
				}
				$sql = 'SELECT id_empresa, nombre_empresa FROM empresa WHERE nombre_empresa LIKE "%'.$term.'%"';
				if( !$this->acl->permiso('reportes_general', 'reportes', 'scsmic') ){
					$empresas = (count($this->_empuser) > 0) ? implode(',', $this->_empuser) : '';
					if(!empty($empresas)){
						$sql.=' AND id_empresa IN('.$empresas.')';
					}
				}
				$this->setQuery($sql);
				$res=$this->getResultadoNumerico();
				if($res){
					while ($fila=$this->getResultado('object')){
						$row['value'] = $fila->nombre_empresa;
						$row['id'] = $fila->id_empresa;
						$row_set[] = $row;//build an array
					}
					$res=json_encode($row_set);
				}
			}
			return $res;
		}
		//Enlista a las empresas para su posterior renderizacion en una tabla
		public function listEmpresa($post = '', $parametros = '')
		{
			$sqljoin = '';
			$sqlsearch = ' WHERE id_empresa>0';
			$sqlgroup = '';
			$sql='SELECT * FROM empresa';
			if (!empty($parametros)) {
				foreach ( $parametros as $key => $value ) {
					$key = 'p'.$key;
					$$key = $value;
				}
				if (isset($pcampos) && !empty($pcampos)) {
					if (is_array($pcampos)) {
						foreach ($pcampos as $value) {
							$campo = 'campo_'.$value;
							$$campo = true;
						}
					}
					else{
						$campo = 'campo_'.$value;
						$$campo = true;
					}
				}
			}
			if (!empty($post)) {
				foreach ($post as $key => $value) {
					$$key = $value;
				}
				if (isset($nombre) && !empty($nombre)) {
					$sqlsearch.=' AND '.$this->txtbuscador('nombre_empresa', $nombre);
				}
				if (isset($razon) && !empty($razon)) {
					$sqlsearch.=' AND '.$this->txtbuscador('razonSocial', $razon);
				}
			}
			$sqlgroup = ' ORDER BY id_empresa';
			$sql = $sql.$sqljoin.$sqlsearch.$sqlgroup;
			$this->sqlpag($sql);
			$resultado = $this->getResultadoNumerico();
			$datos=array();
			$cont=0;
			if($resultado){
				while ($fila=$this->getResultado('object')){
					$cont++;
					$empresa=$fila->nombre_empresa;
					$id=$fila->id_empresa;
					$info = $this->htmlcreator->getTag('a','',array('href'=>_PATH_ABS_.'usuarios/datosempresa/'.$id.'/','class'=>'datos','title'=>'Detalle'));
					$datos[$cont]['info'] = $this->htmlcreator->getTag('div',$info,array('class'=>'print icono sistema-info links'));
					$datos[$cont]['nombre de la empresa'] = $empresa;
					$datos[$cont]['razon social'] = $fila->razonSocial;
					$datos[$cont]['direccion'] = $fila->direccion;
					$datos[$cont]['logo']='';
					if (isset($campo_modificar)) {
						$ledit = $this->htmlcreator->getTag('a', '', array('href'=>_PATH_ABS_.'usuarios/modificar_empresa/'.$id.'/'));
						$datos[$cont]['']['modificar'] = $this->htmlcreator->getTag('div', $ledit, array('class'=>'print icono sistema-editar links', 'title'=>'Editar'));
					}
					if (isset($campo_equipos)) {
						$inventario = $this->htmlcreator->getTag('a', '', array('href'=>_PATH_ABS_.'inventario/inventario_empresa/'.$id.'/'));
						$datos[$cont]['']['inventario'] = $this->htmlcreator->getTag('div', $inventario, array('class'=>'print icono sistema-computer links', 'title'=>'Inventario'));
					}
					
				}
			}
			else{
				$datos[0]['nombre de la empresa']='';
				$datos[0]['razon social'] = '';
			}
			return $datos;
		}
		/*
		**Funcion que obtendra el o los email de un usuario
		*/
		public function get_email_user( $user ){
			$cnx = new Conectar;
			$sql = 'SELECT valor,idrel FROM usuario_empresa_contacto WHERE idrel="'.$user.'" AND tipo="1"';
			$cnx->setQuery($sql);
			$res = $cnx->getResultadoNumerico();
			$datos=array();
			$cont=0;
			//echo $user;
			if ($res) {
				while ($fila = $cnx->getResultado('object')) {
					$cont++;
					$email = $fila->valor;
					//echo $email;
					if(filter_var($email, FILTER_VALIDATE_EMAIL)){
						$datos[$cont]['email'] = $email;
						$datos[$cont]['nombre'] = $this->getNombreUsuario($user);
					}
				}
			}
			return $datos;
		}
		/*
		**Funcion Encargada de obtener 
		**datos de la cuenta del usuario logueado
		**y gusdarlas en datos de session
		*/
		public function getRoles($iduser=0) {
			$id_usuario = ( Session::get('l_id') == null ) ? $iduser : Session::get('l_id');
			$cnx = new Conectar();
			$sql = 'SELECT idperfil, idusuario, idempresa, categoria, folio, estatus FROM usuario_cuenta uc INNER JOIN rol ro ON uc.idperfil=ro.idrol WHERE idusuario="'.$id_usuario.'" AND estatus="1"';
			$cnx->setQuery($sql);
			$resultado = $cnx->getResultadoNumerico();
			$count = 0;
			$data = array();
			$categorias = array();
			$roles = array();
			$empresas = array();
			$folios = array();
			if($resultado) {
				while ( $fila = $cnx->getResultado('object') ) {
					$count++;
					$data[$count]['idperfil'] = $fila->idperfil;
					$data[$count]['idusuario'] = $fila->idusuario;
					$data[$count]['empresa'] = $fila->idempresa;
					$data[$count]['categoria'] = $fila->categoria;
					$data[$count]['folio'] = $fila->folio;
				}
				foreach ($data as $key => $value) {
					$categorias[] = $value['categoria'];
					$roles[$value['idperfil']] = $value['idperfil'];
					if( $value['empresa'] > 0)
						$empresas[] = $value['empresa'];
					$folios[] = $value['folio'];
				}	
			}
			Session::set(_PREFIX_.'l_categorias', $categorias);
			$this->_catuser = $categorias;
			Session::set(_PREFIX_.'l_roles', $roles);
			$this->_roluser = $roles;
			Session::set(_PREFIX_.'l_folios', $folios);
			$this->_foluser = $folios;
			Session::set(_PREFIX_.'l_empresas', $empresas);
			$this->_empuser = $empresas;
		}
		public function getRoles_static( $id = 0) {
			$id_usuario = $id;
			$cnx = new Conectar();
			$sql = 'SELECT idperfil, idusuario, idempresa, categoria, folio, estatus FROM usuario_cuenta uc INNER JOIN rol ro ON uc.idperfil=ro.idrol WHERE idusuario="'.$id_usuario.'" AND estatus="1"';
			$cnx->setQuery($sql);
			$resultado = $cnx->getResultadoNumerico();
			$count = 0;
			$data = array();
			$categorias = array();
			$roles = array();
			$empresas = array();
			$folios = array();
			$data_resp = array();
			if($resultado) {
				while ( $fila = $cnx->getResultado('object') ) {
					$id = $fila->idusuario;
					$data[$id]['idusuario'] = $id;
					$data[$id]['perfiles'][] = $fila->idperfil;
					$data[$id]['empresas'][] = $fila->idempresa;
					$data[$id]['categorias'][] = $fila->categoria;
					$data[$id]['folios'][] = $fila->folio;
				}
				/*
				foreach ($data as $key => $value) {
					$categorias[] = $value['categoria'];
					$roles[$value['idperfil']] = $value['idperfil'];
					if( $value['empresa'] > 0)
						$empresas[] = $value['empresa'];
					$folios[] = $value['folio'];
				}
				*/	
			}
			return $data;
			/*
			$data_resp['_catuser'] = '';
			$data_resp['_roluser'] = '';
			$data_resp['_foluser'] = '';
			$data_resp['_empuser'] = '';
			*/
			/*
			//Session::set(_PREFIX_.'l_categorias', $categorias);
			$this->_catuser = $categorias;
			//Session::set(_PREFIX_.'l_roles', $roles);
			$this->_roluser = $roles;
			//Session::set(_PREFIX_.'l_folios', $folios);
			$this->_foluser = $folios;
			//Session::set(_PREFIX_.'l_empresas', $empresas);
			$this->_empuser = $empresas;
			*/
		}
		/*
		*Modelo encargado en enviar un email a un tecnico
		*para avisarle que se le ha asiganado una orden
		*Requiere Modificacion
		*/
		public function send_mail_tecnico( $user, $orden ) {
			require_once(_LIBRERIAS_.'phpmailer/class.phpmailer.php');
			error_reporting(E_ALL);

			$mail = new PHPMailer();
			$mail->IsSMTP(); // set mailer to use SMTP
			$mail->SMTPDebug  = 1; //2->explicado y extendido
			$mail->From = "info@microged.com";
			$mail->FromName = "infomicroged";
			$mail->CharSet = 'UTF-8';
			$mail->Host = "smtp.gmail.com";//smtp.gmail.com //smtp.live.com
			$mail->SMTPSecure= "tls"; // Used instead of TLS when only POP mail is selected
			$mail->Port = 587; // Used instead of 587 when only POP mail is selected
			$mail->SMTPAuth = true;
			$mail->Username = "info@caribemexicano.com"; // SMTP username
			$mail->Password = "Exito2017"; // SMTP password
			//$mail->AddAddress("yuded@microged.com", "yuded");
			//$mail->AddAddress("angelvargaspool@gmail.com", "avargas");  
			//$mail->AddAddress($value['email'], $value['nombre']);
			$mail->Mailer = "smtp";

			$mail->IsHTML(true); // set email format to HTML
			$mail->Subject = 'Asignacion De Orden De Trabajo '.$orden.' SCSMIC';

			$contenido = $this->vista->getContent('orden:email_orden.html');
			$contenido = str_replace('{{img}}', _IMG_, $contenido);
			$contenido = str_replace('{{msg}}', 'La orden N°'.$orden.' Se le ha asignado', $contenido);
			$mail->Body = $contenido;

			$emails =  $this->get_email_user( $user );
			
			if( !empty($emails) ){
				foreach ( $emails as $llave => $valor ) {
					$mail->AddAddress($valor['email'], $valor['nombre']);
				}
				if( $mail->Send() ) {
					$msg = 'mensaje enviado';
				}
				else{
					$msg = 'error';
				}
			}
			else{
				$msg = 'correo vacio';
			}
			return $msg;
		}
		//------------------------------------------------------------------------
		protected function setClaveContacto($clave = ''){
			$real_clave = empty($clave)?date('Y'):$clave;
			$sql = 'SELECT folio FROM usuario_empresa_contacto WHERE folio REGEXP "^'.$real_clave.'-[0-9]" ORDER BY folio DESC LIMIT 1';
			$pre = $clave;
			$this->setQuery($sql);
			$res = $this->getResultadoNumerico();
			if($res) {
				$fila=$this->getResultado('object');
				$folio_actual = explode('-',$fila->folio);
				
				$numero_nuevo=$folio_actual['1']+1;
				$folio = $real_clave.'-'.str_pad($numero_nuevo, 4, "0", STR_PAD_LEFT);
			}
			else {
				$folio = $real_clave.'-'.str_pad('1', 4, "0", STR_PAD_LEFT);
			}
			return $folio;
		}
		protected function setClaveUser($clave = '') {
			$real_clave = empty($clave)?date('Y'):$clave;
			$sql = 'SELECT folio FROM usuario_cuenta WHERE folio REGEXP "^'.$real_clave.'-[0-9]" ORDER BY folio DESC LIMIT 1';
			$pre = $clave;
			$this->setQuery($sql);
			$res = $this->getResultadoNumerico();
			if($res) {
				$fila=$this->getResultado('object');
				$folio_actual = explode('-',$fila->folio);
				
				$numero_nuevo=$folio_actual['1']+1;
				$folio = $real_clave.'-'.str_pad($numero_nuevo, 4, "0", STR_PAD_LEFT);
			}
			else {
				$folio = $real_clave.'-'.str_pad('1', 4, "0", STR_PAD_LEFT);
			}
			return $folio;
		}
		public function validarnombre($nombre, $id = 0){
			$cnx = new Conectar();
			$sql = 'SELECT id_usuario, nombre FROM usuario WHERE nombre="'.$nombre.'"';
			if ($id>0)
				$sql.=' AND id_usuario!="'.$id.'"';
			$cnx->setQuery($sql);
			$res = $cnx->getResultadoNumerico();
			$respuesta = 0;
			if($res)
				$respuesta = 0;
			else
				$respuesta = 1;
			if(empty($nombre))
				$respuesta = 0;
			return $respuesta;	
		}
		public function validarusuario($usuario, $id = ''){
			$cnx = new Conectar();
			$sql = 'SELECT user, folio FROM usuario_cuenta WHERE user="'.$usuario.'"';
			if(!empty($id))
				$sql.=' AND folio!="'.$id.'"';
			$cnx->setQuery($sql);
			$res = $cnx->getResultadoNumerico();
			if($res)
				$respuesta = 0;
			else
				$respuesta = 1;
			return $respuesta;	
		}
		/*
		**Lista de perfiles de usuario
		*/
		public function perfiles($tipo) {
			$cnx = new conectar();
			$tipo=$cnx->setEscapar($tipo);
			$sql='SELECT rolnombre,idrol FROM rol WHERE categoria="'.$tipo.'"';
			$cnx->setQuery($sql);
			$res=$cnx->getResultadoNumerico();
			$datos=array();
			$cont=0;
			if($res){
				while ( $fila=$cnx->getResultado('object') ) {
					$cont++;
					$datos[$cont]['nombre'] = $fila->rolnombre;
					$datos[$cont]['id'] = $fila->idrol;
				}
			}
			return $datos;
		}
	}
?>