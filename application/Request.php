<?php
	class Request
	{
		private $modulo;//19-10-2012
		private $controlador='';
		private $metodo='';
		private $argumentos=array();
		private $_modules;//19-10-2012

		public function __construct()
		{
			if(isset($_GET['url'])){
				//optenes una variable en concreto y filtramos caracteres
				$url=filter_input(INPUT_GET, 'url',FILTER_SANITIZE_URL);
				$url=explode('/', $url);//dividimos el get
				$url=array_filter($url, 'strlen');//filtra elemtos vacios del array y permite 0's

				//$this->_modules=array('acl');//importante
				//obtiene el elemento principal del array, lo elimina del array, y lo asigana a la variable
				//convertimos a minuscula el valor
				$this->modulo=strtolower(array_shift($url));
				
				if(!$this->modulo)
				{
					//si no existe el modulo
					$this->modulo=false;//modulo falso
				}
				else
				{
					$dir=_ROOT_.'modules'._DS_.$this->modulo._DS_;
					if (file_exists($dir))
					{
						$this->controlador=strtolower(array_shift($url));
						//controlador toma el segundo valor
					}
					else
					{
						$this->controlador=$this->modulo;//controlador toma valor del modulo
						$this->modulo=false;//modulo falso
					}
				}
				//$this->controlador=strtolower(array_shift($url));
				$this->metodo=strtolower(array_shift($url));
				$this->argumentos=$url;//obtiene los datos del array restantes
				//var_dump($this->argumentos);
				//echo $this->controlador;
			}
			else
			{
				$this->_modulo=false;
				$this->controlador='inicio';
				$this->metodo='index';
				$this->argumentos=array();
			}
		}
		public function getModulo()
		{
			$this->modulo=($this->modulo)?$this->modulo:false;
			return $this->modulo;
		}
		public function getControlador()
		{
			$this->controlador=empty($this->controlador)?'inicio':$this->controlador;
			return $this->controlador;
		}
		public function getMetodo()
		{
			$this->metodo=empty($this->metodo)?'index':$this->metodo;
			return $this->metodo;
		}
		public function getArgumentos()
		{
			return $this->argumentos;
		}
	}
?>